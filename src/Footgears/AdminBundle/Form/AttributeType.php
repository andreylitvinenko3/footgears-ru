<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\Attribute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttributeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название'
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Тип',
                'choices' => array_flip(Attribute::getTypes())
            ])
            ->add('sort', IntegerType::class, [
                'label' => 'Сортировка'
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Активен',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Footgears\MainBundle\Document\Attribute'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'attribute';
    }
}