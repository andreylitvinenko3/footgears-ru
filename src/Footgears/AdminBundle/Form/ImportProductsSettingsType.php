<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\ImportProductsSettings;
use Footgears\MainBundle\Registry\ParserRegistry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportProductsSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parserRegistry = new ParserRegistry();

        $builder
            ->add('enabled', CheckboxType::class, [
                'required' => false,
            ])
            ->add('force', CheckboxType::class, [
                'required' => false
            ])
            ->add('delta', CheckboxType::class, [
                'required' => false,
                'label' => 'Загружать только изменения'
            ])
            ->add('url', TextType::class)
            ->add('idXPath', TextType::class, [
                'required' => false,
                'label' => 'idXPath'
            ])
            ->add('nameExpression', TextType::class)
            ->add('oldPriceXPath', TextType::class, [
                'required' => false,
                'label' => 'oldPriceXPath'
            ])
            ->add('importPeriod', TextType::class)
            ->add('parserClass', ChoiceType::class, [
                'choices' => array_combine($parserRegistry->getNames(), $parserRegistry->getNames())
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', ImportProductsSettings::class);
    }
}
