<?php

namespace Footgears\AdminBundle\Form;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Service\ContainerWrapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dm = $this->container->getDocumentManager();

        $qb = $dm->getRepository(Category::class)->getNodesHierarchyQueryBuilder();

        /* @var Category $document */
        $document = $builder->getData();
        if ($document->getId()) {
            $qb->field('_id')->notEqual($document->getId());
        }

        $builder
            ->add('parent', DocumentType::class, [
                'required' => false,
                'class' => Category::class,
                'empty_data' => null,
                'choice_label' => 'shiftedName',
                'label' =>  'Родитель',
                'query_builder' => $qb
            ])
            ->add('name', TextType::class, [
                'label' => 'Название'
            ])
            ->add('shortName', TextType::class, [
                'label' => 'Короткое название'
            ])
            ->add('alias', TextType::class, [
                'label' => 'Алиас'
            ])
            ->add('seo', SeoType::class)
            ->add('saleSeo', SeoType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class
        ]);
    }
}
