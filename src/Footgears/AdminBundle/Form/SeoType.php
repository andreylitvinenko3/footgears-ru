<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\Seo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('h1', TextType::class, [
                'required' => false
            ])
            ->add('text', TextareaType::class, [
                'required' => false
            ])
            ->add('shortText', TextareaType::class, [
                'required' => false
            ])
            ->add('title', TextType::class, [
                'required' => false
            ])
            ->add('description', TextType::class, [
                'required' => false
            ])
            ->add('keywords', TextType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Seo::class]);
    }
}
