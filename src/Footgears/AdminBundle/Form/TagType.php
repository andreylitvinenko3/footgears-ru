<?php

namespace Footgears\AdminBundle\Form;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Service\ContainerWrapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TagType extends AbstractType
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $attributeValues = [];
        $attributes = $this->container->getAttributesService()->getAttributes();

        foreach ($attributes as $attribute) {
            foreach ($attribute->getValues() as $value) {
                $attributeValues[$attribute->getName() . ' ' . $value->getLabel()] = $value->getId();
            }
        }

        $builder
            ->add('alias', TextType::class, [
                'label' => 'Алиас',
            ])
            ->add('category', DocumentType::class, [
                'label' => 'Категория',
                'class' => Category::class,
                'choice_label' => 'shiftedName'
            ])
            ->add('attributes', ChoiceType::class, [
                'required' => false,
                'multiple' => true,
                'label' => 'Атрибуты для поиска товаров',
                'choices' => $attributeValues
            ])
            ->add('searchPhrase', TextType::class, [
                'required' => false,
                'label' => 'Поисковой запрос'
            ])
            ->add('searchKeysOperator', ChoiceType::class, [
                'required' => false,
                'choices' => Tag::SEARCH_KEYS_OPERATORS
            ])
            ->add('active', CheckboxType::class, [
                'required' => false
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array_flip(Tag::TYPES),
                'label' => 'Расположение и тип ссылки'
            ])
            ->add('seo', SeoType::class)
            ->add('productsCount', NumberType::class, [
                'required' => false,
                'disabled' => true
            ])
            ->add('productsCountUpdated', DateTimeType::class, [
                'required' => false,
                'disabled' => true,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tag::class
        ]);
    }
}
