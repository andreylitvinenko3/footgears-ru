<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\Shop;
use Sirian\StorageBundle\Form\Type\StorageFileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logo', StorageFileType::class, [
                'required' => false
            ])
            ->add('name', TextType::class)
            ->add('alias', TextType::class)
            ->add('url', TextType::class)
            ->add('redirectType', ChoiceType::class, [
                'choices' => array_flip(Shop::REDIRECT_TYPES)
            ])
            ->add('originalId', TextType::class)
            ->add('status', ChoiceType::class, [
                'choices' => array_flip(Shop::STATUSES)
            ])
            ->add('systemComment', TextareaType::class, [
                'required' => false
            ])
            ->add('priority', NumberType::class, [
                'label' => 'Приоритет (+ N% к rnd)']
            )
            ->add('seo', SeoType::class)
            ->add('importProductsSettings', ImportProductsSettingsType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'isNew' => false,
            'data_class' => Shop::class
        ]);
    }
}
