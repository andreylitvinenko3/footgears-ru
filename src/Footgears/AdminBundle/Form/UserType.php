<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Логин'
            ])
            ->add('email', TextType::class)
            ->add('roles', ChoiceType::class, [
                'choices' => array_flip(User::getRolesList()),
                'multiple' => true,
                'label' => 'Роли'
            ])
            ->add('vkId', null, [
                'required' => false,
                'label' => 'ID Вконтакте'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'user';
    }
}