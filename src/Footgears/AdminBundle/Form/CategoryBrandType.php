<?php

namespace Footgears\AdminBundle\Form;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\CategoryBrand;
use Footgears\MainBundle\Service\ContainerWrapper;
use Sirian\SuggestBundle\Form\Type\SuggestType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryBrandType extends AbstractType
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dm = $this->container->getDocumentManager();

        $qb = $dm->getRepository(Category::class)->getNodesHierarchyQueryBuilder();

        $builder
            ->add('category', DocumentType::class, [
                'class' => Category::class,
                'empty_data' => null,
                'choice_label' => 'shiftedName',
                'query_builder' => $qb
            ])
            ->add('brand', SuggestType::class, [
                'suggester' => 'brand'
            ])
            ->add('seo', SeoType::class)
            ->add('saleSeo', SeoType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CategoryBrand::class
        ]);
    }
}
