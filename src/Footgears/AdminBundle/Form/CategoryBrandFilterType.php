<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Filter\CategoryBrandFilter;
use Sirian\SuggestBundle\Form\Type\SuggestType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryBrandFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', SuggestType::class, [
                'required' => false,
                'suggester' => 'category'
            ])
            ->add('brand', SuggestType::class, [
                'suggester' => 'brand',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CategoryBrandFilter::class
        ]);
    }
}
