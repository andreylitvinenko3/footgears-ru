<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Filter\ShopFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название',
                'required' => false
            ])
            ->add('status', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip(Shop::STATUSES)
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ShopFilter::class]);
    }
}
