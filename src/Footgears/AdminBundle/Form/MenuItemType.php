<?php

namespace Footgears\AdminBundle\Form;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Footgears\MainBundle\Document\MenuItem;
use Footgears\MainBundle\Service\ContainerWrapper;
use Sirian\SuggestBundle\Form\Type\SuggestType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuItemType extends AbstractType
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $queryBuilder = $this
            ->container
            ->getDocumentManager()
            ->getRepository(MenuItem::class)
            ->createMenuQueryBuilder($options['menu_type'])
        ;

        /**
         * @var MenuItem $menuItem
         */
        $menuItem = $builder->getData();
        if ($menuItem->getId()) {
            $queryBuilder
                ->field('id')->notEqual($menuItem->getId())
            ;
        }

        $builder
            ->add('parent', DocumentType::class, [
                'required' => false,
                'class' => MenuItem::class,
                'choice_label' => 'shiftedLabel',
                'label' =>  'Родитель',
                'query_builder' => $queryBuilder
            ])
            ->add('name', TextType::class, [
                'label' => 'Название',
                'required' => false
            ])
            ->add('linkTitle', TextType::class, [
                'required' => false
            ])
            ->add('brand', SuggestType::class, [
                'suggester' => 'brand',
                'required' => false,
                'label' => 'Бренд'
            ])
            ->add('category', SuggestType::class, [
                'suggester' => 'category',
                'required' => false,
                'label' =>  'Категория'
            ])
            ->add('customUrl', TextType::class, array(
                'required' => false,
                'label' => 'Произвольный URL'
            ))
            ->add('blank', CheckboxType::class, [
                'required' => false,
                'label' => 'Открывать в новом окне'
            ])
            ->add('redirect', CheckboxType::class, [
                'required' => false,
                'label' => 'Через редирект'
            ])
            ->add('class', TextType::class, [
                'label' => 'Класс',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => MenuItem::class
            ])
            ->setRequired('menu_type')
        ;
    }
}
