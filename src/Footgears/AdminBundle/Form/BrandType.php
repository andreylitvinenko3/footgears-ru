<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\Brand;
use Sirian\StorageBundle\Form\Type\StorageFileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BrandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('nameLocal', TextType::class, [
                'required' => false
            ])
            ->add('alias', TextType::class, ['label' => 'Алиас'])
            ->add('enabled', CheckboxType::class, [
                'required' => false,
            ])
            ->add('logo', StorageFileType::class, [
                'required' => false
            ])
            ->add('searchTags', TextType::class, [
                'required' => false,
            ])
            ->add('seo', SeoType::class)
            ->add('saleSeo', SeoType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Brand::class
        ]);
    }
}
