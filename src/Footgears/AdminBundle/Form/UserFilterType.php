<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\User;
use Footgears\MainBundle\Filter\UserFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'required' => false,
                'label' => 'Логин'
            ])
            ->add('email', TextType::class, [
                'required' => false
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => array_flip(User::getRolesList()),
                'multiple' => true,
                'required' => false,
                'label' => 'Роли'
            ])
            ->add('facebookId', TextType::class, [
                'required' => false,
                'label' => 'ID facebook'
            ])
            ->add('vkontakteId', TextType::class, [
                'required' => false,
                'label' => 'ID vkontakte'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserFilter::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'user_filter';
    }
}