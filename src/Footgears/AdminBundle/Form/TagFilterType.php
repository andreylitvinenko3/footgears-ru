<?php

namespace Footgears\AdminBundle\Form;

use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Filter\TagFilter;
use Sirian\SuggestBundle\Form\Type\SuggestType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TagFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextType::class, [
                'required' => false
            ])
            ->add('category', SuggestType::class, [
                'suggester' => 'category',
                'label' => 'Категория',
                'required' => false,
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array_flip(Tag::TYPES),
                'required' => false,
            ])
            ->add('active', ChoiceType::class, [
                'label' => 'Статус',
                'choices' => array_flip([
                    -1 => 'Все',
                    0 => 'Неактивные',
                    1 => 'Активные',
                ])
            ])
            ->add('issetProducts', CheckboxType::class, [
                'required' => false
            ])
            ->add('sortField', ChoiceType::class, [
                'choices' => array_flip([
                    'id' => 'Идентификатору',
                    'title' => 'Названию',
                    'productsCount' => 'Количеству товаров',
                    'approved' => 'Утвержден/не утвержден'
                ]),
                'label' => 'Сортировать по'
            ])
            ->add('sortOrder', ChoiceType::class, [
                'choices' => array_flip([
                    'ASC' => 'по возрастанию',
                    'DESC' => 'по убыванию'
                ]),
                'label' => 'Порядок сортировки'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TagFilter::class
        ]);
    }
}
