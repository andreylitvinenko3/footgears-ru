<?php

namespace Footgears\AdminBundle\Menu;

use Footgears\MainBundle\Document\MenuItem;

class MenuBuilder extends \Sirian\HelperBundle\Menu\MenuBuilder
{
    public function createMainMenu()
    {
        $root = parent::createMainMenu();

        $root
            ->addChild('Главная', ['route' => 'admin_homepage'])
            ->setExtra('icon', 'home')
        ;

        $root
            ->addChild('Магазины', ['route' => 'admin_shop_list'])
            ->setAttribute('class', 'submenu')
            ->setExtra('icon', 'shopping-cart')
                ->addChild('Список', ['route' => 'admin_shop_list'])
                ->getParent()
                ->addChild('По приоритету', ['route' => 'admin_shop_list', 'routeParameters' => ['by' => 'priority']])
                ->getParent()
                ->addChild('Создать', ['route' => 'admin_shop_new'])
        ;

        $root
            ->addChild('Теги', ['route' => 'admin_tag_list'])
            ->setAttribute('class', 'submenu')
            ->setExtra('icon', 'tags')
                ->addChild('Список', ['route' => 'admin_tag_list'])
                ->getParent()
                ->addChild('Создать', ['route' => 'admin_tag_new'])
        ;

        $root
            ->addChild('Бренды', ['route' => 'admin_brand_list'])
            ->setAttribute('class', 'submenu')
            ->setExtra('icon', 'star')
                ->addChild('Список', ['route' => 'admin_brand_list'])
                ->getParent()
                ->addChild('Создать', ['route' => 'admin_brand_new'])
        ;

        $root
            ->addChild('Категории', ['route' => 'admin_category'])
            ->setExtra('icon', 'search')
            ->setAttribute('class', 'submenu')
                ->addChild('Список', ['route' => 'admin_category'])
                ->getParent()
                ->addChild('Категория + бренд', ['route' => 'admin_category_brand_list'])
        ;

        $menuItems = $root
            ->addChild('Меню сайта', ['route' => 'admin_menu_items'])
            ->setAttribute('class', 'submenu')
            ->setExtra('icon', 'bookmark')
        ;

        foreach (MenuItem::TYPES as $type => $title) {
            $menuItems
                ->addChild($title, ['route' => 'admin_menu_items_tree', 'routeParameters' => ['type' => $type]])
            ;
        }

        $root
             ->addChild('Атрибуты', ['route' => 'admin_attributes']);

        $root
            ->addChild('Логи', ['route' => 'admin_import_history'])
            ->setAttribute('class', 'submenu')
            ->addChild('Импорт товаров', ['route' => 'admin_import_history'])
        ;

        $root
            ->addChild('Пользователи', ['route' => 'admin_user_list'])
            ->setExtra('icon', 'user')
        ;

        return $root;
    }
}
