<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\MainBundle\Document\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductController extends Controller
{
    public function showAction($id)
    {
        $product = $this->getDocumentManager()->getRepository(Product::class)->find($id);
        if (!$product) {
            throw new NotFoundHttpException();
        }

        $searchData = $this->getElasticManager()->find($product->getId());

        return $this->render('@Admin/Product/show.html.twig', [
            'product' => $product,
            'searchData' => $searchData
        ]);
    }
}
