<?php

namespace Footgears\AdminBundle\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $admitadStats = $this->getAdmitadDailyStatRepository()
            ->createQueryBuilder()
            ->sort('date', -1)
            ->limit(30)
            ->getQuery()
            ->execute()
        ;

        $importHistory = $this->getImportHistoryRepository()->findBy([], ['startDate' => -1], 5);

        return $this->render('@Admin/Default/index.html.twig', [
            'admitadStats' => $admitadStats,
            'importHistory' => $importHistory
        ]);
    }
}
