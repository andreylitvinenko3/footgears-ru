<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\UserFilterType;
use Footgears\AdminBundle\Form\UserType;
use Footgears\MainBundle\Document\User;
use Footgears\MainBundle\Filter\UserFilter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    public function listAction(Request $request)
    {
        $userFilter = new UserFilter();

        $form = $this->createFilterForm(UserFilterType::class, $userFilter);
        $form->handleRequest($request);

        $query = $this->getDocumentManager()->getRepository(User::class)->createFilterQuery($userFilter);
        $pagination = $this->paginate($query);

        return $this->render('@Admin/User/list.html.twig', [
            'pagination' => $pagination,
            'filterForm' => $form->createView()
        ]);
    }

    public function newAction(Request $request)
    {
        return $this->edit($request, new User());
    }

    public function editAction(Request $request, $id)
    {
        return $this->edit($request, $this->findUser($id));
    }

    public function deleteAction(Request $request, $id)
    {
        $user = $this->findUser($id);

        $dm = $this->getDocumentManager();
        $dm->remove($user);
        $dm->flush();

        $this->addFlash('success', 'Пользователь удален');

        $response = $this->redirectToRoute('admin_user_list');

        if ($request->server->get('HTTP_REFERER')) {
            $response = new RedirectResponse($request->server->get('HTTP_REFERER'));
        }

        return $response;
    }

    private function edit(Request $request, User $user)
    {
        $isNew = !$user->getId();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($user);
            $dm->flush();

            $this->addFlash('success', sprintf('Пользователь успешно %s', $isNew ? 'добавлен' : 'обновлен'));

            return $this->redirectToRoute('admin_user_edit', ['id' => $user->getId()]);
        }

        return $this->render('@Admin/User/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'isNew' => $isNew
        ]);
    }

    /**
     * @param $id
     * @return User
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    private function findUser($id)
    {
        /** @var User $user */
        $user = $this
            ->getDocumentManager()
            ->find(User::class, $id)
        ;

        if (!$user) {
            throw new NotFoundHttpException();
        }

        return $user;
    }
}
