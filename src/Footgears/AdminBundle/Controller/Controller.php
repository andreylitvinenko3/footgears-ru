<?php

namespace Footgears\AdminBundle\Controller;

class Controller extends \Footgears\MainBundle\Controller\Controller
{
    public function createGETFormBuilder($data = null, array $options = array())
    {
        $builder = $this
            ->createFormBuilder($data, array_merge(['csrf_protection' => false, 'allow_extra_fields' => true], $options))
        ;

        $builder->setMethod('GET');

        return $builder;
    }

    /**
     * @param $type
     * @param null $data
     * @param array $options
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createFilterForm($type, $data = null, array $options = [])
    {
        $options = array_merge($options, [
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);

        $builder = $this->container->get('form.factory')->createNamedBuilder('', $type, $data, $options);
        $builder->setMethod('GET');

        return $builder->getForm();
    }
}
