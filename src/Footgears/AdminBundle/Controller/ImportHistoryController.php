<?php

namespace Footgears\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class ImportHistoryController extends Controller
{
    public function listAction(Request $request)
    {
        $query = $this
            ->getImportHistoryRepository()
            ->createQueryBuilder()
            ->sort(['startDate' => -1])
            ->getQuery()
        ;
        $pagination = $this->paginate($query);

        return $this->render('AdminBundle:ImportHistory:list.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
