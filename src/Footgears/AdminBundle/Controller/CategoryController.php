<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\CategoryType;
use Footgears\MainBundle\Document\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    public function moveAction()
    {
        $post = $this->getRequest()->request;

        $id = $post->get('id');
        $parentId = $post->get('parent_id');
        $moveCount = (int)$post->get('after_count');

        $repo = $this->getCategoryRepository();
        /* @var Category $item */
        $item = $repo->find($id);

        if (!$item) {
            throw $this->createNotFoundException('Item not found');
        }

        $dm = $this->getDocumentManager();

        if ($parentId) {
            /* @var Category $parent */
            $parent = $repo->find($parentId);
        } else {
            $parent = null;
        }

        $item->setParent($parent);
        $dm->flush();

        $repo->moveDown($item, true);
        $dm->flush();

        if ($moveCount > 0) {
            $repo->moveUp($item, $moveCount);
            $dm->flush();
        }

        return $this->json([]);
    }

    public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $root = $dm->getRepository(Category::class)->getPreparedTree();

        return $this->render('@Admin/Category/index.html.twig', [
            'root' => $root
        ]);
    }

    public function deleteAction(Request $request, $id)
    {
        $result = ['result' => 'ok'];

        $category = $this->findCategory($id);

        $dm = $this->getDocumentManager();
        $dm->remove($category);
        $dm->flush();

        return $this->json($result);
    }

    protected function edit(Request $request, Category $category)
    {
        $isNew = !$category->getId();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($category);
            $dm->flush();

            $this->addFlash('success', sprintf('Категория %s', $isNew ? 'добавлена' : 'обновлена'));

            return $this->redirectToRoute('admin_category_edit', ['id' => $category->getId()]);
        }

        return $this->render('@Admin/Category/edit.html.twig', [
            'form' => $form->createView(),
            'category' => $category,
            'isNew' => $isNew
        ]);
    }

    public function editAction(Request $request, $id)
    {
        $category = $this->findCategory($id);

        return $this->edit($request, $category);
    }

    public function newAction(Request $request)
    {
        $category = $this->createCategory($request->query->get('parentId'));

        return $this->edit($request, $category);
    }

    protected function createCategory($parentId = null)
    {
        $category = new Category();
        $parent = null;
        if ($parentId) {
            $parent = $this->findCategory($parentId, false);
        }
        $category->setParent($parent);

        return $category;
    }

    private function findCategory($id, $strict = true)
    {
        $dm = $this->getDocumentManager();

        $category = $dm->getRepository(Category::class)->find($id);
        if (!$category && $strict) {
            throw new NotFoundHttpException();
        }

        return $category;
    }
}
