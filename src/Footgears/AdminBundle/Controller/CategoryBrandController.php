<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\CategoryBrandFilterType;
use Footgears\AdminBundle\Form\CategoryBrandType;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\CategoryBrand;
use Footgears\MainBundle\Filter\CategoryBrandFilter;
use Symfony\Component\HttpFoundation\Request;

class CategoryBrandController extends Controller
{
    public function listAction(Request $request)
    {
        $filter = new CategoryBrandFilter();

        $form = $this->createFilterForm(CategoryBrandFilterType::class, $filter);
        $form->handleRequest($request);

        $qb = $this->getDocumentManager()->getRepository(CategoryBrand::class)->createFilteredQB($filter);

        $pagination = $this->paginate($qb);

        return $this->render('@Admin/CategoryBrand/list.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    public function newAction(Request $request)
    {
        $categoryBrand = new CategoryBrand();

        if ($request->query->get('category')) {
            $category = $this->getDocumentManager()->getRepository(Category::class)->find($request->query->get('category'));

            if ($category) {
                $categoryBrand->setCategory($category);
            }
        }

        if ($request->query->get('brand')) {
            $brand = $this->getDocumentManager()->getRepository(Brand::class)->find($request->query->get('brand'));

            if ($brand) {
                $categoryBrand->setBrand($brand);
            }
        }

        return $this->edit($request, $categoryBrand);
    }

    public function editAction(Request $request, $id)
    {
        $categoryBrand = $this->loadCategoryBrand($id);

        return $this->edit($request, $categoryBrand);
    }

    public function deleteAction(Request $request, $id)
    {
        $categoryBrand = $this->loadCategoryBrand($id);

        $dm = $this->getDocumentManager();
        $dm->remove($categoryBrand);
        $dm->flush();

        $this->addFlash('success', 'Настройки страницы удалены');

        return $this->redirectToRoute('admin_category_brand_list');
    }

    protected function edit(Request $request, CategoryBrand $categoryBrand)
    {
        $isNew = !$categoryBrand->getId();

        $form = $this->createForm(CategoryBrandType::class, $categoryBrand);
        $form->handleRequest($request);

        if (!$form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($categoryBrand);
            $dm->flush();

            $this->addFlash('success', sprintf('Настройки страницы "%s" %s', $categoryBrand->getSeo()->getH1(), $isNew ? 'сохранены' : 'обновлены'));

            return $this->redirectToRoute('admin_category_brand_edit', ['id' => $categoryBrand->getId()]);
        }

        return $this->render('@Admin/CategoryBrand/edit.html.twig', [
            'form' => $form->createView(),
            'isNew' => $isNew,
            'categoryBrand' => $categoryBrand
        ]);
    }

    /**
     * @param $id
     * @return CategoryBrand
     */
    protected function loadCategoryBrand($id)
    {
        $categoryBrand = $this->getDocumentManager()->getRepository(CategoryBrand::class)->find($id);
        if (!$categoryBrand) {
            throw $this->createNotFoundException();
        }

        return $categoryBrand;
    }
}