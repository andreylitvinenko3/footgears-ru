<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\MenuItemType;
use Footgears\MainBundle\Document\MenuItem;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class MenuItemController extends Controller
{
    public function indexAction()
    {
        return $this->render('@Admin/MenuItem/index.html.twig', [
            'types' => MenuItem::TYPES
        ]);
    }

    public function treeAction($type)
    {
        $repo = $this->getDocumentManager()->getRepository(MenuItem::class);

        return $this->render('@Admin/MenuItem/tree.html.twig', [
            'type' => $type,
            'root' => $repo->getTree($type)
        ]);
    }

    public function moveAction($type)
    {
        $post = $this->getRequest()->request;

        $id = $post->get('id');
        $parentId = $post->get('parent_id');
        $moveCount = (int)$post->get('after_count');

        $repo = $this->getDocumentManager()->getRepository(MenuItem::class);

        /* @var MenuItem $item */
        $item = $this->loadMenuItem($id);

        $dm = $this->getDocumentManager();

        if ($parentId) {
            $parent = $repo->find($parentId);
        } else {
            $parent = $repo->getRoot($type);
        }

        if (!$parent) {
            throw $this->createNotFoundException('Parent item not found');
        }

        $item->setParent($parent);
        $dm->flush();

        $repo->moveDown($item, true);
        $dm->flush();

        if ($moveCount > 0) {
            $repo->moveUp($item, $moveCount);
            $dm->flush();
        }

        return $this->json([]);
    }

    public function deleteAction(Request $request, $id)
    {
        $result = ['result' => 'ok'];

        if ($request->isMethod('POST') || $request->isMethod('DELETE')) {
            $dm = $this->getDocumentManager();
            $menuItem = $this->getDocumentManager()->getRepository(MenuItem::class)->find($id);
            if ($menuItem) {
                $dm->remove($menuItem);
                $dm->flush();
            }
        } else {
            $result = ['error' => 'Неверный метод'];
        }

        return $this->json($result);
    }

    protected function edit(Request $request, MenuItem $menuItem, $type)
    {
        $isNew = !$menuItem->getId();

        $dm = $this->getDocumentManager();
        $repo = $dm->getRepository(MenuItem::class);

        $form = $this->createForm(MenuItemType::class, $menuItem, ['menu_type' => $type]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$menuItem->getParent()) {
                $menuItem->setParent($repo->getRoot($type));
            }
            $dm->persist($menuItem);
            $dm->flush();

            $this->addFlash('success', sprintf('Элемент меню "%s" %s', $menuItem->getName(), $isNew ? 'добавлен' : 'обновлен'));

            return $this->redirectToRoute('admin_menu_items_edit', ['id' => $menuItem->getId(), 'type' => $type]);
        }

        return $this->render('@Admin/MenuItem/edit.html.twig', [
            'entity' => $menuItem,
            'form' => $form->createView(),
            'type' => $type,
            'isNew' => $isNew
        ]);
    }

    public function editAction(Request $request, $type, $id)
    {
        $menuItem = $this->loadMenuItem($id);

        return $this->edit($request, $menuItem, $type);
    }

    public function newAction(Request $request, $type)
    {
        $menuItem = $this->createMenuItem($type, $request->query->get('parentId'));

        return $this->edit($request, $menuItem, $type);
    }

    protected function createMenuItem($menu, $parentId = null)
    {
        $repository = $this->getDocumentManager()->getRepository(MenuItem::class);

        $menuItem = new MenuItem();

        $parent = null;
        if ($parentId) {
            $parent = $repository->find($parentId);
        }
        if (!$parent) {
            $parent = $repository->getRoot($menu);
        }
        $menuItem->setParent($parent);

        return $menuItem;
    }

    /**
     * @param $id
     * @return MenuItem|null|object
     */
    private function loadMenuItem($id)
    {
        $menuItem = $this->getDocumentManager()->getRepository(MenuItem::class)->find($id);
        if (!$menuItem) {
            throw $this->createNotFoundException();
        }

        return $menuItem;
    }

}
