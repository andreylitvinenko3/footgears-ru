<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\BrandFilterType;
use Footgears\AdminBundle\Form\BrandType;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Filter\BrandFilter;
use Symfony\Component\HttpFoundation\Request;

class BrandController extends Controller
{
    public function listAction(Request $request)
    {
        $filter = new BrandFilter();

        $form = $this->createFilterForm(BrandFilterType::class, $filter);
        $form->handleRequest($request);

        $qb = $this->getDocumentManager()->getRepository(Brand::class)->createFilteredQB($filter);

        $pagination = $this->paginate($qb);

        return $this->render('@Admin/Brand/list.html.twig', [
            'form' => $form->createView(),
            'pagination' => $pagination
        ]);
    }

    public function newAction(Request $request)
    {
        $brand = new Brand();

        return $this->edit($request, $brand);
    }

    public function editAction(Request $request, $id)
    {
        $brand = $this->loadBrand($id);

        return $this->edit($request, $brand);
    }

    public function deleteAction(Request $request, $id)
    {
        $brand = $this->loadBrand($id);

        $dm = $this->getDocumentManager();
        $dm->remove($brand);
        $dm->flush();

        $this->addFlash('success', sprintf('Бренд %s удален', $brand->getName()));

        return $this->redirectToRoute('admin_brand_list');
    }

    protected function edit(Request $request, Brand $brand)
    {
        $isNew = !$brand->getId();

        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($brand);
            $dm->flush();

            $this->addFlash('success', sprintf('Бренд "%s" %s', $brand->getName(), $isNew ? 'добавлен' : 'обновлен'));

            return $this->redirectToRoute('admin_brand_edit', ['id' => $brand->getId()]);
        }

        return $this->render('@Admin/Brand/edit.html.twig', [
            'form' => $form->createView(),
            'brand' => $brand,
            'isNew' => $isNew
        ]);
    }

    protected function loadBrand($id)
    {
        $brand = $this->getDocumentManager()->getRepository(Brand::class)->find($id);
        if (!$brand) {
            throw $this->createNotFoundException();
        }

        return $brand;
    }
}
