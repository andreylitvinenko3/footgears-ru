<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\AttributeType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AttributesController extends Controller
{
    public function listAction(Request $request)
    {
        $attributes = $this->getAttributeRepository()
            ->createQueryBuilder()
            ->sort('sort', 'desc')
            ->getQuery()
            ->execute()
        ;

        return $this->render('AdminBundle:Attributes:list.html.twig', [
            'attributes' => $attributes
        ]);
    }

    public function editAction(Request $request, $id)
    {
        $attribute = $this->getAttributeRepository()->find((int)$id);

        if (!$attribute) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(AttributeType::class, $attribute);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();

            $dm->persist($attribute);
            $dm->flush();

            $this->addFlash('success', 'Атрибут обновлен');

            return $this->redirect($this->generateUrl('admin_attributes'));
        }

        return $this->render('AdminBundle:Attributes:edit.html.twig', [
            'form' => $form->createView(),
            'attribute' => $attribute
        ]);
    }
}