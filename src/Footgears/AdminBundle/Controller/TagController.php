<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\TagFilterType;
use Footgears\AdminBundle\Form\TagType;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Filter\TagFilter;
use Symfony\Component\HttpFoundation\Request;

class TagController extends Controller
{
    public function listAction(Request $request)
    {
        $filter = new TagFilter();

        $filterForm = $this->createFilterForm(TagFilterType::class, $filter);
        $filterForm->handleRequest($request);

        $qb = $this->getDocumentManager()->getRepository(Tag::class)->createFilteredQB($filter);

        if ($filter->getSortField()) {
            switch (mb_strtolower($filter->getSortOrder())) {
                case 'asc':
                case 1:
                    $order = 1;
                    break;

                case 'desc':
                case 0:
                    $order = 0;
                    break;

                default:
                    $order = 1;
            }

            $qb->sort($filter->getSortField(), $order);
        }

        $pagination = $this->paginate($qb);

        return $this->render('@Admin/Tag/list.html.twig', [
            'pagination' => $pagination,
            'form' => $filterForm->createView(),
        ]);
    }

    public function newAction(Request $request)
    {
        $tag = new Tag();

        if ($request->query->has('category')) {
            $category = $this->findCategory($request->query->get('category'));

            $tag->setCategory($category);
        }

        return $this->edit($request, $tag);
    }

    public function editAction(Request $request, $id)
    {
        $tag = $this->findTag($id);
        return $this->edit($request, $tag);
    }

    protected function edit(Request $request, Tag $tag)
    {
        $isNew = !$tag->getId();

        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($tag);
            $dm->flush();

            $this->addFlash('success', $isNew ? 'Страница добавлена' : 'Страница обновлена');

            return $this->redirectToRoute('admin_tag_edit', ['id' => $tag->getId()]);
        }

        return $this->render('@Admin/Tag/edit.html.twig', [
            'isNew' => $isNew,
            'page' => $tag,
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction(Request $request, $id)
    {
        $page = $this->findTag($id);

        $dm = $this->getDocumentManager();
        $dm->remove($page);
        $dm->flush();

        $this->addFlash('success', 'Страница удалена');

        return $this->redirect($request->server->get('HTTP_REFERER', $this->generateUrl('admin_tag_list')));
    }

    /**
     * @param $categoryId
     * @return Category
     */
    private function findCategory($categoryId)
    {
        $category = $this->getDocumentManager()->getRepository(Category::class)->find($categoryId);

        if (!$category) {
            throw $this->createNotFoundException();
        }

        return $category;
    }

    /**
     * @param $id
     * @return Tag
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function findTag($id)
    {
        $tag = $this->getDocumentManager()->getRepository(Tag::class)->find($id);

        if (!$tag) {
            throw $this->createNotFoundException();
        }

        return $tag;
    }
}