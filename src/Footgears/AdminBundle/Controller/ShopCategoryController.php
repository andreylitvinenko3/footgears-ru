<?php

namespace Footgears\AdminBundle\Controller;

use AppKernel;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Process;

class ShopCategoryController extends Controller
{
    public function indexAction($id)
    {
        $shop = $this->getDocumentManager()->getRepository(Shop::class)->find($id);
        if (!$shop) {
            throw new NotFoundHttpException();
        }

        $categories = $this->getCategoryRepository()->getNodesHierarchyQuery()->execute();

        return $this->render('@Admin/ShopCategory/index.html.twig', [
            'root' => $this->getDocumentManager()->getRepository(ShopCategory::class)->getRoot($shop),
            'shop' => $shop,
            'categories' => $categories
        ]);
    }

    public function nodesAction($categoryId)
    {
        $shopCategoryRepository = $this->getDocumentManager()->getRepository(ShopCategory::class);
        $collection = $this->getDocumentManager()->getDocumentCollection(ShopCategory::class);

        $root = $shopCategoryRepository->find($categoryId);
        if (!$root) {
            throw new NotFoundHttpException();
        }

        /* @var ShopCategory[] $nodes */
        $nodes = $shopCategoryRepository->getChildren($root, true);
        $metaInfo = [];


        $facet = [];
        foreach ($nodes as $node) {
            $pipeline = [
                ['$match' => [
                    'root' => $node->getRoot(),
                    'left' => ['$gte' => $node->getLeft()],
                    'right' => ['$lte' => $node->getRight()]
                ]],
                ['$group' => [
                    '_id' => null,
                    'productsCountFromXML' => ['$sum' => '$productsCountFromXML'],
                    'productsCount' => ['$sum' => '$productsCount'],
                    'unhandled' => [
                        '$sum' => [
                            '$cond' => [['$eq' => ['$handled', false]], 1, 0]
                        ]
                    ]
                ]],
                ['$project' => [
                    '_id' => 0,
                    'productsCountFromXML' => '$productsCountFromXML',
                    'importedProductsCount' => '$productsCount',
                    'unhandled' => [
                        '$cond' => [['$gt' => ['$unhandled', 0]], true, false]
                    ]
                ]]
            ];

            $facet[(string)$node->getId()] = $pipeline;
        }

        $result = $collection
            ->aggregate(['$facet' => $facet])
            ->getSingleResult()
        ;

        return $this->render('@Admin/ShopCategory/nodes.html.twig', [
            'nodes' => $nodes,
            'metaInfo' => $result
        ]);
    }

    public function bindAction(Request $request, $categoryId)
    {
        $shopCategory = $this->getShopCategory($categoryId);

        $ourCategoryId = $request->request->get('categoryId');
        $category = null;
        if ($ourCategoryId) {
            $category = $this->getCategoryRepository()->find($ourCategoryId);
        }

        $shopCategory->setCategory($category);

        $dm = $this->getDocumentManager();
        $dm->flush();

        return $this->json([]);
    }

    public function enableToggleAction($categoryId)
    {
        $shopCategory = $this->getShopCategory($categoryId);

        $shopCategory->setEnabled(!!$this->getRequest()->request->get('enabled'));

        $dm = $this->getDocumentManager();
        $dm->flush();

        return $this->json([]);
    }

    public function handleAction($categoryId)
    {
        $shopCategory = $this->getShopCategory($categoryId);

        $collection = $this->getDocumentManager()->getDocumentCollection(ShopCategory::class);
        $collection
            ->update(
                [
                    'root' => $shopCategory->getRoot(),
                    'left' => ['$gte' => $shopCategory->getLeft()],
                    'right' => ['$lte' => $shopCategory->getRight()]
                ],
                ['$set' => ['handled' => true]],
                ['multi' => true]
            )
        ;

        return $this->json([]);
    }

    public function productListAction($categoryId)
    {
        $shopCategory = $this->getShopCategory($categoryId);

        $qb = $this
            ->getDocumentManager()
            ->getRepository(Product::class)
            ->createQueryBuilder()
            ->field('shopCategory')->references($shopCategory)
        ;

        $pagination = $this->paginate($qb->getQuery());

        return $this->render('@Admin/ShopCategory/product_list.html.twig', [
            'pagination' => $pagination,
            'shopCategory' => $shopCategory,
            'shop' => $shopCategory->getShop()
        ]);
    }

    public function updateProductsAction(Request $request, $shopId, $categoryId = null)
    {
        $shop = $this->getShop($shopId);

        $arguments = [$shop->getId()];

        if ($categoryId) {
            /** @var ShopCategory $shopCategory */
            $shopCategory = $this->getDocumentManager()->getRepository(ShopCategory::class)->find($categoryId);

            if (!$shopCategory) {
                throw new NotFoundHttpException();
            }

            $arguments[] = $shopCategory->getId();
        }

        /** @var AppKernel $kernel */
        $kernel = $this->get('kernel');
        $process = new Process('/usr/bin/nohup /usr/bin/php '. $kernel->getRootDir() .'/console footgears:products:update-categories-by-shop-categories --env='. $kernel->getEnvironment() .' ' . implode(' ', $arguments) . ' > /dev/null 2>&1 &');
        $process->run();

        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        return $this->json(['status' => 1]);
    }

    private function getShop($id)
    {
        /* @var Shop $shop */
        $shop = $this->getDocumentManager()->getRepository(Shop::class)->find($id);
        if (!$shop) {
            throw $this->createNotFoundException('Shop not found');
        }
        return $shop;
    }

    /**
     * @param $categoryId
     * @return ShopCategory
     */
    protected function getShopCategory($categoryId)
    {
        $shopCategoryRepository = $this->getDocumentManager()->getRepository(ShopCategory::class);

        /* @var ShopCategory $shopCategory */
        $shopCategory = $shopCategoryRepository->find($categoryId);
        if (!$shopCategory) {
            throw new NotFoundHttpException();
        }

        return $shopCategory;
    }
}
