<?php

namespace Footgears\AdminBundle\Controller;

use Footgears\AdminBundle\Form\ShopFilterType;
use Footgears\AdminBundle\Form\ShopType;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Footgears\MainBundle\Filter\ShopFilter;
use Symfony\Component\HttpFoundation\Request;

class ShopController extends Controller
{
    public function listAction(Request $request)
    {
        $filter = new ShopFilter();
        $form = $this->createFilterForm(ShopFilterType::class, $filter);
        $form->handleRequest($request);

        $qb = $this->getDocumentManager()->getRepository(Shop::class)->createFilteredQB($filter);

        $sort = ['active' => -1];
        if ($request->query->get('by')) {
            $sort[$request->query->get('by')] = -1;
        } else {
            $sort['productsCountFromIndex'] = -1;
        }

        $qb->sort($sort);

        $pagination = $this->paginate($qb);

        $handled = $this->getHandled($pagination->getItems());

        return $this->render('@Admin/Shop/list.html.twig', [
            'pagination' => $pagination,
            'filterForm' => $form->createView(),
            'handled_statuses' => $handled
        ]);
    }

    public function newAction(Request $request)
    {
        return $this->edit($request, new Shop());
    }

    public function editAction(Request $request, $id)
    {
        return $this->edit($request, $this->getShop($id));
    }

    private function edit(Request $request, Shop $shop)
    {
        $isNew = !$shop->getId();

        $form = $this->createForm(ShopType::class, $shop, [
            'isNew' => $isNew
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($shop);
            $dm->flush();

            $this->addFlash('success', sprintf('Магазин успешно %s', $isNew ? 'добавлен' : 'обновлен'));

            return $this->redirectToRoute('admin_shop_edit', ['id' => $shop->getId()]);
        }

        return $this->render('@Admin/Shop/edit.html.twig', [
            'form' => $form->createView(),
            'shop' => $shop,
            'isNew' => $isNew
        ]);
    }

    private function getShop($id)
    {
        /* @var Shop $shop */
        $shop = $this->getDocumentManager()->getRepository(Shop::class)->find($id);
        if (!$shop) {
            throw $this->createNotFoundException('Shop not found');
        }

        return $shop;
    }

    private function getHandled($shops)
    {
        /* @var Shop[] $shops */

        $collection = $this->getDocumentManager()->getDocumentCollection(ShopCategory::class);

        $pipeline = [
            ['$match' => [
                'shop' => ['$in' => array_map(function (Shop $shop) {
                    return $shop->getId();
                }, $shops)]]],
            ['$group' => [
                '_id' => '$shop',
                'unhandled' => [
                    '$sum' => [
                        '$cond' => [['$eq' => ['$handled', false]], 1, 0]
                    ]
                ]
            ]],
            ['$project' => [
                '_id' => '$_id',
                'unhandled' => [
                    '$cond' => [['$gt' => ['$unhandled', 0]], true, false]
                ]
            ]]
        ];

        $result = $collection->aggregate($pipeline)->toArray();

        $data = array_combine(array_column($result, '_id'), array_column($result, 'unhandled'));

        return $data;
    }
}
