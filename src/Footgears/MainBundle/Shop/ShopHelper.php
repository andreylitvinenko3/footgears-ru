<?php

namespace Footgears\MainBundle\Shop;

use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Registry\ParserRegistry;
use Footgears\MainBundle\YandexMarketLanguage\ParserInterface;
use Sirian\Helpers\Url;

class ShopHelper
{
    public static function getDeeplink(Shop $shop, $ulp = '')
    {
        $url = new Url($shop->getUrl());

        if (false !== strpos($url->getHost(), '.admitad.com')) {
            $url->setQueryParam('ulp', $ulp);
            return $url->getUrl();
        }

        return str_replace('{ulp}', $ulp, $url->getUrl());
    }

    /**
     * @param Shop $shop
     * @return ParserInterface
     */
    public static function createParser(Shop $shop)
    {
        $parserClass = $shop->getImportProductsSettings()->getParserClass();
        if (!$parserClass) {
            $parserClass = 'BaseParser';
        }

        $parserRegistry = new ParserRegistry();
        return $parserRegistry->get($parserClass);
    }
}
