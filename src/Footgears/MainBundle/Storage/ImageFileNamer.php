<?php

namespace Footgears\MainBundle\Storage;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class ImageFileNamer implements NamerInterface
{
    /**
     * Creates a name for the file being uploaded.
     *
     * @param object $object The object the upload is attached to.
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object.
     *
     * @return string The file name.
     */
    public function name($object, PropertyMapping $mapping)
    {
        $extension = 'jpg';

        return uniqid('', true) . '.' . $extension;
    }

    public static function generateNameBySource($source)
    {
        return md5($source);
    }
}
