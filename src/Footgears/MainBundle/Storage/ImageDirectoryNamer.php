<?php

namespace Footgears\MainBundle\Storage;

use Symfony\Component\DependencyInjection\Container;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class ImageDirectoryNamer implements DirectoryNamerInterface
{
    /**
     * Creates a directory name for the file being uploaded.
     *
     * @param object $object The object the upload is attached to.
     * @param Propertymapping $mapping The mapping to use to manipulate the given object.
     *
     * @return string The directory name.
     */
    public function directoryName($object, PropertyMapping $mapping)
    {
        $class = get_class($object);
        $class = substr($class, strrpos($class, '\\') + 1);

        $dir = '/' . Container::underscore($class);

        return $dir;
    }
}
