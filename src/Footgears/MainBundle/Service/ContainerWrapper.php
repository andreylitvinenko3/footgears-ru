<?php

namespace Footgears\MainBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ContainerWrapper
{
    use ContainerTrait;

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }
}
