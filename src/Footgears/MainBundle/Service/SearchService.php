<?php

namespace Footgears\MainBundle\Service;

use Doctrine\DBAL\Connection;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Search\BrandSearchRequest;
use Footgears\MainBundle\Search\CategorySearchRequest;
use Footgears\MainBundle\Search\ICategorySearch;
use Footgears\MainBundle\Search\SearchRequest;
use Footgears\MainBundle\Search\SearchResult;
use Footgears\MainBundle\Search\SelectedFilters\Filters\AttributeFilter;
use Footgears\MainBundle\Search\SelectedFilters\Filters\BrandFilter;
use Footgears\MainBundle\Search\SelectedFilters\Filters\PriceRangeFilter;
use Footgears\MainBundle\Search\SelectedFilters\Filters\SaleFilter;
use Footgears\MainBundle\Search\SelectedFilters\Filters\SearchQueryFilter;
use Footgears\MainBundle\Search\SelectedFilters\Filters\ShopFilter;
use Footgears\MainBundle\Search\SelectedFilters\GroupFilters;
use Footgears\MainBundle\Search\SelectedFilters\SelectedFilters;
use Footgears\MainBundle\Search\ShopSearchRequest;

class SearchService extends BaseService
{
    const POPULAR_PRODUCTS = 3;
    const POPULAR_BRANDS = 15;

    public function doProductSearch(SearchRequest $searchRequest)
    {
        $searchResult = $this->getSearchManager()->search($searchRequest);

        return [
            'availableCategories' => $this->getAvailableCategories($searchRequest, $searchResult),
            'attributeFilters' => $this->getAttributeFilters($searchRequest, $searchResult),
            'requestedBrands' => $this->getRequestedBrands($searchRequest),
            'availableShops' => $this->getAvailableShops($searchRequest, $searchResult),
            'request' => $searchRequest,
            'result' => $searchResult,
            'searches' => [],
            'popularBrands' => $this->getPopularBrands($searchRequest),
            'selectedFilters' => $this->getSelectedFilters($searchRequest, $searchResult)
        ];
    }

    protected function getAttributeFilters(SearchRequest $searchRequest, SearchResult $searchResult)
    {
        $attributeFilters = [];

        if (!$searchResult->getAttributeIds()) {
            return $attributeFilters;
        }

        $attributesService = $this->getAttributesService();
        foreach ($searchResult->getAttributeIds() as $id) {
            $id = (int)$id;

            $attribute = $attributesService->getAttributeByValueId($id);

            if (!$attribute || !$attribute->isEnabled()) {
                continue;
            }

            if (!isset($attributeFilters[$attribute->getId()])) {
                $attributeFilters[$attribute->getId()] = [
                    'class' => '',
                    'name' => $attribute->getName(),
                    'sort' => $attribute->getSort(),
                    'values' => []
                ];
            }

            $form = &$attributeFilters[$attribute->getId()];

            $value = $attribute->getValue($id);

            $form['values'][] = [
                'value' => $value->getId(),
                'text' => $value->getLabel(),
                'selected' => in_array($value->getId(), $searchRequest->getAttributeIds())
            ];
            usort($form['values'], function ($a, $b) {
                return $a['text'] < $b['text'] ? -1 : 1;
            });
        }

        usort($attributeFilters, function($a, $b) {
            return $a['sort'] < $b['sort'] ? 1 : -1;
        });

        return $attributeFilters;
    }

    public function getAvailableCategories(SearchRequest $searchRequest, SearchResult $searchResult)
    {
        if ($searchRequest instanceof ICategorySearch && $searchRequest->getCategory()) {
            return $this->getContainer()->get('footgears.categories_service')
                ->getCategoriesTree($searchRequest->getCategory(), $searchRequest)
            ;
        }

        $availableCategories = [];
        if ($searchResult->getAvailableCategories()) {
            $categoryRepository = $this->getDocumentManager()->getRepository(Category::class);

            /* @var Category[] $categories */
            $categories = $categoryRepository->findBy([
                'id' => ['$in' => array_keys($searchResult->getAvailableCategories())]
            ]);

            /* @var Category[] $allCategoryNodes */
            $allCategoryNodes = [];

            foreach ($categories as $category) {
                while ($category) {
                    // На обычном поиске показываем только 2 уровня найденных категорий
                    if (!$searchRequest->getSearchQuery() || $category->getLevel() < 2) {
                        $allCategoryNodes[$category->getId()] = $category;
                    }
                    $category = $category->getParent();
                }
            }

            usort($allCategoryNodes, function (Category $a, Category $b) {
                if ($a->getRoot() == $b->getRoot()) {
                    return $a->getLeft() < $b->getLeft() ? -1 : 1;
                }

                return $a->getRoot() < $b->getRoot() ? -1 : 1;
            });

            $currentNode = null;

            $counts = $searchResult->getAvailableCategories();

            foreach ($allCategoryNodes as $category) {

                $current = false;
                if ($searchRequest instanceof ICategorySearch) {
                    /** @var Category $currentCategory */
                    $currentCategory = $searchRequest->getCategory();

                    $current = $currentCategory &&
                        ($category->getId() == $currentCategory->getId() ||
                            $currentCategory->getParent() && $category->getId() == $currentCategory->getParent()->getId());
                }

                $newNode = [
                    'category' => $category,
                    'children' => [],
                    'level' => $category->getLevel(),
                    'count' => isset($counts[$category->getId()]) ? $counts[$category->getId()] : 0,
                    'parent' => null,
                    'current' => $current
                ];

                if ($category->getLevel() == 0) {
                    $availableCategories[] = &$newNode;
                    $currentNode = &$newNode;
                } else {
                    while ($category->getLevel() <= $currentNode['level']) {
                        $currentNode = &$currentNode['parent'];
                    }
                    $newNode['parent'] = &$currentNode;
                    $currentNode['children'][] = &$newNode;
                    $currentNode = &$newNode;
                }

                unset($newNode);
            }
        }

        return $availableCategories;
    }

    protected function getAvailableShops(SearchRequest $request, SearchResult $result)
    {
        if (!$result->getAvailableShops()) {
            return [];
        }

        $ids = array_keys($result->getAvailableShops());
        if (!$ids) {
            return [];
        }
        $shops = $this
            ->getDocumentManager()
            ->getRepository(Shop::class)
            ->findBy(['_id' => ['$in' => $ids]])
        ;

        $selectedIds = $request->getShopIds() ?: [];

        usort($shops, function (Shop $a, Shop $b) use ($selectedIds) {
            if (in_array($a->getId(), $selectedIds) && !in_array($b->getId(), $selectedIds)) {
                return -1;
            }
            if (!in_array($a->getId(), $selectedIds) && in_array($b->getId(), $selectedIds)) {
                return 1;
            }

            return $a->getName() < $b->getName() ? -1 : 1;
        });
        return $shops;
    }

    protected function getRequestedBrands(SearchRequest $request)
    {
        if (!$request->getBrandIds()) {
            return [];
        }
        return $this->getBrandRepository()->findBy(['_id' => ['$in' => $request->getBrandIds()]]);
    }

    protected function getPopularBrands(SearchRequest $request, $limit = self::POPULAR_BRANDS)
    {
        $brands = [];

        if ($request instanceof CategorySearchRequest) {
            $ids = $this->getSearchManager()->getAvailableBrands($request, 20, false);

            $brands = $this->getBrandRepository()->findBy(['_id' => ['$in' => array_keys($ids)]]);
        }

        return $brands;
    }

    protected function getSelectedFilters(SearchRequest $searchRequest, SearchResult $searchResult)
    {
        $selectedFilters = new SelectedFilters();

        if ($searchRequest->getShopIds() && !($searchRequest instanceof ShopSearchRequest && !$searchRequest->getCategory())) {
            $shops = $this->getShopRepository()->findBy(['_id' => ['$in' => $searchRequest->getShopIds()]]);

            if ($shops) {
                $shopsFilters = new GroupFilters();
                $shopsFilters->setTitle('Магазин');

                foreach ($shops as $shop) {
                    $shopsFilters->addFilter(new ShopFilter($shop));
                }

                $selectedFilters->addGroupFilters($shopsFilters);
            }
        }

        if ($searchRequest->getBrandIds() && !($searchRequest instanceof BrandSearchRequest && !$searchRequest->getCategory())) {
            /** @var Brand[] $brands */
            $brands = $this->getBrandRepository()->findBy(['_id' => ['$in' => $searchRequest->getBrandIds()]]);

            if ($brands) {
                $brandsFilters = new GroupFilters();
                $brandsFilters->setTitle('Бренд');

                foreach ($brands as $brand) {
                    $brandsFilters->addFilter(new BrandFilter($brand));
                }

                $selectedFilters->addGroupFilters($brandsFilters);
            }
        }

        if ($searchRequest->getSearchQuery()) {

            $groupFilters = new GroupFilters();
            $groupFilters
                ->setTitle('Запрос')
                ->addFilter(new SearchQueryFilter($searchRequest->getSearchQuery()))
            ;

            $selectedFilters->addGroupFilters($groupFilters);
        }

        if ($searchRequest->getAttributeIds()) {
            $attributeValuesIds = $searchRequest->getAttributeIds();
            sort($attributeValuesIds);

            foreach ($attributeValuesIds as $attributeValueId) {
                $attribute = $this->getAttributesService()->getAttributeByValueId($attributeValueId);

                if ($attribute) {
                    $name = $attribute->getName();

                    if (!$selectedFilters->hasGroupFilters($name)) {
                        $groupFilters = new GroupFilters();
                        $groupFilters->setTitle($name);

                        $selectedFilters->addGroupFilters($groupFilters);
                    }

                    $attributeValue = $attribute->getValue($attributeValueId);
                    if ($attributeValue) {
                        $selectedFilters->getGroupFilters($name)->addFilter(new AttributeFilter($attributeValue));
                    }
                }
            }
        }

        if (($searchRequest->getPriceFrom() > SearchRequest::$minPrice && $searchRequest->getPriceFrom() != $searchResult->getPriceMin()) || ($searchRequest->getPriceTo() && $searchRequest->getPriceTo() != $searchResult->getPriceMax() )) {
            $groupFilters = new GroupFilters();
            $groupFilters
                ->setTitle('Цена')
                ->addFilter(new PriceRangeFilter($searchRequest->getPriceFrom(), $searchRequest->getPriceTo()))
            ;

            $selectedFilters->addGroupFilters($groupFilters);
        }

        if ($searchRequest->isSale()) {
            $groupFilters = new GroupFilters();
            $groupFilters
                ->setTitle('')
                ->addFilter(new SaleFilter())
            ;

            $selectedFilters->addGroupFilters($groupFilters);
        }

        return $selectedFilters;
    }
}