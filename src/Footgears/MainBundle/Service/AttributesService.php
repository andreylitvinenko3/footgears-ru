<?php

namespace Footgears\MainBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Footgears\MainBundle\Document\Attribute;
use Footgears\MainBundle\Document\AttributeValue;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\YandexMarketLanguage\Offer;

class AttributesService extends BaseService
{
    const ID_STEP = 1000;

    /**
     * @var Attribute[]|ArrayCollection $attributes
     */
    protected static $attributes;

    /**
     * @var array $valuesByAttributeId
     */
    protected static $valuesByAttributeId = [];

    protected $attributesMap = [];

    public function getProductSizeAttributeValues(Product $product)
    {
        $sizes = [];

        foreach ($product->getAttributes() as $valueId) {
            $attribute = $this->getAttributeByValueId($valueId);

            if (!$attribute) {
                continue;
            }

            if ($attribute->getType() == Attribute::TYPE_SIZE) {
                $sizes[] = $attribute->getValue($valueId)->getLabel();
            }
        }

        return $sizes;
    }

    public function parseParameters(Offer $offer)
    {
        $ids = [];

        foreach ($offer->getParameters() as $key => $value) {
            if ($key == 'размер' || $key == 'размеры' || $key == 'size' || $key == 'sizes') {
                if ($offer->isMale()) {
                    if ($offer->isDress()) {
                        $attributes = [$this->getAttribute(Attribute::MALE_DRESS_SIZE)];
                    } elseif ($offer->isShoes()) {
                        $attributes = [$this->getAttribute(Attribute::MALE_SHOE_SIZE)];
                    } else {
                        continue;
                    }
                } elseif ($offer->isFemale()) {
                    if ($offer->isDress()) {
                        $attributes = [$this->getAttribute(Attribute::FEMALE_DRESS_SIZE)];
                    } elseif ($offer->isShoes()) {
                        $attributes = [$this->getAttribute(Attribute::FEMALE_SHOE_SIZE)];
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            } else {
                if (!isset($this->attributesMap[$key])) {
                    $this->attributesMap[$key] = $this->findAttributes($key);
                }
                $attributes = $this->attributesMap[$key];
            }

            /* @var Attribute[] $attributes */
            foreach ($attributes as $attribute) {

                foreach ($this->findValueIds($attribute, $value) as $id) {
                    $ids[] = $id;
                }
            }
        }

        $ids = array_unique($ids);

        return $ids;
    }

    public function getAttributeValuesFromOfferField($elemFieldValue, Attribute $attribute)
    {
        $parameters = [];

        $values = [];
        foreach ($attribute->getValues() as $value) {
            if ($value->getParameters() && isset($value->getParameters()['values'])) {
                $values = array_merge_recursive($values, $value->getParameters()['values']);
            }
        }

        $matches = [];
        if (preg_match_all('/\b(' . implode('|', $values) . ')\b/iu', (string)$elemFieldValue, $matches)) {
            $parameters = $matches[1];
        }

        return $parameters;
    }

    public function generateId()
    {
        $maxId = $this->getAttributeRepository()->getMaxId();

        $id = 0;
        if ($maxId) {
            $id = $maxId + self::ID_STEP;
        }

        return $id;
    }

    /**
     * @return ArrayCollection|Attribute[]
     */
    public function getAttributes()
    {
        if (!self::$attributes) {
            self::$attributes = $this->getAttributeRepository()->all();
        }

        return self::$attributes;
    }

    /**
     * @param $id
     * @return Attribute|null
     */
    public function getAttribute($id)
    {
        return $this->getDocumentManager()->find(Attribute::class, $id);
    }

    /**
     * @param $valueId
     * @return Attribute|null
     */
    public function getAttributeByValueId($valueId)
    {
        $attributes = $this->getAttributes();

        foreach ($attributes as $attribute) {
            foreach ($attribute->getValues() as $value) {
                if ($value->getId() == $valueId) {
                    return $attribute;
                }
            }
        }

        return null;
    }

    public function findAttributes($name)
    {
        $attributes = [];
        foreach ($this->getAttributes() as $attribute) {
            if ($attribute->support($name)) {
                $attributes[] = $attribute;
            }
        }
        return $attributes;
    }

    protected function findValueIds(Attribute $attribute, $value)
    {
        return $this
            ->getAttributesHelpersRegistry()
            ->get($attribute->getType())
            ->findValueIds($attribute, $value)
        ;
    }
}
