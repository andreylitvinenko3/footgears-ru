<?php

namespace Footgears\MainBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseService implements ContainerAwareInterface
{
    use ContainerTrait;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function getContainer()
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
