<?php

namespace Footgears\MainBundle\Service;

use Footgears\MainBundle\Document\CategoryBrand;
use Footgears\MainBundle\Search\BrandSearchRequest;

class CategoryBrandService extends BaseService
{
    public function updateProductsCounts(CategoryBrand $page = null)
    {
        $dm = $this->getDocumentManager();

        if (is_null($page)) {
            $pages = $dm->getRepository(CategoryBrand::class)->createQueryBuilder()->getQuery()->getIterator();
        } else {
            $pages = [$page];
        }

        /** @var CategoryBrand[] $pages */
        foreach ($pages as $page) {
            $page->setProductsCount($this->getRealProductsCount($page));

            $dm->persist($page);
        }

        $dm->flush();
    }

    public function getRealProductsCount(CategoryBrand $page)
    {
        $searchRequest = new BrandSearchRequest($page->getBrand(), $page->getCategory());
        $count = (int)$this->getSearchManager()->countProducts($searchRequest);

        return $count;
    }
}