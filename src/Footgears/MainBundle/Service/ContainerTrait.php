<?php

namespace Footgears\MainBundle\Service;

use Footgears\MainBundle\Attribute\AbstractManager;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\Repository\BrandRepository;
use Footgears\MainBundle\Document\Repository\CategoryRepository;
use Footgears\MainBundle\Document\Repository\ProductRepository;
use Footgears\MainBundle\Document\Repository\ShopCategoryRepository;
use Footgears\MainBundle\Document\Repository\ShopRepository;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Footgears\MainBundle\Elastic\ElasticManager;
use Footgears\MainBundle\Elastic\ElasticSearch;
use Footgears\MainBundle\Menu\MenuBuilder;
use Footgears\MainBundle\Search\SearchManager;
use Footgears\MainBundle\Search\SearchRequestFactory;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Stopwatch\Stopwatch;

trait ContainerTrait
{
    use MongoTrait;

    /**
     * @return Router
     */
    public function getRouter()
    {
        return $this->getContainer()->get('router');
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->getContainer()->get('request_stack')->getMasterRequest();
    }

    /**
     * @return SearchService
     */
    public function getSearchService()
    {
        return $this->getContainer()->get('footgears.search_service');
    }

    /**
     * @return SearchManager
     */
    public function getSearchManager()
    {
        return $this->getContainer()->get('footgears.search_manager');
    }

    /**
     * @return BrandService
     */
    public function getBrandService()
    {
        return $this->getContainer()->get('footgears.brand_service');
    }

    /**
     * @return AttributesService
     */
    public function getAttributesService()
    {
        return $this->getContainer()->get('footgears.attributes_service');
    }

    /**
     * @return CategoriesService
     */
    public function getCategoriesService()
    {
        return $this->getContainer()->get('footgears.categories_service');
    }

    /**
     * @return TagAwareAdapter
     */
    public function getCache()
    {
        /** @var AdapterInterface $adapter */
        $adapter = $this->getContainer()->get('cache.app');

        return new TagAwareAdapter($adapter);
    }

    /**
     * @return ShopRepository
     */
    public function getShopRepository()
    {
        return $this->getDocumentManager()->getRepository(Shop::class);
    }

    /**
     * @return ShopCategoryRepository
     */
    public function getShopCategoryRepository()
    {
        return $this->getDocumentManager()->getRepository(ShopCategory::class);
    }

    /**
     * @return BrandRepository
     */
    public function getBrandRepository()
    {
        return $this->getDocumentManager()->getRepository(Brand::class);
    }

    /**
     * @return ProductRepository
     */
    public function getProductRepository()
    {
        return $this->getDocumentManager()->getRepository(Product::class);
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryRepository()
    {
        return $this->getDocumentManager()->getRepository(Category::class);
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->getContainer()->get('logger');
    }

    /**
     * @return MenuBuilder
     */
    public function getMenuBuilder()
    {
        return $this->getContainer()->get('footgears.menu_builder');
    }

    /**
     * @return AbstractManager
     */
    public function getAttributeManager()
    {
        return $this->getContainer()->get('footgears.attributes.manager');
    }

    public function getMailSender()
    {
        return $this->getContainer()->get('footgears.mail_sender');
    }

    /**
     * @return SearchRequestFactory
     */
    public function getSearchRequestFactory()
    {
        return $this->getContainer()->get('footgears.saarch_factory');
    }

    public function getAdmitadManager()
    {
        return $this->getContainer()->get('footgears.manager.admitad');
    }

    /**
     * @return CategoryBrandService
     */
    public function getCategoryBrandService()
    {
        return $this->getContainer()->get('footgears.service.category_brand');
    }

    /**
     * @return \Footgears\MainBundle\Settings\SeoPages
     */
    public function getSeoPagesSettings()
    {
        return $this->getContainer()->get('footgears.settings.seo_pages');
    }

    /**
     * @return \Footgears\MainBundle\Attribute\Helper\Registry
     */
    public function getAttributesHelpersRegistry()
    {
        return $this->getContainer()->get('footgears.attributes.helpers.registry');
    }

    /**
     * @param array $arguments
     * @return ProcessBuilder
     */
    public function createCommandProcessBuilder(array $arguments = [])
    {
        $console = $this->getContainer()->getParameter('kernel.project_dir') . '/bin/console';
        $builder = new ProcessBuilder(array_merge([$console, '--env=' . $this->getContainer()->getParameter('kernel.environment')], $arguments));
        $builder->setTimeout(null);
        return $builder;
    }

    /**
     * @return \Redis
     */
    public function getRedis()
    {
        return $this->getContainer()->get('app.redis');
    }

    /**
     * @return Stopwatch
     */
    public function getStopwatch()
    {
        return $this->getContainer()->get('debug.stopwatch');
    }

    /**
     * @return ElasticManager
     */
    public function getElasticManager()
    {
        return $this->getContainer()->get('app.elastic_manager');
    }

    public function getElastic()
    {
        return $this->getContainer()->get('app.elastic.client');
    }

    /**
     * @return ElasticSearch
     */
    public function getElasticSearch()
    {
        return $this->getContainer()->get('app.elastic_search');
    }
}
