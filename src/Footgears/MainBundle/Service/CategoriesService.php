<?php

namespace Footgears\MainBundle\Service;

use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Search\SearchRequest;

class CategoriesService extends BaseService
{
    protected static $titleHeads = [
        'в Москве, доставка во все регионы России',
        'по ценам от %min% рублей',
        'от %min% рублей',
        'по низким ценам от %min% рублей',
        'цены от %min% рублей с доставкой по РФ',
        'из раздела %parent%',
        'по акционной цене от %min% руб',
    ];

    protected static $titleTails = [
        'распродажи, акции, скидки',
        'заказать онлайн',
        'каталог %parent%',
        'низкие цены, доставка по РФ',
        'цены, фото, доставка',
        'с доставкой по РФ',
        'лучшие цены',
        'акции, скидки',
        'быстрая доставка по РФ, гарантия качества от производителя',
    ];

    protected static $descriptions = [
        'Интернет молл одежды, обуви и аксессуаров: Скидки на модели из каталога %parent%, цены от %min% руб. на %category% %brand% -50% -70% и доставка по Москве и другим городам России',
        'Каталог одежды, обуви и аксессуаров Footgears: %parent% SALE 50-70% на %category% %brand% заказать онлайн с доставкой по России',
        '%parent% со скидками 50-80%: самые низкие цены от %min% руб. на %category% %brand%, быстрая доставка по России. Товар в наличии! Footgears',
        'Акционное предложение: %parent% со скидками от 50 до 75% только в интернет-молле Footgears. Покупайте %category% %brand% по акции + доставка 0 руб Москва, Россия',
        'Интернет молл одежды, обуви и аксессуаров Footgears предлагает широкий ассортимент товаров из каталога %parent%: %category% %brand% новинки и распродажи. Доставка по РФ',
        'Финальная распродажа в интернет-молле Footgears: %parent% со скидками 50-60-70%, заказывайте %category% %brand% недорого и доставкой по РФ',
        'Новая коллекция в каталоге %parent% уже на Footgears: %category% %brand% все размеры, фото, описания. Гарантия качества и доставка по всей России',
        'Одежда, обувь и аксессуары от ведущих интернет-магазинов на Footgears со скидками: %category% %brand% — лучшие ценовые предложения, скидки, гарантированная доставка в короткие сроки',
        '%amt% товаров в разделе %category% %brand% со скидкой от 40% и бесплатной доставкой по РФ. Интернет-каталог одежды, обуви и аксессуаров Footgears',
        'По вашему запросу найдено %amt% товаров из раздела %category% %brand% в каталоге %parent% от %min% руб с доставкой по всей России. Гарантия качества и лучшие цены в Footgears',
        'Мы отобрали для вас %amt% предложений из раздела %category% %brand% цены от %min% руб, фото, описания товара. Оплата после примерки, быстрая доставка по РФ, АКЦИИ и Скидки',
        'Каталог одежды, обуви и аксессуаров Footgears: %amt% моделей категории %category% %brand% по суперценам от лучших интернет-магазинов по низким ценам от %min% руб',
        'Только %amt% товаров по акционным ценам от %min% руб из раздела %category% %brand%, %parent% купить в интернет-магазине на Footgears',
        'Стильная одежда, обувь и аксессуары: %amt% предложения в категории %category% %brand% в каталоге %parent% со скидками до 75%. Распродажа на Footgears',
        'Тренды сезона: купить %category% %brand% от %min% руб с доставкой по Москве, регионам России в интернет-молле Footgears. Поддержка:',
    ];

    protected static $saleHeads = [
        'SALE!',
        'Распродажа!',
        'Финальная распродажа!',
        'Скидки до 90%'
    ];

    public function generateTitle()
    {
        $head = self::$titleHeads[array_rand(self::$titleHeads)];
        $tail = self::$titleTails[array_rand(self::$titleTails)];

        return sprintf('Купить %%category%% %%brand%% %s - %s в каталоге Footgears', $head, $tail);
    }

    public function generateSaleTitle()
    {
        $head = self::$titleHeads[array_rand(self::$titleHeads)];
        $tail = self::$titleTails[array_rand(self::$titleTails)];

        return sprintf('Купить %%category%% %%brand%% со скидками %s - %s в каталоге Footgears', $head, $tail);
    }

    public function generateDescription()
    {
        return self::$descriptions[array_rand(self::$descriptions)];
    }

    public function generateSaleDescription()
    {
        return sprintf('%s %s', self::$saleHeads[array_rand(self::$saleHeads)], self::$descriptions[array_rand(self::$descriptions)]);
    }

    public function getCategoriesTree(Category $category, SearchRequest $searchRequest = null)
    {
        $categories = $this->getAvailableCategories($category);

        /* @var Category[] $allCategoryNodes */
        $allCategoryNodes = [$category->getId() => $category];

        $counts = [];

        if ($searchRequest) {
            $brandIds = $searchRequest->getBrandIds();
            $shopIds = $searchRequest->getShopIds();

            $searchRequest = $this->getSearchRequestFactory()->createSearchRequest();
            $searchRequest->setFiltersFromRequest($this->getRequest());

            if (!empty($brandIds)) {
                $searchRequest->setBrandIds($brandIds);
            }
            if (!empty($shopIds)) {
                $searchRequest->setShopIds($shopIds);
            }

            $counts = $this->getSearchManager()->getAvailableCategories($searchRequest);
        }

        foreach ($categories as $node) {
            if (($searchRequest ? isset($counts[$node->getId()]) : true) && $this->isAvailableNode($category, $node)) {
                $allCategoryNodes[$node->getId()] = $node;
            }
        }

        $availableCategories = $this->getCategoryRepository()->getTreeOf($allCategoryNodes,
            function (Category $item) use ($category, $counts) {
                return [
                    'category' => $item,
                    'count' => isset($counts[$item->getId()]) ? $counts[$item->getId()] : 0,
                    'current' => $this->isCurrentNode($category, $item),
                ];
        });

        return $availableCategories;
    }

    /**
     * @param Category $currentCategory
     * @return Category[]
     */
    public function getAvailableCategories(Category $currentCategory)
    {
        $categoryIds = [$currentCategory->getId()];

        if ($currentCategory->getParent()) {
            $categoryIds[] = $currentCategory->getParent()->getId();
        }

        $qb = $this->getCategoryRepository()->createQueryBuilder();

        $qb
            ->addOr($qb->expr()
                ->field('root')->equals($currentCategory->getRoot())
                ->field('left')->lte($currentCategory->getLeft())
                ->field('right')->gte($currentCategory->getRight())
            )
            ->addOr(
                $qb->expr()->field('parent')->in($categoryIds)
            )
        ;

        return $qb->getQuery()->execute();
    }

    protected function isAvailableNode(Category $currentCategory, Category $node)
    {
        return $node->isParentFor($currentCategory) || (
                $node->getParent() && ($currentCategory->hasChildren() ?
                    $node->getParent()->getId() == $currentCategory->getId() :
                    $node->getParent()->getId() == $currentCategory->getParent()->getId()
                )
        );
    }

    protected function isCurrentNode(Category $currentCategory, Category $node)
    {
        return
            $node->getId() == $currentCategory->getId() ||
            $currentCategory->getParent() && $node->getId() == $currentCategory->getParent()->getId()
        ;
    }

    public function addCategory(&$categories, Category $category)
    {
        if ($category->getParent()) {
            $this->addCategory($categories, $category->getParent());
        }

        $categories[] = $category->getName();
    }

    /**
     * @param Brand $brand
     * @param int $limit
     * @return Category[]
     */
    public function getPopularsByBrand(Brand $brand, $limit = 4)
    {
        $searchRequest = $this->getSearchRequestFactory()->createBrandSearchRequest($brand);

        $availableCategories = $this->getSearchManager()->getAvailableCategories($searchRequest);

        if (!$availableCategories) {
            return [];
        }

        /** @var Category[] $categories */
        $categories = $this->getCategoryRepository()->findBy(['id' => array_slice(array_keys($availableCategories), 0, $limit * 4)]);

        // так а не array_filter чтобы было меньше запросов в БД
        $result = [];
        foreach ($categories as $category) {
            if ($category->getChildren()->count()) {
                continue;
            }

            $result[] = $category;

            if (count($result) >= $limit) {
                break;
            }
        }

        return array_slice($categories, 0, $limit);
    }
}