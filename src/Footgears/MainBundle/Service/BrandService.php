<?php

namespace Footgears\MainBundle\Service;

use Footgears\MainBundle\Document\Brand;

class BrandService extends BaseService
{
    protected static $titles = [
        '%brand% - каталог коллекции %years% в %host%'
    ];
    
    protected static $seoDescriptions = [
        'Большой выбор одежды, обуви и аксессуаров из каталога %brand% в интернет-каталоге Footgears. Постоянные скидки, доставка во все регионы, удобный выбор.',
    ];

    public function createBrand()
    {
        $brand = new Brand();
        $brand
            ->getSeo()
                ->setTitle($this->getRandomSeoTitle())
                ->setDescription($this->getRandomSeoDescription())
        ;

        return $brand;
    }

    public function getRandomSeoTitle()
    {
        return self::$titles[mt_rand(0, count(self::$titles) - 1)];
    }

    public function getRandomSeoDescription()
    {
        return self::$seoDescriptions[mt_rand(0, count(self::$seoDescriptions) - 1)];
    }

    public function findPopular($limit)
    {
        $cacheItem = $this->getCache()->getItem('brands.popular.' . $limit);
        $cacheItem->expiresAt(new \DateTime('+5 hour'));
        if (!$cacheItem->isHit()) {
            $searchRequest = $this->getSearchRequestFactory()->createSearchRequest();
            $brandIds = $this->getSearchManager()->getAvailableBrands($searchRequest, $limit * 4);

            if (!$brandIds) {
                return [];
            }
            $brandIds = array_keys($brandIds);
            shuffle($brandIds);

            $cacheItem->set($brandIds);
            $this->getCache()->save($cacheItem);
        } else {
            $brandIds = $cacheItem->get();
        }

        if (!$brandIds) {
            return [];
        }

        $brands = $this
            ->getDocumentManager()
            ->getRepository(Brand::class)
            ->findBy(['_id' => ['$in' => array_slice($brandIds, 0, $limit)]])
        ;

        return $brands;
    }

    public function getRelatedByShop($value)
    {
        $qb = $this
            ->getDocumentManager()
            ->getRepository(Brand::class)
            ->createQueryBuilder()
        ;

        $brand = $qb
            ->addOr($qb->expr()->field('name')->equals($value))
            ->addOr($qb->expr()->field('searchTags')->equals(new \MongoRegex('/' . preg_quote($value) . '/i')))
            ->limit(1)
            ->getQuery()
            ->getSingleResult()
        ;

        return $brand;
    }
}