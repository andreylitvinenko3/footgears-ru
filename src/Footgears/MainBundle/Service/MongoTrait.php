<?php

namespace Footgears\MainBundle\Service;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Footgears\MainBundle\Document\AdmitadDailyStat;
use Footgears\MainBundle\Document\Attribute;
use Footgears\MainBundle\Document\Feedback;
use Footgears\MainBundle\Document\ImportHistory;
use Footgears\MainBundle\Document\Repository\AdmitadDailyStatRepository;
use Footgears\MainBundle\Document\Repository\AttributeRepository;
use Footgears\MainBundle\Document\Repository\ImportHistoryRepository;

trait MongoTrait
{
    use ContainerAwareTrait;

    /**
     * @return ManagerRegistry
     */
    public function getMongo()
    {
        return $this->getContainer()->get('doctrine_mongodb');
    }

    /**
     * @return DocumentManager
     */
    public function getDocumentManager()
    {
        return $this->getMongo()->getManager();
    }

    /**
     * @return AdmitadDailyStatRepository
     */
    public function getAdmitadDailyStatRepository()
    {
        return $this->getDocumentManager()->getRepository(AdmitadDailyStat::class);
    }

    public function getFeedbackRepository()
    {
        return $this->getDocumentManager()->getRepository(Feedback::class);
    }

    /**
     * @return AttributeRepository
     */
    public function getAttributeRepository()
    {
        return $this->getDocumentManager()->getRepository(Attribute::class);
    }

    /**
     * @return ImportHistoryRepository
     */
    public function getImportHistoryRepository()
    {
        return $this->getDocumentManager()->getRepository(ImportHistory::class);
    }
}