<?php

namespace Footgears\MainBundle\Form\Type;

use Footgears\MainBundle\Filter\DateRangeFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateRangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateFrom', DateType::class, [
                'widget' => 'single_text',
                'label' => 'с',
                'format' => 'dd.MM.yyyy',
            ])
            ->add('dateTo', DateType::class, [
                'widget' => 'single_text',
                'label' => 'по',
                'format' => 'dd.MM.yyyy',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => 'Период',
            'data_class' => DateRangeFilter::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'date_range';
    }
}
