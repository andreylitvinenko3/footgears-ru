<?php

namespace Footgears\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email'
            ])
            ->add('name', TextType::class, [
                'label' => 'Ваше имя',
                'required' => false
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Текст сообщения'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Отправить сообщение'
            ])
        ;
    }

    public function getBlockPrefix()
    {
        return 'feedback';
    }
}