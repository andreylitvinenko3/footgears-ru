<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\EmbeddedDocument
 */
class Seo
{
    /**
     * @Mongo\Field(type="string")
     */
    protected $h1;

    /**
     * @Mongo\Field(type="string")
     */
    protected $text;

    /**
     * @Mongo\Field(type="string")
     */
    protected $shortText;

    /**
     * @Mongo\Field(type="string")
     */
    protected $title;

    /**
     * @Mongo\Field(type="string")
     */
    protected $description;

    /**
     * @Mongo\Field(type="string")
     */
    protected $keywords;

    public function getH1()
    {
        return $this->h1;
    }

    public function setH1($h1)
    {
        $this->h1 = $h1;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getShortText()
    {
        return $this->shortText;
    }

    public function setShortText($shortText)
    {
        $this->shortText = $shortText;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }
}
