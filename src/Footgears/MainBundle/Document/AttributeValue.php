<?php

namespace Footgears\MainBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/** @Mongo\EmbeddedDocument */
class AttributeValue
{
    /**
     * @var integer
     * @Mongo\Id(strategy="NONE")
     */
    protected $id;

    /**
     * @var string $label
     * @Mongo\Field
     */
    protected $label;

    /**
     * @var array $parameters
     * @Mongo\Hash
     */
    protected $parameters = [];

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function getParameter($key)
    {
        if (!$this->hasParameter($key)) {
            throw new \InvalidArgumentException();
        }

        return $this->parameters[$key];
    }

    public function hasParameter($key)
    {
        return array_key_exists($key, $this->parameters);
    }

    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }
}