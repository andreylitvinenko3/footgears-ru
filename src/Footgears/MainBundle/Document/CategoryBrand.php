<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="category_brand", repositoryClass="Footgears\MainBundle\Document\Repository\CategoryBrandRepository")
 */
class CategoryBrand
{
    /**
     * @Mongo\Id(strategy="INCREMENT", type="int")
     */
    protected $id;

    /**
     * @var Category
     * @Mongo\ReferenceOne(targetDocument="Category", storeAs="id")
     */
    protected $category;

    /**
     * @var Brand
     * @Mongo\ReferenceOne(targetDocument="Brand", storeAs="id")
     */
    protected $brand;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $seo;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $saleSeo;

    /**
     * @Mongo\Field(type="int")
     */
    protected $productsCount = 0;

    public function __construct()
    {
        $this->seo = new Seo();
        $this->saleSeo = new Seo();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand(Brand $brand)
    {
        $this->brand = $brand;
        return $this;
    }

    public function getSeo()
    {
        return $this->seo;
    }

    public function setSeo(Seo $seo)
    {
        $this->seo = $seo;
        return $this;
    }

    public function getSaleSeo()
    {
        return $this->saleSeo;
    }

    public function setSaleSeo(Seo $saleSeo)
    {
        $this->saleSeo = $saleSeo;
        return $this;
    }

    public function getProductsCount()
    {
        return $this->productsCount;
    }

    public function setProductsCount($productsCount)
    {
        $this->productsCount = $productsCount;
        return $this;
    }
}
