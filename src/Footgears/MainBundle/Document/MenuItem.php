<?php

namespace Footgears\MainBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Gedmo\Mapping\Annotation as Gedmo;
use NestedTreeExtension\Traits\NestedSetDocument;

/**
 * @Mongo\Document(collection="menu_items", repositoryClass="Footgears\MainBundle\Document\Repository\MenuItemRepository")
 * @Gedmo\Tree(type="nested")
 */
class MenuItem
{
    use NestedSetDocument;

    const TYPE_MAIN = 'main';
    const TYPE_FOOTER = 'footer';
    const TYPE_FOOTER_TAGS = 'footerTags';
    const TYPE_MOBILE_MAIN = 'mobile_main';

    const TYPES = [
        self::TYPE_MAIN => 'Верхнее меню',
        self::TYPE_FOOTER => 'Меню в футере',
        self::TYPE_FOOTER_TAGS => 'Теги в футере',
        self::TYPE_MOBILE_MAIN => 'Верхнее мобильное'
    ];

    /**
     * @Mongo\Id(strategy="INCREMENT", type="int")
     */
    protected $id;

    /**
     * @var Brand $brand
     * @Mongo\ReferenceOne(targetDocument="Brand", storeAs="id", orphanRemoval="true")
     */
    protected $brand;

    /**
     * @var Category $category
     * @Mongo\ReferenceOne(targetDocument="Category", storeAs="id")
     */
    protected $category;

    /**
     * @Mongo\Field(type="string")
     */
    protected $name;

    /**
     * @var MenuItem
     * @Mongo\ReferenceOne(targetDocument="MenuItem", inversedBy="children", storeAs="id")
     * @Gedmo\TreeParent
     */
    protected $parent;

    /**
     * @Mongo\Field(type="int")
     * @Gedmo\TreeRoot
     */
    protected $root;

    /**
     * @var MenuItem[]|ArrayCollection
     * @Mongo\ReferenceMany(targetDocument="MenuItem", mappedBy="parent", orphanRemoval="true", sort={"left"="asc"})
     */
    protected $children;

    /**
     * @Mongo\Field(type="string")
     */
    protected $class;

    /**
     * @Mongo\Field(type="string")
     */
    protected $customUrl = '';

    /**
     * @Mongo\Field(type="bool")
     */
    protected $redirect = false;

    /**
     * @Mongo\Field(type="bool")
     */
    protected $blank = false;

    /**
     * @Mongo\Field(type="string")
     */
    protected $linkTitle;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand(Brand $brand = null)
    {
        $this->brand = $brand;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category = null)
    {
        $this->category = $category;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(self $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot($root)
    {
        $this->root = $root;
        return $this;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    public function getCustomUrl()
    {
        return $this->customUrl;
    }

    public function setCustomUrl($customUrl)
    {
        $this->customUrl = $customUrl;
        return $this;
    }

    public function isRedirect()
    {
        return $this->redirect;
    }

    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
        return $this;
    }

    public function isBlank()
    {
        return $this->blank;
    }

    public function setBlank($blank)
    {
        $this->blank = $blank;
        return $this;
    }

    public function getLinkTitle()
    {
        return $this->linkTitle;
    }

    public function setLinkTitle($linkTitle)
    {
        $this->linkTitle = $linkTitle;
        return $this;
    }

    public function getLabel()
    {
        if ($this->getLevel() === 0) {
            return '----';
        }

        if ($this->name) {
            return $this->name;
        }
        if ($this->getBrand()) {
            return $this->getBrand()->getName();
        }
        if ($this->getCategory()) {
            return $this->getCategory()->getName();
        }
        return '';
    }

    public function getShiftedLabel()
    {
        return str_repeat(chr(194) . chr(160), $this->getLevel() * 4) . $this->getLabel();
    }

    public function __toString()
    {
        return $this->getLabel();
    }
}
