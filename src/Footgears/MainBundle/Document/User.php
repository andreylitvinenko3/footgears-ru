<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @Mongo\Document(collection="users", repositoryClass="Footgears\MainBundle\Document\Repository\UserRepository")
 */
class User implements AdvancedUserInterface
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    const ROLES = [
        self::ROLE_USER => 'Пользователь',
        self::ROLE_ADMIN => 'Админ',
        self::ROLE_SUPER_ADMIN => 'Суперадмин',
    ];

    /**
     * @Mongo\Id(strategy="INCREMENT")
     */
    protected $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $username;

    /**
     * @Mongo\Field(type="string")
     */
    protected $email;

    /**
     * @Mongo\Field(type="string")
     */
    protected $password;

    /**
     * @Mongo\Field(type="string")
     */
    protected $salt;

    protected $plainPassword;

    /**
     * @var bool
     * @Mongo\Field(type="bool")
     */
    protected $enabled = true;

    /**
     * @Mongo\Collection
     */
    protected $roles = [];

    /**
     * @Mongo\Field(type="date")
     */
    protected $createDate;

    /**
     * @Mongo\Field(type="string")
     */
    protected $vkId;

    public function __construct()
    {
        $this->roles = [];
        $this->createDate = new \DateTime();
    }

    public static function createFakeEmail()
    {
        return sprintf('%s@fake.email', md5(uniqid() . microtime(true)));
    }

    public function addRole($role)
    {
        $this->roles[] = $role;

        $this->roles = array_values(array_unique($this->roles));
        return $this;
    }

    public function removeRole($role)
    {
        $this->roles = array_values(array_diff($this->roles, [$role]));
        return $this;
    }

    public function setRoles($roles)
    {
        $this->roles = array_values(array_unique($roles));

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        if (!in_array(self::ROLE_USER, $roles)) {
            $roles[] = self::ROLE_USER;
        }

        return $roles;
    }

    public function isAdmin()
    {
        return in_array(self::ROLE_ADMIN, $this->roles) || $this->isSuperAdmin();
    }

    public function isSuperAdmin()
    {
        return in_array(self::ROLE_SUPER_ADMIN, $this->roles);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = mb_strtolower($username);

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = mb_strtolower($email);

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getCreateDate()
    {
        return $this->createDate;
    }

    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
        return $this;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public static function getRolesList()
    {
        return self::ROLES;
    }

    public function isUser(User $user = null)
    {
        return null !== $user && $this->getId() === $user->getId();
    }

    public function getVkId()
    {
        return $this->vkId;
    }

    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }

    public function isFakeEmail()
    {
        return strpos($this->email, '@fake.email') !== false;
    }
}
