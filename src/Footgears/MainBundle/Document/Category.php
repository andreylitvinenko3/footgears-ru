<?php

namespace Footgears\MainBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Gedmo\Mapping\Annotation as Gedmo;
use NestedTreeExtension\Traits\NestedSetDocument;

/**
 * @Mongo\Document(collection="categories", repositoryClass="Footgears\MainBundle\Document\Repository\CategoryRepository")
 * @Gedmo\Tree(type="nested")
 */
class Category
{
    use NestedSetDocument;

    const RECOMMEND_PRODUCT_COUNT = 5;

    /**
     * @Mongo\Id(strategy="INCREMENT", type="int")
     */
    protected $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $name;

    /**
     * @Mongo\Field(type="string")
     */
    protected $shortName;

    /**
     * @Mongo\Field(type="string")
     */
    protected $alias;

    /**
     * @var Category
     * @Gedmo\TreeParent
     * @Mongo\ReferenceOne(targetDocument="Category", storeAs="id")
     */
    protected $parent;

    /**
     * @Mongo\Field(type="int")
     * @Gedmo\TreeRoot
     */
    protected $root;

    /**
     * @Mongo\Field(type="int")
     */
    protected $productsCount;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $seo;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $saleSeo;

    /**
     * @var ArrayCollection|ShopCategory[]
     * @Mongo\ReferenceMany(targetDocument="ShopCategory", mappedBy="category")
     */
    protected $shopCategories;

    public function __construct()
    {
        $this->seo = new Seo();
        $this->saleSeo = new Seo();

        $this->shopCategories = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(self $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getShortName()
    {
        return $this->shortName;
    }

    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function getProductsCount()
    {
        return $this->productsCount;
    }

    public function setProductsCount($productsCount)
    {
        $this->productsCount = $productsCount;
        return $this;
    }

    public function getSeo()
    {
        return $this->seo;
    }

    public function setSeo($seo)
    {
        $this->seo = $seo;
        return $this;
    }

    public function getSaleSeo()
    {
        return $this->saleSeo;
    }

    public function setSaleSeo($saleSeo)
    {
        $this->saleSeo = $saleSeo;
        return $this;
    }

    public function getShiftedName()
    {
        return str_repeat(chr(194) . chr(160), $this->getLevel() * 4) . $this->getName();
    }

    public function getPathName()
    {
        if ($this->parent) {
            return $this->parent->getPathName() . ' / ' .$this->shortName;
        }
        return $this->shortName;
    }

    public function isParentFor(Category $node)
    {
        if ($this->left < $node->getLeft() && $this->right > $node->getRight()) {
            return true;
        }
        return false;
    }

    public function isParent($id)
    {
        if ($this->parent) {
            if ($this->parent->getId() == $id) {
                return true;
            } else {
                return $this->parent->isParent($id);
            }
        }

        return false;
    }

    public function hasChildren()
    {
        return $this->right - $this->left > 1;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot($root)
    {
        $this->root = $root;
        return $this;
    }

    public function getShopCategories()
    {
        return $this->shopCategories;
    }
}
