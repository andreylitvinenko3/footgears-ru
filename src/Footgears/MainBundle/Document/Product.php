<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Sirian\Helpers\TextUtils;

/**
 * @Mongo\Document(collection="products", repositoryClass="Footgears\MainBundle\Document\Repository\ProductRepository")
 */
class Product
{
    /**
     * @Mongo\Id(strategy="NONE")
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    private $originalId;

    /**
     * @Mongo\Field(type="string")
     */
    private $name;

    /**
     * @Mongo\Field(type="string")
     */
    private $url;

    /**
     * @var integer
     * @Mongo\Field(type="int")
     */
    protected $importVersion = 0;

    /**
     * @Mongo\Field(type="collection")
     */
    private $attributes = [];

    /**
     * @var float
     * @Mongo\Field(type="float")
     */
    private $price = 0;

    /**
     * @Mongo\Field(type="string")
     */
    protected $currency;

    /**
     * @var float
     * @Mongo\Field(type="float")
     */
    private $oldPrice = 0;

    /**
     * @var Brand
     * @Mongo\ReferenceOne(targetDocument="Brand", storeAs="id")
     */
    protected $brand;

    /**
     * @var Category
     * @Mongo\ReferenceOne(targetDocument="Category", storeAs="id")
     */
    protected $category;

    /**
     * @var ShopCategory
     * @Mongo\ReferenceOne(targetDocument="ShopCategory", storeAs="id")
     */
    protected $shopCategory;

    /**
     * @var Shop
     * @Mongo\ReferenceOne(targetDocument="Shop", storeAs="id")
     */
    protected $shop;

    /**
     * @var array
     * @Mongo\Field(type="collection")
     */
    protected $images = [];

    /**
     * @Mongo\Field(type="string")
     */
    protected $xml;

    /**
     * @var bool
     * @Mongo\Field(type="bool")
     */
    protected $enabled = true;

    protected $valid;

    /**
     * @var \DateTIme
     * @Mongo\Field(type="date")
     */
    protected $disableDate;

    public static function generateId($shopId, $originalId)
    {
        return sprintf('%s:%s', $shopId, $originalId);
    }

    protected function updateId()
    {
        if (!$this->shop || !$this->originalId) {
            return;
        }

        $this->id = self::generateId($this->shop->getId(), $this->originalId);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOriginalId()
    {
        return $this->originalId;
    }

    public function setOriginalId($originalId)
    {
        $this->originalId = $originalId;
        $this->updateId();
        return $this;
    }

    public function setName($name)
    {
        $this->name = (string)$name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = (string)$url;
        return $this;
    }

    public function getPrice()
    {
        return (float)$this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getOldPrice()
    {
        return (float)$this->oldPrice;
    }

    public function setOldPrice($oldPrice)
    {
        $this->oldPrice = $oldPrice;
        return $this;
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function setShop(Shop $shop)
    {
        $this->shop = $shop;
        $this->updateId();

        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand(Brand $brand = null)
    {
        $this->brand = $brand;
        return $this;
    }

    public function isSale()
    {
        return $this->getOldPrice() && $this->getOldPrice() > $this->getPrice();
    }

    public function getDiscountSum()
    {
        if (!$this->oldPrice) {
            return 0;
        }

        if (!$this->isSale()) {
            return 0;
        }

        return $this->oldPrice - $this->price;
    }

    public function getDiscount()
    {
        if (!$this->isSale()) {
            return 0;
        }

        return round(100 - 100 * $this->getPrice() / $this->getOldPrice());
    }

    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getParams()
    {
        if (!$this->xml) {
            return [];
        }

        $xml = simplexml_load_string($this->xml);
        $params = [];

        foreach ($xml->param as $param) {
            $name = (string)$param['name'];
            if (isset($param['unit'])) {
                $name .= ' (' . $param['unit'] . ')';
            }
            $value = (string)$param;
            if ($value) {
                if (!isset($params[$name]) || !in_array($value, $params[$name])) {
                    $params[$name][] = $value;
                }
            }
        }

        return $params;
    }

    public function getDescription()
    {
        if (!$this->xml) {
            return '';
        }
        $xml = simplexml_load_string($this->xml);
        return (string)$xml->description;
    }

    public function getModel()
    {
        if (!$this->xml) {
            return '';
        }

        $xml = simplexml_load_string($this->xml);
        return $xml->model ? (string)$xml->model : '';
    }

    public function getArticle()
    {
        if (!$this->xml) {
            return '';
        }

        $xml = simplexml_load_string($this->xml);
        return (string)$xml->model;
    }

    public function getFormattedXml()
    {
        $dom = dom_import_simplexml(simplexml_load_string($this->xml))->ownerDocument;
        $dom->formatOutput = true;
        return $dom->saveXML();
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category = null)
    {
        $this->category = $category;
        return $this;
    }

    public function getShopCategory()
    {
        return $this->shopCategory;
    }

    public function setShopCategory(ShopCategory $shopCategory)
    {
        $this->shopCategory = $shopCategory;
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function setXml($xml)
    {
        $this->xml = $xml;
        return $this;
    }

    public function getXml()
    {
        return $this->xml;
    }

    public function getSlug()
    {
        return TextUtils::slug($this->name) . '-';
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = (bool)$enabled;
        return $this;
    }

    public function disable()
    {
        $this->enabled = false;
        $this->disableDate = new \DateTime();
    }

    public function activate()
    {
        $this->enabled = true;
        $this->disableDate = null;
    }

    public function getDisableDate()
    {
        return $this->disableDate;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    public function getFirstImage()
    {
        if (count($this->images)) {
            return $this->images[0];
        }

        return null;
    }

    public function getImportVersion()
    {
        return $this->importVersion;
    }

    public function setImportVersion($importVersion)
    {
        $this->importVersion = $importVersion;
        return $this;
    }

    public function isValid()
    {
        if (!is_null($this->valid)) {
            return $this->valid;
        }

        $this->valid =
            $this->shop && $this->shop->isActive()
            && $this->brand && $this->getBrand()->isEnabled()
            && $this->category
            && $this->shopCategory && $this->shopCategory->isEnabledTree()
        ;

        return $this->valid;
    }
}
