<?php

namespace Footgears\MainBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="attributes", repositoryClass="Footgears\MainBundle\Document\Repository\AttributeRepository")
 */
class Attribute
{
    const TYPE_SIZE = 'size';
    const TYPE_RANGE = 'range';
    const TYPE_CONTENT = 'content';
    const TYPE_EXACT = 'exact';

    const FEMALE_SHOE_SIZE = 1;
    const MALE_SHOE_SIZE = 1001;
    const FEMALE_DRESS_SIZE = 2001;
    const MALE_DRESS_SIZE = 3001;
    const COLOR = 4001;
    const SEAZON = 5001;
    const YEAR = 6001;
    const HEEL_HEIGHT = 7001;
    const HEEL_TYPE = 8001;
    const CONTENT = 9001;
    const GENDER = 10001;
    const MANUFACTURER = 11001;

    protected static $types = [
        self::TYPE_SIZE => 'Размер',
        self::TYPE_RANGE => 'Диапазон',
        self::TYPE_CONTENT => 'Состав',
        self::TYPE_EXACT => 'Точное значение',
    ];

    /**
     * @var integer
     * @Mongo\Id(strategy="NONE")
     */
    protected $id;

    /**
     * @var string $name
     * @Mongo\Field
     */
    protected $name;

    /**
     * @var ArrayCollection $aliases
     * @Mongo\Collection
     */
    protected $aliases;

    /**
     * @var int $sort
     * @Mongo\Field(type="integer")
     */
    protected $sort = 0;

    /**
     * @var ArrayCollection|AttributeValue[]
     * @Mongo\EmbedMany(targetDocument="AttributeValue", strategy="set")
     */
    protected $values;

    /**
     * @var string $type
     * @Mongo\Field
     */
    protected $type = self::TYPE_EXACT;

    /**
     * @var bool $enabled
     * @Mongo\Boolean
     */
    protected $enabled = true;

    public function __construct()
    {
        $this->values = new ArrayCollection();
        $this->aliases = new ArrayCollection();
    }

    public static function getTypes()
    {
        return self::$types;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getAliases()
    {
        return $this->aliases;
    }

    public function setAliases($aliases)
    {
        $this->aliases = $aliases;

        return $this;
    }

    public function addAlias($alias)
    {
        $this->aliases[] = mb_strtolower($alias);
        return $this;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function setValues($values)
    {
        foreach ($values as $value) {
            $this->addValue($value);
        }

        return $this;
    }

    public function addValue(AttributeValue $value)
    {
        if (!$this->values->contains($value)) {

            if (!$value->getId()) {
                $id = $this->getId() + $this->values->count() + 1;
                $value->setId($id);
            }

            $this->values->add($value);
        }

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getValueByParameter($parameter)
    {
        foreach ($this->values as $value) {
            if ($value->hasParameter('values') && in_array(mb_strtolower($parameter), $value->getParameter('values'))) {
                return $value;
            }
        }

        return null;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = (bool)$enabled;
        return $this;
    }

    public function getValue($valueId)
    {
        foreach ($this->values as $value) {
            if ($value->getId() == $valueId) {
                return $value;
            }
        }

        return null;
    }

    public function support($name)
    {
        $name = mb_strtolower($name);
        if (mb_strtolower($this->name) == $name) {
            return true;
        }
        foreach ($this->aliases as $alias) {
            if (mb_strtolower($alias) == $name) {
                return true;
            }
        }
        return false;
    }
}