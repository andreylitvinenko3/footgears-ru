<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="import_histories", repositoryClass="Footgears\MainBundle\Document\Repository\ImportHistoryRepository")
 */
class ImportHistory
{
    const STATUS_ACTIVE = 'active';
    const STATUS_SUCCESS = 'success';
    const STATUS_CANCELED = 'canceled';
    const STATUS_EXCEPTION = 'exception';

    const PROGRESS_WAIT_DOWNLOAD = 'wait_download';
    const PROGRESS_DOWNLOAD_START = 'download_start';
    const PROGRESS_DOWNLOAD = 'download';
    const PROGRESS_WAIT_PREPARE_FEED = 'wait_prepare_feed';
    const PROGRESS_PREPARE_FEED_START = 'prepare_feed_start';
    const PROGRESS_PREPARE_FEED = 'prepare_feed';
    const PROGRESS_WAIT_PARSING = 'wait_parsing';
    const PROGRESS_PARSING_START = 'parsing_start';
    const PROGRESS_PARSING_XML = 'parsing_xml';
    const PROGRESS_PARSING_CATEGORIES = 'parsing_categories';
    const PROGRESS_PARSING_PRODUCTS = 'parsing_products';
    const PROGRESS_DISABLE_PRODUCTS = 'disable_products';
    const PROGRESS_DISABLE_PRODUCTS_END = 'disable_products_end';
    const PROGRESS_UPDATE_COUNTERS = 'update_counters';
    const PROGRESS_FINISH = 'finish';

    /**
     * @var int $id
     * @Mongo\Id
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $url;

    /**
     * @var bool
     * @Mongo\Field(type="bool")
     */
    protected $delta = false;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $fileSize;

    /**
     * @var Shop
     * @Mongo\ReferenceOne(targetDocument="Shop", simple=true, cascade={"persist"})
     */
    protected $shop;

    /**
     * @Mongo\Field(type="date")
     */
    protected $startDate;

    /**
     * @var \DateTime
     * @Mongo\Field(type="date")
     */
    protected $updateDate;

    /**
     * @Mongo\Field(type="date")
     */
    protected $endDate;

    /**
     * @Mongo\Field(type="string")
     */
    protected $status = self::STATUS_ACTIVE;

    /**
     * @Mongo\Field(type="string")
     */
    protected $progress = self::PROGRESS_WAIT_DOWNLOAD;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $newCategories = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $updatedCategories = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $removedCategories = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $products = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $newProducts = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $updatedProducts = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $skippedProducts = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $disabledProducts = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $indexAddProducts = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $indexDeleteProducts = 0;

    /**
     * @Mongo\Field(type="string")
     */
    protected $exception;

    public function __construct()
    {
        $this->startDate = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function setShop(Shop $shop)
    {
        $this->shop = $shop;
        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($date)
    {
        $this->startDate = $date;

        return $this;
    }

    public function getNewCategories()
    {
        return $this->newCategories;
    }

    public function setNewCategories($newCategories)
    {
        $this->newCategories = $newCategories;

        return $this;
    }

    public function incNewCategories($inc = 1)
    {
        $this->newCategories += $inc;
        return $this;
    }

    public function getRemovedCategories()
    {
        return $this->removedCategories;
    }

    public function setRemovedCategories($removedCategories)
    {
        $this->removedCategories = $removedCategories;

        return $this;
    }

    public function incRemovedCategories($inc = 1)
    {
        $this->removedCategories += $inc;
        return $this;
    }

    public function getUpdatedCategories()
    {
        return $this->updatedCategories;
    }

    public function setUpdatedCategories($updatedCategories)
    {
        $this->updatedCategories = $updatedCategories;

        return $this;
    }

    public function incUpdatedCategories($inc = 1)
    {
        $this->updatedCategories += $inc;
        return $this;
    }

    public function getNewProducts()
    {
        return $this->newProducts;
    }

    public function setNewProducts($newProducts)
    {
        $this->newProducts = $newProducts;

        return $this;
    }

    public function incNewProducts($inc = 1)
    {
        $this->newProducts += $inc;
        return $this;
    }

    public function getUpdatedProducts()
    {
        return $this->updatedProducts;
    }

    public function setUpdatedProducts($updateProducts)
    {
        $this->updatedProducts = $updateProducts;

        return $this;
    }

    public function incUpdatedProducts($inc = 1)
    {
        $this->updatedProducts += $inc;
        return $this;
    }

    public function getSkippedProducts()
    {
        return $this->skippedProducts;
    }

    public function setSkippedProducts($skippedProducts)
    {
        $this->skippedProducts = $skippedProducts;
        return $this;
    }

    public function incSkippedProducts($inc = 1)
    {
        $this->skippedProducts += $inc;
        return $this;
    }

    public function getException()
    {
        return $this->exception;
    }

    public function setException($exception)
    {
        $this->exception = $exception;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getProgress()
    {
        return $this->progress;
    }

    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getFileSize()
    {
        return $this->fileSize;
    }

    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    public function getIndexAddProducts()
    {
        return $this->indexAddProducts;
    }

    public function setIndexAddProducts($indexAddProducts)
    {
        $this->indexAddProducts = $indexAddProducts;
        return $this;
    }

    public function incIndexAddProducts($inc = 1)
    {
        $this->indexAddProducts += $inc;
        return $this;
    }

    public function getIndexDeleteProducts()
    {
        return $this->indexDeleteProducts;
    }

    public function incIndexDeleteProducts($inc = 1)
    {
        $this->indexDeleteProducts += $inc;
        return $this;
    }

    public function setIndexDeleteProducts($indexDeleteProducts)
    {
        $this->indexDeleteProducts = $indexDeleteProducts;
        return $this;
    }

    public function getDisabledProducts()
    {
        return $this->disabledProducts;
    }

    public function setDisabledProducts($disabledProducts)
    {
        $this->disabledProducts = $disabledProducts;
        return $this;
    }

    public function incDisabledProducts($inc = 1)
    {
        $this->disabledProducts += $inc;
        return $this;
    }

    public function isDelta()
    {
        return $this->delta;
    }

    public function setDelta($delta)
    {
        $this->delta = $delta;
        return $this;
    }
}
