<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\EmbeddedDocument
 */
class ImportProductsSettings
{
    /**
     * @Mongo\Field(type="bool")
     */
    protected $enabled = false;

    /**
     * @Mongo\Field(type="bool")
     */
    protected $force = false;

    /**
     * @Mongo\Field(type="bool")
     */
    protected $delta = false;

    /**
     * @Mongo\Field(type="string")
     */
    protected $url;

    /**
     * @Mongo\Field(type="string")
     */
    protected $idXPath;

    /**
     * @Mongo\Field(type="string")
     */
    protected $nameExpression = 'name';

    /**
     * @Mongo\Field(type="string")
     */
    protected $oldPriceXPath;

    /**
     * @Mongo\Field(type="date")
     */
    protected $lastImport;

    /**
     * @Mongo\Field(type="string")
     */
    protected $importPeriod = 'P1D';

    /**
     * @Mongo\Field(type="int")
     */
    protected $importVersion = 0;

    /**
     * @Mongo\Field(type="string")
     */
    protected $parserClass = 'BaseParser';

    public function __construct()
    {
        $this->lastImport = new \DateTime('2014-01-01');
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getNameExpression()
    {
        return $this->nameExpression;
    }

    public function setNameExpression($nameExpression)
    {
        $this->nameExpression = $nameExpression;

        return $this;
    }

    public function getOldPriceXPath()
    {
        return $this->oldPriceXPath;
    }

    public function setOldPriceXPath($oldPriceXPath)
    {
        $this->oldPriceXPath = $oldPriceXPath;
        return $this;
    }

    public function getLastImport()
    {
        return $this->lastImport;
    }

    public function setLastImport($lastImport)
    {
        $this->lastImport = $lastImport;

        return $this;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getIdXPath()
    {
        return $this->idXPath;
    }

    public function setIdXPath($xPath)
    {
        $this->idXPath = $xPath;
        return $this;
    }

    public function getImportPeriod()
    {
        return $this->importPeriod;
    }

    public function setImportPeriod($importPeriod)
    {
        $this->importPeriod = $importPeriod;
        return $this;
    }

    public function getNextImportDate()
    {
        if (!$this->lastImport) {
            return new \DateTime('-1 day');
        }
        $date = clone $this->lastImport;
        $date->add(new \DateInterval($this->importPeriod));
        return $date;
    }

    public function needImport()
    {
        return new \DateTime() > $this->getNextImportDate();
    }

    public function isForce()
    {
        return $this->force;
    }

    public function setForce($force)
    {
        $this->force = $force;
        return $this;
    }

    public function getImportVersion()
    {
        return $this->importVersion;
    }

    public function setImportVersion($importVersion)
    {
        $this->importVersion = $importVersion;
        return $this;
    }

    public function getParserClass()
    {
        return $this->parserClass;
    }

    public function setParserClass($parserClass)
    {
        $this->parserClass = $parserClass;
        return $this;
    }

    public function incImportVersion($inc = 1)
    {
        $this->importVersion += $inc;
        return $this;
    }

    public function isDelta()
    {
        return $this->delta;
    }

    public function setDelta($delta)
    {
        $this->delta = !!$delta;
        return $this;
    }
}
