<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="admitad_daily_stats", repositoryClass="Footgears\MainBundle\Document\Repository\AdmitadDailyStatRepository")
 */
class AdmitadDailyStat 
{
    /**
     * @Mongo\Id
     */
    protected $id;

    /**
     * @var \DateTime $date
     * @Mongo\Field(type="date")
     */
    protected $date;

    /**
     * @var int $clicks
     * @Mongo\Field(type="integer")
     */
    protected $clicks;

    /**
     * @var float $ctr
     * @Mongo\Field(type="float")
     */
    protected $ctr;

    /**
     * @var float $ecpc
     * @Mongo\Field(type="float")
     */
    protected $ecpc;

    /**
     * @var float $cr
     * @Mongo\Field(type="float")
     */
    protected $cr;

    /**
     * @var float $acpm
     * @Mongo\Field(type="float")
     */
    protected $acpm;

    /**
     * @var float $paymentSumDeclined
     * @Mongo\Field(type="float")
     */
    protected $paymentSumDeclined;

    /**
     * @var float $paymentSumApproved
     * @Mongo\Field(type="float")
     */
    protected $paymentSumApproved;

    /**
     * @var float $paymentSumOpen
     * @Mongo\Field(type="float")
     */
    protected $paymentSumOpen;

    /**
     * @var int $salesSum
     * @Mongo\Field(type="integer")
     */
    protected $salesSum;

    /**
     * @var int $salesSumApproved
     * @Mongo\Field(type="integer")
     */
    protected $salesSumApproved;

    /**
     * @var int $salesSumOpen
     * @Mongo\Field(type="integer")
     */
    protected $salesSumOpen;

    /**
     * @var int $salesSumDeclined
     * @Mongo\Field(type="integer")
     */
    protected $salesSumDeclined;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function getClicks()
    {
        return $this->clicks;
    }

    public function setClicks($clicks)
    {
        $this->clicks = $clicks;
        return $this;
    }

    public function getCtr()
    {
        return $this->ctr;
    }

    public function setCtr($ctr)
    {
        $this->ctr = $ctr;
        return $this;
    }

    public function getEcpc()
    {
        return $this->ecpc;
    }

    public function setEcpc($ecpc)
    {
        $this->ecpc = $ecpc;
        return $this;
    }

    public function getCr()
    {
        return $this->cr;
    }

    public function setCr($cr)
    {
        $this->cr = $cr;
        return $this;
    }

    public function getAcpm()
    {
        return $this->acpm;
    }

    public function setAcpm($acpm)
    {
        $this->acpm = $acpm;
        return $this;
    }

    public function getPaymentSumDeclined()
    {
        return $this->paymentSumDeclined;
    }

    public function setPaymentSumDeclined($paymentSumDeclined)
    {
        $this->paymentSumDeclined = $paymentSumDeclined;
        return $this;
    }

    public function getPaymentSumApproved()
    {
        return $this->paymentSumApproved;
    }

    public function setPaymentSumApproved($paymentSumApproved)
    {
        $this->paymentSumApproved = $paymentSumApproved;
        return $this;
    }

    public function getPaymentSumOpen()
    {
        return $this->paymentSumOpen;
    }

    public function setPaymentSumOpen($paymentSumOpen)
    {
        $this->paymentSumOpen = $paymentSumOpen;
        return $this;
    }

    public function getSalesSum()
    {
        return $this->salesSum;
    }

    public function setSalesSum($salesSum)
    {
        $this->salesSum = $salesSum;
        return $this;
    }

    public function getSalesSumApproved()
    {
        return $this->salesSumApproved;
    }

    public function setSalesSumApproved($salesSumApproved)
    {
        $this->salesSumApproved = $salesSumApproved;
        return $this;
    }

    public function getSalesSumOpen()
    {
        return $this->salesSumOpen;
    }

    public function setSalesSumOpen($salesSumOpen)
    {
        $this->salesSumOpen = $salesSumOpen;
        return $this;
    }

    public function getSalesSumDeclined()
    {
        return $this->salesSumDeclined;
    }

    public function setSalesSumDeclined($salesSumDeclined)
    {
        $this->salesSumDeclined = $salesSumDeclined;
        return $this;
    }
}