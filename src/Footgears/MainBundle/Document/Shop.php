<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Sirian\StorageBundle\Document\FileEmbed;

/**
 * @Mongo\Document(collection="shops", repositoryClass="Footgears\MainBundle\Document\Repository\ShopRepository")
 */
class Shop
{
    const PARTNER_ADMITAD = 'admitad';

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_BLOCKED = 'blocked';

    const STATUSES = [
        self::STATUS_PENDING => 'в обработке',
        self::STATUS_APPROVED => 'утвержден',
        self::STATUS_BLOCKED => 'заблокирован'
    ];

    const REDIRECT_URL = 'url';
    const REDIRECT_DEEPLINK = 'deeplink';

    const REDIRECT_TYPES = [
        self::REDIRECT_URL => 'По URL',
        self::REDIRECT_DEEPLINK => 'По {ulp}'
    ];

    /**
     * @Mongo\Id(strategy="INCREMENT", type="int")
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $partner = self::PARTNER_ADMITAD;

    /**
     * @Mongo\Field(type="int")
     */
    protected $originalId;

    /**
     * @Mongo\Field(type="string")
     */
    protected $status = self::STATUS_PENDING;

    /**
     * @Mongo\Field(type="bool")
     */
    protected $active = false;

    /**
     * @Mongo\Field(type="string")
     */
    protected $systemComment;

    /**
     * @Mongo\EmbedOne(targetDocument="ImportProductsSettings")
     * @var ImportProductsSettings
     */
    protected $importProductsSettings;

    /**
     * @Mongo\Field(type="string")
     */
    protected $name;

    /**
     * @Mongo\Field(type="string")
     */
    protected $letter;

    /**
     * @Mongo\Field(type="string")
     */
    protected $alias;

    /**
     * @Mongo\Field(type="string")
     */
    protected $url;

    /**
     * @Mongo\Field(type="string")
     */
    protected $redirectType = self::REDIRECT_URL;

    /**
     * Количество товаров всего
     *
     * @Mongo\Field(type="int")
     */
    protected $productsCount;

    /**
     * Количество товаров в индексе
     *
     * @Mongo\Field(type="int")
     */
    protected $productsCountFromIndex = 0;

    /**
     * @var FileEmbed
     * @Mongo\EmbedOne(targetDocument="Sirian\StorageBundle\Document\FileEmbed")
     */
    protected $logo;

    /**
     * @Mongo\Field(type="int")
     */
    private $priority = 0;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $seo;

    public function __construct()
    {
        $this->importProductsSettings = new ImportProductsSettings();
        $this->seo = new Seo();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $name = (string)$name;
        $this->name = $name;
        $this->letter = strtolower(mb_substr($name, 0, 1));
        return $this;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function getOriginalId()
    {
        return $this->originalId;
    }

    public function setOriginalId($originalId)
    {
        $this->originalId = $originalId;
        return $this;
    }

    public function getLetter()
    {
        return $this->letter;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusName()
    {
        if (self::STATUSES[$this->status] === null) {
            throw new \LogicException();
        }

        return self::STATUSES[$this->status];
    }

    public function setStatus($status)
    {
        $this->status = $status;
        $this->computeActive();
        return $this;
    }

    public function isActive()
    {
        return $this->active;
    }

    public function computeActive()
    {
        $this->active = true;

        if ($this->status !== self::STATUS_APPROVED) {
            $this->active = false;
        }

        $importSettings = $this->importProductsSettings;

        if (!$importSettings) {
            return $this;
        }

        if (!$importSettings->isEnabled()) {
            $this->active = false;
        }

        if (!$importSettings->getUrl()) {
            $this->active = false;
            $this->systemComment = 'Нет товарного фида';
        }

        return $this;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    public function setRedirectType($redirectType)
    {
        $this->redirectType = $redirectType;
        return $this;
    }

    public function getRedirectType()
    {
        return $this->redirectType;
    }

    public function getSystemComment()
    {
        return $this->systemComment;
    }

    public function setSystemComment($systemComment)
    {
        $this->systemComment = $systemComment;
        return $this;
    }

    public function getProductsCount()
    {
        return $this->productsCount;
    }

    public function setProductsCount($productsCount)
    {
        $this->productsCount = $productsCount;
        return $this;
    }

    public function getProductsCountFromIndex()
    {
        return $this->productsCountFromIndex;
    }

    public function setProductsCountFromIndex($productsCountFromIndex)
    {
        $this->productsCountFromIndex = $productsCountFromIndex;
        return $this;
    }

    public function getImportProductsSettings()
    {
        return $this->importProductsSettings;
    }

    public function setImportProductsSettings(ImportProductsSettings $importProductsSettings)
    {
        $this->importProductsSettings = $importProductsSettings;
        $this->computeActive();
        return $this;
    }

    public function getSeo()
    {
        return $this->seo;
    }

    public function setSeo($seo)
    {
        $this->seo = $seo;
        return $this;
    }

    public function deactivate($string)
    {
        $this->stat = self::STATUS_BLOCKED;
        $this->active = false;
        $this->systemComment = $string;
        return $this;
    }
}
