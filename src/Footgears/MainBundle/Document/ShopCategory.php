<?php

namespace Footgears\MainBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Gedmo\Mapping\Annotation as Gedmo;
use NestedTreeExtension\Traits\NestedSetDocument;

/**
 * @Mongo\Document(collection="shop_categories", repositoryClass="Footgears\MainBundle\Document\Repository\ShopCategoryRepository")
 * @Mongo\HasLifecycleCallbacks
 * @Gedmo\Tree(type="nested")
 */
class ShopCategory
{
    use NestedSetDocument;

    /**
     * @Mongo\Id(strategy="NONE", type="string")
     */
    protected $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $originalId = 0;

    /**
     * @var Shop $shop
     * @Mongo\ReferenceOne(targetDocument="Shop", storeAs="id", orphanRemoval="true")
     */
    protected $shop;

    /**
     * @var Category $category
     * @Mongo\ReferenceOne(targetDocument="Category", storeAs="id", inversedBy="shopCategories")
     */
    protected $category;

    /**
     * @Mongo\Field(type="string")
     */
    protected $name;

    /**
     * @var ShopCategory
     * @Mongo\ReferenceOne(targetDocument="ShopCategory", inversedBy="children", storeAs="id")
     * @Gedmo\TreeParent
     */
    protected $parent;

    /**
     * @Mongo\Field(type="int")
     * @Gedmo\TreeRoot
     */
    protected $root;

    /**
     * @var \DateTime
     * @Mongo\Field(type="date")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Mongo\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @Mongo\Field(type="bool")
     */
    protected $enabled = true;

    /**
     * @Mongo\Field(type="bool")
     */
    protected $handled = false;

    /**
     * @Mongo\Field(type="int")
     */
    protected $productsCount = 0;

    /**
     * @Mongo\Field(type="int")
     */
    protected $productsCountFromXML = 0;

    /**
     * @var ShopCategory[]|ArrayCollection
     * @Mongo\ReferenceMany(targetDocument="ShopCategory", mappedBy="parent", orphanRemoval="true", sort={"left"="asc"})
     */
    protected $children;

    public static function generateId($originalId, $shopId)
    {
        return trim(sprintf('%s_%s', $shopId, $originalId), '_');
    }

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->children = new ArrayCollection();
    }

    public function getPathName()
    {
        if ($this->parent) {
            return $this->parent->getPathName() . ' / ' .$this->name;
        }
        return $this->name;
    }

    public function isEnabledTree()
    {
        if (!$this->enabled) {
            return false;
        }

        if ($this->parent) {
            return $this->parent->isEnabledTree();
        }

        return true;
    }

    public function getRealCategory()
    {
        if ($this->category && $this->isEnabledTree()) {
            return $this->category;
        }

        if ($this->parent) {
            return $this->parent->getRealCategory();
        }

        return null;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(ShopCategory $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getOriginalId()
    {
        return $this->originalId;
    }

    public function setOriginalId($originalId)
    {
        $this->originalId = $originalId;
        return $this;
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function setShop(Shop $shop)
    {
        $this->shop = $shop;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category = null)
    {
        $this->category = $category;
        return $this;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isHandled()
    {
        return $this->handled;
    }

    public function setHandled($handled)
    {
        $this->handled = $handled;
        return $this;
    }

    public function getProductsCount()
    {
        return $this->productsCount;
    }

    public function setProductsCount($productsCount)
    {
        $this->productsCount = $productsCount;
        return $this;
    }

    public function incProductsCount($inc = 1)
    {
        $this->productsCount += (int)$inc;
    }

    public function getProductsCountFromXML()
    {
        return $this->productsCountFromXML;
    }

    public function setProductsCountFromXML($productsCountFromXML)
    {
        $this->productsCountFromXML = $productsCountFromXML;
        return $this;
    }

    public function incProductsCountFromXM($inc = 1)
    {
        $this->productsCountFromXML += (int)$inc;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setChildren($children)
    {
        /** @var ShopCategory[] $children */
        foreach ($children as $child) {
            $child->setParent($this);
        }

        $this->children = $children;
        return $this;
    }

    public function addChildren(ShopCategory $shopCategory)
    {
        $children = new ArrayCollection((array)$this->children);
        $children->add($shopCategory);

        $this->children = $children;
        return $this;
    }

    /**
     * @Mongo\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot($root)
    {
        $this->root = $root;
        return $this;
    }
}
