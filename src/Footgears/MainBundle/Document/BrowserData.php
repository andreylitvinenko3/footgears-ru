<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Footgears\MainBundle\Http\UAParser;

/**
 * @Mongo\EmbeddedDocument
 */
class BrowserData
{
    const DEVICE_TYPE_MOBILE = 'mobile';
    const DEVICE_TYPE_TABLET = 'tablet';
    const DEVICE_TYPE_DESKTOP = 'desktop';

    const QUICK_DEVICE_WINDOWS_TABLET = 'Windows:tablet';
    const QUICK_DEVICE_WINDOWS_MOBILE = 'Windows:mobile';
    const QUICK_DEVICE_WINDOWS_DESKTOP = 'Windows:desktop';
    const QUICK_DEVICE_IPAD = 'iPad:tablet';
    const QUICK_DEVICE_IPHONE = 'iPhone:mobile';
    const QUICK_DEVICE_ANDROID_MOBILE = 'Android:mobile';
    const QUICK_DEVICE_ANDROID_TABLET = 'Android:tablet';
    const QUICK_DEVICE_APPLE_DESKTOP = 'Apple:desktop';
    const QUICK_DEVICE_LINUX_DESKTOP = 'Linux:desktop';

    protected static $devices = [
        self::QUICK_DEVICE_WINDOWS_TABLET => 'Windows (tablet)',
        self::QUICK_DEVICE_WINDOWS_MOBILE => 'Windows (mobile)',
        self::QUICK_DEVICE_WINDOWS_DESKTOP => 'Windows (desktop)',
        self::QUICK_DEVICE_IPAD => 'iPad (tablet)',
        self::QUICK_DEVICE_IPHONE => 'iPhone (mobile)',
        self::QUICK_DEVICE_ANDROID_MOBILE => 'Android (mobile)',
        self::QUICK_DEVICE_ANDROID_TABLET => 'Android (tablet)',
        self::QUICK_DEVICE_APPLE_DESKTOP => 'Mac (desktop)',
        self::QUICK_DEVICE_LINUX_DESKTOP => 'Linux (desktop)',
    ];

    protected static $deviceTypes = [
        self::DEVICE_TYPE_DESKTOP => 'Desktop',
        self::DEVICE_TYPE_MOBILE => 'Mobile',
        self::DEVICE_TYPE_TABLET => 'Tablet',
    ];

    /**
     * @Mongo\Field(type="string")
     */
    protected $userAgent;

    /**
     * @Mongo\Field(type="string")
     */
    protected $browserName = '';

    /**
     * @Mongo\Field(type="int")
     */
    protected $browserMajor = 0;

    /**
     * @Mongo\Field(type="string")
     */
    protected $browserVersion = null;

    /**
     * @Mongo\Field(type="string")
     */
    protected $engineName;

    /**
     * @Mongo\Field(type="string")
     */
    protected $engineVersion;

    /**
     * @Mongo\Field(type="string")
     */
    protected $osName = '';

    /**
     * @Mongo\Field(type="string")
     */
    protected $osVersion = null;

    /**
     * @Mongo\Field(type="string")
     */
    protected $deviceVendor = '';

    /**
     * @Mongo\Field(type="string")
     */
    protected $deviceModel = '';

    /**
     * @Mongo\Field(type="string")
     */
    protected $deviceType;

    private static function normalizeOSVersion($name, $version)
    {
        switch ($name) {
            case 'iOS':
            case 'Ubuntu':
            case 'Windows Phone':
            case 'Chromium OS':
            case 'Android':
                return self::getMajor($version);
            case 'Mac OS':
                return implode('.', array_slice(explode('.', $version), 0, 2));
                break;
        }
        return $version;
    }

    protected static function getMajor($version)
    {
        return intval(explode('.', $version)[0]);
    }

    public static function create($userAgent)
    {
        $parsed = UAParser::parse($userAgent);

        $item = new BrowserData();
        $item->userAgent = $userAgent;

        $item->browserMajor = (int)$parsed['browser']['major'];
        $item->browserName = $parsed['browser']['name'];
        $item->browserVersion = $parsed['browser']['version'];

        $item->engineName = $parsed['engine']['name'];
        $item->engineVersion = $parsed['engine']['version'];

        $item->osName = $parsed['os']['name'];
        $item->osVersion = (string)self::normalizeOSVersion($parsed['os']['name'], $parsed['os']['version']);

        $item->deviceModel = $parsed['device']['model'] ?: 'Desktop';
        $item->deviceVendor = $parsed['device']['vendor'] ?: 'Desktop';
        $item->deviceType = $parsed['device']['type'] ?: 'desktop';


        return $item;
    }

    public static function getDevices()
    {
        return self::$devices;
    }

    public function getDevice()
    {
        $deviceType = $this->getDeviceType();

        if ('Mac OS' == $this->osName) {
            return self::QUICK_DEVICE_APPLE_DESKTOP;
        }

        if ('Windows' == $this->osName || 'Windows Phone' == $this->osName) {
            switch ($deviceType) {
                case self::DEVICE_TYPE_TABLET:
                    return self::QUICK_DEVICE_WINDOWS_TABLET;
                case self::DEVICE_TYPE_MOBILE:
                    return self::QUICK_DEVICE_WINDOWS_MOBILE;
                case self::DEVICE_TYPE_DESKTOP:
                    return self::QUICK_DEVICE_WINDOWS_DESKTOP;
            }
        }

        if ('iPhone' == $this->deviceModel) {
            return self::QUICK_DEVICE_IPHONE;
        }

        if ('iPad' == $this->deviceModel) {
            return self::QUICK_DEVICE_IPAD;
        }

        if ('Android' == $this->osName) {
            switch ($deviceType) {
                case self::DEVICE_TYPE_TABLET:
                    return self::QUICK_DEVICE_ANDROID_TABLET;
                case self::DEVICE_TYPE_MOBILE:
                    return self::QUICK_DEVICE_ANDROID_MOBILE;
            }
        }

        return null;
    }

    public function getUserAgent()
    {
        return $this->userAgent;
    }

    public function isMobile()
    {
        return UAParser::MOBILE === $this->getDeviceType();
    }

    public function getRaw()
    {
        return [
            'userAgent' => $this->getUserAgent(),
            'browserName' => $this->getBrowserName(),
            'browserMajor' => $this->getBrowserMajor(),
            'browserVersion' => $this->getBrowserVersion(),
            'engineName' => $this->getEngineName(),
            'engineVersion' => $this->getEngineVersion(),
            'osName' => $this->getOSName(),
            'osVersion' => $this->getOSVersion(),
            'deviceVendor' => $this->getDeviceVendor(),
            'deviceModel' => $this->getDeviceModel(),
            'deviceType' => $this->getDeviceType(),
        ];
    }

    public function isTablet()
    {
        return UAParser::TABLET === $this->getDeviceType();
    }


    public function getDeviceType()
    {
        return $this->deviceType ?: self::DEVICE_TYPE_DESKTOP;
    }

    public static function getDeviceTypes()
    {
        return self::$deviceTypes;
    }

    public function isDesktop()
    {
        return !$this->isTablet() && !$this->isMobile();
    }

    public function getBrowserName()
    {
        return $this->browserName;
    }

    public function getBrowserMajor()
    {
        return $this->browserMajor;
    }

    public function getBrowserVersion()
    {
        return $this->browserVersion;
    }

    public function getEngineName()
    {
        return $this->engineName;
    }

    public function getEngineVersion()
    {
        return $this->engineVersion;
    }

    public function getOsName()
    {
        return $this->osName;
    }

    public function getOsVersion()
    {
        return $this->osVersion;
    }

    public function getDeviceVendor()
    {
        return $this->deviceVendor;
    }

    public function getDeviceModel()
    {
        return $this->deviceModel;
    }

    public function getBrowserId()
    {
        return Browser::generateId($this->browserName, $this->browserMajor);
    }

    public function getOsId()
    {
        return OS::generateId($this->osName, $this->osVersion);
    }
}
