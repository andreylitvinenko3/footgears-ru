<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="clicks", repositoryClass="Footgears\MainBundle\Document\Repository\ClickRepository")
 */
class Click
{
    /**
     * @Mongo\Id(strategy="NONE")
     */
    protected $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $uid;

    /**
     * @var bool
     * @Mongo\Field(type="bool")
     */
    protected $duplicate = false;

    /**
     * @var \DateTime
     * @Mongo\Field(type="date")
     */
    protected $clickDate;

    /**
     * @Mongo\Field(type="string")
     */
    protected $ip;

    /**
     * @var BrowserData
     * @Mongo\EmbedOne(targetDocument="BrowserData")
     */
    protected $browser;

    /**
     * @var Shop
     * @Mongo\ReferenceOne(targetDocument="Shop", simple=true)
     */
    protected $shop;

    /**
     * @var Category
     * @Mongo\ReferenceOne(targetDocument="Category", simple=true)
     */
    protected $category;

    /**
     * @var Product
     * @Mongo\ReferenceOne(targetDocument="Product", simple=true)
     */
    protected $product;

    /**
     * @Mongo\Field(type="string")
     */
    protected $sourceUrl;

    /**
     * @var bool
     * @Mongo\Field(type="bool")
     */
    protected $valid;
}
