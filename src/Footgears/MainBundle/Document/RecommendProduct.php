<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="recommend_products", repositoryClass="Footgears\MainBundle\Document\Repository\RecommendProductRepository")
 */
class RecommendProduct
{
    /**
     * @Mongo\Id(strategy="NONE")
     */
    protected $id;

    /**
     * @var Category $category
     * @Mongo\ReferenceOne(targetDocument="Category", simple=true)
     */
    protected $category;

    /**
     * @var Product $product
     * @Mongo\ReferenceOne(targetDocument="Product", simple=true)
     */
    protected $product;

    /**
     * @Mongo\Field(type="int")
     */
    private $position;

    public static function generateId($categoryId, $position)
    {
        return sprintf('%d:%d', $categoryId, $position);
    }

    private function updateId()
    {
        if (!$this->category || !$this->position) {
            return;
        }

        $this->id = self::generateId($this->category->getId(), $this->position);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
        $this->updateId();
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
        $this->updateId();
        return $this;
    }
}
