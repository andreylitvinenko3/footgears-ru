<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;
use Sirian\StorageBundle\Document\FileEmbed;

/**
 * @Mongo\Document(collection="brands", repositoryClass="Footgears\MainBundle\Document\Repository\BrandRepository")
 */
class Brand
{
    /**
     * @Mongo\Id(strategy="INCREMENT", type="int")
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    private $name;

    /**
     * @Mongo\Field(type="string")
     */
    private $alias;

    /**
     * @Mongo\Field(type="string")
     */
    private $nameLocal;

    /**
     * @Mongo\Field(type="string")
     */
    protected $letter;

    /**
     * @Mongo\Field(type="collection")
     */
    private $searchTags = [];

    /**
     * @Mongo\Field(type="int")
     */
    private $productsCount = 0;

    /**
     * @var FileEmbed
     * @Mongo\EmbedOne(targetDocument="Sirian\StorageBundle\Document\FileEmbed")
     */
    protected $logo;

    /**
     * @var bool
     * @Mongo\Field(type="bool")
     */
    protected $enabled = true;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $seo;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $saleSeo;

    public function __construct()
    {
        $this->seo = new Seo();
        $this->saleSeo = new Seo();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $name = (string)$name;
        $this->name = $name;
        $this->letter = mb_strtolower(mb_substr($name, 0, 1));
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSearchTags()
    {
        return $this->searchTags;
    }

    public function setSearchTags($searchTags)
    {
        $this->searchTags = $searchTags;
        return $this;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function getLetter()
    {
        return $this->letter;
    }

    public function getProductsCount()
    {
        return $this->productsCount;
    }

    public function setProductsCount($productsCount)
    {
        $this->productsCount = $productsCount;
        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    public function setNameLocal($nameLocal)
    {
        $this->nameLocal = $nameLocal;
        return $this;
    }

    public function getNameLocal()
    {
        return $this->nameLocal;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function getSeo()
    {
        return $this->seo;
    }

    public function setSeo($seo)
    {
        $this->seo = $seo;
        return $this;
    }

    public function getSaleSeo()
    {
        return $this->saleSeo;
    }

    public function setSaleSeo($saleSeo)
    {
        $this->saleSeo = $saleSeo;
        return $this;
    }
}
