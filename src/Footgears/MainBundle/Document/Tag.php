<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="tags", repositoryClass="Footgears\MainBundle\Document\Repository\TagRepository")
 */
class Tag
{
    const TYPE_POPULAR = 'popular';
    const TYPE_POPULAR_TOP = 'popular_top';
    const TYPE_FILTER = 'filter';

    const TYPES = [
//        self::POPULAR => 'Популярные',
        self::TYPE_POPULAR_TOP => 'Популярные вверху',
        self::TYPE_FILTER => 'В фильтрах',
    ];

    const SEARCH_KEYS_OPERATOR_AND = 'and';
    const SEARCH_KEYS_OPERATOR_OR = 'or';

    const SEARCH_KEYS_OPERATORS = [
        self::SEARCH_KEYS_OPERATOR_AND => self::SEARCH_KEYS_OPERATOR_AND,
        self::SEARCH_KEYS_OPERATOR_OR => self::SEARCH_KEYS_OPERATOR_OR
    ];

    /**
     * @Mongo\Id(strategy="NONE", type="string")
     */
    protected $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $alias;

    /**
     * @var Category
     * @Mongo\ReferenceOne(targetDocument="Category", storeAs="id")
     */
    protected $category;

    /**
     * @Mongo\Field(type="collection")
     */
    protected $attributes = [];

    /**
     * @Mongo\Field(type="string")
     */
    protected $searchPhrase;

    /**
     * @Mongo\Field(type="string")
     */
    protected $searchKeysOperator = self::SEARCH_KEYS_OPERATOR_AND;

    /**
     * @Mongo\Field(type="bool")
     */
    protected $active = false;

    /**
     * @Mongo\Field(type="string")
     */
    protected $type = self::TYPE_POPULAR;

    /**
     * @Mongo\Field(type="int")
     */
    protected $productsCount = 0;

    /**
     * @var \DateTime
     * @Mongo\Field(type="date")
     */
    protected $productsCountUpdated;

    /**
     * @var Seo
     * @Mongo\EmbedOne(targetDocument="Seo")
     */
    protected $seo;

    public function __construct()
    {
        $this->seo = new Seo();
    }

    public static function generateId($categoryId, $alias)
    {
        return sprintf('%s_%s', $categoryId, $alias);
    }

    protected function updateId()
    {
        if (!$this->category || !$this->alias) {
            return;
        }

        $this->id = self::generateId($this->category->getId(), $this->alias);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        $this->updateId();
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category)
    {
        $this->category = $category;
        $this->updateId();
        return $this;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function getSearchPhrase()
    {
        return $this->searchPhrase;
    }

    public function setSearchPhrase($searchPhrase)
    {
        $this->searchPhrase = $searchPhrase;
        return $this;
    }

    public function getSearchKeysOperator()
    {
        return $this->searchKeysOperator;
    }

    public function setSearchKeysOperator($searchKeysOperator)
    {
        $this->searchKeysOperator = $searchKeysOperator;
        return $this;
    }

    public function isActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getTypeName()
    {
        return self::TYPES[$this->type];
    }

    public function getProductsCount()
    {
        return $this->productsCount;
    }

    public function setProductsCount($productsCount)
    {
        $this->productsCount = $productsCount;
        return $this;
    }

    public function getProductsCountUpdated()
    {
        return $this->productsCountUpdated;
    }

    public function setProductsCountUpdated(\DateTime $productsCountUpdated)
    {
        $this->productsCountUpdated = $productsCountUpdated;
        return $this;
    }

    public function getSeo()
    {
        return $this->seo;
    }

    public function setSeo(Seo $seo)
    {
        $this->seo = $seo;
        return $this;
    }

    public function getTitle()
    {
        if ($this->seo->getH1()) {
            return $this->seo->getH1();
        }

        return $this->alias;
    }
}
