<?php

namespace Footgears\MainBundle\Document\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\CategoryBrand;
use Footgears\MainBundle\Document\MenuItem;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\ShopCategory;
use Footgears\MainBundle\Document\Tag;

class CategoryListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $category = $args->getDocument();

        if (!$category instanceof Category) {
            return;
        }

        $dm = $args->getDocumentManager();

        $dm
            ->getDocumentCollection(CategoryBrand::class)
            ->remove(['category' => $category->getId()])
        ;

        $dm
            ->getDocumentCollection(Product::class)
            ->update(
                ['category' => $category->getId()],
                ['$set' => [
                    'category' => null
                ]],
                ['multi' => true]
            )
        ;

        $dm
            ->getDocumentCollection(ShopCategory::class)
            ->update(
                ['category' => $category->getId()],
                ['set' => [
                    'handled' => false,
                    'category' => null
                ]],
                ['multi' => true]
            )
        ;

        $dm
            ->getDocumentCollection(Tag::class)
            ->remove(['category' => $category->getId()])
        ;

        $menuItems = $dm->getRepository(MenuItem::class)
            ->findBy(['category' => $category->getId()])
        ;

        foreach ($menuItems as $menuItem) {
            $dm->remove($menuItem);
        }
        $dm->flush();
    }
}
