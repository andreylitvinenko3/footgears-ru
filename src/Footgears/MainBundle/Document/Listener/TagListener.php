<?php

namespace Footgears\MainBundle\Document\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Service\ContainerWrapper;

class TagListener
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->updateProductsCount($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->updateProductsCount($args);
    }

    private function updateProductsCount(LifecycleEventArgs $args)
    {
        $document = $args->getDocument();

        if (!$document instanceof Tag) {
            return;
        }

        $this->container->getContainer()->get('app.tag_service')->updateProductsCount($document);
    }
}
