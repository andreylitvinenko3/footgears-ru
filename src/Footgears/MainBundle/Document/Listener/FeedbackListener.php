<?php

namespace Footgears\MainBundle\Document\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Footgears\MainBundle\Document\Feedback;
use Footgears\MainBundle\Service\ContainerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FeedbackListener
{
    use ContainerTrait;

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $feedback = $args->getDocument();

        if (!$feedback instanceof Feedback) {
            return;
        }

        $this->getMailSender()->send($this->getContainer()->getParameter('admin_email'), 'new_feedback', ['feedback' => $feedback]);
    }
}