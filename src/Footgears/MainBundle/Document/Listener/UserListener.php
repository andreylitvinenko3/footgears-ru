<?php

namespace Footgears\MainBundle\Document\Listener;

use Footgears\MainBundle\Document\User;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Event\PreFlushEventArgs;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class UserListener extends DocumentListener
{
    private $encoderFactory;

    public function __construct(EncoderFactory $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $user = $args->getDocument();
        if ($user instanceof User && $user->getPlainPassword()) {
            $this->updatePassword($user);
        }
    }

    public function preFlush(PreFlushEventArgs $args)
    {
        $uow = $args->getDocumentManager()->getUnitOfWork();
        $identities = $uow->getIdentityMap();

        foreach ($identities as $identity) {
            foreach ($identity as $document) {
                if ($document instanceof User && $document->getPlainPassword()) {
                    $this->updatePassword($document);
                }
            }
        }
    }

    protected function updatePassword(User $user)
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
    }
}
