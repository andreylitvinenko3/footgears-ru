<?php

namespace Footgears\MainBundle\Document\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Footgears\MainBundle\Document\MenuItem;
use Footgears\MainBundle\Service\ContainerWrapper;

class MenuItemListener
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->clearCache($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->clearCache($args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->clearCache($args);
    }

    protected function clearCache(LifecycleEventArgs $args)
    {
        $document = $args->getDocument();

        if (!$document instanceof MenuItem) {
            return;
        }

        $type = $this->container->getDocumentManager()->getRepository(MenuItem::class)->getMenuType($document);

        $this->container->getCache()->deleteItem('menu.' . $type);
    }
}
