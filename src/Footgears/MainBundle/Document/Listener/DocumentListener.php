<?php

namespace Footgears\MainBundle\Document\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;

class DocumentListener
{
    protected function recomputeSingleEntityChangeSet(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getDocument();
        $this->recomputeEntity($eventArgs, $entity);
    }

    protected function recomputeEntity(LifecycleEventArgs $eventArgs, $entity)
    {
        $em = $eventArgs->getDocumentManager();
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($entity));
        $uow->recomputeSingleDocumentChangeSet($meta, $entity);
    }

    protected function computeEntity(LifecycleEventArgs $eventArgs, $entity)
    {
        $em = $eventArgs->getDocumentManager();
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($entity));
        $uow->computeChangeSet($meta, $entity);
    }
}
