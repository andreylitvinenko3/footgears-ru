<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/** @Mongo\EmbeddedDocument */
class FilterPagesInfo
{
    /**
     * @Mongo\Field(type="integer")
     */
    protected $emptySearchKeys = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $emptyManual = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $notApproved = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $emptySeoTags = 0;

    /**
     * @Mongo\Field(type="integer")
     */
    protected $emptyBySearchKeys = 0;

    public function getEmptySearchKeys()
    {
        return $this->emptySearchKeys;
    }

    public function setEmptySearchKeys($emptySearchKeys)
    {
        $this->emptySearchKeys = $emptySearchKeys;
        return $this;
    }

    public function getEmptyManual()
    {
        return $this->emptyManual;
    }

    public function setEmptyManual($emptyManual)
    {
        $this->emptyManual = $emptyManual;
        return $this;
    }

    public function getNotApproved()
    {
        return $this->notApproved;
    }

    public function setNotApproved($notApproved)
    {
        $this->notApproved = $notApproved;
        return $this;
    }

    public function getEmptySeoTags()
    {
        return $this->emptySeoTags;
    }

    public function setEmptySeoTags($emptySeoTags)
    {
        $this->emptySeoTags = $emptySeoTags;
        return $this;
    }

    public function getEmptyBySearchKeys()
    {
        return $this->emptyBySearchKeys;
    }

    public function setEmptyBySearchKeys($emptyBySearchKeys)
    {
        $this->emptyBySearchKeys = $emptyBySearchKeys;
        return $this;
    }
}