<?php

namespace Footgears\MainBundle\Document;

use Doctrine\MongoDB\GridFSFile;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="files")
 */
class File
{
    /**
     * @Mongo\Id
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    private $name;

    /**
     * @var GridFSFile
     * @Mongo\File
     */
    private $file;

    /**
     * @Mongo\Field(type="string")
     */
    private $mimeType;

    /**
     * @var \DateTime
     * @Mongo\Field(type="date")
     */
    private $uploadDate;

    /**
     * @Mongo\Field(type="integer")
     */
    private $length;

    /**
     * @Mongo\Field(type="integer")
     */
    private $chunkSize;

    /**
     * @Mongo\Field(type="string")
     */
    private $md5;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }

    public function getUploadDate()
    {
        return $this->uploadDate;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function getChunkSize()
    {
        return $this->chunkSize;
    }

    public function getMd5()
    {
        return $this->md5;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }
}
