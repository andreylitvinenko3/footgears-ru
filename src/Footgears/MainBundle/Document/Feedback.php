<?php

namespace Footgears\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="feedbacks")
 */
class Feedback
{
    /**
     * @var int $id
     * @Mongo\Id(strategy="INCREMENT")
     */
    protected $id;

    /**
     * @var string $email
     * @Mongo\Field
     */
    protected $email;

    /**
     * @var string $name
     * @Mongo\Field
     */
    protected $name;

    /**
     * @var string $text
     * @Mongo\Field
     */
    protected $text;

    /**
     * @var \DateTime $created
     * @Mongo\Date
     */
    protected $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }
}