<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Filter\TagFilter;

class TagRepository extends DocumentRepository
{
    public function createFilteredQB(TagFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getText()) {
            $this->textSearch($qb->field('seo.h1'), $filter->getText());
        }

        if ($filter->getCategory()) {
            $qb->field('category')->references($filter->getCategory());
        }

        if ($filter->getType()) {
            $qb->field('type')->equals($filter->getType());
        }

        if ($filter->isActive() != -1 && !is_null($filter->isActive())) {
            $qb->field('active')->equals(!!$filter->isActive());
        }

        if ($filter->issetProducts()) {
            $qb->field('productsCount')->gt(0);
        }

        return $qb;
    }

    /**
     * @param Tag $tag
     * @return Tag|null
     */
    public function getSimilarPage(Tag $tag)
    {
        return $this->createQueryBuilder()
            ->field('id')->notEqual($tag->getId())
            ->field('seo.h1')->equals($tag->getSeo()->getH1())
            ->sort(['productsCount' => -1])
            ->limit(1)
            ->getQuery()
            ->getSingleResult()
        ;
    }
}
