<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Document\MenuItem;

class MenuItemRepository extends TreeRepository
{
    public function getRoot($name)
    {
        $res = $this
            ->getRootNodesQueryBuilder()
            ->field('name')->equals($name)
            ->getQuery()
            ->getSingleResult()
        ;

        if (!empty($res)) {
            return $res;
        }

        $root = new MenuItem();
        $root->setName($name);

        $dm = $this->getDocumentManager();
        $dm->persist($root);
        $dm->flush();

        return $root;
    }

    public function createMenuQueryBuilder($menu)
    {
        $root = $this->getRoot($menu);

        return $this
            ->getNodesHierarchyQueryBuilder($root)
        ;
    }

    public function getTree($name)
    {
        $root = $this->getRoot($name);

        $items = $this
            ->getNodesHierarchyQuery($root)
            ->toArray()
        ;

        return $this->buildArrayTree($root, $items);
    }

    public function getMenuType(MenuItem $menuItem)
    {
        if ($menuItem->getLevel() == 0) {
            return $menuItem->getName();
        }

        /* @var MenuItem $root */
        $root = $this->find($menuItem->getRoot());
        if (!$root) {
            throw new \RuntimeException("Root element not found");
        }

        return $root->getName();
    }
}
