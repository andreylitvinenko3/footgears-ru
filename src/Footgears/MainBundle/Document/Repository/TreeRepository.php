<?php

namespace Footgears\MainBundle\Document\Repository;

use NestedTreeExtension\Document\Repository\NestedTreeRepository;

class TreeRepository extends NestedTreeRepository
{
    public function getCollection()
    {
        return $this->dm->getDocumentCollection($this->documentName);
    }

    protected function buildArrayTree($root, array $items)
    {
        $root = [
            'children' => [],
            'parent' => null,
            'level' => $root->getLevel(),
            'item' => $root
        ];
        $currentNode = &$root;

        foreach ($items as $item) {
            while ($item->getLevel() <= $currentNode['level']) {
                $currentNode = &$currentNode['parent'];
            }

            $node = [
                'item' => $item,
                'level' => $item->getLevel(),
                'children' => [],
                'parent' => &$currentNode,
            ];

            $currentNode['children'][] = &$node;

            if ($item->getLevel() > $currentNode['level']) {
                $currentNode = &$node;
            }
            unset($node);
        }

        $root = $this->sort($root);

        return $root;
    }

    protected function sort($node)
    {
        foreach ($node['children'] as $key => $child) {
            $node['children'][$key] = $this->sort($child);
        }

        if (!empty($node['children'])) {
            usort($node['children'], function ($a, $b) {
                $aL = $a['item']->getLeft();
                $bL = $b['item']->getLeft();

                if ($aL === $bL) {
                    return 0;
                }

                return $aL > $bL ? 1 : -1;
            });
        }

        return $node;
    }
}
