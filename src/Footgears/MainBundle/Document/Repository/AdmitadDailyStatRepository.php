<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Document\AdmitadDailyStat;

class AdmitadDailyStatRepository extends DocumentRepository
{
    public function getByDate(\DateTime $date)
    {
        $date->setTime(0, 0, 0);

        $row = $this->findOneBy([
            'date' => $date
        ]);

        if (!$row) {
            $row = new AdmitadDailyStat();
            $row->setDate($date);

            $this->getDocumentManager()->persist($row);
            $this->getDocumentManager()->flush($row);
        }

        return $row;
    }
}