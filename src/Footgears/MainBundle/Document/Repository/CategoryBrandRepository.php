<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Filter\CategoryBrandFilter;

class CategoryBrandRepository extends DocumentRepository
{
    public function createFilteredQB(CategoryBrandFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getCategory()) {
            $qb->field('category')->references($filter->getCategory());
        }

        if ($filter->getBrand()) {
            $qb->field('brand')->references($filter->getBrand());
        }

        return $qb;
    }
}
