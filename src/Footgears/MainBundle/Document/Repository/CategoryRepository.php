<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Document\Category;

class CategoryRepository extends TreeRepository
{
    public function getPreparedTree()
    {
        $root = new Category();
        $root->setLevel(-1);

        $items = $this
            ->getNodesHierarchyQuery()->toArray()
        ;

        return $this->buildArrayTree($root, $items);
    }

    public function getTreeOf(array $items, $mergeFunction)
    {
        usort($items, function (Category $a, Category $b) {
            return $a->getLeft() < $b->getLeft() ? -1 : 1;
        });

        $nodes = [];

        $currentNode = null;

        foreach ($items as $node) {
            $newNode = array_merge([
                'category' => $node,
                'children' => [],
                'level' => $node->getLevel(),
                'parent' => null,
            ], $mergeFunction($node));

            if ($node->getLevel() == 0) {
                $nodes[] = &$newNode;
                $currentNode = &$newNode;
            } else {
                while ($node->getLevel() <= $currentNode['level']) {
                    $currentNode = &$currentNode['parent'];
                }
                $newNode['parent'] = &$currentNode;
                $currentNode['children'][] = &$newNode;
                $currentNode = &$newNode;
            }

            unset($newNode);
        }

        return $nodes;
    }

    /**
     * @param $alias
     * @return Category|null
     */
    public function findOneByAlias($alias)
    {
        return $this->findOneBy(['alias' => $alias]);
    }
}
