<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;

class ShopCategoryRepository extends TreeRepository
{
    public function getRoot(Shop $shop)
    {
        $root = $this
            ->createQueryBuilder()
            ->field('shop')->references($shop)
            ->field('level')->equals(0)
            ->limit(1)
            ->getQuery()
            ->getSingleResult()
        ;

        if (!$root) {
            $root = new ShopCategory();
            $root
                ->setId((string)$shop->getId())
                ->setShop($shop)
                ->setHandled(true)
            ;

            $dm = $this->getDocumentManager();
            $dm->persist($root);
            $dm->flush();
        }

        return $root;
    }
}
