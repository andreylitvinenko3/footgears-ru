<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Filter\BrandFilter;

class BrandRepository extends DocumentRepository
{
    public function createFilteredQB(BrandFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getName()) {
            $this->textSearch($qb->field('name'), $filter->getName());
        }

        return $qb;
    }
}
