<?php
namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Filter\UserFilter;

class UserRepository extends DocumentRepository
{
    public function createFilterQuery(UserFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getUsername()) {
            $this->textSearch($qb->field('username'), $filter->getUsername());
        }

        if ($filter->getEmail()) {
            $this->textSearch($qb->field('email'), $filter->getEmail());
        }

        if ($filter->getVkontakteId()) {
            $qb->field('vkId')->equals($filter->getVkontakteId());
        }

        if ($filter->getRoles()) {
            $qb->field('roles')->in($filter->getRoles());
        }

        return $qb->getQuery();
    }
}