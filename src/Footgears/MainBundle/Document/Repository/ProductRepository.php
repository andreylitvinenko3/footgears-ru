<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Document\Shop;

class ProductRepository extends DocumentRepository
{
    public function findByIds($ids)
    {
        if (!$ids) {
            return [];
        }

        $products = $this->findBy(['_id' => ['$in' => $ids]]);

        return $this->sortByIds($products, $ids);
    }

    public function getProductsCount()
    {
        $result = $this->getCollection()->aggregate([
            ['$group' => [
                '_id' => '$enabled',
                'cnt' => ['$sum' => 1]
            ]]
        ])->toArray();

        $counts = [
            'active' => 0,
            'disabled' => 0
        ];

        foreach ($result as $row) {
            $counts[$row['_id'] ? 'active' : 'disabled'] = $row['cnt'];
        }

        return $counts;
    }

    public function countInShop(Shop $shop)
    {
        $result = $this
            ->getCollection()
            ->count(['shop' => $shop->getId()])
        ;

        return $result;
    }
}
