<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Filter\ShopFilter;

class ShopRepository extends DocumentRepository
{
    public function createFilteredQB(ShopFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getName()) {
            $this->textSearch($qb->field('name'), $filter->getName());
        }

        if ($filter->getStatus()) {
            $qb->field('status')->equals($filter->getStatus());
        }

        return $qb;
    }
}
