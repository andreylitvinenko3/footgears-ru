<?php

namespace Footgears\MainBundle\Document\Repository;

use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\RecommendProduct;

class RecommendProductRepository extends DocumentRepository
{
    public function getPositions(Category $category)
    {
        /** @var RecommendProduct[] $recommendProducts */
        $recommendProducts = $this->findBy(['category' => $category->getId()]);

        $positions = [];
        foreach ($recommendProducts as $recommendProduct) {
            $positions[] = $recommendProduct->getPosition();
        }

        if (count($positions) > Category::RECOMMEND_PRODUCT_COUNT) {
            $positions = array_slice($positions, 0, Category::RECOMMEND_PRODUCT_COUNT);
        } elseif (count($positions) < Category::RECOMMEND_PRODUCT_COUNT) {
            $limit = Category::RECOMMEND_PRODUCT_COUNT - count($positions);
            for ($i = 0; $i < $limit; $i++) {
                while (true) {
                    $id = mt_rand(1000, 99999);
                    if (!in_array($id, $positions)) {
                        $positions[] = $id;
                        break;
                    }
                }
            }
        }

        return $positions;
    }
}
