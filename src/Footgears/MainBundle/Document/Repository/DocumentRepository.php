<?php

namespace Footgears\MainBundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository as BaseDocumentRepository;
use Doctrine\MongoDB\Query\Builder;

class DocumentRepository extends BaseDocumentRepository
{
    public function getCollection()
    {
        return $this->dm->getDocumentCollection($this->documentName);
    }

    protected function textSearch(Builder $builder, $text)
    {
        $builder->equals(new \MongoRegex('/' . preg_quote($text) . '/i'));
    }

    protected function sortByIds($items, $ids)
    {
        $assoc = [];
        foreach ($items as $item) {
            $assoc[$item->getId()] = $item;
        }

        $result = [];
        foreach ($ids as $id) {
            if (isset($assoc[$id])) {
                $result[] = $assoc[$id];
            }
        }

        return $result;
    }
}
