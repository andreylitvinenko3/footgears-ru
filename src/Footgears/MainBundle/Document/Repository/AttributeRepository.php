<?php

namespace Footgears\MainBundle\Document\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Footgears\MainBundle\Document\Attribute;

class AttributeRepository extends DocumentRepository
{
    public function getMaxId()
    {
        $max = $this
            ->createQueryBuilder()
            ->sort('id', -1)
            ->limit(1)
            ->getQuery()
            ->getSingleResult()
        ;

        if ($max) {
            return $max->getId();
        } else {
            return 0;
        }
    }

    /**
     * @return Attribute[]|ArrayCollection
     */
    public function all()
    {
        /** @var Attribute[] $rows */
        $rows = $this->findAll();
        $attributes = new ArrayCollection();

        foreach ($rows as $attribute) {
            $attributes->set($attribute->getId(), $attribute);
        }

        return $attributes;
    }
}
