<?php

namespace Footgears\MainBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('main');

        $rootNode
            ->children()
            ->scalarNode('project_name')->defaultValue('Default')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
