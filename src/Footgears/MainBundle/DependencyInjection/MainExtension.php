<?php

namespace Footgears\MainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class MainExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $this->loadSeoSettingsFile($container, $config);

        $definition = new Definition(sprintf('Footgears\MainBundle\Attribute\%sManager', $config['project_name']));
        $container->setDefinition('footgears.attributes.manager', $definition);
    }

    protected function loadSeoSettingsFile(ContainerBuilder $container, $config)
    {
        $seoSettingsFile = $container->getParameter('kernel.project_dir') . '/app/config/seo/' . strtolower($config['project_name']). '.yml';

        if (file_exists($seoSettingsFile)) {
            $loader = new Loader\YamlFileLoader($container, new FileLocator($container->getParameter('kernel.project_dir') . '/app/config/seo/'));
            $loader->load(strtolower($config['project_name']). '.yml');
        } else {
            throw new \Exception(sprintf('File "%s" is not found', $seoSettingsFile));
        }
    }
}
