<?php

namespace Footgears\MainBundle\Elastic\Logger;

class ElasticLogger
{
    protected $queries;

    public $debug;

    public function __construct($debug = false)
    {
        $this->debug = $debug;
        $this->queries = [];
    }

    public function logQuery($path, $method, $body, $time, $result, $queryCache)
    {
        if ($this->debug) {
            $this->queries[] = array(
                'path' => $path,
                'method' => $method,
                'body' => $body,
                'result' => $result,
                'executionMS' => $time,
                'cache' => $queryCache
            );
        }
    }

    public function getNbQueries()
    {
        return count($this->queries);
    }

    public function getQueries()
    {
        return $this->queries;
    }
}
