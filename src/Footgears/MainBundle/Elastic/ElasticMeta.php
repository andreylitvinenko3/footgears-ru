<?php

namespace Footgears\MainBundle\Elastic;

class ElasticMeta
{
    const TYPE_PRODUCT = 'product';

    const EVENT_NAME = 'elastic';

    const QUERY_CACHE_MISS = 'miss';
    const QUERY_CACHE_HIT = 'hit';
    const QUERY_CACHE_PUT = 'put';

    const CACHE_TAG_SEARCH_RESULT = 'search_result';
}
