<?php

namespace Footgears\MainBundle\Elastic\Search;

use Footgears\MainBundle\Search\SearchResultInterface;

class SearchResult implements SearchResultInterface
{
    protected $total;

    protected $maxScore;

    protected $hits = [];

    protected $ids = [];

    protected $aggregations = [];

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    public function setMaxScore($maxScore)
    {
        $this->maxScore = $maxScore;
        return $this;
    }

    public function setHits($hits)
    {
        $this->hits = $hits;
        $this->ids = array_map(function ($hit) {
            return $hit['_id'];
        }, $this->hits);

        return $this;
    }

    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getMaxScore()
    {
        return $this->maxScore;
    }

    public function getHits()
    {
        return $this->hits;
    }

    public function getIds()
    {
        return $this->ids;
    }

    public function getAggregations()
    {
        return $this->aggregations;
    }

    public function getAggregate($key)
    {
        if (!isset($this->aggregations[$key])) {
            throw new \LogicException();
        }

        return $this->aggregations[$key];
    }
}
