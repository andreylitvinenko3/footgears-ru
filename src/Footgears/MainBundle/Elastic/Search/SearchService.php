<?php

namespace Footgears\MainBundle\Elastic\Search;

use Footgears\MainBundle\Search\SearchRequest;
use Footgears\MainBundle\Service\ContainerWrapper;

class SearchService
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function search(SearchRequest $request)
    {
        
    }
}
