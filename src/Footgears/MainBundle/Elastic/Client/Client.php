<?php

namespace Footgears\MainBundle\Elastic\Client;

use \Elasticsearch\Client as BaseClient;
use Footgears\MainBundle\Elastic\ElasticMeta;
use Footgears\MainBundle\Elastic\Logger\ElasticLogger;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class Client extends BaseClient
{
    /**
     * @var ElasticLogger
     */
    protected $logger;

    /**
     * @var AdapterInterface
     */
    protected $cache;

    public function setLogger(ElasticLogger $logger)
    {
        $this->logger = $logger;
    }

    public function search($params = [])
    {
        if ($this->logger) {
            $start = microtime(true);
        }

        $queryCache = ElasticMeta::QUERY_CACHE_MISS;
        if ($this->cache) {

            $cacheItem = $this->getCacheItem($params);
            $cacheItem->expiresAfter(60 * 60);

            if (!$cacheItem->isHit()) {
                $response = parent::search($params);
                $cacheItem->set($response);
                $cacheItem->tag(ElasticMeta::CACHE_TAG_SEARCH_RESULT);

                $this->cache->save($cacheItem);

                $queryCache = ElasticMeta::QUERY_CACHE_PUT;
            } else {
                $response = $cacheItem->get();
                $queryCache = ElasticMeta::QUERY_CACHE_HIT;
            }
        } else {
            $response = parent::search($params);
        }

        if ($this->logger) {
            $time = microtime(true) - $start;

            $path = sprintf('%s/%s/_search', $params['index'], $params['type']);
            $method = 'GET';
            $this->logger->logQuery($path, $method, $params['body'], $time, $response, $queryCache);
        }

        return $response;
    }

    public function setCache(AdapterInterface $cache)
    {
        $this->cache = $cache;
    }

    private function getCacheItem($params)
    {
        return $this->cache->getItem('elastic.' . md5(serialize($params)));
    }
}
