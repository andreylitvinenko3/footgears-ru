<?php

namespace Footgears\MainBundle\Elastic\Client;

use \Elasticsearch\ClientBuilder as BaseClientBuilder;
use Elasticsearch\Transport;

class ClientBuilder extends BaseClientBuilder
{
    protected $debug;

    public function getDebug()
    {
        return $this->debug;
    }

    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }

    protected function instantiate(Transport $transport, callable $endpoint, array $registeredNamespaces)
    {
        return new Client($transport, $endpoint, $registeredNamespaces);
    }

    public function setLogger($logger)
    {
        if ($this->debug) {
            parent::setLogger($logger);
        }
        return $this;
    }
}
