<?php

namespace Footgears\MainBundle\Elastic;

use Footgears\MainBundle\Elastic\Search\SearchResult;
use Symfony\Component\Stopwatch\Stopwatch;

class ElasticSearch
{
    const QUERY_TYPE_FILTERED = 'filtered';
    const QUERY_TYPE_CONSTANT_SCORE = 'constant_score';


    protected $filterType = self::QUERY_TYPE_CONSTANT_SCORE;
    protected $filters = [];
    protected $mustNotFilters = [];
    protected $sort;
    protected $size;
    protected $from;
    protected $query = [];
    protected $aggregations = [];
    protected $fields = [];

    protected $randomScore = false;

    protected $debug;

    /**
     * @var ElasticManager
     */
    protected $manager;

    /**
     * @var Stopwatch
     */
    protected $stopwatch;

    public function __construct(ElasticManager $manager, $debug = false)
    {
        $this->manager = $manager;
        $this->debug = $debug;
    }

    public function reset()
    {
        $this->filterType = self::QUERY_TYPE_CONSTANT_SCORE;
        $this->filters = [];
        $this->mustNotFilters = [];
        $this->sort = null;
        $this->size = null;
        $this->from = null;
        $this->query = [];
        $this->aggregations = [];
        $this->fields = [];
    }

    public function setFilterType($filterType)
    {
        $this->filterType = $filterType;
        return $this;
    }

    public function setStopwatch($stopwatch)
    {
        $this->stopwatch = $stopwatch;
        return $this;
    }

    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    public function setRandomScore($randomScore)
    {
        $this->randomScore = $randomScore;
        return $this;
    }

    public function addFilter($criteria)
    {
        if (!in_array($criteria, $this->filters)) {
            $this->filters[] = $criteria;
        }

        return $this;
    }

    public function setAggregation($key, $aggregation)
    {
        $this->aggregations[$key] = $aggregation;
        return $this;
    }

    /**
     * @return SearchResult
     */
    public function search()
    {
        $body = [
            'stored_fields' => $this->isDebug() ? [] : ($this->fields ? $this->fields : ['_id']),
            'size' => (!is_null($this->size) ? $this->size : 10),
            'from' => (int)$this->from,
            'query' => $this->prepareQuery(),
        ];

//        if ($this->isDebug()) {
//            $body['explain'] = true;
//        }

        if ($this->sort) {
            $body['sort'] = $this->sort;
        }

        $searchResult = $this->manager->search(ElasticMeta::TYPE_PRODUCT, $body);
        return $searchResult;
    }

    public function aggregate()
    {
        if (!$this->aggregations) {
            throw new \LogicException();
        }

        $body = [
            'size' => 0,
            'query' => $this->prepareQuery(),
            'aggs' => $this->aggregations,
        ];
        
        $searchResult = $this->manager->search(ElasticMeta::TYPE_PRODUCT, $body);

        return $searchResult;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    public function addQuery($query)
    {
        if (!in_array($query, $this->query)) {
            $this->query[] = $query;
        }
        return $this;
    }

    private function isDebug()
    {
//        return false;
        return $this->debug;
    }

    public function addMustNotFilter($filter)
    {
        if (!in_array($filter, $this->mustNotFilters)) {
            $this->mustNotFilters[] = $filter;
        }
        return $this;
    }

    private function prepareQuery()
    {
        $query = [];

        if ($this->query) {
            $query['bool']['must'] = $this->query;
        }

        $query['bool']['filter']['bool']['must'] = $this->filters;

        if ($this->mustNotFilters) {
            $query['bool']['filter']['bool']['must_not'] = $this->mustNotFilters;
        }

        return $query;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }
}
