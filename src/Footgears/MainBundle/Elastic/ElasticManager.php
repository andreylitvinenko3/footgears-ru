<?php

namespace Footgears\MainBundle\Elastic;

use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Elastic\Client\Client;
use Footgears\MainBundle\Elastic\Search\SearchResult;
use Footgears\MainBundle\Service\ContainerWrapper;

class ElasticManager
{
    /**
     * @var ContainerWrapper
     */
    private $container;

    /**
     * @var Client
     */
    private $client;

    private $indexName;

    public function __construct(ContainerWrapper $container, Client $client, $indexName)
    {
        $this->container = $container;
        $this->client = $client;
        $this->indexName = $indexName;
    }

    public function getIndexName()
    {
        return $this->indexName;
    }

    public function indexProduct(Product $product)
    {
        $this->indexProducts([$product]);
    }

    /**
     * @param Product[]|\Traversable $products
     */
    public function indexProducts($products)
    {
        if (!$products) {
            return;
        }

        $params = [];
        foreach ($products as $product) {
            if (!$product->isValid()) {
                continue;
            }

            $params[] = [
                'index' => [
                    '_id' => $product->getId(),
                    '_index' => $this->indexName,
                    '_type' => ElasticMeta::TYPE_PRODUCT
                ]
            ];
            $node = $product->getCategory();
            $categoryNames = [];
            $categoryIds = [];

            while ($node) {
                $categoryNames[] = $node->getName();
                $categoryIds[] = $node->getId();
                $node = $node->getParent() ? $node->getParent() : null;
            }

            $weight = rand(100000 * (min($product->getShop()->getPriority() * 100, 10000) / 10000), 100000);

            // оставляем отключенные товары в индексе но отправляем в конец списка
            if (!$product->isEnabled()) {
                $weight = -1;
            }

            $text = '';
            $xml = simplexml_load_string($product->getXml());
            if ($xml) {
                $text = (string)$xml->description;

                foreach ($xml->param as $param) {
                    $attributes = $param->attributes();

                    $str = ' ';
                    if ($attributes['name']) {
                        $str .= $attributes['name'];
                    }
                    $str .= ' ' . (string)$param;

                    $text .= $str;
                }
            }

            $params[] = [
                'categoryIds' => $categoryIds,
                'categories' => $categoryNames,
                'text' => $text,
                'shop' => [
                    'id' => $product->getShop()->getId(),
                    'name' => $product->getShop()->getName()
                ],
                'brand' => [
                    'id' => $product->getBrand()->getId(),
                    'name' => $product->getBrand()->getName()
                ],
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'price' => $product->getPrice(),
                'sale' => $product->isSale(),
                'discount' => $product->getDiscount(),
                'discount_sum' => $product->getDiscountSum(),
                'visits' => 0,
                'attributes' => $product->getAttributes(),
                'weight' => $weight,
                'updatedAt' => (new \DateTime())->getTimestamp()
            ];
        }

        if ($params) {
            $this->client->bulk(['body' => $params]);
        }
    }

    public function deleteProductFromIndex(Product $product)
    {
        $this->deleteProductsFromIndex([$product]);
    }

    /**
     * @param Product[]|array|\Traversable $products
     */
    public function deleteProductsFromIndex($products)
    {
        if (!$products) {
            return;
        }
        $params = [];
        foreach ($products as $product) {
            $id = $product instanceof Product ? $product->getId() : $product;
            $params[] = [
                'delete' => [
                    '_id' => $id,
                    '_index' => $this->indexName,
                    '_type' => ElasticMeta::TYPE_PRODUCT
                ]
            ];
        }
        $this->client->bulk(['body' => $params]);
    }

    public function createIndex()
    {
        $this->client->indices()->create([
            'index' => $this->indexName,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 1,
                    'refresh_interval' => '10m',
                    'analysis' => [
                        'filter' => [
                            'en_stop' => [
                                'type' => 'stop',
                                'stopwords' => '_english_'
                            ],
                            'ru_stop' => [
                                'type' => 'stop',
                                'stopwords' => '_russian_'
                            ],
                            'en_snow' => [
                                'type' => 'snowball',
                                'language' => 'English'
                            ],
                            'ru_snow' => [
                                'type' => 'snowball',
                                'language' => 'Russian'
                            ],
                            'delimiter' => [
                                'type' => 'word_delimiter',
                                'preserve_original' => true,
                                'protected_words' => ['mp3', 'wi-fi']
                            ],
                            'ngram' => [
                                'type' => 'nGram',
                                'min_gram' => 2,
                                'max_gram' => 3
                            ]
                        ],
                        'analyzer' => [
                            'name_analyzer' => [
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase', 'en_stop', 'ru_stop', 'en_snow', 'ru_snow', 'delimiter', 'ngram']
                            ],
                            'category_analyzer' => [
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase', 'en_stop', 'ru_stop', 'en_snow', 'ru_snow']
                            ],
                            'description_analyzer' => [
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase', 'en_stop', 'ru_stop', 'en_snow', 'ru_snow']
                            ],
                            'brand_analyzer' => [
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase', 'en_stop', 'ru_stop', 'ngram']
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    ElasticMeta::TYPE_PRODUCT => [
                        '_all' => [
                            'enabled' => false
                        ],
                        'properties' => [
                            'name' => [
                                'type' => 'text',
                                'analyzer' => 'name_analyzer',
                            ],
                            'price' => [
                                'type' => 'integer',
                                'index' => 'not_analyzed'
                            ],
                            'visits' => [
                                'type' => 'integer',
                                'index' => 'not_analyzed'
                            ],
                            'attributes' => [
                                'type' => 'integer',
                                'index' => 'not_analyzed'
                            ],
                            'categories' => [
                                'type' => 'text',
                                'analyzer' => 'category_analyzer'
                            ],
                            'categoryIds' => [
                                'type' => 'integer',
                                'index' => 'not_analyzed'
                            ],
                            'brand' => [
                                'properties' => [
                                    'id' => [
                                        'type' => 'integer',
                                        'index' => 'not_analyzed'
                                    ],
                                    'name' => [
                                        'type' => 'text',
                                        'analyzer' => 'brand_analyzer'
                                    ]
                                ]
                            ],
                            'shop' => [
                                'properties' => [
                                    'id' => [
                                        'type' => 'integer',
                                        'index' => 'not_analyzed'
                                    ],
                                    'name' => [
                                        'type' => 'text',
                                        'analyzer' => 'brand_analyzer'
                                    ]
                                ]
                            ],
                            'text' => [
                                'type' => 'text',
                                'analyzer' => 'name_analyzer'
                            ],
                            'description' => [
                                'type' => 'text',
                                'analyzer' => 'description_analyzer',
                                'index_options' => 'docs'
                            ],
                            'sale' => [
                                'type' => 'boolean',
                                'index' => 'not_analyzed'
                            ],
                            'discount' => [
                                'type' => 'byte',
                                'index' => 'not_analyzed'
                            ],
                            'discount_sum' => [
                                'type' => 'integer',
                                'index' => 'not_analyzed'
                            ],
                            'weight' => [
                                'type' => 'integer',
                                'index' => 'not_analyzed'
                            ],
                            'updatedAt' => [
                                'type' => 'date',
                                'index' => 'not_analyzed'
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function getClient()
    {
        return $this->client;
    }

    public function existsIndex()
    {
        return $this->client->indices()->exists(['index' => $this->indexName]);
    }

    public function dropIndex()
    {
        return $this->client->indices()->delete(['index' => $this->indexName]);
    }

    public function search($type, $body)
    {
        $stopwatch = $this->container->getContainer()->get('debug.stopwatch');
        $stopwatch->start(ElasticMeta::EVENT_NAME);

        $params = [
            'index' => $this->indexName,
            'type' => $type,
            'body' => $body
        ];

        $result = $this->client->search($params);
        $stopwatch->stop(ElasticMeta::EVENT_NAME);

        return $this->buildResult($result);
    }

    public function find($id, $type = ElasticMeta::TYPE_PRODUCT)
    {
        $params = [
            'index' => $this->indexName,
            'type' => $type,
            'id' => $id
        ];

        try {
            $result = $this->client->get($params);

            return $result['_source'];
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param $result
     * @return SearchResult
     */
    private function buildResult($result)
    {
        $searchResult = new SearchResult();
        $searchResult
            ->setTotal($result['hits']['total'])
            ->setMaxScore($result['hits']['max_score'])
            ->setHits($result['hits']['hits'])
        ;

        if (isset($result['aggregations'])) {
            $searchResult->setAggregations($result['aggregations']);
        }

        return $searchResult;
    }
}
