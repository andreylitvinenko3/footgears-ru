<?php

namespace Footgears\MainBundle\Elastic\DataCollector;

use Footgears\MainBundle\Elastic\ElasticMeta;
use Footgears\MainBundle\Elastic\Logger\ElasticLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

class ElasticDataCollector extends DataCollector
{
    /**
     * @var ElasticLogger
     */
    protected $elasticLogger;

    public function __construct(ElasticLogger $elasticLogger)
    {
        $this->elasticLogger = $elasticLogger;
    }

    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $caches = [
            // todo: сделать правильную передачу параметра
            'enabled' => true,
            'counts' => [
                ElasticMeta::QUERY_CACHE_PUT => 0,
                ElasticMeta::QUERY_CACHE_HIT => 0,
                ElasticMeta::QUERY_CACHE_MISS => 0,
            ],
        ];

        foreach ($this->elasticLogger->getQueries() as $query) {
            $caches['counts'][$query['cache']]++;
        }

        $this->data['queries'] = $this->elasticLogger->getQueries();
        $this->data['nb_queries'] = $this->elasticLogger->getNbQueries();
        $this->data['caches'] = $caches;
    }

    public function getQueryCount()
    {
        return $this->data['nb_queries'];
    }

    public function getQueries()
    {
        return $this->data['queries'];
    }

    public function getTime()
    {
        $time = 0;
        foreach ($this->data['queries'] as $query) {
            $time += $query['executionMS'];
        }
        return $time;
    }

    public function isCacheEnabled()
    {
        return $this->data['caches']['enabled'];
    }

    public function getCachePutsCount()
    {
        return $this->data['caches']['counts'][ElasticMeta::QUERY_CACHE_PUT];
    }

    public function getCacheHitsCount()
    {
        return $this->data['caches']['counts'][ElasticMeta::QUERY_CACHE_HIT];
    }

    public function getCacheMissesCount()
    {
        return $this->data['caches']['counts'][ElasticMeta::QUERY_CACHE_MISS];
    }

    public function getName()
    {
        return 'elastic';
    }
}
