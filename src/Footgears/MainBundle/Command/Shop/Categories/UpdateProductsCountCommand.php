<?php

namespace Footgears\MainBundle\Command\Shop\Categories;

use Doctrine\MongoDB\Iterator;
use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateProductsCountCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('shop:categories:update_products_count')
            ->addArgument('shop_id', InputArgument::IS_ARRAY, 'Shop ids')
            ->setDescription('Обновления количества товаров в категориях магазина')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->logger;

        $dm = $this->getDocumentManager();
        $repository = $dm->getRepository(Shop::class);

        /** @var Shop[] $shops */
        if ($input->getArgument('shop_id')) {
            $shops = $repository->findBy(['_id' => ['$in' => $input->getArgument('shop_id')]]);
        } else {
            $shops = $repository->findAll();
        }

        $shopsCount = count($shops);

        $logger->info(sprintf('Found %d shops', $shopsCount));

        $i = 1;
        foreach ($shops as $shop) {
            /** @var ShopCategory[]|Iterator $categories */
            $categories = $dm
                ->getRepository(ShopCategory::class)
                ->createQueryBuilder()
                ->field('shop')->references($shop)
                ->getQuery()
                ->getIterator()
            ;

            $shopCategoriesCount = $categories->count();

            $logger->info(sprintf('[%d/%d] %s: categories: %d', $i, $shopsCount, $shop->getName(), $shopCategoriesCount));

            $j = 1;
            foreach ($categories as $category) {
                $productsCount = $dm
                    ->getDocumentCollection(Product::class)
                    ->count(['shopCategory' => $category->getId()])
                ;

                $category->setProductsCount($productsCount);
                $dm->persist($category);

                $logger->info(sprintf('[%d/%d] %s, (%d/%d) %s: %d', $i, $shopsCount, $shop->getName(), $j++, $shopCategoriesCount, $category->getName(), $productsCount));
            }
            $dm->flush();

            $i++;
        }
    }
}