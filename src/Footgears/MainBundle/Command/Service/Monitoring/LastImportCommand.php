<?php

namespace Footgears\MainBundle\Command\Service\Monitoring;

use Footgears\MainBundle\Document\ImportHistory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LastImportCommand extends BaseMonitoringCommand
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->getImportHistoryRepository();
        /** @var ImportHistory $lastImport */
        $lastImport = $repo
            ->createQueryBuilder()
            ->field('endDate')->notEqual(null)
            ->sort('endDate', -1)
            ->limit(1)
            ->getQuery()
            ->getSingleResult()
        ;

        if (!$lastImport || $lastImport->getEndDate() < new \DateTime('-6 hour')) {
            $this->sendNotify('last_import', [
                'importHistory' => $lastImport
            ]);
        }
    }
}
