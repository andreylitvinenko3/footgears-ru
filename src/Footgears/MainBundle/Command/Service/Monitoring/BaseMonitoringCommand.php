<?php

namespace Footgears\MainBundle\Command\Service\Monitoring;

use Footgears\MainBundle\Command\BaseCommand;

class BaseMonitoringCommand extends BaseCommand
{
    protected function sendNotify($template, $params = [])
    {
        $mailSender = $this->getMailSender();

        $mailSender->send('andreylit@gmail.com', $template, $params);
    }
}
