<?php

namespace Footgears\MainBundle\Command\Service\Monitoring;

use Footgears\MainBundle\Document\Category;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductsInCategoriesCommand extends BaseMonitoringCommand
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getDocumentManager();

        $repo = $dm->getRepository(Category::class);
        $total = $repo
            ->createQueryBuilder()
            ->getQuery()
            ->count()
        ;

        $emptyCategoriesCount = $repo
            ->createQueryBuilder()
            ->field('productsCount')->equals(0)
            ->getQuery()
            ->count()
        ;

        if (100 * $emptyCategoriesCount / $total > 10) {
            $this->sendNotify('empty_categories', [
                'total' => $total,
                'emptyCategories' => $emptyCategoriesCount
            ]);
        }
    }
}
