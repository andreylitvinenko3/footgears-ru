<?php

namespace Footgears\MainBundle\Command\Service;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\AdmitadDailyStat;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\CategoryBrand;
use Footgears\MainBundle\Document\ImportHistory;
use Footgears\MainBundle\Document\MenuItem;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\RecommendProduct;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Document\User;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class IndexCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->addOption('force', InputOption::VALUE_NONE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configuration = [];

        $configuration[AdmitadDailyStat::class] = [
            [['date' => 1], ['expireAfterSeconds' => 45 * 86400, 'unique' => true]],
        ];

        $configuration[ImportHistory::class] = [
            [['startDate' => 1], ['expireAfterSeconds' => 32 * 86400]]
        ];

        $configuration[User::class] = [
            [['username' => 1], ['unique' => true]],
            [['email' => 1], ['unique' => true]]
        ];

        $configuration[Product::class] = [
            [['shop' => 1, 'originalId' => 1], ['unique' => true]],
            [['enabled' => 1, 'disableDate' => 1]],
            [['productsCountFromIndex' => -1]],
            [['shopCategory' => 1]]
        ];

        $configuration[Shop::class] = [
            [['partner' => 1, 'originalId' => 1], ['unique' => true]],
            [['alias' => 1], ['unique' => true]],
            [['url' => 1], ['unique' => true]],
            [['active' => 1]]
        ];

        $configuration[Brand::class] = [
            [['alias' => 1], ['unique' => true]],
            [['enabled' => 1, 'letter' => 1, 'productsCount' => 1]]
        ];

        $configuration[RecommendProduct::class] = [
            [['category' => 1]]
        ];

        $configuration[Category::class] = [
            [['alias' => 1], ['unique' => true]],
            [['left' => 1, 'right' => 1]],
            [['parent' => 1]]
        ];

        $configuration[ShopCategory::class] = [
            [['left' => 1, 'parent' => 1, 'root' => 1]]
        ];

        $configuration[CategoryBrand::class] = [
            [['category' => 1, 'brand' => 1], ['unique' => true]]
        ];

        $configuration[MenuItem::class] = [
            [['root' => 1, 'left' => 1, 'right' => 1]],
            [['parent' => 1, 'name' => 1, 'left' => 1]]
        ];

        $configuration[Tag::class] = [
            [['category' => 1, 'alias' => 1], ['unique' => true]],
            [['category' => 1, 'type' => 1, 'active' => 1, 'productsCount' => 1]],
            [['productsCountUpdated' => 1]]
        ];

        $force = $input->getOption('force');


        foreach ($configuration as $document => $indexes) {
            $collection = $this
                ->getDocumentManager()
                ->getDocumentCollection($document)
                ->getMongoCollection()
            ;
            $this->createIndexes($collection, $indexes, $force);
        }
    }

    protected function createIndexes(\MongoCollection $collection, $indexes, $force)
    {
        $this->logger->info(sprintf('Ensure indexes for %s', $collection->getName()));

        $exist = [];
        $existIndexInfo = [];
        foreach ($collection->getIndexInfo() as $index) {
            if (isset($index['key']['_id']) && count($index['key']) == 1) {
                continue;
            }

            $name = $this->getIndexName($index['key']);
            $existIndexInfo[$name] = $index;

            $options = $index;
            unset($options['ns']);
            unset($options['key']);
            unset($options['name']);
            unset($options['v']);
            unset($options['background']);
            if (isset($options['sparse']) && false === $options['sparse']) {
                unset($options['sparse']);
            }
            ksort($options);
            $exist[$name] = $options;
        }

        $newIndexes = [];

        foreach ($indexes as $data) {
            $options = [];
            $keys = $data[0];
            if (isset($data[1])) {
                $options = $data[1];
            }
            $name = $this->getIndexName($keys);
            $newIndexes[$name] = [$keys, $options];
            ksort($options);
            $equal = true;
            $fullName = sprintf('%s.%s', $collection->getName(), $name);
            if (isset($exist[$name])) {
                if (json_encode($options) != json_encode($exist[$name])) {
                    $equal = false;
                    $this->logger->warning(sprintf('Different options for index %s', $fullName), [
                        'old' => $exist[$name],
                        'new' => $options
                    ]);
                }
            } else {
                $this->logger->warning(sprintf('Index %s is not exist', $fullName));
            }

            if ($force) {
                if (!$equal) {
                    $this->logger->info(sprintf('Delete old index %s', $fullName));
                    $collection->deleteIndex($keys);
                }

                $this->logger->info(sprintf('Ensure index %s', $fullName), [
                    'keys' => $keys,
                    'options' => $options
                ]);

                $collection->createIndex($keys, array_merge($options, [
                    'background' => true,
                    'socketTimeoutMS' => 30 * 60 * 1000
                ]));
            }
        }

        foreach ($exist as $name => $options) {
            $fullName = sprintf('%s.%s', $collection->getName(), $name);
            if (!isset($newIndexes[$name])) {
                $this->logger->warning(sprintf('Extra index %s', $fullName), $existIndexInfo[$name]);
                if ($force) {
                    $this->logger->info(sprintf('Delete old index %s', $fullName));
                    $collection->deleteIndex($existIndexInfo[$name]['key']);
                }
            }
        }
    }

    protected function getIndexName($keys)
    {
        $data = [];
        foreach ($keys as $key => $direction) {
            $data[] = $key . '_' . $direction;
        }
        return implode('_', $data);
    }
}
