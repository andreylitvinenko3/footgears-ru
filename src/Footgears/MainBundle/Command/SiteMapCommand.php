<?php

namespace Footgears\MainBundle\Command;

use Doctrine\MongoDB\Iterator;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\RecommendProduct;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\Tag;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\RouterInterface;

class SiteMapCommand extends BaseCommand
{
    const PRODUCT_PRIORITY = 0.3;
    const PRODUCT_CHANGEFREQ = 'weekly';
    const BATCH_SIZE = 500;

    const MAX_LINES = 40000;

    const CATEGORY_BRAND_CHANGEFREQ = 'daily';
    const CATEGORY_BRAND_PRIORITY = 1;

    protected $lastmod;

    protected function configure()
    {
        $this
            ->setName('footgears:sitemap')
            ->setDescription('Create sitemap')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->lastmod = (new \DateTime())->format('Y-m-d');

        $webDir = $this->getContainer()->getParameter('web_dir');

        $sitemapsPath = $webDir . '/sitemap/footgears.ru';
        $newSitemapsPath = $sitemapsPath . '_new';

        $fs = new Filesystem();

        if ($fs->exists($newSitemapsPath)) {
            $fs->remove($newSitemapsPath);
        }
        $fs->mkdir($newSitemapsPath, 0777);

        $this->addPages($newSitemapsPath);

//        $this->addRecommendProducts($newSitemapsPath);
        $this->addCategoryBrandPages($newSitemapsPath);
        $this->addOtherPages($newSitemapsPath);

        if ($fs->exists($sitemapsPath)) {
            $fs->remove($sitemapsPath);
        }
        $fs->rename($newSitemapsPath, $sitemapsPath, true);

        $this->logger->info(sprintf('Sitemap path: %s', $sitemapsPath));
    }

    protected function openFile($filePath)
    {
        $content = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;
        $fp = gzopen($filePath, 'w9');
        gzwrite($fp, $content);

        return $fp;
    }

    protected function closeFile($fp)
    {
        gzwrite($fp, '</urlset>');
    }

    protected function addUrl($fp, $route, $routeParams, $changefreq, $priority)
    {
        /* @var Router $router */
        $router = $this->getContainer()->get('router');

        $point = sprintf('<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%s</priority></url>',
            $router->generate($route, $routeParams, RouterInterface::ABSOLUTE_URL),
            $this->lastmod,
            $changefreq,
            $priority
        );

        gzwrite($fp, $point . PHP_EOL);
    }

    private function addCategoryBrandPages($filePath)
    {
        $this->logger->info(sprintf('Create category brand pages sitemap'));

        $filePathTemplate = $filePath . '/category_brand_%d.xml.gz';

        $inFiles = 0;
        $fileNumber = 0;

        /** @var Category[] $categories */
        $categories = $this->getCategoryRepository()->findAll();

        $filePath = sprintf($filePathTemplate, $fileNumber);
        $fp = $this->openFile($filePath);

        foreach ($categories as $category) {
            $this->logger->info(sprintf('Create brands sitemap for %s', $category->getName()));

            $searchRequest = $this->getSearchRequestFactory()->createCategorySearchRequest($category);

            $data = $this->getSearchManager()->getAvailableBrands($searchRequest);

            if (!$data) {
                continue;
            }

            /** @var Brand[] $brands */
            $brands = $this->getBrandRepository()->findBy(['id' => array_keys($data)]);

            foreach ($brands as $brand) {
                if ($brand->getProductsCount() < 4) {
                    continue;
                }
                $params = ['alias' => $category->getAlias(), 'brandAlias' => $brand->getAlias()];

                $this->addUrl($fp, 'category_brand', $params, self::CATEGORY_BRAND_CHANGEFREQ, self::CATEGORY_BRAND_PRIORITY);
                $inFiles++;

                if ($inFiles >= self::MAX_LINES) {
                    $inFiles = 0;

                    $this->closeFile($fp);
                    $fp = $this->openFile(sprintf($filePathTemplate, ++$fileNumber));
                }
            }
        }

        $this->closeFile($fp);
    }

    /**
     * @param $newSitemapsPath
     */
    protected function addOtherPages($newSitemapsPath)
    {
        $this->logger->info(sprintf('Create sitemap for %s pages', 'other'));

        $fp = $this->openFile($newSitemapsPath . '/other.xml.gz');

        $this->addUrl($fp, 'sale', [], 'daily', 0.7);
        $this->addUrl($fp, 'brands', [], 'monthly', 0.1);
        $this->addUrl($fp, 'shops', [], 'monthly', 0.1);
        $this->addUrl($fp, 'about', [], 'monthly', 0.1);
        $this->addUrl($fp, 'contacts', [], 'monthly', 0.1);

        $this->addBrandsByLetterPages($fp);

        $this->closeFile($fp);
    }

    private function addRecommendProducts($sitemapPath)
    {
        $template = $sitemapPath . '/recommend_%d.xml.gz';

        $inFile = 0;
        $files = 0;

        $fp = $this->openFile(sprintf($template, ++$files));

        $recommendProducts = $this->getDocumentManager()->getRepository(RecommendProduct::class)->findAll();
        foreach ($recommendProducts as $product) {
            $params = [
                'alias' => $product->getCategory()->getAlias(),
                'id' => $product->getPosition()
            ];
            $this->addUrl($fp, 'category_product', $params, 'daily', 1);

            if (++$inFile >= 40000) {
                $this->closeFile($fp);
                $inFile = 0;

                $fp = $this->openFile(sprintf($template, ++$files));
            }
        }

        $this->closeFile($fp);
    }

    private function addBrandsByLetterPages($fp)
    {
        $letters = [];
        for ($let = ord('a'); $let <= ord('z'); $let++) {
            $letters[] = chr($let);
        }

        $letters[] = 'rus';
        $letters[] = 'other';

        foreach ($letters as $letter) {
            $this->addUrl($fp, 'brands', ['letter' => $letter], 'monthly', 0.6);
        }
    }

    private function addPages($newSitemapsPath)
    {
        $routes = [
            Brand::class => [
                'route' => 'brand',
                'priority' => 0.7,
                'changefreq' => 'daily',
                'qb' => $this
                    ->getDocumentManager()
                    ->getRepository(Brand::class)
                    ->createQueryBuilder()
                    ->field('alias')->notIn([null, ''])
                    ->field('enabled')->equals(true)
                    ->field('productsCount')->gt(0)
            ],
            Shop::class => [
                'route' => 'shop',
                'priority' => 0.5,
                'changefreq' => 'daily',
                'qb' => $this
                    ->getDocumentManager()
                    ->getRepository(Shop::class)
                    ->createQueryBuilder()
                    ->field('active')->equals(true)
                    ->field('productsCountFromIndex')->gt(0)
            ],
            Category::class => [
                'route' => 'category',
                'priority' => 0.9,
                'changefreq' => 'daily',
                'qb' => $this
                    ->getDocumentManager()
                    ->getRepository(Category::class)
                    ->createQueryBuilder()
            ],
            Tag::class => [
                'route' => 'category_tag',
                'priority' => 0.9,
                'changefreq' => 'daily',
                'qb' => $this
                    ->getDocumentManager()
                    ->getRepository(Tag::class)
                    ->createQueryBuilder()
                    ->field('active')->equals(true)
            ]
        ];

        $this->logger->info(sprintf('Memory usage: %.2fMb', memory_get_usage(true) / 1024 / 1024));

        $dm = $this->getDocumentManager();
        foreach ($routes as $className => $data) {

            $this->logger->info(sprintf('Create sitemap files to %s', $className));

            $lines = 0;
            $fileNum = 0;

            /** @var Iterator $iterator */
            $iterator = $data['qb']->getQuery()->getIterator();

            if (!$iterator->count()) {
                $this->logger->warning($className . ': No pages');
                continue;
            }

            $this->logger->info(sprintf('Create sitemap for %s pages. Found %d rows', $className, $iterator->count()));

            $filePath = sprintf('%s/%s_%d.xml.gz', $newSitemapsPath, $data['route'], $fileNum);
            $fp = $this->openFile($filePath);

            foreach ($iterator as $row) {
                if ($row instanceof Tag) {
                    $routeParams = [
                        'alias' => $row->getCategory()->getAlias(),
                        'tagAlias' => $row->getAlias()
                    ];
                } else {
                    $routeParams = ['alias' => $row->getAlias()];
                }

                $this->addUrl($fp, $data['route'], $routeParams, $data['changefreq'], $data['priority']);
                $lines++;

                $dm->detach($row);

                if ($lines >= self::MAX_LINES) {
                    $this->closeFile($fp);

                    $this->logger->info(sprintf('File %d done.', $filePath));

                    $filePath = sprintf('%s/%s_%d.xml.gz', $newSitemapsPath, $data['route'], ++$fileNum);
                    $fp = $this->openFile($filePath);

                    $lines = 0;
                }
            }
            $dm->clear();

            $this->logger->info(sprintf('Memory usage: %.2fMb', memory_get_usage(true) / 1024 / 1024));

            $this->closeFile($fp);
        }
    }
}
