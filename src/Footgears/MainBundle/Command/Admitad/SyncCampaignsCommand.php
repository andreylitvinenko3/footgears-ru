<?php

namespace Footgears\MainBundle\Command\Admitad;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Shop;
use GuzzleHttp\Client;
use Sirian\Helpers\TextUtils;
use Sirian\StorageBundle\Document\FileEmbed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SyncCampaignsCommand extends BaseCommand
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getDocumentManager();

        $admitadUrl = 'http://export.admitad.com/ru/webmaster/websites/208894/partners/export/?user=alitvinenko&code=123b6a954f&filter=1&keyword=&region=00&action_type=&status=active&format=xml&version=2';

        $client = new Client(['timeout' => 20]);

        $response = $client->get($admitadUrl);

        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
            $this->logger->error(sprintf('Error on get program list: %s', $response->getReasonPhrase() ? $response->getReasonPhrase() : (string)$response->getBody()));
            return;
        }

        $xml = (string)$response->getBody();

        $simpleXml = simplexml_load_string($xml);
        if (!$simpleXml) {
            $this->logger->error('Error on get simplexml object');
            return;
        }

        $shopRepository = $this->getDocumentManager()->getRepository(Shop::class);

        /** @var Shop[] $shops */
        /** @var Shop[] $assoc */
        $shops = $shopRepository->findBy([
            'partner' => Shop::PARTNER_ADMITAD
        ]);

        $assoc = [];
        foreach ($shops as $shop) {
            $assoc[$shop->getOriginalId()] = $shop;
        }

        $activeIds = [];
        foreach ($simpleXml->advcampaign as $item) {
            $originalId = (int)$item->id;

            $activeIds[] = $originalId;

            if (!isset($assoc[$originalId])) {
                $name = (string)$item->name;

                $shop = new Shop();
                $shop
                    ->setOriginalId($originalId)
                    ->setUrl((string)$item->gotolink)
                    ->setName($name)
                    ->setAlias(TextUtils::slug($name))
                ;

                $assoc[$originalId] = $shop;

                $this->logger->info(sprintf('New shop: #%s %s (%s)', $shop->getOriginalId(), $shop->getName(), $shop->getAlias()));
            }

            $shop = $assoc[$originalId];

            if ($shop->getStatus() == Shop::STATUS_BLOCKED) {
                $this->logger->warning(sprintf('Shop #%s %s exists and blocked. Reason: %s', $shop->getId(), $shop->getName(), $shop->getSystemComment()));
            }

            $this->setImage($shop, (string)$item->logo);

            $shop
                ->getImportProductsSettings()
                    ->setUrl((string)$item->original_products)
            ;

            $dm->persist($shop);
        }

        $dm->flush();

        $this->disableNonActiveShops($activeIds);
    }

    private function setImage(Shop $shop, $logoUrl)
    {
        $fileInfo = new \SplFileInfo($logoUrl);

        if ($shop->getLogo() && $shop->getLogo()->getFile()->getMd5() === md5_file($logoUrl)) {
            return;
        }

        $localFileTemplate = sprintf('/var/tmp/footgears_shop_logo_%s', $shop->getOriginalId());

        file_put_contents($localFileTemplate, file_get_contents($logoUrl));

        $file = new UploadedFile($localFileTemplate, $fileInfo->getFilename(), null, null, null, true);

        $logo = new FileEmbed();
        $logo->setUploadedFile($file);

        $shop->setLogo($logo);
    }

    private function disableNonActiveShops(array $activeIds)
    {
        if (!$activeIds) {
            return;
        }

        /** @var Shop[] $shops */
        $shops = $this
            ->getDocumentManager()
            ->getRepository(Shop::class)
            ->findBy([
                'partner' => Shop::PARTNER_ADMITAD,
                'originalId' => ['$nin' => $activeIds],
                'status' => ['$ne' => Shop::STATUS_BLOCKED]
            ])
        ;

        $dm = $this->getDocumentManager();
        foreach ($shops as $shop) {
            $shop->deactivate('Отключен в Admitad');
            $dm->persist($shop);
        }

        $dm->flush();
    }
}
