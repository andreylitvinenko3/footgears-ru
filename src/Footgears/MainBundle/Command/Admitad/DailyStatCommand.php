<?php

namespace Footgears\MainBundle\Command\Admitad;

use Admitad\Api\Exception\ApiException;
use Footgears\MainBundle\Command\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DailyStatCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('admitad:daily')
            ->setDescription('Получить из admitad статистику')
            ->addArgument('start_date', InputArgument::OPTIONAL, 'Start date. Format: YYYY-MM-DD')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $startDate = new \DateTime();

        if ($input->getArgument('start_date')) {
            $startDate->modify($input->getArgument('start_date'));
        } else {
            $startDate->modify('-1 month');
        }
        $startDate->setTime(0, 0, 0);

        $admitadApi = $this->getAdmitadManager()->getApiClient();

        try {
            $iterator = $admitadApi
                ->selfAuthorize($this->getContainer()->getParameter('admitad.client_id'), $this->getContainer()->getParameter('admitad.client_secret'), $this->getContainer()->getParameter('admitad.scope'))
                ->getIterator('/statistics/dates/', [
                    'order_by' => 'datetime',
                    'date_start' => $startDate->format('d.m.Y'),
            ]);

            $dm = $this->getDocumentManager();

            foreach ($iterator as $row) {
                $date = new \DateTime($row['date']);

                $dailyStat = $this->getAdmitadDailyStatRepository()->getByDate($date);

                $dailyStat
                    ->setClicks($row['clicks'])
                    ->setAcpm($row['acpm'])
                    ->setCr($row['cr'])
                    ->setCtr($row['ctr'])
                    ->setEcpc($row['ecpc'])
                    ->setPaymentSumApproved($row['payment_sum_approved'])
                    ->setPaymentSumDeclined($row['payment_sum_declined'])
                    ->setPaymentSumOpen($row['payment_sum_open'])
                    ->setSalesSum($row['sales_sum'])
                    ->setSalesSumApproved($row['sales_sum_approved'])
                    ->setSalesSumDeclined($row['sales_sum_declined'])
                    ->setSalesSumOpen($row['sales_sum_open'])
                ;

                $dm->persist($dailyStat);
            }
        } catch (ApiException $e) {
            $this->getLogger()->addCritical('DailyStatCommand: ' . $e->getMessage());
            return;
        }

        $dm->flush();
    }

}