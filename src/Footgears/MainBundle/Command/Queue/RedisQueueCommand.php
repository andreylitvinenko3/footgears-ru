<?php

namespace Footgears\MainBundle\Command\Queue;

abstract class RedisQueueCommand extends AbstractQueueCommand
{
    protected $lastPrepareTime;
    protected $prepareInterval = 60;
    protected $lastFlushTime;
    protected $flushInterval = 180;
    protected $needFlush = false;
    protected $batchSize = 500;
    protected $prepared = false;

    protected function loadBatch()
    {
        $key = $this->getQueueKey();
        $limit = $this->batchSize;

        $redis = $this->getRedis();

        $redis->multi(\Redis::ATOMIC|\Redis::MULTI);
        $redis->lRange($key, 0, $limit - 1);
        $redis->lTrim($key, $limit, -1);
        $rows = $redis->exec();

        if (!empty($rows[0])) {
            $redis->incrBy($key . ':count', count($rows[0]));
            return $rows[0];
        }

        return [];
    }

    protected function unserialize($row)
    {
        return unserialize($row);
    }

    abstract protected function getQueueKey();

    public function getMemoryLimit()
    {
        return 1000 * 1024 * 1024;
    }

    protected function flushAndClear()
    {
        $dm = $this->getDocumentManager();
        $dm->flush();
        $dm->clear();
        gc_collect_cycles();
    }
}
