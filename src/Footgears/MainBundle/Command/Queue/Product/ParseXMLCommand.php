<?php

namespace Footgears\MainBundle\Command\Queue\Product;

use Footgears\MainBundle\Document\Attribute;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\File;
use Footgears\MainBundle\Document\ImportHistory;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\Seo;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Footgears\MainBundle\Shop\ShopHelper;
use Footgears\MainBundle\YandexMarketLanguage\Offer;
use Footgears\MainBundle\YandexMarketLanguage\ParserInterface;
use Footgears\MainBundle\YandexMarketLanguage\YooxParser;
use Sirian\Helpers\TextUtils;
use Sirian\YandexMarketLanguage\Category;
use Sirian\YMLParser\Parser\CategoriesEvent;
use Sirian\YMLParser\Parser\OfferEvent;
use Symfony\Component\Console\Output\OutputInterface;

class ParseXMLCommand extends ProductCommand
{
    const FLUSH_BATCH_SIZE = 500;

    protected $brandCache = [];

    /**
     * @var ShopCategory[]
     */
    protected $shopCategories;

    protected function doHandleRow(ImportHistory $history)
    {
        $this->shopCategories = [];
        $shop = $history->getShop();
        $shop->getImportProductsSettings()->incImportVersion();

        $history
            ->setNewCategories(0)
            ->setUpdatedCategories(0)
            ->setRemovedCategories(0)
            ->setProducts(0)
            ->setNewProducts(0)
            ->setUpdatedProducts(0)
            ->setSkippedProducts(0)
            ->setDisabledProducts(0)
            ->setIndexAddProducts(0)
            ->setIndexDeleteProducts(0)
        ;

        $this->logger->info(sprintf('Import #%d %s', $shop->getId(), $shop->getName()));

        // todo: перейти на новый парсер
        $parser = ShopHelper::createParser($shop);

        $filename = $this->getXmlFile($shop, $parser);

        $offers = [];
        $index = 0;
        $parser->addListener('offer', function (OfferEvent $event) use($history, &$offers, &$index) {
            $shop = $history->getShop();

            /** @var Offer $offer */
            $offer = $event->getOffer();

            if ($shop->getImportProductsSettings()->getIdXPath()) {
                $offer->setId($this->getXPath($offer, $shop->getImportProductsSettings()->getIdXPath()));
            }

            $index++;

            $attribute = $this->getAttributesService()->getAttribute(Attribute::CONTENT);

            if ($attribute) {
                $additionalParameters = $this->getAttributesService()->getAttributeValuesFromOfferField($offer->getDescription(), $attribute);

                foreach ($additionalParameters as $parameter) {
                    $offer->addParameter($attribute->getName(), $parameter);
                }
            }

            $offers[mb_strtolower($offer->getId())] = $offer;

            if (count($offers) >= 200) {
                $this->flushOffers($history, $offers);
                $this->logger->info(sprintf('Flush products: %d. Memory usage: %.2f', $index, memory_get_usage() / 1024 / 1024));
                $offers = [];
            }
        });

        $parser->addListener('categories', function (CategoriesEvent $categoriesEvent) use ($shop, $history) {
            $this->logger->info('Updating category tree');

            $history->setProgress(ImportHistory::PROGRESS_PARSING_CATEGORIES);
            $this->updateHistoryAndFlush($history);

            $parsedCategories = $categoriesEvent->getCategories();

            $category = new Category();
            $category
                ->setName('')
                ->setId('')
            ;

            $root = [
                'category' => $category,
                'children' => [],
                'parent' => null
            ];

            $nodes = [];

            /* @var Category $parsedCategory */
            foreach ($parsedCategories as $parsedCategory) {
                $nodes[$parsedCategory->getId()] = [
                    'category' => $parsedCategory,
                    'children' => [],
                    'parent' => null
                ];
            }

            foreach ($parsedCategories as $parsedCategory) {
                if ($parsedCategory->getParent()) {
                    $parent = &$nodes[$parsedCategory->getParent()->getId()];
                } else {
                    $parent = &$root;
                }
                $node = &$nodes[$parsedCategory->getId()];
                $parent['children'][] = &$node;
                $node['parent'] = &$parent;
            }

            $data = [];
            $ids = [];
            $cnt = 1;
            $updated = (new \DateTime())->getTimestamp();

            $walk = function ($node, $level = 0) use (&$data, &$cnt, $shop, &$ids, &$walk) {
                $lft = $cnt++;
                foreach ($node['children'] as $child) {
                    $walk($child, $level + 1);
                }
                $rgt = $cnt++;

                /* @var Category $category */
                $category = $node['category'];
                $id = ShopCategory::generateId($shop->getId(), $category->getId());
                $ids[] = $id;
                $data[$level][] = [
                    'id' => $id,
                    'name' => $category->getName(),
                    'originalId' => $category->getId(),
                    'shop' => $shop->getId(),
                    'enabled' => true,
                    'parentId' => $node['parent'] ? ShopCategory::generateId($shop->getId(), $node['parent']['category']->getId()) : null,
                    'left' => $lft,
                    'right' => $rgt,
                    'level' => $level,
                    'root' => $shop->getId(),
                ];
            };
            $walk($root);

            if (!$ids) {
                $this->logger->warning('No categories parsing');
                return;
            }
            ksort($data);

            $mongoCollection = $this->getDocumentManager()->getDocumentCollection(ShopCategory::class)->getMongoCollection();

            $this->logger->info('Insert categories');
            foreach ($data as $level => $rows) {
                foreach (array_chunk($rows, 200) as $items) {
                    $batch = new \MongoUpdateBatch($mongoCollection);

                    foreach ($items as $row) {
                        $update = [
                            '$setOnInsert' => [
                                'shop' => $row['shop'],
                                'originalId' => $row['originalId'],
                                'enabled' => $row['enabled'],
                                'root' => $row['root'],
                                'handled' => false
                            ],
                            '$set' => [
                                'left' => $row['left'],
                                'right' => $row['right'],
                                'level' => $row['level'],
                                'name' => $row['name'],
                                'parent' => $row['parentId'],
                                'updated' => new \MongoDate($updated),
                            ]
                        ];

                        $batch->add([
                            'q' => ['_id' => $row['id']],
                            'u' => $update,
                            'upsert' => true
                        ]);
                    }

                    $batch->execute();
                }
            }

            $this->deleteShopCategories($history, $ids);

            $this->logger->info('Build categories mapping');

            $dm = $this->getDocumentManager();

            $iterator = $this->getShopCategoryRepository()
                ->createQueryBuilder()
                ->field('shop')->references($shop)
                ->getQuery()
                ->getIterator()
            ;

            /* @var ShopCategory $shopCategory */
            foreach ($iterator as $shopCategory) {
                $realCategory = $shopCategory->getRealCategory();
                $this->shopCategories[$shopCategory->getOriginalId()] = [
                    'categoryId' => $realCategory ? $realCategory->getId() : null,
                    'shopCategoryId' => $shopCategory->getId()
                ];

                $dm->detach($shopCategory);
                if ($realCategory) {
                    $dm->detach($realCategory);
                }
            }
            $this->logger->info('Build categories mapping end');

            $dm->clear(ShopCategory::class);
            $dm->clear(\Footgears\MainBundle\Document\Category::class);
            $this->logger->info('Category tree updated');
        });

        $parser->parse($filename);
        $this->flushOffers($history, $offers);

        $history->setProducts($parser->getOffersCount());
        $this->updateHistoryAndFlush($history);

        if (!$history->isDelta()) {
            // блокируем отсутствующие товары только при полном импорте
            $this->disableOutdatedProducts($history);
        }

        $history->setProgress(ImportHistory::PROGRESS_UPDATE_COUNTERS);
        $this->updateHistoryAndFlush($history);

        // обновляем кол-во товаров в магазине
        $this->updateShopProductsCount($history);
        // обновляем кол-во товаров в категориях магазина
        $this->updateShopCategoriesProductsCount($this->output, $history);

        $history
            ->setProgress(ImportHistory::PROGRESS_FINISH)
            ->setStatus(ImportHistory::STATUS_SUCCESS)
            ->setEndDate(new \DateTime())
        ;
        $this->updateHistoryAndFlush($history);

        $this->logger->info('Parse end.');
    }

    protected function getXmlFile(Shop $shop, ParserInterface $parser)
    {
        if ($parser instanceof YooxParser) {
            return $this->getXmlFileNameModify($shop);
        }

        return parent::getXmlFileName($shop);
    }

    /**
     * @param ImportHistory $history
     * @param Offer[] $offers
     */
    protected function flushOffers(ImportHistory $history, $offers)
    {
        if (!$offers) {
            return;
        }

        $dm = $this->getDocumentManager();

        $shop = $history->getShop();
        $settings = $shop->getImportProductsSettings();

        $history->setProgress(ImportHistory::PROGRESS_PARSING_PRODUCTS);

        $assoc = $this->loadProducts($history->getShop(), $offers);

        /** @var Product[] $products */
        $products = [];

        $namePartFields = preg_split('/\s*,\s*/', $settings->getNameExpression());

        foreach ($offers as $offer) {
//            $this->logger->info(sprintf('Save offer #%s', $offer->getId()));

            if ($offer->isDeleted()) {
                $this->logger->info('Offer is deleted', ['id' => $offer->getId()]);

                if (isset($assoc[$offer->getId()]) && $assoc[$offer->getId()]->isEnabled()) {
                    $product = $assoc[$offer->getId()];
                    $product->disable();

                    $dm->persist($product);

                    $products[mb_strtolower($offer->getId())] = $product;

                    $history->incDisabledProducts();
                } else {
                    $history->incSkippedProducts();
                }

                continue;
            }

            if (!$offer->getCategory()) {
                $history->incSkippedProducts();

                $this->logger->info('No category', ['id' => $offer->getId()]);
                continue;
            }

            if (!$offer->getPictures() || !$offer->getCurrency()) {
                $history->incSkippedProducts();

                $this->logger->info('No pictures or currency', ['id' => $offer->getId()]);
                continue;
            }

            if (!$offer->getId()) {
                $history->incSkippedProducts();

                $this->logger->info('No ID', ['id' => $offer->getId()]);
                continue;
            }

            if (!array_key_exists($offer->getCategory()->getId(), $this->shopCategories)) {
                $history->incSkippedProducts();

                $this->logger->info('Not found shop category', ['id' => $offer->getId()]);
                continue;
            }

            if (!isset($assoc[$offer->getId()])) {
                $product = new Product();
                $product
                    ->setShop($shop)
                    ->setOriginalId($offer->getId())
                ;

                $dm->persist($product);
                $assoc[$offer->getId()] = $product;

                $history->incNewProducts();
            } else {
                $history->incUpdatedProducts();
            }

            $nameParts = [];
            foreach ($namePartFields as $partField) {
                $nameParts[] = $this->getXPath($offer, $partField);
            }

            $product = $assoc[$offer->getId()];

            // todo: доработать парсер так чтобы для каждой категории считалось кол-во товаров в xml
            $offerCategory = $offer->getCategory();
            $categoryId = $this->shopCategories[$offerCategory->getId()]['categoryId'];
            $shopCategoryId = $this->shopCategories[$offerCategory->getId()]['shopCategoryId'];

            $brandId = null;
            $vendor = html_entity_decode((string)$offer->getVendor());
            if ($vendor) {
                $brandId = $this->findBrand($vendor);
            }

            $currency = $offer->getCurrency()->getId();
            $currency = strtr($currency, ['RUR' => 'RUB']);

            $oldPrice = $this->getXPath($offer, $settings->getOldPriceXPath());
            if (!$oldPrice) {
                $oldPrice = $offer->getOldPrice();
            }

            $product
                ->setName(html_entity_decode(implode(' ', $nameParts)))
                ->setXml((string)$offer->getXml()->asXML())
                ->setUrl($offer->getUrl())
                ->setPrice((float)$offer->getPrice())
                ->setOldPrice($oldPrice)
                ->setShop($shop)
                ->setBrand($brandId ? $dm->getReference(Brand::class, $brandId) : null)
                ->setCurrency($currency)
                ->setAttributes($this->getAttributesService()->parseParameters($offer))
                ->setShopCategory($shopCategoryId ? $dm->getReference(ShopCategory::class, $shopCategoryId) : null)
                ->setCategory($categoryId ? $dm->getReference(\Footgears\MainBundle\Document\Category::class, $categoryId) : null)
                ->setImages($offer->getPictures())
                ->setImportVersion($history->getShop()->getImportProductsSettings()->getImportVersion())
                ->activate()
            ;

            $dm->persist($product);

            $products[mb_strtolower($offer->getId())] = $product;
        }

        $this->updateHistoryAndFlush($history);

        $deleteBatch = [];
        $insertBatch = [];

        $productIds = array_map(function (Product $product) {
            return $product->getId();
        }, $products);

        $dm->clear(Product::class);

        $products = $this->getProductRepository()->findByIds(array_values($productIds));

        foreach ($products as $product) {
            /** @var Product $product */
            if ($product->isValid()) {
                $insertBatch[] = $product;
            } else {
                $deleteBatch[] = $product->getId();
            }
        }

        $elasticManager = $this->getElasticManager();
        $elasticManager->indexProducts($insertBatch);
        $elasticManager->deleteProductsFromIndex($deleteBatch);

        $history
            ->incIndexAddProducts(count($insertBatch))
            ->incIndexDeleteProducts(count($deleteBatch))
        ;
        $this->updateHistoryAndFlush($history);

        $dm->clear(Product::class);
        $dm->clear(Brand::class);
        $dm->clear(ShopCategory::class);
        $dm->clear(\Footgears\MainBundle\Document\Category::class);
        $dm->clear(Seo::class);

        $count = gc_collect_cycles();
        $this->logger->info(sprintf('gc_collect_cycles = %d', $count));

        $this->showUOW();
    }

    protected function loadBatch()
    {
        $collection = $this->getImportHistoryRepository()->getCollection();

        $history = $collection->findAndUpdate([
            'status' => ImportHistory::STATUS_ACTIVE,
            'progress' => ImportHistory::PROGRESS_WAIT_PARSING
        ], [
            '$set' => [
                'progress' => ImportHistory::PROGRESS_PARSING_START
            ]
        ]);

        if (!$history) {
            return [];
        }

        return [$history['_id']];
    }

    /**
     * @param Shop $shop
     * @param Offer[] $offers
     * @return Product[]
     */
    protected function loadProducts(Shop $shop, array $offers)
    {
        if (!$offers) {
            return [];
        }

        /**
         * @var Product[] $products
         * @var Product[] $assoc
         */
        $products = $this->getProductRepository()->findBy([
            '_id' => ['$in' => array_values(array_map(function (Offer $offer) use ($shop) {
                return Product::generateId($shop->getId(), $offer->getId());
            }, $offers))]
        ]);

        $assoc = [];
        foreach ($products as $product) {
            $assoc[$product->getOriginalId()] = $product;
        }

        return $assoc;
    }

    protected function findBrand($vendor)
    {
        if (!empty($this->brandCache[$vendor])) {
            return $this->brandCache[$vendor];
        }

        $brandRepo = $this
            ->getDocumentManager()
            ->getRepository(Brand::class)
        ;

        $qb = $brandRepo->createQueryBuilder();

        $brand = $qb
            ->addOr($qb->expr()->field('name')->equals($vendor))
            ->addOr($qb->expr()->field('searchTags')->equals(new \MongoRegex('/' . preg_quote($vendor) . '/i')))
            ->limit(1)
            ->getQuery()
            ->getSingleResult()
        ;

        if (!$brand) {
            $alias = TextUtils::slug($vendor);
            $brand = $brandRepo->findOneBy(['alias' => $alias]);

            if (!$brand) {
                $brand = $this->getBrandService()->createBrand();
                $brand
                    ->setName($vendor)
                    ->setAlias($alias)
                ;

                $dm = $this->getDocumentManager();
                $dm->persist($brand);
                $dm->flush();
            }
        }

        $this->brandCache[$vendor] = $brand->getId();

        return $this->brandCache[$vendor];
    }

    protected function disableOutdatedProducts(ImportHistory $history)
    {
        $this->logger->info('Disable outdated products');

        $history->setProgress(ImportHistory::PROGRESS_DISABLE_PRODUCTS);
        $this->updateHistoryAndFlush($history);

        $dm = $this->getDocumentManager();

        $shop = $history->getShop();

        while (true) {
            $cursor = $this->getProductRepository()
                ->createQueryBuilder()
                ->field('shop')->references($shop)
                ->field('enabled')->equals(true)
                ->field('importVersion')->lt($shop->getImportProductsSettings()->getImportVersion())
                ->limit(200)
                ->getQuery()
                ->getIterator()
            ;

            $count = $cursor->count();

            if (!$count) {
                break;
            }

            $updateBatch = [];
            $deleteBatch = [];

            /** @var Product $product */
            foreach ($cursor as $product) {
                $product->disable();

                if ($product->isValid()) {
                    $updateBatch[$product->getId()] = $product;
                } else {
                    $deleteBatch[$product->getId()] = $product->getId();
                }

                $dm->persist($product);
            }

            $dm->flush();

            $this->getElasticManager()->indexProducts($updateBatch);
            $this->getElasticManager()->deleteProductsFromIndex($deleteBatch);

            $dm->clear(Product::class);
            $dm->clear(Brand::class);
            $dm->clear(ShopCategory::class);
            $dm->clear(\Footgears\MainBundle\Document\Category::class);
            $dm->clear(Seo::class);

            $count = gc_collect_cycles();

            $this->logger->info(sprintf('gc_collect_cycles = %d', $count));

            $this->showUOW();

            $history
                ->incIndexAddProducts(count($updateBatch))
                ->incIndexDeleteProducts(count($deleteBatch))
                ->incDisabledProducts($count)
            ;
            $this->updateHistoryAndFlush($history);
        }

        $history->setProgress(ImportHistory::PROGRESS_DISABLE_PRODUCTS_END);
        $this->updateHistoryAndFlush($history);

        $this->logger->info('Disabled ' . $history->getDisabledProducts() . ' outdated products');
    }

    private function deleteShopCategories(ImportHistory $history, $existIds)
    {
        $this->logger->info('Delete old categories');

        $disabledIds = $this->getDocumentManager()->getDocumentCollection(ShopCategory::class)
            ->find([
                '_id' => ['$nin' => $existIds],
                'shop' => $history->getShop()->getId()
            ], ['_id' => true])
            ->sort(['level' => -1, 'left' => -1])
            ->toArray()
        ;

        $dIds = array_keys($disabledIds);

        $this->logger->info(sprintf('Found %d old categories', count($dIds)));

        if (!$dIds) {
            return;
        }

        $productCollection = $this
            ->getProductRepository()
            ->getCollection()
        ;

        foreach ($dIds as $id) {
            $result = $productCollection
                ->find([
                    'shopCategory' => $id,
                    'enabled' => true
                ], ['_id' => true])
                ->toArray()
            ;
            $ids = array_keys($result);

            if (!$ids) {
                break;
            }

            $this->getElasticManager()->deleteProductsFromIndex($ids);

            $result = $productCollection->remove(['_id' => ['$in' => $ids]]);

            $history
                ->incDisabledProducts($result['n'])
                ->incIndexDeleteProducts(count($ids))
            ;
            $this->updateHistoryAndFlush($history);
        }

        $this->logger->info('Remove old categories from Database');

        $dm = $this->getDocumentManager();
        foreach ($dIds as $dId) {
            $sCat = $dm->getRepository(ShopCategory::class)->find((string)$dId);

            if (!$sCat) {
                continue;
            }

            $this->logger->info(sprintf('Remove category %s', $sCat->getName()));

            $dm->remove($sCat);
            $dm->flush();
        }

        $dm->clear(ShopCategory::class);
        $dm->clear(Product::class);
    }

    private function updateShopCategoriesProductsCount(OutputInterface $output, ImportHistory $history)
    {
        $this->logger->info('Run script: shop:categories:update_products_count');
        $this->runCommand('shop:categories:update_products_count', [
            'shop_id' => [$history->getShop()->getId()],
            '--env' => $this->getContainer()->get('kernel')->getEnvironment()
        ], $output);
    }

    private function updateShopProductsCount(ImportHistory $history)
    {
        $this->logger->info('Update shop products count');

        $shop = $history->getShop();

        $searchRequest = $this->getSearchRequestFactory()->createShopSearchRequest($shop);
        $count = $this->getSearchManager()->countProducts($searchRequest);

        $total = $this
            ->getProductRepository()
            ->getCollection()
            ->count(['shop' => $shop->getId()])
        ;

        $shop
            ->setProductsCount($total)
            ->setProductsCountFromIndex($count)
        ;

        $dm = $this->getDocumentManager();
        $dm->persist($shop);
        $dm->flush();

        $this->logger->info(sprintf('Products in shop %s: %d/%d', $shop->getName(), $count, $total));
    }

    private function getXPath(Offer $offer, $xpath)
    {
        if (!$xpath) {
            return null;
        }

        $params = $offer->getXml()->xpath($xpath);

        if (!$params) {
            return null;
        }

        return (string)$params[0];
    }
}
