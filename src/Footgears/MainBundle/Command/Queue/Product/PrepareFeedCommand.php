<?php

namespace Footgears\MainBundle\Command\Queue\Product;

use Footgears\MainBundle\Document\ImportHistory;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Shop\ShopHelper;
use Footgears\MainBundle\YandexMarketLanguage\YooxParser;
use Sirian\YandexMarketLanguage\Category;
use Sirian\YMLParser\Parser\CategoriesEvent;

class PrepareFeedCommand extends ProductCommand
{
    protected function doHandleRow(ImportHistory $history)
    {
        $shop = $history->getShop();
        $this->logger->info(sprintf('Prepare xml for shop %d %s (History: %d)', $shop->getId(), $shop->getName(), $history->getId()));

        $history->setProgress(ImportHistory::PROGRESS_PREPARE_FEED);
        $this->updateHistoryAndFlush($history);

        $this->prepareFeed($history);

        $history->setProgress(ImportHistory::PROGRESS_WAIT_PARSING);
        $this->updateHistoryAndFlush($history);

        $this->logger->info('Prepare finished');
    }

    protected function loadBatch()
    {
        $collection = $this->getImportHistoryRepository()->getCollection();

        $history = $collection->findAndUpdate([
            'status' => ImportHistory::STATUS_ACTIVE,
            'progress' => ImportHistory::PROGRESS_WAIT_PREPARE_FEED
        ], [
            '$set' => [
                'progress' => ImportHistory::PROGRESS_PREPARE_FEED_START
            ]
        ]);

        if (!$history) {
            return [];
        }

        return [$history['_id']];
    }

    private function prepareFeed(ImportHistory $history)
    {
        $shop = $history->getShop();

        $parser = ShopHelper::createParser($shop);

        if (!$parser->isNeedPrepareFeed()) {
            $this->logger->info(sprintf('Shop %s feed in not need prepare', $shop->getName()));
            return;
        }

        if ($parser instanceof YooxParser) {
            $this->prepareYooxFeed($shop, $parser);
        }
    }

    private function prepareYooxFeed(Shop $shop, YooxParser $parser)
    {
        // нужно пересобрать xml с правильными категориями
        $this->logger->info('Prepare categories');

        $filename = $this->getXmlFileName($shop);

        $parser->addListener('prepareCategories', function (CategoriesEvent $categoriesEvent) use ($shop, &$filename) {
            // собрали категории у товаров
            // добавляем их в xml
            // и сохраняем файл
            $parsedCategories = $categoriesEvent->getCategories();

            $xmlReader = new \XMLReader();
            $xmlReader->open($filename);

            $xmlWriter = new \XMLWriter();
            $xmlWriter->openUri($this->getXmlFileNameModify($shop));
            $xmlWriter->startDocument('1.0', 'UTF-8');
            $xmlWriter->setIndent(true);
            $xmlWriter->setIndentString('    ');

            $this->logger->info(sprintf('save %d categories', count($parsedCategories)));

            $path = [];
            while ($xmlReader->read()) {

                switch ($xmlReader->nodeType) {
                    case \XMLReader::ATTRIBUTE:
                        dump('attribute'); die;
                        break;

                    case \XMLReader::ELEMENT:

                        array_push($path, $xmlReader->name);

                        $xmlWriter->startElement($xmlReader->name);

                        if ($xmlReader->hasAttributes) {
                            while($xmlReader->moveToNextAttribute()) {
                                $xmlWriter->writeAttribute($xmlReader->name, $xmlReader->value);
                            }
                            $xmlReader->moveToElement();
                        }

                        if ($xmlReader->isEmptyElement) {
                            $xmlWriter->endElement();
                            array_pop($path);
                        }
                        break;

                    case \XMLReader::TEXT:
                        $xmlWriter->text($xmlReader->value);
                        break;

                    case \XMLReader::CDATA:
                        $xmlWriter->writeCdata($xmlReader->value);
                        break;

                    case \XMLReader::ENTITY_REF:
                        dump('entity_ref'); die;
                        break;

                    case \XMLReader::ENTITY:
                        dump('entity'); die;
                        break;

                    case \XMLReader::DOC:
                        dump('doc'); die;
                        break;

                    case \XMLReader::END_ELEMENT:
                        if (implode('/', $path) == 'yml_catalog/shop/categories') {
                            $this->addCategories($xmlWriter, $parsedCategories);
                        }

                        $xmlWriter->endElement();
                        array_pop($path);
                        break;

                    case \XMLReader::END_ENTITY:
                        dump('entity'); die;
                        break;
                }
            }
            $xmlReader->close();

            $this->logger->info('Additional categories saved');
        });

        $parser->prepareCategories($filename);
    }

    private function addCategories(\XMLWriter $xmlWriter, $categories)
    {
        /** @var Category[] $categories */
        foreach ($categories as $category) {
            $xmlWriter->startElement('category');
            $xmlWriter->writeAttribute('id', $category->getId());

            if ($category->getParent()) {
                $xmlWriter->writeAttribute('parentId', $category->getParent()->getId());
            }

            $xmlWriter->text($category->getName());
            $xmlWriter->endElement();
        }
    }
}
