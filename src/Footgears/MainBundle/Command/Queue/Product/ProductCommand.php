<?php

namespace Footgears\MainBundle\Command\Queue\Product;

use Footgears\MainBundle\Command\Queue\AbstractQueueCommand;
use Footgears\MainBundle\Document\ImportHistory;
use Footgears\MainBundle\Document\Shop;

abstract class ProductCommand extends AbstractQueueCommand
{
    protected $sleepInterval = 10000000;

    protected function handleRow($data)
    {
        $dm = $this->getDocumentManager();
        $dm->clear();

        /* @var ImportHistory $history */
        $history = $this->getImportHistoryRepository()->find($data);

        try {
            $this->doHandleRow($history);
        } catch (\Exception $e) {
            $history
                ->setStatus(ImportHistory::STATUS_EXCEPTION)
                ->setException($e->getMessage())
            ;
            $dm->persist($history);

            $this->logger->error($history->getShop()->getName() . ': ' . $e->getMessage());
        }

        $dm->flush();
        $this->clear();
    }

    abstract protected function doHandleRow(ImportHistory $history);

    protected function getXmlFileName(Shop $shop)
    {
        $prefix = $this->getContainer()->getParameter('mongo_database');

        return '/var/tmp/' . $prefix . '_shop_' . $shop->getId() . '.xml';
    }

    protected function getXmlFileNameModify(Shop $shop)
    {
        $prefix = $this->getContainer()->getParameter('mongo_database');
        return sprintf('/var/tmp/' . $prefix . '_shop_%s_modify.xml', $shop->getId());
    }

    protected function updateHistoryAndFlush(ImportHistory $history)
    {
        $history
            ->setUpdateDate(new \DateTime())
        ;

        $shop = $history->getShop();
        $shop
            ->getImportProductsSettings()
            ->setLastImport(new \DateTime())
            ->setForce(false)
        ;

        $dm = $this->getDocumentManager();
        $dm->persist($history);
        $dm->persist($shop);
        $dm->flush();
    }

    protected function clear()
    {
        $dm = $this->getDocumentManager();
        $dm->clear();
        $count = gc_collect_cycles();

        $this->logger->info(sprintf('gc_collect_cycles = %d', $count));
    }
}
