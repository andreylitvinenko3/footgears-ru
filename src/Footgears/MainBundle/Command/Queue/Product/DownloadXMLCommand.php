<?php

namespace Footgears\MainBundle\Command\Queue\Product;

use Footgears\MainBundle\Document\ImportHistory;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\Filesystem\Filesystem;

class DownloadXMLCommand extends ProductCommand
{
    /**
     * @param ImportHistory $history
     * @throws \Exception
     */
    protected function doHandleRow(ImportHistory $history)
    {
        $shop = $history->getShop();
        $this->logger->info(sprintf('Download xml for shop %d %s (History: %d)', $shop->getId(), $shop->getName(), $history->getId()));

        $history->setProgress(ImportHistory::PROGRESS_DOWNLOAD);
        $this->updateHistoryAndFlush($history);

        $filename = $this->getXmlFileName($shop);

        $download = true;
        if (file_exists($filename) && time() - filemtime($filename) < 3 * 60 * 60) {
            try {
                $this->validateXMLFile($filename);
                $download = false;
            } catch (\Exception $e) {

            }
        }

        if ($download) {
            $this->downloadXml($history, $filename);
            $this->validateXMLFile($filename);
        }

        $this->preProcess($filename);

        $history->setProgress(ImportHistory::PROGRESS_WAIT_PREPARE_FEED);
        $this->updateHistoryAndFlush($history);

        $this->logger->info('Download finished');
    }

    /**
     * @param ImportHistory $history
     * @param $filename
     * @throws \Exception
     */
    protected function downloadXml(ImportHistory $history, $filename)
    {
        $this->logger->info(sprintf('Downloading "%s" to "%s" ', $history->getUrl(), $filename));

        $request = new Request('GET', $history->getUrl());

        $downloadBytesInterval = 1024 * 1024 * 5;
        $lastDownloadBytes = 0;

        $client = new Client();
        $response = $client->send($request, [
            'sink' => $filename,
            'timeout' => 3 * 3600,
            'http_errors' => false,
            'progress' => function ($downloadTotal, $downloadedBytes, $uploadTotal, $uploadedBytes) use ($downloadBytesInterval, &$lastDownloadBytes, &$history) {
                if ($downloadedBytes > $lastDownloadBytes + $downloadBytesInterval) {
                    $lastDownloadBytes = $downloadedBytes;

                    $this->logger->info(sprintf('Downloaded %.2fMb', $downloadedBytes / 1024 / 1024));

                    $history->setFileSize($downloadedBytes);
                    $this->updateHistoryAndFlush($history);
                }

            }
        ]);

        if ($response->getStatusCode() != 200) {
            unlink($filename);
            throw new \Exception('HTTP status code is ' . $response->getStatusCode());
        }

        $history->setFileSize(filesize($filename));
        $this->updateHistoryAndFlush($history);
    }

    /**
     * @param $filename
     * @throws \Exception
     */
    protected function validateXMLFile($filename)
    {
        $this->logger->info(sprintf('Validating xml "%s"', $filename));

        if (!file_exists($filename)) {
            throw new \Exception(sprintf('XML file "%s" not found', $filename));
        }

        $xml_parser = xml_parser_create();
        if (!($fp = fopen($filename, "r"))) {
            throw new \Exception(sprintf('Could not open XML file "%s"', $filename));
        }

        while (true) {
            $data = fread($fp, 4096);
            if (!xml_parse($xml_parser, $data, feof($fp))) {
                $fileNameBackup = sprintf('%s.%s', $filename, date('YmdHis'));

                $filesystem = new Filesystem();
                $filesystem->rename($filename, $fileNameBackup, true);

                throw new \Exception(sprintf('XML file "%s" is invalid. Line: %d. Error: %s', $fileNameBackup, xml_get_current_line_number($xml_parser), xml_error_string(xml_get_error_code($xml_parser))));
            }
            if (feof($fp)) {
                break;
            }
        }

        xml_parser_free($xml_parser);
    }

    private function preProcess($filename)
    {
        $tmp = $filename . '.tmp';
        $f1 = fopen($filename, 'r');
        $f2 = fopen($tmp, 'w');
        $buffer = '';
        while (!feof($f1)) {
            $buffer .= fgets($f1, 4096);

            $pos = strrpos($buffer, '>');

            if (false !== $pos) {
                $data = substr($buffer, 0, $pos + 1);

                $data = strtr($data, ['<shops>' => '', '</shops>' => '']);
                fputs($f2, $data);
                $buffer = substr($buffer, $pos + 1);
            }
        }
        fclose($f1);
        fclose($f2);

        rename($tmp, $filename);
    }

    protected function loadBatch()
    {
        $collection = $this->getImportHistoryRepository()->getCollection();

        $history = $collection->findAndUpdate([
            'status' => ImportHistory::STATUS_ACTIVE,
            'progress' => ImportHistory::PROGRESS_WAIT_DOWNLOAD
        ], [
            '$set' => [
                'progress' => ImportHistory::PROGRESS_DOWNLOAD_START
            ]
        ]);

        if (!$history) {
            return [];
        }

        return [$history['_id']];
    }
}
