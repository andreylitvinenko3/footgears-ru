<?php

namespace Footgears\MainBundle\Command\Queue;

use Footgears\MainBundle\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractQueueCommand extends BaseCommand
{
    protected $lastPrepareTime;
    protected $prepareInterval = 60;
    protected $lastFlushTime;
    protected $flushInterval = 180;
    protected $needFlush = false;
    protected $batchSize = 500;
    protected $prepared = false;
    protected $sleepInterval = 20000;
    protected $maxRunningTime = 3600;
    protected $handled = 0;

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->catchSignals();

        $this->lastFlushTime = time();
        $start = time();
        while (true) {
            if ($this->stopSignal) {
                $this->flush();
                break;
            }

            if (memory_get_usage() > $this->getMemoryLimit()) {
                $this->logger->warning('Memory usage is too high');
                $this->flush();
                return -1;
            }

            if (time() - $start > $this->maxRunningTime) {
                $this->logger->warning('Max running time reached');
                $this->flush();
                return -1;
            }

            $this->handle();
        }

        return 0;
    }

    protected function prepare()
    {
        $this->logger->info('Prepare');
        $this->prepared = true;
        $this->lastPrepareTime = time();
        $this->doPrepare();
    }

    protected function doPrepare()
    {

    }

    abstract protected function loadBatch();

    public function handle()
    {
        $rows = $this->loadBatch();

        if ($rows) {
            $this->handleBatch($rows);
            $this->handled += count($rows);
            $this->logger->debug(sprintf('Handled: %d. Memory: %.2f', $this->handled, memory_get_usage() / 1024 / 1024));
        }

        if (time() - $this->lastFlushTime >= $this->flushInterval) {
            $this->flush();
        }

        if (count($rows) < $this->batchSize) {
            usleep($this->sleepInterval);
        }
    }

    public function flush()
    {
        if (!$this->needFlush) {
            return;
        }
        $this->needFlush = false;

        $start = microtime(true);
        $logger = $this->logger;
        $logger->info(sprintf('Flush started. Memory: %.2f', memory_get_usage() / 1024 / 1024));
        $this->doFlush();
        $logger->info(sprintf('Flush finished in %.3fs. Memory: %.2f', microtime(true) - $start, memory_get_usage() / 1024 / 1024));
        $this->lastFlushTime = time();
    }

    protected function doFlush()
    {

    }

    protected function handleBatch($rows)
    {
        if (!$this->prepared || time() - $this->lastPrepareTime > $this->prepareInterval) {
            $this->prepare();
        }
        $this->needFlush = true;

        $this->handleRows($rows);
    }

    protected function handleRows($rows)
    {
        foreach ($rows as $row) {
            try {
                $data = $this->unserialize($row);

                $this->handleRow($data);
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
        }
    }

    protected function handleRow($data)
    {

    }

    protected function unserialize($row)
    {
        return $row;
    }

    public function getMemoryLimit()
    {
        return 1000 * 1024 * 1024;
    }
}
