<?php

namespace Footgears\MainBundle\Command\OneStartCommands;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Attribute;
use Footgears\MainBundle\Document\AttributeValue;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetAttributesCommand extends BaseCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('one:set-attributes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getDocumentManager();

        $attributes = $this->getAttributeManager()->getAttributes();
        ksort($attributes);

        foreach ($attributes as $attribute) {
            $attributeDB = new Attribute();

            $attributeDB
                ->setId($attribute->getFirstId())
                ->setName($attribute->getName())
                ->setAliases($attribute->getAliases())
                ->setSort($attribute->getSort())
            ;

            foreach ($attribute->getValues() as $value) {
                $valueDB = new AttributeValue();

                $valueDB
                    ->setId($value->getId())
                    ->setLabel($value->getLabel())
                    ->setParameters($value->getParameters())
                ;

                $attributeDB->addValue($valueDB);
            }

            $em->persist($attributeDB);
        }

        $em->flush();
    }

}