<?php

namespace Footgears\MainBundle\Command\Tag;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\DBAL\Type\ProductsFilterPageType;
use Footgears\MainBundle\Document\Attribute;
use Footgears\MainBundle\Document\AttributeValue;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Entity\Category;
use Footgears\MainBundle\Entity\ProductsFilterPage;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateTagsCommand extends BaseCommand
{
    /**
     * @var array|Attribute[]
     */
    protected $attributes;

    protected $group1 = [297, 323, 326, 351, 358, 429, 458, 484, 505, 567, 570]; // красное
    protected $group2 = [300, 310, 318, 319, 361, 362, 364, 79, 431, 438, 91, 277, 486, 564]; // красная
    protected $group3 = [312, 355, 440]; // красный

    protected function configure()
    {
        $this
            ->addArgument('category_id', InputArgument::OPTIONAL, 'Category ID')
            ->addOption('parent', 'p', InputOption::VALUE_OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var NestedTreeRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);

        $categories = [];
        if ($input->getArgument('category_id')) {
            $categories[] = $categoryRepository->find($input->getArgument('category_id'));
        } elseif ($input->getOption('parent')) {
            /** @var Category $mainCategory */
            $mainCategory = $categoryRepository->find($input->getOption('parent'));

            if (!$mainCategory) {
                throw new \Exception('Category not found');
            }

            /** @var Category[] $categories */
            $categories = $categoryRepository->getChildren($mainCategory);
        }

        $categoriesCount = count($categories);

        $this->logger->info(sprintf('Found %d categories to create filter pages', $categoriesCount));

        if (!$categoriesCount) {
            return;
        }

        $this->attributes = $this->getAttributesService()->getAttributes();

        $i = 0;
        /** @var Category[] $categories */
        foreach ($categories as $category) {
            $tags = $this->createCategoryTags($category);

            $this->logger->info(sprintf('[%d/%d] %s: added %d pages', ++$i, $categoriesCount, $category->getName(), $tags));
        }
    }

    /**
     * @param Category $category
     * @return int
     */
    public function createCategoryTags(Category $category)
    {
        $pagesCount = 0;

        $em = $this->getDoctrine()->getManager();

        foreach ($this->attributes as $attribute) {

            if (!$attribute->isEnabled()) {
                $this->logger->debug(sprintf('Skip %s - %s: attribute disabled', $category->getName(), $attribute->getName()));
                continue;
            }

            if (!$this->acceptedCategoryFilterPage($category, $attribute)) {
                $this->logger->debug(sprintf('Skip %s - %s: not accepted', $category->getName(), $attribute->getName()));
                continue;
            }

            foreach ($attribute->getValues() as $value) {

                if ($this->isExistPage($category, $attribute, $value)) {
                    $this->logger->debug(sprintf('Skipped %s - %s - %s: page exist', $category->getName(), $attribute->getName(), $value->getLabel()));
                    continue;
                }

                $name = $this->getPageName($attribute, $value, $category);

                if (!$name) {
                    $this->logger->debug(sprintf('Skipped %s - %s - %s: not name', $category->getName(), $attribute->getName(), $value->getLabel()));
                    continue;
                }

                $alias = $this->getPageAlias($attribute, $value);

                if (!$alias) {
                    $this->logger->debug(sprintf('Skipped %s - %s - %s: not alias', $category->getName(), $attribute->getName(), $value->getLabel()));
                    continue;
                }

                $this->logger->info(sprintf('%s - %s', $category->getPathName(), $name));

                // формируем объект страницы
                $page = new ProductsFilterPage();
                $page
                    ->setApproved(true)
                    ->setShowOnlyAdmins(false)
                    ->setCategory($category)
                    ->setAttributes([$value->getId()])
                    ->setType(ProductsFilterPageType::FILTER)
                    ->setTitle($name)
                    ->setH1Title($this->ucfirst($name))
                    ->setAlias($alias)
                    ->setSeoDescription($this->getRandomSeoDescriptionString($category))
                    ->setSeoTitle($this->getRandomSeoTitleString())
                ;
                $em->persist($page);

//                dump($page); die;

                $pagesCount++;
            }
        }
        $em->flush();

        return $pagesCount;
    }

    protected function isExistPage(Category $category, Attribute $attribute, AttributeValue $value)
    {
        $count = $this
//            ->getDocumentManager()
//            ->getRepository(Tag::class)
//            ->getCollection()
//            ->count([
//                'type' => Tag::TYPE_FILTER,
//                'attributes' => $attribute->getId()
//            ])
            ->getDoctrine()
            ->getRepository(ProductsFilterPage::class)
            ->createQueryBuilder('pfp')
            ->select('COUNT(pfp)')
            ->where('pfp.category = :category')
            ->andWhere('pfp.type = :type')
            ->andWhere('pfp.attributes = :attributes')
            ->setParameter('category', $category)
            ->setParameter('type', ProductsFilterPageType::FILTER)
            ->setParameter('attributes', [$value->getId()])
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return !!$count;
    }

    protected function getPageName(Attribute $attribute, AttributeValue $value, Category $category)
    {
        $categoryName = $category->getName();

        switch ($attribute->getId()) {
            case Attribute::COLOR:
                $name = $this->getColorAttributeTitle($value, $category) . ' ' . $categoryName;
                break;

            case Attribute::CONTENT:
                $name = $this->getContentTitle($value, $category);
                break;

            case Attribute::HEEL_HEIGHT:
                $name = $this->getHeelTypeTitle($value, $category);
                break;

            case Attribute::SEAZON:
                $name = $this->getSeazonTitle($value, $category);
                break;

            case Attribute::MANUFACTURER:
                $name = $this->getManufacturerTitle($value, $categoryName);
                break;

            default:
                $name = '';
                break;
        }

        return mb_strtolower($name);
    }

    protected function getHeelTypeTitle(AttributeValue $value, Category $category)
    {
        switch ($value->getLabel()) {
            case 'До 3 сантиметров':
                $title = $category->getName() . ' на каблуке ниже 3 см';
                break;

            case '3 - 5 см':
                $title = $category->getName() . ' на низком каблуке';
                break;

            case '5 - 7 см':
                $title = $category->getName() . ' на среднем каблуке';
                break;

            case '7 - 10 см':
                $title = $category->getName() . ' на высоком каблуке';
                break;

            case 'От 10 сантиметров':
                $title = $category->getName() . ' на каблуке выше 10 см';
                break;

            default:
                $title = '';
                break;
        }

        return $title;
    }

    protected function getColorAttributeTitle(AttributeValue $value, Category $category)
    {
        switch ($value->getLabel()) {
            case 'синий':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'синее';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'синяя';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'синий';
                } else {
                    $title = 'синие';
                }
                break;

            case 'камуфляж':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'камуфляжное';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'камуфляжная';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'камуфляжный';
                } else {
                    $title = 'камуфляжные';
                }
                break;

            default:
                if (in_array($category->getId(), $this->group1)) {
                    $title = preg_replace('/(.{2})$/iu', 'ое', $value->getLabel());
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = preg_replace('/(.{2})$/iu', 'ая', $value->getLabel());
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = preg_replace('/(.{2})$/iu', 'ый', $value->getLabel());
                } else {
                    $title = preg_replace('/(.{2})$/iu', 'ые', $value->getLabel());
                }

        }

        return $title;
    }

    protected function getContentTitle(AttributeValue $value, Category $category)
    {
        $categoryName = $category->getName();

        switch ($value->getLabel()) {
            case 'Хлопок':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'хлопковое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'хлопковая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'хлопковый';
                } else {
                    $title = 'хлопковые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Кожа':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'кожаное';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'кожаная';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'коженый';
                } else {
                    $title = 'кожаные';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Кашемир':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'кашемировое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'кашемировая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'кашемировый';
                } else {
                    $title = 'кашемировые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Мех':
                $title = $categoryName . ' с мехом';
                break;

            case 'Лайкра':
                $title = $categoryName . ' из лайкры';
                break;

            case 'Искусственная кожа':
                $title = $categoryName . ' из искусственной кожи';
                break;

            case 'Искусственная замша':
                $title = $categoryName . ' из искусственной замши';
                break;

            case 'Текстиль':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'текстильное';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'текстильная';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'текстильный';
                } else {
                    $title = 'текстильные';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Муслин':
                $title = $categoryName . ' из муслина';
                break;

            case 'Атлас':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'атласное';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'атласная';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'атласный';
                } else {
                    $title = 'атласные';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Лаковая кожа':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'лаковое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'лаковая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'лаковый';
                } else {
                    $title = 'лаковые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Твил':
                $title = $categoryName . ' из твила';
                break;

            case 'Гипюр':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'гипюровое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'гипюровая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'гипюровый';
                } else {
                    $title = 'гипюровые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Неопрен':
                $title = $categoryName . ' из неопрена';
                break;

            case 'Пух':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'пуховое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'пуховая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'пуховый';
                } else {
                    $title = 'пуховые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Шерсть':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'шерстяное';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'шерстяная';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'шерстяной';
                } else {
                    $title = 'шерстяные';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Эластан':
                $title = $categoryName . ' с эластаном';
                break;

            case 'Акрил':
                $title = $categoryName . ' с акрилом';
                break;

            case 'Резина':
                $title = $categoryName . ' c резиной';
                break;

            case 'Вискоза':
                $title = $categoryName . ' из вискозы';
                break;

            case 'Лен':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'льняное';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'льняная';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'льняной';
                } else {
                    $title = 'льняные';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Альпака':
                $title = $categoryName . ' из альпаки';
                break;

            case 'Шелк':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'шелковое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'шелковая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'шелковый';
                } else {
                    $title = 'шелковые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Тенсел':
                $title = $categoryName . ' из тенсела';
                break;

            case 'Замша':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'замшевое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'замшевая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'замшевый';
                } else {
                    $title = 'замшевые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Нубук':
                $title = $categoryName . ' из нубука';
                break;

            case 'Овчина':
                $title = $categoryName . ' из овчины';
                break;

            case 'Ангора':
                $title = $categoryName . ' из ангоры';
                break;

            case 'Шифон':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'шифоновое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'шифоновая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'шифоновый';
                } else {
                    $title = 'шифоновые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Фланель':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'фланелевое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'фланелевая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'фланелевый';
                } else {
                    $title = 'фланелевые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Вельвет':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'вельветовое';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'вельветовая';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'вельветовый';
                } else {
                    $title = 'вельветовые';
                }

                $title = $title . ' ' . $categoryName;
                break;

            default:
                $title = '';
                break;
        }

        return $title;
    }

    protected function getManufacturerTitle(AttributeValue $value, $categoryName)
    {
        switch ($value->getLabel()) {
            case 'Россия':
                $title = $categoryName . ' из России';
                break;

            case 'Китай':
                $title = $categoryName . ' из Китая';
                break;

            case 'Италия':
                $title = $categoryName . ' из Италии';
                break;

            case 'Болгария':
                $title = $categoryName . ' из Болгарии';
                break;

            case 'Индия':
                $title = $categoryName . ' из Индии';
                break;

            case 'США':
                $title = $categoryName . ' из США';
                break;

            case 'Турция':
                $title = $categoryName . ' из Турции';
                break;

            case 'Германия':
                $title = $categoryName . ' из Германии';
                break;

            case 'Франция':
                $title = $categoryName . ' из Франции';
                break;

            case 'Испания':
                $title = $categoryName . ' из Испании';
                break;

            case 'Португалия':
                $title = $categoryName . ' из Португалии';
                break;

            case 'Пакистан':
                $title = $categoryName . ' из Пакистана';
                break;

            case 'Таиланд':
                $title = $categoryName . ' из Таиланда';
                break;

            case 'Индонезия':
                $title = $categoryName . ' из Индонезии';
                break;

            case 'Швеция':
                $title = $categoryName . ' из Швеции';
                break;

            case 'Украина':
                $title = $categoryName . ' из Украины';
                break;

            case 'Англия':
                $title = $categoryName . ' из Англии';
                break;

            case 'Беларусь':
                $title = $categoryName . ' из Беларуси';
                break;

            case 'Польша':
                $title = $categoryName . ' из Польши';
                break;

            case 'Тайвань':
                $title = $categoryName . ' из Тайвани';
                break;

            case 'Малайзия':
                $title = $categoryName . ' из Малайзии';
                break;

            case 'Эстония':
                $title = $categoryName . ' из Эстонии';
                break;

            case 'Финляндия':
                $title = $categoryName . ' из финляндии';
                break;

            case 'Мексика':
                $title = $categoryName . ' из Мексики';
                break;

            default:
                $title = '';
                break;
        }

        return $title;
    }

    protected function getSeazonTitle(AttributeValue $value, Category $category)
    {
        $categoryName = $category->getName();

        switch ($value->getLabel()) {
            case 'Весна/Осень':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'демисезонное';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'демисезонная';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'демисезонный';
                } else {
                    $title = 'демисезонные';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Лето':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'летнее';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'летняя';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'летний';
                } else {
                    $title = 'летние';
                }

                $title = $title . ' ' . $categoryName;
                break;

            case 'Зима':
                if (in_array($category->getId(), $this->group1)) {
                    $title = 'зимнее';
                } elseif (in_array($category->getId(), $this->group2)) {
                    $title = 'зимняя';
                } elseif (in_array($category->getId(), $this->group3)) {
                    $title = 'зимний';
                } else {
                    $title = 'зимние';
                }

                $title = $title . ' ' . $categoryName;
                break;

            default:
                $title = '';
                break;
        }

        return $title;
    }

    private function acceptedCategoryFilterPage(Category $category, Attribute $attribute)
    {
        $result = true;
        if ($attribute->getId() == Attribute::YEAR) {
            $result = false;
        } else {
            switch ($this->searchRootCategoryId($category)) {
                case 25: // ж обувь
                case 181: // обувь для девочек
                case 18: // м обувь
                case 182: // обувь для мальчиков
                    $attributes = [Attribute::COLOR, Attribute::CONTENT, Attribute::HEEL_HEIGHT, Attribute::MANUFACTURER, Attribute::SEAZON];
                break;

                case 296: // ж одежда
                case 428: // м одежда
                case 546: // детская ж одежда
                case 483: // детская м одежда
                    $attributes = [Attribute::COLOR, Attribute::CONTENT, Attribute::MANUFACTURER, Attribute::SEAZON];
                break;

                case 371: // ж аксессуары
                case 462: // м аксессуары
                case 548: // детские ж аксессуары
                case 500: // детские м аксессуары
                    $attributes = [Attribute::COLOR, Attribute::CONTENT, Attribute::MANUFACTURER];
                    break;

                default:
                    $attributes = [];
                    break;
            }

            if (!in_array($attribute->getId(), $attributes)) {
                $result = false;
            }
        }

        return $result;
    }

    private function getPageAlias(Attribute $attribute, AttributeValue $value)
    {
        $aliases = [
            'Высота каблука 3 - 5 см' => 'low-heel',
            'Высота каблука 5 - 7 см' => 'middle-heel',
            'Высота каблука 7 - 10 см' => 'high-heel',
            'Высота каблука До 3 сантиметров' => 'heel-up-to-3inch',
            'Высота каблука От 10 сантиметров' => 'heel-above-10inch',
//            'Год 2010' => 'year-2010',
//            'Год 2011' => 'year-2011',
//            'Год 2012' => 'year-2012',
//            'Год 2013' => 'year-2013',
//            'Год 2014' => 'year-2014',
//            'Год 2015' => 'year-2015',
//            'Размер женской обуви 30' => 'woman-shoes-size-30',
//            'Размер женской обуви 31' => 'woman-shoes-size-31',
//            'Размер женской обуви 32' => 'woman-shoes-size-32',
//            'Размер женской обуви 33' => 'woman-shoes-size-33',
//            'Размер женской обуви 34' => 'woman-shoes-size-34',
//            'Размер женской обуви 35' => 'woman-shoes-size-35',
//            'Размер женской обуви 36' => 'woman-shoes-size-36',
//            'Размер женской обуви 37' => 'woman-shoes-size-37',
//            'Размер женской обуви 38' => 'woman-shoes-size-38',
//            'Размер женской обуви 39' => 'woman-shoes-size-39',
//            'Размер женской обуви 40' => 'woman-shoes-size-40',
//            'Размер женской обуви 41' => 'woman-shoes-size-41',
//            'Размер женской обуви 42' => 'woman-shoes-size-42',
//            'Размер женской обуви 43' => 'woman-shoes-size-43',
//            'Размер женской обуви 44' => 'woman-shoes-size-44',
//            'Размер женской обуви 45' => 'woman-shoes-size-45',
//            'Размер женской одежды 38' => 'woman-clothes-size-38',
//            'Размер женской одежды 40' => 'woman-clothes-size-40',
//            'Размер женской одежды 42' => 'woman-clothes-size-42',
//            'Размер женской одежды 44' => 'woman-clothes-size-44',
//            'Размер женской одежды 46' => 'woman-clothes-size-46',
//            'Размер женской одежды 48' => 'woman-clothes-size-48',
//            'Размер женской одежды 50' => 'woman-clothes-size-50',
//            'Размер женской одежды 52' => 'woman-clothes-size-52',
//            'Размер женской одежды 54' => 'woman-clothes-size-54',
//            'Размер женской одежды 56' => 'woman-clothes-size-56',
//            'Размер женской одежды 58' => 'woman-clothes-size-58',
//            'Размер женской одежды 60' => 'woman-clothes-size-60',
//            'Размер женской одежды 62' => 'woman-clothes-size-62',
//            'Размер мужской обуви 39' => 'men-shoes-size-39',
//            'Размер мужской обуви 40' => 'men-shoes-size-40',
//            'Размер мужской обуви 41' => 'men-shoes-size-41',
//            'Размер мужской обуви 42' => 'men-shoes-size-42',
//            'Размер мужской обуви 43' => 'men-shoes-size-43',
//            'Размер мужской обуви 44' => 'men-shoes-size-44',
//            'Размер мужской обуви 45' => 'men-shoes-size-45',
//            'Размер мужской обуви 46' => 'men-shoes-size-46',
//            'Размер мужской обуви 47' => 'men-shoes-size-47',
//            'Размер мужской обуви 48' => 'men-shoes-size-48',
//            'Размер мужской одежды 38' => 'men-clothes-size-38',
//            'Размер мужской одежды 40' => 'men-clothes-size-40',
//            'Размер мужской одежды 42' => 'men-clothes-size-42',
//            'Размер мужской одежды 44' => 'men-clothes-size-44',
//            'Размер мужской одежды 46' => 'men-clothes-size-46',
//            'Размер мужской одежды 48' => 'men-clothes-size-48',
//            'Размер мужской одежды 50' => 'men-clothes-size-50',
//            'Размер мужской одежды 52' => 'men-clothes-size-52',
//            'Размер мужской одежды 54' => 'men-clothes-size-54',
//            'Размер мужской одежды 56' => 'men-clothes-size-56',
//            'Размер мужской одежды 58' => 'men-clothes-size-58',
//            'Размер мужской одежды 60' => 'men-clothes-size-60',
//            'Размер мужской одежды 62' => 'men-clothes-size-62',
//            'Размер мужской одежды 64' => 'men-clothes-size-64',
//            'Размер мужской одежды 66' => 'men-clothes-size-66',
//            'Размер мужской одежды 68' => 'men-clothes-size-68',
//            'Размер мужской одежды 70' => 'men-clothes-size-70',
            'Сезон Весна/Осень' => 'autumn-spring-season',
            'Сезон Зима' => 'winter-season',
            'Сезон Лето' => 'summer-season',
            'Состав Акрил' => 'consist-acrylic',
            'Состав Альпака' => 'consist-alpaca',
            'Состав Ангора' => 'consist-angora',
            'Состав Атлас' => 'consist-atlas',
            'Состав Вельвет' => 'consist-velvet',
            'Состав Вискоза' => 'consist-viscose',
            'Состав Гипюр' => 'consist-guipure',
            'Состав Джерси' => 'consist-jersey',
            'Состав Замша' => 'consist-suede',
            'Состав Искусственная замша' => 'consist-fake-suede',
            'Состав Искусственная кожа' => 'consist-fake-leather',
            'Состав Кашемир' => 'consist-cashmere',
            'Состав Кожа' => 'consist-leather',
            'Состав Лайкра' => 'consist-lycra',
            'Состав Лаковая кожа' => 'consist-patent-leather',
            'Состав Лен' => 'consist-flax',
            'Состав Мех' => 'consist-fur',
            'Состав Муслин' => 'consist-muslin',
            'Состав Нейлон' => 'consist-nylon',
            'Состав Неопрен' => 'consist-neoprene',
            'Состав Нубук' => 'consist-nubuck',
            'Состав Овчина' => 'consist-sheepskin',
            'Состав ПВХ' => 'consist-pvc',
            'Состав Перо' => 'consist-feather',
            'Состав Полиамид' => 'consist-polyamide',
            'Состав Полиуретан' => 'consist-polyurethane',
            'Состав Полиэстер' => 'consist-polyester',
            'Состав Пух' => 'consist-fluff',
            'Состав Резина' => 'consist-rubber',
            'Состав Силикон' => 'consist-silicone',
            'Состав Твил' => 'consist-twill',
            'Состав Текстиль' => 'consist-textile',
            'Состав Тенсел' => 'consist-tencel',
            'Состав Фланель' => 'consist-flannel',
            'Состав Хлопок' => 'consist-cotton',
            'Состав Шелк' => 'consist-silk',
            'Состав Шерсть' => 'consist-wool',
            'Состав Шифон' => 'consist-chiffon',
            'Состав Эластан' => 'consist-elastane',
            'Тип каблука Квадратный' => 'heel-type-square',
            'Тип каблука Конусообразный' => 'heel-type-cone-shaped',
            'Тип каблука Платформа' => 'heel-type-platform',
            'Тип каблука Плоская подошва' => 'heel-type-flat-sole',
            'Тип каблука Танкетка' => 'heel-type-tankette',
            'Тип каблука Шпилька' => 'heel-type-pin',
            'Цвет бежевый' => 'color-beige',
            'Цвет белый' => 'color-white',
            'Цвет голубой' => 'color-blue',
            'Цвет желтый' => 'color-yellow',
            'Цвет зеленый' => 'color-green',
            'Цвет коричневый' => 'color-brown',
            'Цвет красный' => 'color-red',
            'Цвет оранжевый' => 'color-orange',
            'Цвет розовый' => 'color-pink',
            'Цвет серый' => 'color-gray',
            'Цвет синий' => 'color-darkblue',
            'Цвет фиолетовый' => 'color-purple',
            'Цвет черный' => 'color-black',
            'Цвет камуфляж' => 'color-camo',
            'Страна производитель Россия' => 'from-russia',
            'Страна производитель Китай' => 'from-china',
            'Страна производитель Италия' => 'from-italia',
            'Страна производитель Болгария' => 'from-bulgaria',
            'Страна производитель Индия' => 'from-india',
            'Страна производитель США' => 'from-usa',
            'Страна производитель Турция' => 'from-turkey',
            'Страна производитель Германия' => 'from-germany',
            'Страна производитель Франция' => 'from-france',
            'Страна производитель Испания' => 'from-spain',
            'Страна производитель Португалия' => 'from-portugal',
            'Страна производитель Пакистан' => 'from-pakistan',
            'Страна производитель Таиланд' => 'from-thailand',
            'Страна производитель Индонезия' => 'from-indonezia',
            'Страна производитель Швеция' => 'from-sweden',
            'Страна производитель Украина' => 'from-ukraine',
            'Страна производитель Англия' => 'from-england',
            'Страна производитель Беларусь' => 'from-belarus',
            'Страна производитель Польша' => 'from-poland',
            'Страна производитель Тайвань' => 'from-taiwan',
            'Страна производитель Малайзия' => 'from-malayzia',
            'Страна производитель Эстония' => 'from-estonia',
            'Страна производитель Финляндия' => 'from-finland',
            'Страна производитель Мексика' => 'from-mexico',
        ];

        $label = $attribute->getName() . ' ' . $value->getLabel();

        $alias = '';
        if (array_key_exists($label, $aliases)) {
            $alias = $aliases[$label];
        }

        return $alias;
    }

    private function ucfirst($string, $e ='utf-8') {
        if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) {
            $string = mb_strtolower($string, $e);
            $upper = mb_strtoupper($string, $e);
            preg_match('#(.)#us', $upper, $matches);
            $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
        } else {
            $string = ucfirst($string);
        }
        return $string;
    }

    private function getRandomSeoTitleString()
    {
        $heads = [
            "Купить %tag%",
        ];
        
        $postfixes = [
            " в интернет магазине на Footgears",
            ". Скидки 50-70%, Доставка по РФ",
            " с доставкой по России",
            ". Цены, скидки, доставка по РФ",
            " недорого в интернет магазине с доставкой по РФ",
            " - низкие цены, SALE 50-70%, доставка по РФ",
            " в Москве. Доставка по РФ, скидки и акции",
            ". Бесплатная доставка по РФ, цены, фото",
            ". Заказать онлайн в интернет-магазине",
            ". Акции и Скидки до 75%",
            " из каталога %category% на Footgears",
            " недорого. В наличии, смотреть фото"
        ];

        return sprintf('%s%s', $heads[mt_rand(0, count($heads) - 1)], $postfixes[mt_rand(0, count($postfixes) - 1)]);
    }

    private function getRandomSeoDescriptionString(Category $category)
    {
        $strings = [
            'Ищете %tag%? Тогда вам сюда! Посмотрите наш каталог и купите то что вам нужно',
            'Искали %tag%? Посмотрите наш каталог от всех интернет-магазинов рунета и купите то что больше всего понравится',
            'Искали %tag%? Наш каталог - то что вам нужно! Все интернет-магазины в Footgears',
            'Вы хотите %tag%? Посмотрите наш каталог. Товары от популярных интернет-магазинов и удобный поиск не дадут вам уйти без покупки',
            '%tag% в каталоге на Footgears. Хорошие скидки вас приятно удивят!',
            'Акции и скидки на %tag% каждый день! Успей купить!',
            'Интересуют товары из каталога %tag%? У нас есть что вам предложить! Скидки, акции, доставка во все регионы РФ от популярных интернет-магазинов - наших партнеров',
            'Давно ли вы себя баловали? В каталоге %tag% вы купите то, что будет вас радовать долго время!',
            'Побалуйте себя покупкой из каталога %tag%. А акции и скидки помогут сэкономить',
        ];

        $string = $strings[array_rand($strings)];

        return $string;
    }

    private function searchRootCategoryId(Category $category)
    {
        $roots = [296, 25, 371, 428, 18, 462, 181, 182, 483, 546, 500, 548];

        $parentId = null;

        foreach ($roots as $id) {
            if ($category->getId() == $id || $category->isParent($id)) {
                $parentId = $id;
                break;
            }
        }

        return $parentId;
    }
}