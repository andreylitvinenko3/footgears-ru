<?php

namespace Footgears\MainBundle\Command\Tag;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Tag;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateProductsCountCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setDescription('Обновление количества товаров в тегах')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = 500;
        $dm = $this->getDocumentManager();

        $startDate = new \DateTime();
        
        $qb = $dm
            ->getRepository(Tag::class)
            ->createQueryBuilder()
        ;
        
        $tags = $qb
            ->addOr($qb->expr()->field('productsCountUpdated')->lt(new \MongoDate($startDate->getTimestamp())))
            ->addOr($qb->expr()->field('productsCountUpdated')->exists(false))
            ->sort(['productsCountUpdated' => 1])
            ->limit($limit)
            ->getQuery()
            ->execute()
        ;

        /** @var Tag[] $tags */
        foreach ($tags as $tag) {
            $this->logger->info($tag->getId());

            $this->getContainer()->get('app.tag_service')->updateProductsCount($tag);
        }
    }
}
