<?php

namespace Footgears\MainBundle\Command\Category;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\RecommendProduct;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RecommendProductsCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->addArgument('categoryIds', InputArgument::IS_ARRAY, null, [])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $categoryIds = $input->getArgument('categoryIds');

        $dm = $this->getDocumentManager();

        $repository = $dm->getRepository(RecommendProduct::class);

        $categoryRepository = $this->getCategoryRepository();

        if ($categoryIds) {
            $categories = $categoryRepository->findBy(['_id' => ['$in' => $categoryIds]]);
        } else {
            $categories = $categoryRepository->findAll();
        }

        $this->logger->info(sprintf('Found %d categories', count($categories)));

        /** @var Category[] $categories */
        foreach ($categories as $category) {
            $this->logger->info(sprintf('Build recommend products in %s', $category->getName()));

            $positions = $repository->getPositions($category);

            $repository->getCollection()->remove(['category' => $category->getId()]);

            $searchRequest = $this->getSearchRequestFactory()->createCategorySearchRequest($category);

            $ids = $this->getSearchManager()->getRandomProductIds($searchRequest, Category::RECOMMEND_PRODUCT_COUNT);

            if (!$ids) {
                $this->logger->info(sprintf('Not found random products from %s %s', $category->getId(), $category->getName()));
                continue;
            }

            $batch = new \MongoInsertBatch($repository->getCollection()->getMongoCollection());
            foreach ($ids as $id) {
                $position = array_shift($positions);

                $batch->add([
                    '_id' => RecommendProduct::generateId($category->getId(), $position),
                    'category' => $category->getId(),
                    'product' => $id,
                    'position' => $position
                ]);
            }
            $batch->execute();
        }
    }
}
