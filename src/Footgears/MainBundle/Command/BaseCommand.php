<?php

namespace Footgears\MainBundle\Command;

use AppKernel;
use Psr\Log\LoggerInterface;
use Footgears\MainBundle\Service\ContainerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends ContainerAwareCommand
{
    use ContainerTrait;

    protected $stopSignal = false;
    protected $stopSignalTime = 0;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct()
    {
        $name = get_class($this);
        $name = preg_replace('/Command$/', '', $name);

        $name = strtolower(preg_replace(array('/([A-Z]+)([A-Z][a-z])/', '/([a-z\d])([A-Z])/'), array('\\1-\\2', '\\1-\\2'), strtr($name, [__NAMESPACE__ . '\\' => '', '\\' => ':'])));

        parent::__construct($name);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->input = $input;
        $this->output = $output;
        $this->logger = $this->getLogger();
    }

    protected function catchSignals()
    {
        declare(ticks = 1);

        foreach([SIGINT, SIGTERM] as $signal) {
            pcntl_signal($signal,  function($signal) {
                if (microtime(true) - $this->stopSignalTime < 1) {
                    $this->logger->info(sprintf('Signal: %d! Kill', $signal));
                    exit(-1);
                }
                $this->logger->info(sprintf('Signal: %d! Terminating... To force exist - press ctrl+c twice', $signal));
                $this->stopSignal = true;
                $this->stopSignalTime = microtime(true);
            });
        }
    }

    protected function runCommand($command, $arguments, OutputInterface $output)
    {
        $application = new Application($this->getContainer()->get('kernel'));
        $application->setAutoExit(false);

        $arguments = array_merge([
            'command' => $command,
        ], $arguments);

        $input = new ArrayInput($arguments);

        $application->run($input, $output);
    }

    protected function showUOW()
    {
        $dm = $this->getDocumentManager();
        foreach ($dm->getUnitOfWork()->getIdentityMap() as $class => $item) {
            var_dump(sprintf('%s: %s', $class, count($item)));
        }

        var_dump(sprintf('Memory usage: %.2fMb', memory_get_usage(true) / 1024 / 1024));
    }
}
