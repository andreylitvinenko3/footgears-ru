<?php

namespace Footgears\MainBundle\Command\Parsers;

use Doctrine\MongoDB\Iterator;
use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Http\HttpUtil;
use Footgears\MainBundle\ParseImage\ParserManager;
use Sirian\StorageBundle\Document\FileEmbed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BrandsLogoCommand extends BaseCommand
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lastId = 0;

        $qb = $this
            ->getDocumentManager()
            ->getRepository(Brand::class)
            ->createQueryBuilder()
            ->field('logo')->equals(null)
            ->field('productsCount')->gt(0)
        ;

        $dm = $this->getDocumentManager();

        while (true) {
            $cloneQb = clone $qb;

            /** @var Brand[]|Iterator $brands */
            $brands = $cloneQb
                ->field('id')->gt($lastId)
                ->sort('id')
                ->limit(100)
                ->getQuery()
                ->getIterator()
            ;

            if (!$brands->count()) {
                break;
            }

            $this->logger->info('start');

            foreach ($brands as $brand) {
                $lastId = $brand->getId();

                try {
                    $this->logger->warning($brand->getName());

                    $this->setImage($brand);

                    $dm->persist($brand);
                } catch (\Exception $e) {
                    $this->logger->error(sprintf('%s - %s', $brand->getAlias(), $e->getMessage()));
                }
            }

            $dm->flush();
            $dm->clear();

            $this->logger->info('sleeping...');
            sleep(5);
        }
    }

    protected function setImage(Brand $brand)
    {
        $logoUrl = $this->getImagePath($brand->getAlias());

        $logoUrl = HttpUtil::ensureProtocol($logoUrl);

        $this->uploadImage($brand, $logoUrl);
    }

    private function uploadImage(Brand $brand, $logoUrl)
    {
        $fileInfo = new \SplFileInfo($logoUrl);

        $localFileTemplate = sprintf('/var/tmp/footgears_brand_logo_%s', $brand->getAlias());

        file_put_contents($localFileTemplate, file_get_contents($logoUrl));

        $file = new UploadedFile($localFileTemplate, $fileInfo->getFilename(), null, null, null, true);

        $logo = new FileEmbed();
        $logo->setUploadedFile($file);

        $brand->setLogo($logo);
    }

    private function getImagePath($alias)
    {
        $manager = new ParserManager();
        $path = $manager->parse($alias);

        return $path;
    }
}
