<?php

namespace Footgears\MainBundle\Command\Products;

use Doctrine\MongoDB\Iterator;
use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Search\BrandSearchRequest;
use Footgears\MainBundle\Search\CategorySearchRequest;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCountersCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setDescription('Обновление количества товаров у категорий, брендов, категорий+бренд, категорий+магазин');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->updateBrandCounters();
        $this->updateCategoryCounters();

        $this->logger->info('Updating products count for category-brand pages');
        $this->getCategoryBrandService()->updateProductsCounts();
    }

    private function updateBrandCounters()
    {
        $this->logger->info('Updating products count for brands');

        $dm = $this->getDocumentManager();

        $lastId = 0;
        $iteration = 0;
        while (true) {
            /** @var Brand[]|Iterator $brands */
            $brands = $dm
                ->getRepository(Brand::class)
                ->createQueryBuilder()
                ->field('enabled')->equals(true)
                ->field('id')->gt($lastId)
                ->sort(['id' => 1])
                ->limit(500)
                ->getQuery()
                ->getIterator()
            ;

            if (!$brands->count()) {
                break;
            }

            foreach ($brands as $brand) {
                $lastId = $brand->getId();

                $searchRequest = new BrandSearchRequest($brand);
                $count = (int)$this->getSearchManager()->countProducts($searchRequest);

                $brand->setProductsCount($count);
                $dm->persist($brand);
            }

            $dm->flush();
            $dm->clear();

//            $this->showUOW();

            $this->logger->info(sprintf('Iteration %d. Last brandId: %d. Memory Usage: %.2fMb', ++$iteration, $lastId, memory_get_usage(true) / 1024 / 1024));
        }
    }

    private function updateCategoryCounters()
    {
        $this->logger->info('Updating products count for categories');

        $dm = $this->getDocumentManager();

        /** @var Category[]|Iterator $categories */
        $categories = $dm
            ->getRepository(Category::class)
            ->createQueryBuilder()
            ->sort(['id' => 1])
            ->getQuery()
            ->getIterator()
        ;

        foreach ($categories as $category) {
            $searchRequest = new CategorySearchRequest($category);
            $count = (int)$this->getSearchManager()->countProducts($searchRequest);

            $category->setProductsCount($count);
            $dm->persist($category);

            if ($category->getProductsCount() < 10) {
                $this->logger->info(sprintf('%s (#%d) - is not enough products: %d.', $category->getName(), $category->getId(), $category->getProductsCount()));
            }
        }

        $dm->flush();
        $dm->clear();

        $this->logger->info(sprintf('Memory Usage: %.2fMb', memory_get_usage(true) / 1024 / 1024));
    }
}