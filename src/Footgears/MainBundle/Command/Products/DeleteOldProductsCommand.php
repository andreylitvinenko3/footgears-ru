<?php

namespace Footgears\MainBundle\Command\Products;

use Footgears\MainBundle\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteOldProductsCommand extends BaseCommand
{
    const DAYS = 20;

    protected function configure()
    {
        $this->setName('products:delete-old');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        while (true) {
            $cursor = $this->getProductRepository()
                ->getCollection()
                ->getMongoCollection()
                ->find([
                        'enabled' => false,
                        'disableDate' => ['$lt' => new \MongoDate((new \DateTime('-' . self::DAYS . ' day'))->getTimestamp())]
                    ],
                    ['_id' => true]
                )
                ->limit(200)
            ;

            $ids = array_keys(iterator_to_array($cursor));

            if (!$ids) {
                break;
            }

            $this->getElasticManager()->deleteProductsFromIndex($ids);

            $result = $this
                ->getProductRepository()
                ->getCollection()
                ->remove(['_id' => ['$in' => $ids]]);

            $this->getLogger()->info(sprintf('Delete %d old products (%d days)', $result['n'], self::DAYS));
        }
    }
}
