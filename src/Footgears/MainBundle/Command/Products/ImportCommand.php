<?php

namespace Footgears\MainBundle\Command\Products;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\ImportHistory;
use Footgears\MainBundle\Document\Shop;
use Sirian\Helpers\Url;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->addOption('force', 'f', InputOption::VALUE_NONE)
            ->addArgument('shops', InputArgument::IS_ARRAY)
            ->setDescription('Отправка магазина в очередь импорта')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getDocumentManager();

        $qb = $dm
            ->getRepository(Shop::class)
            ->createQueryBuilder()
            ->field('active')->equals(true)
            ->field('importProductsSettings.enabled')->equals(true)
        ;

        $shopIds = array_map(function ($id) {
            return $id;
        }, $input->getArgument('shops'));
        if ($shopIds) {
            $qb->field('_id')->in($shopIds);
        }

        /* @var Shop[] $shops */
        $shops = $qb->getQuery()->execute();

        foreach ($shops as $shop) {
            $this->logger->info(sprintf('Shop #%d %s', $shop->getId(), $shop->getName()));
            $count = $this->getProductRepository()->countInShop($shop);

            $shop->setProductsCount($count);

            $importSettings = $shop->getImportProductsSettings();
            if (!$input->getOption('force') && !$importSettings->needImport() && !$importSettings->isForce()) {
                $this->logger->info(sprintf('Skip shop "%s" - import not needed', $shop->getName()));
                continue;
            }

            /* @var ImportHistory $history */
            if ($history = $this->getImportHistoryRepository()->findOneBy(['shop' => $shop->getId(), 'status' => ImportHistory::STATUS_ACTIVE])) {
                if ($history->getUpdateDate() && (new \DateTime())->getTimestamp() - $history->getUpdateDate()->getTimestamp() > 3600) {
                    $history->setStatus(ImportHistory::STATUS_CANCELED);
                    $this->logger->warning(sprintf('Stop stucked import %d %s (%s)', $shop->getId(), $shop->getName(), $history->getId()));
                } else {
                    $this->logger->warning(sprintf('Skip %d %s - active import (%s) already exists', $shop->getId(), $shop->getName(), $history->getId()));
                    continue;
                }
            }

            $this->logger->info(sprintf('Schedule import for %d %s', $shop->getId(), $shop->getName()));

            $importUrl = $importSettings->getUrl();
            if ($importSettings->isDelta() && $importSettings->getLastImport()) {
                $url = new Url($importUrl);
                $url->setQueryParam('last_import', $importSettings->getLastImport()->format('Y.m.d.H.i'));

                $importUrl = $url->getUrl();
            }

            $history = new ImportHistory();
            $history
                ->setShop($shop)
                ->setStatus(ImportHistory::STATUS_ACTIVE)
                ->setProgress(ImportHistory::PROGRESS_WAIT_DOWNLOAD)
                ->setUrl($importUrl)
                ->setDelta($importSettings->isDelta())
            ;

            $dm->persist($shop);
            $dm->persist($history);
        }

        $dm->flush();
    }
}
