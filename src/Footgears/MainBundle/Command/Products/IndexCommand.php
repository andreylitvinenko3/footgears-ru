<?php

namespace Footgears\MainBundle\Command\Products;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\Seo;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Footgears\MainBundle\Elastic\ElasticMeta;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class IndexCommand extends BaseCommand
{
    const BATCH_SIZE = 500;

    protected function configure()
    {
        $this
            ->setName('products:index')
            ->addOption('drop-index', null, InputOption::VALUE_NONE)
            ->addOption('import', null, InputOption::VALUE_NONE)
            ->addOption('limit', null, InputOption::VALUE_REQUIRED)
            ->addOption('shop', null, InputOption::VALUE_REQUIRED)
            ->setDescription('Переиндексация товаров')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->ensureIndex($input);

        if (!$input->getOption('import')) {
            return;
        }

        $dm = $this->getDocumentManager();

        $qb = $this
            ->getProductRepository()
            ->createQueryBuilder()
        ;

        if ($input->getOption('shop')) {
            $shop = $this->getDocumentManager()->getRepository(Shop::class)->find($input->getOption('shop'));

            if (!$shop) {
                $this->logger->error(sprintf('Not found shop %s', $input->getOption('shop')));
                return;
            }

            $qb->field('shop')->references($shop);
        }

        $cloneQb = clone $qb;
        $total = (int)$cloneQb->getQuery()->count();

        if (!$total) {
            return;
        }

        $elasticManager = $this->getElasticManager();

        $this->logger->info('Load categories');
        $this->getCategoryRepository()->findAll();

        $this->logger->info('Import');

        $result = $qb
            ->sort(['_id' => 1])
            ->getQuery()
            ->getIterator()
        ;

        $insertBatch = [];
        $deleteBatch = [];

        $cnt = 0;
        foreach ($result as $product) {
            /* @var Product $product */

            $cnt++;

            $dm->detach($product);

            if ($product->isValid()) {
                $insertBatch[] = $product;
            } else {
                $deleteBatch[] = $product;
            }

            if ($cnt % 500 == 0 || $cnt >= $result->count()) {
                $elasticManager->indexProducts($insertBatch);
                $elasticManager->deleteProductsFromIndex($deleteBatch);

                $insertBatch = [];
                $deleteBatch = [];

                $dm->clear(Seo::class);
                $dm->clear(Brand::class);
                $dm->clear(ShopCategory::class);

                gc_collect_cycles();
                $this->getLogger()->info(sprintf('[%d/%d %.2f%%] Memory: %.2fMb', $cnt, $result->count(), $cnt * 100 / $result->count(), memory_get_usage(true) / 1024 / 1024));
                $this->showUOW();
            }


            if ($input->getOption('limit') && $cnt >= (int)$input->getOption('limit')) {
                break;
            }
        }

        $this->getCache()->invalidateTags([ElasticMeta::CACHE_TAG_SEARCH_RESULT]);
    }

    protected function ensureIndex(InputInterface $input)
    {
        $this->logger->info('Ensure index');

        $marketManager = $this->getElasticManager();

        $exists = $marketManager->existsIndex();

        if ($exists) {
            if (!$input->getOption('drop-index')) {
                return;
            }
            $this->logger->warning('Drop index');
            $marketManager->dropIndex();
        }

        $this->logger->info('Create index');

        $marketManager->createIndex();
    }
}
