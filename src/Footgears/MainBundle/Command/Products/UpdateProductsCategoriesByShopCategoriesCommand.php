<?php

namespace Footgears\MainBundle\Command\Products;

use Footgears\MainBundle\Command\BaseCommand;
use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\ShopCategory;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateProductsCategoriesByShopCategoriesCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('footgears:products:update-categories-by-shop-categories')
            ->setDescription('Update products categories by shop_categories')
            ->addArgument('shop_id', InputArgument::OPTIONAL, 'Shop id')
            ->addArgument('shop_category', InputArgument::OPTIONAL, 'Shop category id')
            ->setDescription('Обновление категории товара по категории магазина')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getDocumentManager();
        $shopRepository = $dm->getRepository(Shop::class);

        if ($input->getArgument('shop_id')) {
            $shops = $shopRepository->findBy(['id' => (int)$input->getArgument('shop_id')]);
        } else {
            $shops = $shopRepository->findBy(['enabled' => 1]);
        }

        $this->logger->info(sprintf('Found %d shops', count($shops)));

        $i = 0;

        /** @var Shop[] $shops */
        foreach ($shops as $shop) {
            $this->logger->info(sprintf('[%d/%d] Update products in shop %s', ++$i, count($shops), $shop->getName()));

            $qb = $dm
                ->getRepository(ShopCategory::class)
                ->createQueryBuilder()
                ->field('shop')->references($shop)
            ;

            if ($input->getArgument('shop_category')) {
                $qb->field('id')->equals($input->getArgument('shop_category'));
            }

            /** @var ShopCategory[] $shopCategories */
            $shopCategories = $qb->getQuery()->execute();

            foreach ($shopCategories as $shopCategory) {
                $category = $shopCategory->getRealCategory();

                $dm
                    ->getRepository(Product::class)
                    ->getCollection()
                    ->update(
                        ['shopCategory' => $shopCategory->getId()],
                        ['$set' => [
                            'category' => $category ? $category->getId() : null
                        ]],
                        ['multiple' => true]
                    )
                ;

                $dm->clear();
            }

            $this->logger->info(sprintf('Memory: %.2fMb', memory_get_usage() / 1024 / 1024));
        }
    }
}