<?php

namespace Footgears\MainBundle\Controller;

use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\CategoryBrand;
use Footgears\MainBundle\Document\RecommendProduct;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Search\SearchResult;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CategoryController extends Controller
{
    public function indexAction(Request $request, $alias, $isSale = false)
    {
        $repo = $this->getCategoryRepository();

        $category = $repo->findOneByAlias($alias);
        if (!$category) {
            throw $this->createNotFoundException();
        }

        if ($isSale) {
            $request->query->set('isSale', $isSale);
        }

        $searchRequest = $this->getSearchRequestFactory()->createCategorySearchRequest($category);
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        /** @var SearchResult $searchResult */
        $searchResult = $data['result'];

        $isEmpty = false;
        if ($searchResult->getPage() == 1 && $searchResult->getTotal() <= 8) {
            $isEmpty = true;
        }

        $similarProducts = [];
        if ($isEmpty) {
            if ($isSale) {
                $similarProducts = $this->getCategoryProducts($category);
            } elseif ($category->getParent()) {
                $similarProducts = $this->getCategoryProducts($category->getParent());
            }
        }

        $recommend = [];
        if (!$isEmpty) {
            $recommend = $this->getDocumentManager()->getRepository(RecommendProduct::class)->findBy(['category' => $category->getId()]);
        }

        return $this->render('@Main/Category/index.html.twig', array_merge($data, [
            'category' => $category,
            'isSale' => $isSale,
            'similar' => $similarProducts,
//            'recommend' => $recommend
        ]));
    }

    public function searchByShopAction(Request $request, $alias, $shopAlias, $isSale = false)
    {
        $repo = $this->getCategoryRepository();

        $category = $repo->findOneByAlias($alias);
        /** @var Shop $shop */
        $shop = $this->getShopRepository()->findOneBy(['alias' => $shopAlias]);

        if (!$category || !$shop) {
            throw $this->createNotFoundException();
        }

        if ($isSale) {
            $request->query->set('isSale', $isSale);
        }

        $searchRequest = $this->getSearchRequestFactory()->createShopSearchRequest($shop, $category);
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        $relatedBrand = $this->getBrandRepository()->findBy(['alias' => $shop->getAlias()]);

        return $this->render('@Main/Category/by_shops.html.twig', array_merge($data, [
            'category' => $category,
            'shop' => $shop,
            'isSale' => $isSale,
            'relatedBrand' => $relatedBrand
        ]));
    }

    public function searchByBrandAction(Request $request, $alias, $brandAlias, $isSale = false)
    {
        $repo = $this->getCategoryRepository();

        $category = $repo->findOneByAlias($alias);
        /** @var Brand $brand */
        $brand = $this->getBrandRepository()->findOneBy(['alias' => $brandAlias]);

        if (!$category || !$brand) {
            throw $this->createNotFoundException();
        }

        if ($isSale) {
            $request->query->set('isSale', $isSale);
        }

        /** @var CategoryBrand $pageInfo */
        $pageInfo = $this->getDocumentManager()->getRepository(CategoryBrand::class)->findOneBy([
            'category' => $category,
            'brand' => $brand
        ]);

        $searchRequest = $this->getSearchRequestFactory()->createBrandSearchRequest($brand, $category);
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        /** @var SearchResult $searchResult */
        $searchResult = $data['result'];

        $isEmpty = false;
        if ($searchResult->getPage() == 1 && $searchResult->getTotal() <= 8) {
            $isEmpty = true;
        }

        $similarProducts = [];
        if ($isEmpty) {
            $similarProducts = $this->getCategoryProducts($category);
        }

        return $this->render('@Main/Category/by_brands.html.twig', array_merge($data, [
            'category' => $category,
            'brand' => $brand,
            'pageInfo' => $pageInfo,
            'isSale' => $isSale,
            'similar' => $similarProducts
        ]));
    }

    public function searchByTagAction(Request $request, $alias, $tagAlias)
    {
        $repo = $this->getCategoryRepository();

        $category = $repo->findOneByAlias($alias);

        $tagRepository = $this->getDocumentManager()->getRepository(Tag::class);

        /** @var Tag $tag */
        $tag = $tagRepository->findOneBy([
            'alias' => $tagAlias,
            'category' => $category,
        ]);

        if (!$category || !$tag) {
            throw $this->createNotFoundException();
        }

        $this->checkPageAccess($tag);

        $searchRequest = $this->getSearchRequestFactory()->createTagSearchRequest($tag);
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        $canonical = null;

        /** @var Tag $similarTag */
        $similarTag = $tagRepository->getSimilarPage($tag);
        if ($similarTag) {
            if ($tag->getProductsCount() < $similarTag->getProductsCount()
                || ($tag->getProductsCount() == $similarTag->getProductsCount() && $tag->getType() == Tag::TYPE_FILTER)
            ) {
                $canonical = $this->generateUrl('category_tag', ['alias' => $similarTag->getCategory()->getAlias(), 'tagAlias' => $similarTag->getAlias()], UrlGeneratorInterface::ABSOLUTE_URL);
            }
        }

        return $this->render('@Main/Category/by_tag.html.twig', array_merge($data, [
            'canonical' => $canonical,
            'category' => $category,
            'tag' => $tag,
        ]));
    }

    public function searchByTagBrandAction(Request $request, $alias, $tagAlias, $brandAlias)
    {
        $repo = $this->getCategoryRepository();

        $category = $repo->findOneByAlias($alias);

        $tagRepository = $this->getDocumentManager()->getRepository(Tag::class);

        /** @var Tag $tag */
        $tag = $tagRepository->findOneBy([
            'alias' => $tagAlias,
            'category' => $category,
        ]);
        
        if (!$category || !$tag) {
            throw $this->createNotFoundException();
        }

        $this->checkPageAccess($tag);

        $brand = $this->getDocumentManager()->getRepository(Brand::class)->findOneBy(['alias' => $brandAlias]);
        if (!$brand) {
            throw new NotFoundHttpException();
        }

        $searchRequest = $this->getSearchRequestFactory()->createTagSearchRequest($tag);
        $searchRequest->setFiltersFromRequest($request);
        $searchRequest->setBrandIds([$brand->getId()]);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        return $this->render('@Main/Category/tag_brand.html.twig', array_merge($data, [
            'category' => $category,
            'tag' => $tag,
            'brand' => $brand
        ]));
    }

    protected function checkPageAccess(Tag $page)
    {
        if (!$page->isActive() && (!$this->getUser() || !$this->getUser()->isAdmin())) {
            throw $this->createNotFoundException();
        }
    }

    protected function getCategoryProducts(Category $category)
    {
        $products = [];

        while ($category) {
            $searchRequest = $this->getSearchRequestFactory()->createCategorySearchRequest($category);

            $count = $this->getSearchManager()->countProducts($searchRequest);

            if ($count < 60) {
                if (!$category->getParent()) {
                    break;
                }

                $category = $category->getParent();
                continue;
            }

            $searchRequest->setPage(2);

            $products = $this->getSearchManager()->simpleSearch($searchRequest)->getProducts();

            if ($products) {
                break;
            }

            if (!$category->getParent()) {
                break;
            }

            $category = $category->getParent();
        }

        return $products;
    }
}
