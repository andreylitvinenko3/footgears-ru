<?php

namespace Footgears\MainBundle\Controller;

use Doctrine\ORM\AbstractQuery;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Feedback;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Form\Type\FeedbackType;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $popularBrands = $this->getBrandService()->findPopular(5);

        $popularShops = $this
            ->getDocumentManager()
            ->getRepository(Shop::class)
            ->createQueryBuilder()
            ->field('active')->equals(true)
            ->limit(5)
            ->sort(['productsCount' => -1])
            ->getQuery()
            ->execute()
        ;

        return $this->render('@Main/Default/index.html.twig', [
            'popularBrands' => $popularBrands,
            'popularShops' => $popularShops,
        ]);
    }

    public function sitemapAction(Request $request)
    {
        $webDir = $this->getContainer()->getParameter('web_dir');

        // сюда складываем файлы
        $sitemapsPath = $webDir . '/sitemap/';

        $fs = new Filesystem();
        $finder = new Finder();

        $content = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        if ($fs->exists($sitemapsPath)) {
            /** @var \SplFileInfo[] $files */
            $files = $finder->files()->name('*.xml.gz')->in($sitemapsPath);

            foreach ($files as $file) {
                $content .= sprintf('<sitemap><loc>%s/sitemap_%s</loc><lastmod>%s</lastmod></sitemap>', $request->getSchemeAndHttpHost(), $file->getBasename(), date('c', $file->getMTime()));
            }
        }
        $content .= '</sitemapindex>';

        return new Response($content);
    }

    public function sitemapPageAction()
    {
        $dm = $this->getDocumentManager();
        $root = $dm->getRepository(Category::class)->getPreparedTree();

        return $this->render('@Main/Static/sitemap.html.twig', [
            'categories' => $root
        ]);

    }

    public function outAction(Request $request)
    {
        $url = $request->query->get('url');
        $url = $this->container->get('footgears.cipher')->decrypt($url);

        if (!$url) {
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->redirect($url);
    }

    public function contactsAction(Request $request)
    {
        $feedback = new Feedback();

        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($feedback);
            $dm->flush();

            $this->addFlash('success', 'Спасибо! Ваше сообщение отправлено. Мы обязательно с вами свяжемся');
            return $this->redirectToRoute('contacts');
        }

        return $this->render('@Main/Default/contacts.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
