<?php

namespace Footgears\MainBundle\Controller;

use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Search\SearchRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    public function searchAction(Request $request)
    {
        $query = $request->query->get('q');

        if ($query) {
            /** @var Brand $brand */
            $brand = $this->getDocumentManager()->getRepository(Brand::class)->findOneBy(['name' => $query]);

            if ($brand) {
                return $this->redirectToRoute('brand', ['alias' => $brand->getAlias()]);
            }
        }

        $searchRequest = $this->getSearchRequestFactory()->createSearchRequest();
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        return $this->render('@Main/Search/search.html.twig', array_merge([], $data));
    }

    public function saleAction(Request $request)
    {
        $request->query->set('isSale', true);

        $searchRequest = $this->getSearchRequestFactory()->createSearchRequest();
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        return $this->render('@Main/Search/sale.html.twig', array_merge([], $data));
    }

    public function countAction(Request $request)
    {
        $searchRequest = new SearchRequest();

        if ($request->query->get('tagId')) {
            /** @var Tag $tag */
            $tag = $this->getDocumentManager()->find(Tag::class, $request->query->get('tagId'));

            if ($tag) {
                $searchRequest = $this->getSearchRequestFactory()->createTagSearchRequest($tag);
            }
        }

        $searchRequest->setFiltersFromRequest($request);
        $count = $this->getSearchManager()->count($searchRequest);

        return $this->json([
            'count' => $count
        ]);
    }

    public function autocompleteAction()
    {
        // todo: make autocomplete search by brands, category, shops

        $rows = [];
        return new JsonResponse([
            'html' => $this->renderView('MainBundle:Search:autocomplete.html.twig', [
                'rows' => $rows
            ])
        ]);
    }

    public function suggestBrandsAction(Request $request)
    {
        $include = null;

        if ($request->query->get('tag') && $request->query->get('categoryId')) {
            /** @var Tag $tag */
            $tag = $this->getDocumentManager()->find(Tag::class, $request->query->get('tag'));

            if (!$tag) {
                return $this->createNotFoundException();
            }

            $searchRequest = $this->getSearchRequestFactory()->createTagSearchRequest($tag);
        } else {
            $searchRequest = $this->getSearchRequestFactory()->createSearchRequest();
        }

        $searchRequest->setFiltersFromRequest($request);
        $brandIds = $searchRequest->getBrandIds();
        $searchRequest->setBrandIds(null);

        $name = trim((string)$request->query->get('search'));

        $r = $this->getSearchManager()->getAvailableBrands($searchRequest, 40, true, $brandIds, $name);

        $items = [];
        if ($r) {
            $items = array_combine(array_keys($r), array_map(function ($row) {
                return [
                    'id' => '',
                    'name' => '',
                    'productsLocal' => $row
                ];
            }, $r));

            /** @var Brand[] $brands */
            $brands = $this->getBrandRepository()->findBy(['_id' => ['$in' => array_keys($r)]]);
            foreach ($brands as $brand) {
                $items[$brand->getId()]['id'] = $brand->getId();
                $items[$brand->getId()]['name'] = $brand->getName();
                $items[$brand->getId()]['alias'] = $brand->getAlias();
            }

            $items = array_filter($items, function ($item) {
                return !empty($item['name']);
            });

            usort($items, function($a, $b) {
                if ($a['productsLocal'] == $b['productsLocal']) {
                    if ($a['name'] == $b['name']) {

                        return 0;
                    }
                    return ($a['name'] < $b['name']) ? -1 : 1;
                }
                return ($a['productsLocal'] > $b['productsLocal']) ? -1 : 1;
            });
        }

        return $this->json([
            'items' => $items
        ]);
    }
}
