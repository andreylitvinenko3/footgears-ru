<?php

namespace Footgears\MainBundle\Controller;

use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Service\ContainerTrait;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Sirian\Helpers\Url;
use Symfony\Component\HttpFoundation\Response;

class Controller extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{
    use ContainerTrait;

    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param $target
     * @param int $limit
     * @param int $page
     * @param array $options
     * @return PaginationInterface
     */
    public function paginate($target, $limit = 50, $page = 1, $options = array())
    {
        $r = $this->getRequest();

        return $this->container
            ->get('knp_paginator')
            ->paginate(
                $target,
                $r->query->get('page', $r->request->get('page', $page)),
                $r->query->get('limit', $r->request->get('limit', $limit)),
                $options
            )
        ;
    }

    public function redirect($url, $status = 302, Shop $shop = null)
    {
        $url = new Url($url);

        if (preg_match('#\.(admitad|grifanme|pafutos)\.com$#i', $url->getHost())) {
            $request = $this->getRequest();

            $path = parse_url($request->headers->get('referer'), PHP_URL_PATH);

            $url->setQueryParam('subid3', $path);

            if ($request->getSession()->has('source')) {
                $url->setQueryParam('subid4', $request->getSession()->get('source'));
            }
        }

//        if ($shop) {
//            return $this->render('redirect.html.twig', [
//                'shop' => $shop,
//                'redirect_url' => $url->getUrl()
//            ]);
//        }

        return parent::redirect($url->getUrl(), $status);
    }
}
