<?php

namespace Footgears\MainBundle\Controller;

use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Shop\ShopHelper;
use Symfony\Component\HttpFoundation\Request;

class ShopController extends Controller
{
    public function indexAction(Request $request, $alias)
    {
        $repo = $this->getDocumentManager()->getRepository(Shop::class);

        $shop = $repo->findOneBy(['alias' => $alias]);

        if (!$shop || !$shop->isActive()) {
            throw $this->createNotFoundException();
        }

        $searchRequest = $this->getSearchRequestFactory()->createShopSearchRequest($shop);
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        $relatedBrand = $this->getBrandService()->getRelatedByShop($shop->getName());

        return $this->render('@Main/Shop/index.html.twig', array_merge($data, [
            'shop' => $shop,
            'relatedBrand' => $relatedBrand
        ]));
    }

    public function listAction()
    {
        $shops = $this
            ->getDocumentManager()
            ->getRepository(Shop::class)
            ->findBy(
                ['productsCountFromIndex' => ['$gt' => 0]],
                ['productsCountFromIndex' => -1]
            )
        ;

        return $this->render('@Main/Shop/list.html.twig', [
            'shops' => $shops
        ]);
    }

    public function goAction(Request $request, $alias)
    {
        $repo = $this->getDocumentManager()->getRepository(Shop::class);

        $shop = $repo->findOneBy(['alias' => $alias]);
        if (!$shop) {
            throw $this->createNotFoundException('Магазин не найден');
        }

        return $this->redirect(ShopHelper::getDeeplink($shop, $request->query->getAlnum('ulp')), 302, $shop);
    }
}
