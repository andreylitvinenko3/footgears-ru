<?php

namespace Footgears\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostbackController extends Controller
{
    public function indexAction(Request $request)
    {
        file_put_contents('/var/tmp/postback_' . $request->getHost(), json_encode($request->query->all()), FILE_APPEND);

        return new Response();
    }
}