<?php

namespace Footgears\MainBundle\Controller;

use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Search\SearchResult;
use Symfony\Component\HttpFoundation\Request;

class BrandController extends Controller
{
    const POPULAR_NAMES_COUNT = 5;

    public function indexAction(Request $request, $alias, $isSale = false)
    {
        $brand = $this->getDocumentManager()->getRepository(Brand::class)->findOneBy(['alias' => $alias]);

        if (!$brand) {
            throw $this->createNotFoundException();
        }

        if ($isSale) {
            $request->query->set('isSale', $isSale);
        }

        $searchRequest = $this->getSearchRequestFactory()->createBrandSearchRequest($brand);
        $searchRequest->setFiltersFromRequest($request);

        $data = $this->getSearchService()->doProductSearch($searchRequest);

        /** @var SearchResult $result */
        $result = $data['result'];

        $popular = [];
        if (!$result->getTotal()) {
            if ($isSale) {
                $message = sprintf('К сожалению, сейчас скидок на товары %s нет, но скоро они обязательно появятся. Ознакомьтесь, пожалуйста, со списком всех товаров бренда %s', $brand->getName(), $brand->getName());

                $this->addFlash('warning', $message);
            } else {
                $message = sprintf('К сожалению, на данный момент у нас нет в наличии товаров %s, но скоро они обязательно появятся. Ознакомьтесь, пожалуйста, со списком популярных брендов', $brand->getName());
                $this->addFlash('warning', $message);

                $popular = $this->getBrandService()->findPopular(12);
            }
        }

        $popularCategories = $this->getBrandPopularCategories($result, 2);

        return $this->render('@Main/Brand/index.html.twig', array_merge($data, [
            'brand' => $brand,
            'isSale' => $isSale,
            'popularCategories' => $popularCategories,
            'popular' => $popular
        ]));
    }

    public function listAction($letter)
    {
        $requestLetter = $letter;

        $all = false;
        if (!$letter) {
            $all = true;
            $letters = [];
            for ($let = ord('a'); $let <= ord('z'); $let++) {
                $letters[] = chr($let);
            }

            $letters[] = 'rus';
            $letters[] = 'other';
        } else {
            $letters = [$letter];
        }

        $brands = [];

        foreach ($letters as $let) {
            $let = mb_strtolower($let);

            $qb = $this
                ->createLetterQueryBuilder($let)
            ;

            if ($all) {
                $qb
                    ->sort(['productsCount' => -1])
                    ->limit(20)
                ;
            }

            $brands[$let] = $qb->getQuery()->toArray();

            uasort($brands[$let], function (Brand $a, Brand $b) {
                return mb_strtolower($a->getName()) < mb_strtolower($b->getName()) ? -1 : 1;
            });
        }

        $popularNames = $this->popularBrandNames($letter);

        return $this->render('@Main/Brand/list.html.twig', [
            'all' => $all,
            'brands' => $brands,
            'current' => $requestLetter,
            'popular' => !$requestLetter ? $this->getBrandService()->findPopular(12) : null,
            'popularNames' => $popularNames
        ]);
    }

    /**
     * @param $letter
     * @return Brand[]
     */
    private function popularBrandNames($letter)
    {
        $brandRepository = $this->getDocumentManager()->getRepository(Brand::class);

        if ($letter) {
            $qb = $this->createLetterQueryBuilder($letter);
        } else {
            $qb = $brandRepository
                ->createQueryBuilder()
                ->field('productsCount')->gt(0)
                ->field('enabled')->equals(true)
            ;
        }

        $brands = $qb
            ->hydrate(false)
            ->sort(['productsCount' => -1])
            ->limit(self::POPULAR_NAMES_COUNT)
            ->getQuery()
            ->toArray()
        ;

        $names = array_map(function($brand) {
            return $brand['name'];
        }, $brands);

        return $names;
    }

    private function getBrandPopularCategories(SearchResult $result, $limit)
    {
        if (!$result->getAvailableCategories()) {
            return [];
        }

        $ids = [];
        foreach ($result->getAvailableCategories() as $categoryId => $count) {
            if ($count < 100) {
                continue;
            }

            $ids[] = $categoryId;
        }

        if (!$ids) {
            return [];
        }

        $data = $this
            ->getDocumentManager()
            ->getRepository(Category::class)
            ->createQueryBuilder()
            ->field('id')->in($ids)
            ->field('level')->gt(1)
            ->limit($limit)
            ->getQuery()
            ->toArray()
        ;

        shuffle($data);

        return $data;
    }

    private function createLetterQueryBuilder($letter)
    {
        $qb = $this
            ->getDocumentManager()
            ->getRepository(Brand::class)
            ->createQueryBuilder()
            ->field('enabled')->equals(true)
        ;

        switch ($letter) {
            case 'rus':
                $qb->field('letter')
                    ->gte('а')
                    ->lte('я')
                ;
                break;

            case 'other':
                $qb
                    ->addAnd($qb->expr()->field('letter')->not($qb->expr()->gte('а')->lte('я')))
                    ->addAnd($qb->expr()->field('letter')->not($qb->expr()->gte('a')->lte('z')))
                ;
                break;

            default:
                $qb->field('letter')->equals($letter);
        }

        $qb->field('productsCount')->gt(0);

        return $qb;
    }
}
