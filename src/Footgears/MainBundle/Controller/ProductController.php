<?php

namespace Footgears\MainBundle\Controller;

use Footgears\MainBundle\Document\Product;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Entity\Category;
use Footgears\MainBundle\DBAL\Type\RedirectType;
use Footgears\MainBundle\Entity\ProductsFilterPage;
use Footgears\MainBundle\Managers\LastDisplayProductsManager;
use Footgears\MainBundle\Search\SearchRequest;
use Footgears\MainBundle\Search\SearchResult;
use Sirian\Helpers\Url;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductController extends Controller
{
    public function showAction(Request $request, $id, $slug = '')
    {
        /* @var Product $product */
        $product = $this->getProductRepository()->find($id);

        if (!$product) {
            throw $this->createNotFoundException();
        }

        if ($product->getSlug() !== $slug) {
            return $this->redirectToRoute('product_show', ['id' => $id, 'slug' => $product->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        return $this->render('@Main/Product/show.html.twig', [
            'product' => $product,
            'similarProducts' => []
        ]);
    }

    public function buyAction(Request $request, $id)
    {
        $time = $request->query->get('t');

        if (!$time || (new \DateTime('-1 day'))->getTimestamp() > $time) {
            return new Response('', Response::HTTP_GONE);
        }

        /* @var Product $product */
        $product = $this->getProductRepository()->find($id);
        if (!$product) {
            throw $this->createNotFoundException('Товар не найден');
        }

        if (!$product->getShop()->isActive()) {
            $this->addFlash('info', sprintf('Магазин %s временно отключен', $product->getShop()->getName()));
            return $this->redirect($request->headers->get('referer', $this->generateUrl('homepage')));
        }

        /** @var MobileDetector $mobileDetector */
        $mobileDetector = $this->get('mobile_detect.mobile_detector.default');

        $device = 'desktop';
        if ($mobileDetector->isMobile()) {
            $device = 'mobile';
        } elseif ($mobileDetector->isTablet()) {
            $device = 'tablet';
        }

        $url = new Url($product->getUrl());

        $ulp = $url->getQueryParam('ulp');
        $url->unsetQueryParam('ulp');
        $url->setQueryParam('subid', $device);

        // параметр ulp должен быть в конце
        if ($ulp) {
            $url->setQueryParam('ulp', $ulp);
        }

        $url = $url->getUrl();

        return $this->redirect($url, 302, $product->getShop());
    }

    /**
     * @param Product[] $products
     * @param array $options
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cardsAction($products, $options = array())
    {
        $options = array_merge([
            'show_preview' => false,
            'from' => null
        ], $options);

        return $this->render('@Main/Product/cards.html.twig', [
            'products' => $products,
            'options' => $options
        ]);
    }

    public function listAction(Request $request)
    {
        if ($request->query->get('tagId')) {
            /** @var Tag $tag */
            $tag = $this->getDocumentManager()->find(Tag::class, $request->query->get('tagId'));

            if (!$tag) {
                return $this->json([]);
            }

            $searchRequest = $this->getSearchRequestFactory()->createTagSearchRequest($tag);
        } else {
            $searchRequest = $this->getSearchRequestFactory()->createSearchRequest();
        }

        $searchRequest->setFiltersFromRequest($request);

        $result = $this->getSearchManager()->simpleSearch($searchRequest);

        $data = [
            'products' => $this->renderView('@Main/Product/cards.html.twig', [
                'products' => $result->getProducts()
            ]),
            'pagination' => $this
                ->get('knp_paginator.twig.extension.pagination')
                ->render($this->get('twig'), $result->getPagination(), '@Main/pagination.html.twig')
        ];

        return $this->json($data);
    }
}
