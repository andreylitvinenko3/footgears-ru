<?php

namespace Footgears\MainBundle\Controller;

use Footgears\MainBundle\Document\User;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        
        /* @var AuthenticationUtils $helper */
        $helper = $this->container->get('security.authentication_utils');

        $error = $helper->getLastAuthenticationError();

        if ($error instanceof LockedException) {
            $error = 'Аккаунт заблокирован';
        } elseif ($error instanceof DisabledException) {
            $error = 'Аккаунт не активен';
        } elseif ($error) {
            $error = $error->getMessage();
        }

        if ($error) {
            $this->addFlash('error', $error);
        }

        return $this->render('@Main/Security/login.html.twig');
    }
}
