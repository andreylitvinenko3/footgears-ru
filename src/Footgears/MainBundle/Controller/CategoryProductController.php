<?php

namespace Footgears\MainBundle\Controller;

use Doctrine\ORM\AbstractQuery;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\RecommendProduct;
use Footgears\MainBundle\Document\Tag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryProductController extends Controller
{
    /**
     * @param Request $request
     * @param $alias
     * @param $id
     * @return Response
     */
    public function indexAction(Request $request, $alias, $id)
    {
        /** @var Category $category */
        $category = $this->getDocumentManager()->getRepository(Category::class)->findOneByAlias($alias);
        if (!$category) {
            throw $this->createNotFoundException();
        }

        /** @var RecommendProduct $recommendProduct */
        $recommendProduct = $this
            ->getDocumentManager()
            ->getRepository(RecommendProduct::class)
            ->find(RecommendProduct::generateId($category->getId(), $id))
        ;
        if (!$recommendProduct) {
            throw $this->createNotFoundException();
        }

        $product = $recommendProduct->getProduct();

        $tags = [];
        if ($product->getCategory()) {
            $result = $this
                ->getDocumentManager()
                ->getRepository(Tag::class)
                ->createQueryBuilder()
                ->field('category')->references($product->getCategory())
                ->field('active')->equals(true)
                ->field('type')->in([Tag::TYPE_POPULAR, Tag::TYPE_POPULAR_TOP])
                ->field('productsCount')->gt(10)
                ->getQuery()
                ->toArray()
            ;

            shuffle($result);
            $tags = array_slice($result, 0, 10);
        }

        return $this->render('@Main/CategoryProduct/index.html.twig', [
            'recommendProduct' => $recommendProduct,
            'product' => $product,
            'similarProducts' => [],
            'tags' => $tags,
        ]);
    }
}
