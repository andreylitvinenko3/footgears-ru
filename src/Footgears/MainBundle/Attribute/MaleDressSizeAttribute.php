<?php

namespace Footgears\MainBundle\Attribute;

class MaleDressSizeAttribute extends SizeAttribute
{
    public function __construct($firstId)
    {
        parent::__construct($firstId, 'Размер мужской одежды');
        $this
            ->createValue([38,'3XS'])
            ->createValue([40,'3XS', '2XS'])
            ->createValue([42,'2XS'])
            ->createValue([44,'XS'])
            ->createValue([46,'XS', 'S'])
            ->createValue([48,'S', 'M'])
            ->createValue([50, 'M', 'L'])
            ->createValue([52, 'L', 'XL'])
            ->createValue([54, 'XL', '2XL'])
            ->createValue([56, '2XL', '3XL'])
            ->createValue([58, '3XL'])
            ->createValue([60, '4XL'])
            ->createValue([62, '4XL', '5XL'])
            ->createValue([64, '5XL', '6XL'])
            ->createValue([66, '6XL'])
            ->createValue([68, '6XL', '7XL'])
            ->createValue([70, '7XL'])
        ;
    }
}
