<?php

namespace Footgears\MainBundle\Attribute;

class ContentAttribute extends ExactFormsAttribute
{
    public function __construct($firstId)
    {
        parent::__construct($firstId, 'Состав');

        $this
            ->addAlias('материал')
        ;

        $this
            ->createValue(['Хлопок', 'хлопка', 'хлопковый', 'хлопковая'])
            ->createValue(['Кожа', 'кожи', 'кожаный', 'кожаная'])
            ->createValue(['Полиамид', 'Полиамида'])
            ->createValue(['Кашемир', 'Кашемира', 'кашемировый'])
            ->createValue(['Мех', 'Меха', 'меховой', 'меховая'])
            ->createValue(['Перо', 'Пера', 'перьевой', 'перьевая'])
            ->createValue(['Нейлон', 'Нейлона', 'нейлоновый'])
            ->createValue(['Лайкра', 'Лайкры'])
            ->createValue(['Полиуретан', 'Полиуретана', 'полиуретановая'])
            ->createValue('Искусственная кожа', 'искусственной кожи') // 9010
            ->createValue('Искусственная замша')
            ->createValue('Текстиль')
            ->createValue(['Муслин', 'Муслина'])
            ->createValue(['Атлас', 'Атласа'])
            ->createValue('Лаковая кожа')
            ->createValue(['Твил', 'Твила'])
            ->createValue(['Гипюр', 'Гипюра'])
            ->createValue('Джерси')
            ->createValue(['Неопрен', 'Неопрена', 'неопреновый'])
            ->createValue(['Пух', 'Пуха', 'пуховой']) // 9020
            ->createValue(['Шерсть', 'Шерсти', 'шерстяной'])
            ->createValue(['Эластан', 'Эластана'])
            ->createValue(['Акрил', 'Акрила', 'акриловая'])
            ->createValue(['Резина', 'Резины', 'резиновый', 'резиновая', 'резиновое'])
            ->createValue(['Вискоза', 'Вискозы']) // 9025
            ->createValue(['Лен', 'Льна', 'льняной'])
            ->createValue(['Альпака', 'Альпаки'])
            ->createValue(['Шелк', 'Шелка', 'шелковый', 'шелковая'])
            ->createValue(['Тенсел', 'Тенсела'])
            ->createValue(['Замша', 'Замши', 'замшевая']) // 9030
            ->createValue(['Нубук', 'Нубука'])
            ->createValue(['Овчина', 'Овчины'])
            ->createValue(['Полиэстер', 'Полиэстера'])
            ->createValue('ПВХ')
            ->createValue(['Ангора', 'Ангоры'])
            ->createValue(['Шифон', 'Шифона'])
            ->createValue(['Фланель', 'Фланельа'])
            ->createValue(['Вельвет', 'Вельвета'])
            ->createValue(['Силикон', 'Силикона', 'силиконовая', 'силиконовое'])
        ;
    }

    public function normalize($val)
    {
        $val = parent::normalize($val);
        return preg_replace('/\d+\%\s?/iu', '', $val);
    }
}