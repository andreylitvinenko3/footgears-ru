<?php

namespace Footgears\MainBundle\Attribute;

class HeelHeightAttribute extends Attribute
{
    public function __construct($firstId)
    {
        parent::__construct($firstId, 'Высота каблука');

        $this
            ->createValue('До 3 сантиметров', 0, 3)
            ->createValue('3 - 5 см', 3, 5)
            ->createValue('5 - 7 см', 5, 7)
            ->createValue('7 - 10 см', 7, 10)
            ->createValue('От 10 сантиметров', 10, 100)
        ;
    }

    public function createValue($label, $min, $max)
    {
        $this->addValue(new Value($label, ['min' => $min, 'max' => $max]));
        return $this;
    }

    public function findValueIds($rawValues)
    {
        $ids = [];
        foreach ($this->normalize($rawValues) as $value) {
            foreach ($this->values as $val) {
                if ($value >= $val->getParameter('min') && $value <= $val->getParameter('max')) {
                    $ids[$val->getId()] = 1;
                }
            }
        }

        return array_keys($ids);
    }

    public function normalize($val)
    {
        if (preg_match_all('/\b\d+(?:\.\d+)?\b/', mb_strtolower($val), $matches)) {
            return $matches[0];
        } else {
            return [];
        }
    }
}
