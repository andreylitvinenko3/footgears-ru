<?php

namespace Footgears\MainBundle\Attribute;

class DefaultManager extends AbstractManager
{
    public function __construct()
    {
        $this->addFemaleShoeSizes();
        $this->addMaleShoeSizes();
        $this->addSeasons();
        $this->addYears();
        $this->addHeelType();

        $this->addAttribute(new HeelHeightAttribute(AbstractManager::HEEL_HEIGHT));
        $this->addAttribute(new FemaleDressSizeAttribute(AbstractManager::FEMALE_DRESS_SIZE));
        $this->addAttribute(new MaleDressSizeAttribute(AbstractManager::MALE_DRESS_SIZE));
        $this->addAttribute(new ContentAttribute(AbstractManager::CONTENT));
        $this->addAttribute(new Gender(AbstractManager::GENDER));
        $this->addAttribute(new Manufacturer(AbstractManager::MANUFACTURER));
        $this->addAttribute(new ColorAttribute(AbstractManager::COLOR));

    }

    private function addFemaleShoeSizes()
    {
        $attr = new SizeAttribute(AbstractManager::FEMALE_SHOE_SIZE, 'Размер женской обуви');

        for ($i = 30; $i <= 45; $i++) {
            $attr->createValue($i);
        }

        $this->addAttribute($attr);
    }

    private function addMaleShoeSizes()
    {
        $attr = new SizeAttribute(AbstractManager::MALE_SHOE_SIZE, 'Размер мужской обуви');

        for ($i = 39; $i <= 48; $i++) {
            $attr->createValue($i);
        }

        $this->addAttribute($attr);
    }

    private function addSeasons()
    {
        $attr = new ExactFormsAttribute(AbstractManager::SEAZON, 'Сезон');

        $attr
            ->addAlias('Сезонность')
            ->addAlias('Коллекция')
            ->createValue(['Весна/Осень', 'Демисезон', 'Мульти', 'Весна', 'Осень', 'spring'])
            ->createValue(['Лето', 'summer'])
            ->createValue(['Зима', 'winter'])
        ;

        $attr->setSort(100000);

        $this->addAttribute($attr);
    }

    private function addYears()
    {
        $attr = new ExactFormsAttribute(AbstractManager::YEAR, 'Год');
        $attr->addAlias('коллекция');

        for ($i = 2010; $i <= date('Y'); $i++) {
            $attr->createValue($i);
        }

        $this->addAttribute($attr);
    }

    private function addHeelType()
    {
        $attr = new ExactFormsAttribute(AbstractManager::HEEL_TYPE, 'Тип каблука');

        $attr
            ->createValue('Квадратный')
            ->createValue('Конусообразный')
            ->createValue('Платформа')
            ->createValue('Плоская подошва')
            ->createValue('Танкетка')
            ->createValue('Шпилька')
        ;

        $this->addAttribute($attr);
    }
}
