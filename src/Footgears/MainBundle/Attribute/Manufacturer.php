<?php

namespace Footgears\MainBundle\Attribute;

class Manufacturer extends ExactFormsAttribute
{
    protected static $chinaValue = 'Китай';

    public static function getChinaValue()
    {
        return self::$chinaValue;
    }

    public function __construct($firstId)
    {
        parent::__construct($firstId, 'Страна производитель');

        $this
            ->addAlias('производство')
            ->addAlias('Страна-изготовитель')
        ;

        $this
            ->createValue(['Россия'])
            ->createValue([self::$chinaValue])
            ->createValue(['Италия'])
            ->createValue(['Болгария'])
            ->createValue(['Индия'])
            ->createValue(['США'])
            ->createValue(['Турция'])
            ->createValue(['Германия'])
            ->createValue(['Франция'])
            ->createValue(['Испания'])
            ->createValue(['Португалия'])
            ->createValue(['Пакистан'])
            ->createValue(['Таиланд'])
            ->createValue(['Индонезия'])
            ->createValue(['Швеция'])
            ->createValue(['Украина'])
            ->createValue(['Англия', 'Великобритания'])
            ->createValue(['Беларусь', 'Белоруссия'])
            ->createValue(['Польша'])
            ->createValue(['Тайвань'])
            ->createValue(['Малайзия'])
            ->createValue(['Эстония'])
            ->createValue(['Финляндия'])
            ->createValue(['Мексика'])
        ;
    }
}