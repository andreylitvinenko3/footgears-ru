<?php

namespace Footgears\MainBundle\Attribute;

class ExactFormsAttribute extends Attribute
{
    public function createValue($values)
    {
        $values = (array)$values;
        $this->addValue(new Value($values[0], ['values' => $values]));
        return $this;
    }

    public function findValueIds($rawValues)
    {
        $ids = [];

        foreach ($this->normalize($rawValues) as $val) {
            $val = mb_strtolower($val);
            foreach ($this->values as $id => $value) {
                if (isset($ids[$id])) {
                    continue;
                }
                foreach ($value->getParameter('values') as $valueForm) {
                    if ($val == mb_strtolower($valueForm)) {
                        $ids[$id] = 1;
                    }
                }
            }
        }
        return array_keys($ids);
    }

    public function normalize($val)
    {
        if (preg_match_all('/[a-z0-9а-яё]+/iu', mb_strtolower($val), $matches)) {
            return $matches[0];
        } else {
            return [];
        }
    }
}
