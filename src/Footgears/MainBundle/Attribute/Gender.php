<?php

namespace Footgears\MainBundle\Attribute;

class Gender extends ExactFormsAttribute
{
    public static $manKeys = ['мужчинам', 'мужской', 'man', 'male', 'men'];
    public static $womanKeys = ['женщинам', 'женский', 'woman', 'female', 'women', 'womens', 'lady', 'ladies', 'lady', 'femininas', 'girl', 'girls'];

    public function __construct($firstId)
    {
        parent::__construct($firstId, 'Пол');

        $this
            ->createValue(self::$womanKeys)
            ->createValue(self::$manKeys)
            ->createValue(['юнисекс', 'unisex', 'унисекс'])
        ;
    }
}