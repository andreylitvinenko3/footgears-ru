<?php

namespace Footgears\MainBundle\Attribute;

abstract class Attribute
{
    protected $name = '';
    protected $hint = '';
    protected $aliases = [];
    protected $firstId = 0;

    protected $sort = 0;

    /**
     * @var Value[]
     */
    protected $values = [];

    public function __construct($firstId, $name, $hint = '')
    {
        $this->firstId = $firstId;
        $this->name = $name;
        $this->hint = $hint;
    }

    public function addValue(Value $value)
    {
        $id = $this->firstId + count($this->values);
        $value->setId($id);
        $this->values[$id] = $value;
        return $this;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function getValue($id)
    {
        return $this->values[$id];
    }

    abstract public function findValueIds($rawValues);

    public function supports($name)
    {
        $name = mb_strtolower($name);
        if (mb_strtolower($this->name) == $name) {
            return true;
        }
        foreach ($this->aliases as $alias) {
            if (mb_strtolower($alias) == $name) {
                return true;
            }
        }
        return false;
    }

    public function addAlias($alias)
    {
        $this->aliases[] = mb_strtolower($alias);
        return $this;
    }

    public function getAliases()
    {
        return $this->aliases;
    }

    public function getFirstId()
    {
        return $this->firstId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getHint()
    {
        return $this->hint;
    }

    public function getClass()
    {
        return '';
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
        return $this;
    }
}
