<?php

namespace Footgears\MainBundle\Attribute;

class FemaleDressSizeAttribute extends SizeAttribute
{
    public function __construct($firstId)
    {
        parent::__construct($firstId, 'Размер женской одежды');
        $this
            ->createValue([38,'2XS'])
            ->createValue([40, '2XS', '3XS'])
            ->createValue([42, 'XS', 'S'])
            ->createValue([44, 'S'])
            ->createValue([46, 'M'])
            ->createValue([48, 'L'])
            ->createValue([50, 'L', 'XL'])
            ->createValue([52, 'XL', '2XL'])
            ->createValue([54, '2XL', '3XL'])
            ->createValue([56, '3XL'])
            ->createValue([58, '4XL'])
            ->createValue([60, '5XL'])
            ->createValue([62, '6XL'])
        ;
    }
}
