<?php

namespace Footgears\MainBundle\Attribute;

class Manager
{
    const FEMALE_SHOE_SIZE = 1;
    const MALE_SHOE_SIZE = 1001;
    const FEMALE_DRESS_SIZE = 2001;
    const MALE_DRESS_SIZE = 3001;
    const COLOR = 4001;
    const SEAZON = 5001;
    const YEAR = 6001;
    const HEEL_HEIGHT = 7001;
    const HEEL_TYPE = 8001;
    const CONTENT = 9001;
    const MANUFACTURER = 11001;

    /**
     * @var Attribute[]
     */
    protected $attributes = [];

    public function __construct()
    {
        $this->addFemaleShoeSizes();
        $this->addMaleShoeSizes();
        $this->addColors();
        $this->addSeasons();
        $this->addYears();
        $this->addHeelType();

        $this->addAttribute(new HeelHeightAttribute(self::HEEL_HEIGHT));
        $this->addAttribute(new FemaleDressSizeAttribute(self::FEMALE_DRESS_SIZE));
        $this->addAttribute(new MaleDressSizeAttribute(self::MALE_DRESS_SIZE));
        $this->addAttribute(new ContentAttribute(self::CONTENT));
        $this->addAttribute(new Manufacturer(self::MANUFACTURER));
    }

    public function addAttribute(Attribute $attribute)
    {
        return $this->attributes[$attribute->getFirstId()] = $attribute;
    }

    public function getAttribute($firstId)
    {
        if (!isset($this->attributes[$firstId])) {
            return null;
        }
        return $this->attributes[$firstId];
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function findAttributes($name)
    {
        $attributes = [];
        foreach ($this->attributes as $attribute) {
            if ($attribute->supports($name)) {
                $attributes[] = $attribute;
            }
        }
        return $attributes;
    }

    private function addFemaleShoeSizes()
    {
        $attr = new SizeAttribute(self::FEMALE_SHOE_SIZE, 'Размер женской обуви');

        for ($i = 30; $i <= 45; $i++) {
            $attr->createValue($i);
        }

        $this->addAttribute($attr);
    }

    private function addMaleShoeSizes()
    {
        $attr = new SizeAttribute(self::MALE_SHOE_SIZE, 'Размер мужской обуви');

        for ($i = 39; $i <= 48; $i++) {
            $attr->createValue($i);
        }

        $this->addAttribute($attr);
    }

    private function addColors()
    {
        $attr = new ExactFormsAttribute(self::COLOR, 'Цвет');
        $attr
            ->addAlias('цвета')
            ->addAlias('color')
            ->addAlias('colors')
            ->createValue(['синий', 'blue'])
            ->createValue(['фиолетовый', 'purple'])
            ->createValue(['белый', 'white'])
            ->createValue(['серый', 'gray'])
            ->createValue(['черный', 'black'])
            ->createValue('голубой')
            ->createValue(['желтый', 'yellow'])
            ->createValue(['оранжевый', 'рыжий', 'ржавый', 'orange'])
            ->createValue(['красный', 'red'])
            ->createValue(['розовый', 'pink'])
            ->createValue(['бежевый', 'beige'])
            ->createValue(['коричневый', 'brown'])
            ->createValue(['зеленый', 'green'])
            ->createValue(['камуфляж', 'camouflage', 'camo', 'army'])
        ;

        $this->addAttribute($attr);
    }

    public function getAttributeByValueId($attributeValueId)
    {
        $firstId = floor($attributeValueId / 1000) * 1000 + 1;
        return $this->getAttribute($firstId);
    }

    private function addSeasons()
    {
        $attr = new ExactFormsAttribute(self::SEAZON, 'Сезон');

        $attr
            ->addAlias('Сезонность')
            ->addAlias('Коллекция')
            ->createValue(['Весна/Осень', 'Демисезон', 'Мульти', 'Весна', 'Осень'])
            ->createValue('Лето')
            ->createValue('Зима')
        ;

        $this->addAttribute($attr);
    }

    private function addYears()
    {
        $attr = new ExactFormsAttribute(self::YEAR, 'Год');
        $attr->addAlias('коллекция');

        for ($i = 2010; $i <= date('Y'); $i++) {
            $attr->createValue($i);
        }

        $this->addAttribute($attr);
    }

    private function addHeelType()
    {
        $attr = new ExactFormsAttribute(self::HEEL_TYPE, 'Тип каблука');

        $attr
            ->createValue('Квадратный')
            ->createValue('Конусообразный')
            ->createValue('Платформа')
            ->createValue('Плоская подошва')
            ->createValue('Танкетка')
            ->createValue('Шпилька')
        ;

        $this->addAttribute($attr);
    }
}
