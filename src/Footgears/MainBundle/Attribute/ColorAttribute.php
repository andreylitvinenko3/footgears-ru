<?php

namespace Footgears\MainBundle\Attribute;

class ColorAttribute extends ExactFormsAttribute
{
    public function __construct($firstId)
    {
        parent::__construct($firstId, 'Цвет');

        $this
            ->addAlias('цвета')
            ->addAlias('color')
            ->addAlias('colors')
            ->createValue(['синий', 'blue'])
            ->createValue(['фиолетовый', 'purple'])
            ->createValue(['белый', 'white'])
            ->createValue(['серый', 'gray'])
            ->createValue(['черный', 'black'])
            ->createValue('голубой')
            ->createValue(['желтый', 'yellow'])
            ->createValue(['оранжевый', 'рыжий', 'ржавый', 'orange'])
            ->createValue(['красный', 'red'])
            ->createValue(['розовый', 'pink'])
            ->createValue(['бежевый', 'beige'])
            ->createValue(['коричневый', 'brown'])
            ->createValue(['зеленый', 'green'])
            ->createValue(['камуфляж', 'camouflage', 'camo', 'army'])
        ;

        $this->setSort(100);
    }
}