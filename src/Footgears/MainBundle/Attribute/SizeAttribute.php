<?php

namespace Footgears\MainBundle\Attribute;

class SizeAttribute extends ExactFormsAttribute
{
    public function normalize($val)
    {
        if (preg_match_all('/\b(\d+|[2-7]X[SL]|XS|S|M|L|XL)\b/i', $val, $matches)) {
            return $matches[1];
        }
        return [];
    }

//    public function getClass()
//    {
//        return 'size';
//    }
}
