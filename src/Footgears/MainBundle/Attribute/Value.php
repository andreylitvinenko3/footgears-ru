<?php

namespace Footgears\MainBundle\Attribute;

class Value
{
    protected $id;
    protected $label;
    protected $parameters = [];

    public function __construct($label, $parameters = [])
    {
        $this->label = $label;
        $this->parameters = (array)$parameters;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function getParameter($key)
    {
        return $this->parameters[$key];
    }

    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }
}
