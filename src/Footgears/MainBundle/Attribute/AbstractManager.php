<?php

namespace Footgears\MainBundle\Attribute;

class AbstractManager
{
    const FEMALE_SHOE_SIZE = 1;
    const MALE_SHOE_SIZE = 1001;
    const FEMALE_DRESS_SIZE = 2001;
    const MALE_DRESS_SIZE = 3001;
    const COLOR = 4001;
    const SEAZON = 5001;
    const YEAR = 6001;
    const HEEL_HEIGHT = 7001;
    const HEEL_TYPE = 8001;
    const CONTENT = 9001;
    const GENDER = 10001;
    const MANUFACTURER = 11001;

    /**
     * @var Attribute[]
     */
    protected $attributes = [];

    public function getAttributes()
    {
        return $this->attributes;
    }

    protected function addAttribute(Attribute $attribute)
    {
        return $this->attributes[$attribute->getFirstId()] = $attribute;
    }

    public function getAttribute($firstId)
    {
        if (!isset($this->attributes[$firstId])) {
            return null;
        }

        return $this->attributes[$firstId];
    }

    public function findAttributes($name)
    {
        $attributes = [];
        foreach ($this->attributes as $attribute) {
            if ($attribute->supports($name)) {
                $attributes[] = $attribute;
            }
        }
        return $attributes;
    }

    public function getAttributeByValueId($attributeValueId)
    {
        $firstId = floor($attributeValueId / 1000) * 1000 + 1;
        return $this->getAttribute($firstId);
    }
}