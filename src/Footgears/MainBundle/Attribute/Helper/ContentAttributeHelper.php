<?php

namespace Footgears\MainBundle\Attribute\Helper;

class ContentAttributeHelper extends ExactAttributeHelper
{
    public function normalize($value)
    {
        $value = parent::normalize($value);
        return preg_replace('/\d+\%\s?/iu', '', $value);
    }

    public function getName()
    {
        return 'content';
    }
}