<?php

namespace Footgears\MainBundle\Attribute\Helper;

use Footgears\MainBundle\Document\Attribute;

class RangeAttributeHelper implements AttributeHelperInterface
{
    public function findValueIds(Attribute $attribute, $rawValues)
    {
        $ids = [];
        foreach ($this->normalize($rawValues) as $value) {
            foreach ($attribute->getValues() as $val) {
                if ($val->hasParameter('min') && $val->hasParameter('max') && $value >= $val->getParameter('min') && $value <= $val->getParameter('max')) {
                    $ids[$val->getId()] = 1;
                }
            }
        }

        return array_keys($ids);
    }

    public function normalize($value)
    {
        if (preg_match_all('/\b\d+(?:\.\d+)?\b/', mb_strtolower($value), $matches)) {
            return $matches[0];
        } else {
            return [];
        }
    }

    public function getName()
    {
        return 'range';
    }
}