<?php

namespace Footgears\MainBundle\Attribute\Helper;

class Registry
{
    /**
     * @var array|AttributeHelperInterface[] $helpers
     */
    protected $helpers = [];

    public function __construct()
    {
        $this
            ->add(new SizeAttributeHelper())
            ->add(new ExactAttributeHelper())
            ->add(new RangeAttributeHelper())
            ->add(new ContentAttributeHelper())
        ;
    }

    protected function add(AttributeHelperInterface $helper)
    {
        $this->helpers[$helper->getName()] = $helper;
        return $this;
    }

    public function get($name)
    {
        if (!array_key_exists($name, $this->helpers)) {
            throw new \InvalidArgumentException();
        }

        return $this->helpers[$name];
    }
}