<?php

namespace Footgears\MainBundle\Attribute\Helper;

class SizeAttributeHelper extends ExactAttributeHelper
{
    public function normalize($val)
    {
        if (preg_match_all('/\b(\d+|[2-7]X[SL]|XS|S|M|L|XL)\b/i', $val, $matches)) {
            return $matches[1];
        }
        return [];
    }

    public function getName()
    {
        return 'size';
    }
}