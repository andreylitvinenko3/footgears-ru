<?php

namespace Footgears\MainBundle\Attribute\Helper;

use Footgears\MainBundle\Document\Attribute;

class ExactAttributeHelper implements AttributeHelperInterface
{
    public function findValueIds(Attribute $attribute, $rawValues)
    {
        $ids = [];

        foreach ($this->normalize($rawValues) as $val) {
            $val = mb_strtolower($val);
            foreach ($attribute->getValues() as $value) {
                if (isset($ids[$value->getId()])) {
                    continue;
                }

                if ($value->hasParameter('values')) {
                    foreach ($value->getParameter('values') as $valueForm) {
                        if ($val == mb_strtolower($valueForm)) {
                            $ids[$value->getId()] = 1;
                        }
                    }
                }
            }
        }

        return array_keys($ids);
    }

    public function normalize($val)
    {
        if (preg_match_all('/[a-z0-9а-яё]+/iu', mb_strtolower($val), $matches)) {
            return $matches[0];
        } else {
            return [];
        }
    }

    public function getName()
    {
        return 'exact';
    }
}