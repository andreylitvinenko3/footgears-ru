<?php

namespace Footgears\MainBundle\Attribute\Helper;

use Footgears\MainBundle\Document\Attribute;

interface AttributeHelperInterface
{
    public function findValueIds(Attribute $attribute, $rawValues);
    public function getName();
}