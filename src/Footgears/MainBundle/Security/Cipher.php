<?php

namespace Footgears\MainBundle\Security;

class Cipher
{
    public function __construct($key)
    {
    }

    public function encrypt($input)
    {
        return base64_encode($this->foo($input));
    }

    public function decrypt($input)
    {
        return $this->foo(base64_decode($input));
    }

    protected function foo($string)
    {

        for ($i = 0; $i < strlen($string); $i++) {
            $string[$i] = chr(ord($string[$i]) ^ 17);
        }
        return $string;
    }
}
