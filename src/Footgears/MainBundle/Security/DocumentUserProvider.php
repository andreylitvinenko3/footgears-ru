<?php

namespace Footgears\MainBundle\Security;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Footgears\MainBundle\Document\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class DocumentUserProvider implements UserProviderInterface, OAuthAwareUserProviderInterface
{
    /**
     * @var ObjectManager
     */
    protected $dm;

    /**
     * @var string
     */
    protected $class = User::class;

    /**
     * @var ObjectRepository
     */
    protected $repository;

    /**
     * @var array
     */
    protected $properties = array(
        'identifier' => 'id',
        'vkontakte' => 'vkId',
        'odnoklassniki' => 'okId'
    );

    public function __construct(DocumentManager $dm, array $properties = [])
    {
        $this->dm = $dm;
        $this->properties = array_merge($this->properties, $properties);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        $username = mb_strtolower($username);

        if (false !== strpos($username, '@')) {
            $criteria = ['email' => $username];
        } else {
            $criteria = ['username' => $username];
        }

        $user = $this->findUser($criteria);
        if (!$user) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
//dump($username, $response->getEmail(), $response->getNickname(), $response->getResponse()); die;
        $user = $this->findUser([$this->getProperty($response) => $username]);
        if (null === $user || null === $username) {

            $email = $response->getEmail();
            if (!$email) {
                $email = User::createFakeEmail();
            }

            try {
                $user = $this->loadUserByUsername($email);
            } catch (UsernameNotFoundException $e) {
                $user = new User();

                $user
                    ->setEmail($email)
                    ->setUsername($email)
                    ->setPlainPassword(md5($email . $response->getAccessToken()))
                ;
            }

            $accessor = PropertyAccess::createPropertyAccessor();
            $accessor->setValue($user, $this->getProperty($response), $username);

            $this->updateUser($user);
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        $accessor   = PropertyAccess::createPropertyAccessor();
        $identifier = $this->properties['identifier'];
        if (!$this->supportsClass(get_class($user)) || !$accessor->isReadable($user, $identifier)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $userId = $accessor->getValue($user, $identifier);
        if (null === $user = $this->findUser(array($identifier => $userId))) {
            throw new UsernameNotFoundException(sprintf('User with ID "%d" could not be reloaded.', $userId));
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === $this->class || is_subclass_of($class, $this->class);
    }

    /**
     * @param array $criteria
     *
     * @return object
     */
    protected function findUser(array $criteria)
    {
        if (null === $this->repository) {
            $this->repository = $this->dm->getRepository($this->class);
        }

        return $this->repository->findOneBy($criteria);
    }

    /**
     * Gets the property for the response.
     *
     * @param UserResponseInterface $response
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getProperty(UserResponseInterface $response)
    {
        $resourceOwnerName = $response->getResourceOwner()->getName();

        if (!isset($this->properties[$resourceOwnerName])) {
            throw new \RuntimeException(sprintf("No property defined for entity for resource owner '%s'.", $resourceOwnerName));
        }

        return $this->properties[$resourceOwnerName];
    }

    private function updateUser(User $user)
    {
        $this->dm->persist($user);
        $this->dm->flush();
    }
}
