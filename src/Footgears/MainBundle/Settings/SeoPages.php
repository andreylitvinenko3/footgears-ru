<?php

namespace Footgears\MainBundle\Settings;

class SeoPages
{
    /**
     * @var Fields
     */
    protected $main;

    /**
     * @var Fields
     */
    protected $about;

    /**
     * @var Fields
     */
    protected $contacts;

    /**
     * @var Fields
     */
    protected $partnership;

    /**
     * @var Fields
     */
    protected $rules;

    /**
     * @var Fields
     */
    protected $return;

    /**
     * @var Fields
     */
    protected $sitemap;

    /**
     * @var Fields
     */
    protected $brands;

    /**
     * @var Fields
     */
    protected $brandsAZ;

    /**
     * @var Fields
     */
    protected $brandsRUS;

    /**
     * @var Fields
     */
    protected $brands09;

    /**
     * @var Fields
     */
    protected $brand;

    /**
     * @var Fields
     */
    protected $shops;

    /**
     * @var Fields
     */
    protected $shop;

    /**
     * @var Fields
     */
    protected $product;

    /**
     * @var Fields
     */
    protected $category;

    /**
     * @var Fields
     */
    protected $categoryBrand;

    /**
     * @var Fields
     */
    protected $categoryShop;

    /**
     * @var Fields
     */
    protected $tag;

    protected $categorySale;

    protected $categoryBrandSale;

    protected $categoryShopSale;

    protected $brandSale;

    protected $sale;

    public function __construct(array $settings)
    {
        $this->init($settings);
    }

    protected function init(array $settings)
    {
        foreach ($settings as $page => $data) {
            if (!property_exists($this, $page)) {
                throw new \LogicException(sprintf('Property %s in %s not found', $page, get_class($this)));
            }

            $fields = new Fields();
            $fields
                ->setTitle($data['title'])
                ->setDescription($data['description'])
                ->setKeywords($data['keywords'])
            ;

            $this->$page = $fields;
        }
    }

    public function getMain()
    {
        return $this->main;
    }

    public function getAbout()
    {
        return $this->about;
    }

    public function getContacts()
    {
        return $this->contacts;
    }

    public function getPartnership()
    {
        return $this->partnership;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getReturn()
    {
        return $this->return;
    }

    public function getSitemap()
    {
        return $this->sitemap;
    }

    public function getBrands()
    {
        return $this->brands;
    }

    public function getBrandsAZ()
    {
        return $this->brandsAZ;
    }

    public function getBrandsRUS()
    {
        return $this->brandsRUS;
    }

    public function getBrands09()
    {
        return $this->brands09;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getShops()
    {
        return $this->shops;
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getCategoryBrand()
    {
        return $this->categoryBrand;
    }

    public function getCategoryShop()
    {
        return $this->categoryShop;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getCategorySale()
    {
        return $this->categorySale;
    }

    public function getCategoryBrandSale()
    {
        return $this->categoryBrandSale;
    }

    public function getCategoryShopSale()
    {
        return $this->categoryShopSale;
    }

    public function getBrandSale()
    {
        return $this->brandSale;
    }

    public function getSale()
    {
        return $this->sale;
    }
}