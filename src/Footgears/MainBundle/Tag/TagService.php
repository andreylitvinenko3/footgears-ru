<?php

namespace Footgears\MainBundle\Tag;

use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Search\TagSearchRequest;
use Footgears\MainBundle\Service\ContainerWrapper;

class TagService
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    public function updateProductsCount(Tag $tag)
    {
        $dm = $this->container->getDocumentManager();

        $searchRequest = new TagSearchRequest($tag);
        $productsCount = $this->container->getSearchManager()->countProducts($searchRequest);

        $tag
            ->setProductsCount($productsCount)
            ->setProductsCountUpdated(new \DateTime())
        ;
        $dm->persist($tag);
        $dm->flush();
    }
}