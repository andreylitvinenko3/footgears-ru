<?php

namespace Footgears\MainBundle\Process;

use Sirian\Helpers\Process\Event\ProcessEvent;

class Queue extends \Sirian\Helpers\Process\Queue
{
    public function run()
    {
        if ($this->isStarted) {
            return;
        }

        $this->isStarted = true;

        while (!$this->isDrain()) {
            $this->tick();

            usleep(1000);
        }

        $this->isStarted = false;
    }

    public function tick()
    {
        foreach ($this->activeProcesses as $key => $process) {
            if (!$process->isRunning()) {
                $this->eventDispatcher->dispatch(self::EVENT_PROCESS_FINISHED, new ProcessEvent($process));
                unset($this->activeProcesses[$key]);
            }
        }

        if (!$this->isSaturated()) {
            $this->process();
        }
    }
}