<?php

namespace Footgears\MainBundle\Process\Provider;

use Sirian\Helpers\Process\Provider\ProviderInterface;

class IteratorProvider implements ProviderInterface
{
    /**
     * @var \Iterator $iterator
     */
    protected $iterator;

    public function __construct(\Iterator $iterator)
    {
        $this->iterator = $iterator;
        $this->iterator->rewind();
    }

    public function provide()
    {
        $process = $this->iterator->current();
        $this->iterator->next();
        return $process;
    }

    public function isDrain()
    {
        return !$this->iterator->valid();
    }
}