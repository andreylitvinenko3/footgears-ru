<?php

namespace Footgears\MainBundle\Pagination;

use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

class Pagination extends SlidingPagination
{
    protected $maxPage;

    public function __construct($params = [])
    {
        parent::__construct($params);
        $this->setCustomParameters([]);
        $this->setPaginatorOptions([]);
    }

    public function getPaginationData()
    {
        $realCount = $this->totalCount;
        if ($this->maxPage) {
            $this->totalCount = min($realCount, $this->maxPage * $this->numItemsPerPage);
        }
        $data = parent::getPaginationData();
        $this->totalCount = $realCount;
        $data['totalCount'] = $realCount;
        $data['items'] = $this->getItems();

        return $data;
    }

    public function setMaxPage($maxPage)
    {
        $this->maxPage = $maxPage;
    }

    public function getOffset()
    {
        return ($this->getCurrentPageNumber() - 1) * $this->getItemNumberPerPage();
    }
}
