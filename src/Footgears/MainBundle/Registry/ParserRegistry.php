<?php

namespace Footgears\MainBundle\Registry;

use Footgears\MainBundle\YandexMarketLanguage\BaseParser;
use Footgears\MainBundle\YandexMarketLanguage\ParserInterface;
use Footgears\MainBundle\YandexMarketLanguage\YooxParser;

class ParserRegistry
{
    protected $parsers = [];

    public function __construct()
    {
        $this
            ->add(new BaseParser())
            ->add(new YooxParser())
        ;
    }

    public function add(ParserInterface $parser)
    {
        $this->parsers[$parser->getName()] = $parser;
        return $this;
    }

    public function get($name)
    {
        if (!isset($this->parsers[$name])) {
            throw new \InvalidArgumentException();
        }

        return $this->parsers[$name];
    }

    public function getNames()
    {
        return array_keys($this->parsers);
    }
} 