<?php

namespace Footgears\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Tag extends Constraint
{
    public $filterTagMessage = 'The filter tag must have only one attribute';

    public $emptySettingsMessage = 'Select attributes or specify a search phrase';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}