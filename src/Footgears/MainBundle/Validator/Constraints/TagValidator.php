<?php

namespace Footgears\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TagValidator extends ConstraintValidator
{
    public function validate($tag, Constraint $constraint)
    {
        if (!$tag instanceof \Footgears\MainBundle\Document\Tag) {
            throw new \LogicException();
        }

        if ($tag->getType() === \Footgears\MainBundle\Document\Tag::TYPE_FILTER) {
            if (count($tag->getAttributes()) !== 1) {
                $this->context->buildViolation($constraint->filterTagMessage)
                    ->atPath('attributes')
                    ->addViolation()
                ;
            }
        }

        if (!count($tag->getAttributes()) || empty($tag->getSearchPhrase())) {
            if (count($tag->getAttributes()) !== 1) {
                $this->context->buildViolation($constraint->emptySettingsMessage)
                    ->atPath('attributes')
                    ->addViolation()
                ;
            }
        }
    }
}
