<?php


namespace Footgears\MainBundle\YandexMarketLanguage;


interface ParserInterface 
{
    public function getName();
    public function isNeedPrepareFeed();
    public function getOffersCount();
    public function parse($file);
}