<?php

namespace Footgears\MainBundle\YandexMarketLanguage;

use Sirian\YMLParser\Offer\VendorModelOffer;

class Offer extends VendorModelOffer
{
    const IS_DRESS = 'dress';
    const IS_SHOES = 'shoes';

    protected static $dressTemplates = [
        'бель', 'Блуз', 'боди', 'бриджи', 'брюки', 'бюстгальтер', 'ветровк', 'джемпер', 'джинсы', 'дубленк', 'жакет',
        'жилет', 'капри', 'кардиган', 'кимоно', 'колгот', 'комбинезон', 'корсаж', 'костюм', 'кофт', 'купальник',
        'куртк', 'леггинсы', 'лонгслив', 'лосины', 'майк', 'накидк', 'носки', 'одежд', 'пальто', 'пиджак', 'пижам',
        'плать', 'плащ', 'пуховик', 'рубашк', 'сарафан', 'свитер', 'свитшот', 'термобелье', 'толстовк', 'топ',
        'туник', 'футболк', 'халат', 'чулки', 'шорты', 'штаны', 'шуб', 'юбк', 'поло', 'блейзер', 'чинос', 'худи',
        'болеро', 'корсет'
    ];

    protected static $shoesTemplates = [
        'балетки', 'босоножки', 'ботильоны', 'ботинки', 'валенки', 'кеды', 'кроссовки', 'лоферы', 'мокасины', 'обув',
        'сабо', 'сандалии', 'сапоги', 'сапожки', 'сланцы', 'туфли', 'угги', 'унты', 'шлепанцы', 'эспадрильи',
        'вьетнамки', 'броги', 'платформ', 'каблук', 'танкетк', 'дерби', 'оксфорды', 'подошв'
    ];

    protected $oldPrice = 0;

    protected $sizes = '';

    protected $parameters = [];

    protected $shoesOrDress;

    public function getOldPrice()
    {
        return $this->oldPrice;
    }

    public function setOldPrice($oldPrice)
    {
        $this->oldPrice = (float)$oldPrice;
        return $this;
    }

    public function isMale()
    {
        return $this->hasCategory('/мужск|мужч/ui');
    }

    public function isFemale()
    {
        return $this->hasCategory('/женск|женщ/ui');
    }

    public function isDress()
    {
        $this->setDressShoesType();
        return $this->shoesOrDress === self::IS_DRESS;
    }

    public function isShoes()
    {
        $this->setDressShoesType();
        return $this->shoesOrDress === self::IS_SHOES;
    }

    protected function setDressShoesType()
    {
        if ($this->shoesOrDress) {
            return;
        }

        $category = $this->getCategory();

        $dressRegexp = '/(' . implode('|', self::$dressTemplates) . ')/iu';
        $shoesRegexp = '/(' . implode('|', self::$shoesTemplates) . ')/iu';

        while ($category) {
            $isDress = $isShoes = false;

            if (preg_match($dressRegexp, $category->getName())) {
                $isDress = true;
            }
            if (preg_match($shoesRegexp, $category->getName())) {
                $isShoes = true;
            }

            if ($isDress !== $isShoes) {
                $this->shoesOrDress = $isDress ? self::IS_DRESS : self::IS_SHOES;
                return;
            }

            $category = $category->getParent();
        }
    }

    private function hasCategory($regexp)
    {
        $category = $this->getCategory();
        while ($category) {
            if (preg_match($regexp, $category->getName())) {
                return true;
            }
            $category = $category->getParent();
        }
        return false;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function hasParameter($name)
    {
        $name = mb_strtolower($name);
        return isset($this->parameters[$name]);
    }

    public function getParameter($name)
    {
        $name = mb_strtolower($name);
        if (!$this->hasParameter($name)) {
            return null;
        }

        return $this->parameters[$name];
    }

    public function setParameters(array $parameters)
    {
        $this->parameters = [];
        foreach ($parameters as $name => $value) {
            $value = $this->prepareParameterValue($value);
            $this->parameters[mb_strtolower($name)] = $value;
        }

        return $this;
    }

    public function setParameter($name, $value)
    {
        $value = $this->prepareParameterValue($value);
        $name = mb_strtolower($name);
        $this->parameters[$name] = $value;
        return $this;
    }

    public function addParameter($name, $value)
    {
        $name = mb_strtolower($name);
        $value = $this->prepareParameterValue($value);

        if (isset($this->parameters[$name])) {
            $this->parameters[$name] .= ', ' . $value;
        } else {
            $this->setParameter($name, $value);
        }
    }

    public function addParameters($params)
    {
        $this->setParameters(array_merge($this->parameters, $params));
        return $this;
    }

    protected function prepareParameterValue($value)
    {
        // вырезаем проценты
        return preg_replace('/\d+\%\s?/iu', '', $value);
    }

    public function isDeleted()
    {
        return $this->getAttribute('deleted') === 'true';
    }
}
