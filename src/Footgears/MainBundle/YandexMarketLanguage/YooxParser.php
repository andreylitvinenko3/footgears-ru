<?php

namespace Footgears\MainBundle\YandexMarketLanguage;

use Sirian\YMLParser\Parser\CategoriesEvent;
use Sirian\YMLParser\Parser\ShopEvent;
use Sirian\YMLParser\Shop;

class YooxParser extends BaseParser
{
    private $path = [];

    public function prepareCategories($filename)
    {
        $shop = null;
        $this->path = [];

        $this->xmlReader->open($filename);

        $xml = $this->xmlReader;
        while ($xml->read()) {
            if ($xml->nodeType == \XMLReader::END_ELEMENT) {
                array_pop($this->path);
                continue;
            }


            if ($xml->nodeType == \XMLReader::ELEMENT) {
                array_push($this->path, $xml->name);
                $path = implode('/', $this->path);

                if ($xml->isEmptyElement) {
                    array_pop($this->path);
                }


                switch ($path) {
                    case 'yml_catalog/shop':
                        $shop = $this->factory->createShop();
                        $this->dispatch('shop', new ShopEvent($shop));
                        break;

                    case 'yml_catalog/shop/offers/offer/market_category':
                        $this->prepareCategory($shop);
                        break;

                    default:
                }
            }
        }

        $this->xmlReader->close();

        $this->dispatch('prepareCategories', new CategoriesEvent($shop->getCategories()));
    }

    protected function prepareCategory(Shop $shop)
    {
        $xml = $this->loadElementXml();

        $categoriesNames = explode(' / ', (string)$xml);

        $parent = null;
        foreach ($categoriesNames as $name) {
            $id = md5((string)$name);
            $name = (string)$name;

            if ($parent) {
                $id .= '_' . $parent->getId();
            }

            $category = $shop->getCategory($id);
            if (!$category) {
                $category = $this->factory->createCategory();

                $category
                    ->setId($id)
                    ->setName($name)
                ;
            }

            if ($parent) {
                $category->setParent($parent);
            }

            $parent = $category;
            $shop->addCategory($category);
        }
    }

    protected function createOffer(\SimpleXMLElement $elem, Shop $shop)
    {
        /** @var Offer $offer */
        $offer = parent::createOffer($elem, $shop);

        $xml = $elem->market_category;
        $categoriesNames = explode(' / ', (string)$xml);

        $id = [];
        foreach ($categoriesNames as $part) {
            $id[] = md5($part);
        }

        $id = implode('_', array_reverse($id));

        $offer->setCategory($shop->getCategory($id));

        return $offer;
    }

    public function isNeedPrepareFeed()
    {
        return true;
    }
} 