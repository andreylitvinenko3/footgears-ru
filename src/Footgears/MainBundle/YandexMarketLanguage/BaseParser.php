<?php

namespace Footgears\MainBundle\YandexMarketLanguage;

use Footgears\MainBundle\Attribute\Manager;
use Sirian\YMLParser\Exception\YMLException;
use Sirian\YMLParser\Parser\Parser;
use Sirian\YMLParser\Shop;

class BaseParser extends Parser implements ParserInterface
{
    protected $offersCount = 0;

    public function __construct()
    {
        $factory = new Factory();
        parent::__construct($factory);
    }

    public function isNeedPrepareFeed()
    {
        return false;
    }

    protected function createOffer(\SimpleXMLElement $elem, Shop $shop)
    {
        $this->offersCount++;

        /**
         * @var Offer $offer
         */
        $offer = parent::createOffer($elem, $shop);

        $offer->setId((string)$offer->getAttribute('group_id', $offer->getAttribute('id')));

        // todo: remove this
        $offer->setOldPrice($offer->getAttribute('старая цена'));
        foreach (['oldPrice', 'OLDprice', 'oldprice', 'OLDPrice', 'old_price'] as $oldPriceField) {
            $oldPrice = (float)$elem->{$oldPriceField};
            if ($oldPrice) {
                $offer->setOldPrice($oldPrice);
                break;
            }
        }

        if ((string)$elem['original_id']) {
            $offer->setId((string)$elem['original_id']);
        }

        if ((string)$elem->name) {
            $offer->setName((string)$elem->name);
        }

        $currencyId = strtr((string)$elem->currencyId, ['RUR' => 'RUB']);
        if ($shop->getCurrency($currencyId)) {
            $offer->setCurrency($shop->getCurrency($currencyId));
        } else {
            $currency = $this->factory->createCurrency();
            $currency->setId($currencyId);

            $offer->setCurrency($currency);
        }

        foreach ($elem->param as $param) {
            $offer->addParameter((string)$param['name'], (string)$param);
        }

        return $offer;
    }

    public function getName()
    {
        $class = get_class($this);
        return basename(str_replace('\\', '/', $class));
    }

    public function getOffersCount()
    {
        return $this->offersCount;
    }

    protected function createCurrency(\SimpleXMLElement $elem, Shop $shop)
    {
        $elem['id'] = strtr((string)$elem['id'], ['RUR' => 'RUB']);
        return parent::createCurrency($elem, $shop);
    }

    public function createSimpleOffer(\SimpleXMLElement $elem, Shop $shop)
    {
        return $this->createOffer($elem, $shop);
    }

    protected function parseCategories(Shop $shop)
    {
        $xml = $this->loadElementXml();

        $parents = [];
        foreach ($xml->category as $elem) {
            try {
                $shop->addCategory($this->createCategory($elem, $shop));

                foreach (['parentId', 'parent_id'] as $field) {
                    if (isset($elem[$field])) {
                        $parents[(string)$elem['id']] = (string)$elem[$field];
                        break;
                    }
                }
            } catch (YMLException $e) {
                continue;
            }
        }

        foreach ($parents as $id => $parentId) {
            if ($id != $parentId) {
                $parent = $shop->getCategory($parentId);
            } else {
                $parent = null;
            }
            $shop
                ->getCategory($id)
                ->setParent($parent)
            ;
        }
        return $shop->getCategories();
    }

    protected function createCategory(\SimpleXMLElement $elem, Shop $shop)
    {
        $id = (string)$elem['id'];

        if (!$id) {
            throw new YMLException('Category id can not be empty');
        }

        $category = $this->factory->createCategory();
        $category
            ->setId($id)
            ->setName((string)$elem)
        ;

        return $category;
    }
}
