<?php

namespace Footgears\MainBundle\Http;

class HttpUtil
{
    public static function getCanonicalDomain($url)
    {
        $url = self::ensureProtocol($url);

        $host = mb_strtolower(parse_url($url, PHP_URL_HOST), 'UTF-8');
        $host = idn_to_utf8($host);

        return preg_replace('/^www\./', '', $host);
    }

    public static function belongsTo($url, $to)
    {
        $src = self::getCanonicalDomain($url);
        $target = self::getCanonicalDomain($to);

        $sub = '.' . $target;

        return $src === $target || strrpos($src, $sub) === strlen($src) - strlen($sub);
    }

    public static function ensureProtocol($url)
    {
        $url = preg_replace('#^//#i', 'http://', $url);

        if (!preg_match('#^https?://#i', $url)) {
            $url = 'http://' . $url;
        }

        return $url;
    }
}
