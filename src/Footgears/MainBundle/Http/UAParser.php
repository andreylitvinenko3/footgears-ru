<?php

namespace Footgears\MainBundle\Http;

class UAParser
{
    const LIBVERSION = '0.7.9';
    const UNKNOWN = '?';
    const NAME = 'name';
    const VERSION = 'version';
    const ARCHITECTURE = 'architecture';
    const MODEL = 'model';
    const VENDOR = 'vendor';
    const TYPE = 'type';
    const MOBILE = 'mobile';
    const TABLET = 'tablet';
    const CONSOLE = 'console';
    const SMARTTV = 'smarttv';
    const WEARABLE = 'wearable';

    protected static $userAgent;

    protected $maps = [
        'browser' => [
            'oldsafari' => [
                'version' => [
                    '1.0'   => '/8',
                    '1.2'   => '/1',
                    '1.3'   => '/3',
                    '2.0'   => '/412',
                    '2.0.2' => '/416',
                    '2.0.3' => '/417',
                    '2.0.4' => '/419',
                    '?'     => '/'
                ]
            ]
        ],

        'device' => [
            'amazon' => [
                'model' => [
                    'Fire Phone' => ['SD', 'KF']
                ]
            ],
            'sprint' => [
                'model' => [
                    'Evo Shift 4G' => '7373KT'
                ],
                'vendor' => [
                    'HTC'       => 'APA',
                    'Sprint'    => 'Sprint'
                ]
            ]
        ],

        'os' => [
            'windows' => [
                'version' => [
                    'ME'        => '4.90',
                    'NT 3.11'   => 'NT3.51',
                    'NT 4.0'    => 'NT4.0',
                    '2000'      => 'NT 5.0',
                    'XP'        => ['NT 5.1', 'NT 5.2'],
                    'Vista'     => 'NT 6.0',
                    '7'         => 'NT 6.1',
                    '8'         => 'NT 6.2',
                    '8.1'       => 'NT 6.3',
                    '10'        => ['NT 6.4', 'NT 10.0'],
                    'RT'        => 'ARM'
                ]
            ]
        ]
    ];

    protected static $instance;

    protected $regexes;

    private function __construct()
    {

    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new UAParser();
        }
        return self::$instance;
    }

    public static function parse($userAgent)
    {
        $instance = self::getInstance();
        return [
            'browser' => $instance->getBrowser($userAgent),
            'engine' => $instance->getEngine($userAgent),
            'os' => $instance->getOS($userAgent),
            'device' => $instance->getDevice($userAgent),
            'cpu' => $instance->getCPU($userAgent)
        ];
    }

    protected function getBrowser($userAgent)
    {
        $browser = $this->rgx($userAgent, 'browser');
        $browser['major'] = explode('.', $browser['version'])[0];
        return $browser;
    }

    protected function getEngine($userAgent)
    {
        return $this->rgx($userAgent, 'engine');
    }

    protected function getDevice($userAgent)
    {
        return $this->rgx($userAgent, 'device');
    }

    protected function getCPU($userAgent)
    {
        return $this->rgx($userAgent, 'cpu');
    }

    protected function getOS($userAgent)
    {
        return $this->rgx($userAgent, 'os');
    }

    protected function init()
    {
        if ($this->regexes) {
            return;
        }
        $this->regexes = [

            'browser' => [[
                // Presto based
                '/(opera\smini)\/([\w\.-]+)/i',                                       // Opera Mini
                '/(opera\s[mobiletab]+).+version\/([\w\.-]+)/i',                      // Opera Mobi/Tablet
                '/(opera).+version\/([\w\.]+)/i',                                     // Opera > 9.80
                '/(opera)[\/\s]+([\w\.]+)/i'                                          // Opera < 9.80

                ], [UAParser::NAME, UAParser::VERSION], [

                '/\s(opr)\/([\w\.]+)/i'                                               // Opera Webkit
                ], [[UAParser::NAME, 'Opera'], UAParser::VERSION], [

                // Mixed
                '/(kindle)\/([\w\.]+)/i',                                             // Kindle
                '/(lunascape|maxthon|netfront|jasmine|blazer)[\/\s]?([\w\.]+)*/i',
                                                                                    // Lunascape/Maxthon/Netfront/Jasmine/Blazer

                // Trident based
                '/(avant\s|iemobile|slim|baidu)(?:browser)?[\/\s]?([\w\.]*)/i',
                                                                                    // Avant/IEMobile/SlimBrowser/Baidu
                '/(?:ms|\()(ie)\s([\w\.]+)/i',                                        // Internet Explorer

                // Webkit/KHTML based
                '/(rekonq)\/([\w\.]+)*/i',                                            // Rekonq
                '/(chromium|flock|rockmelt|midori|epiphany|silk|skyfire|ovibrowser|bolt|iron|vivaldi|iridium)\/([\w\.-]+)/i'
                                                                                    // Chromium/Flock/RockMelt/Midori/Epiphany/Silk/Skyfire/Bolt/Iron/Iridium
                ], [UAParser::NAME, UAParser::VERSION], [

                '/(trident).+rv[:\s]([\w\.]+).+like\sgecko/i'                         // IE11
                ], [[UAParser::NAME, 'IE'], UAParser::VERSION], [

                '/(edge)\/((\d+)?[\w\.]+)/i'                                          // Microsoft Edge
                ], [UAParser::NAME, UAParser::VERSION], [

                '/(yabrowser)\/([\w\.]+)/i'                                           // Yandex
                ], [[UAParser::NAME, 'Yandex'], UAParser::VERSION], [

                '/(comodo_dragon)\/([\w\.]+)/i'                                       // Comodo Dragon
                ], [[UAParser::NAME, '/_/', ' '], UAParser::VERSION], [

                '/(chrome|omniweb|arora|[tizenoka]{5}\s?browser)\/v?([\w\.]+)/i',
                                                                                    // Chrome/OmniWeb/Arora/Tizen/Nokia
                '/(qqbrowser)[\/\s]?([\w\.]+)/i'
                                                                                    // QQBrowser
                ], [UAParser::NAME, UAParser::VERSION], [

                '/(uc\s?browser)[\/\s]?([\w\.]+)/i',
                '/ucweb.+(ucbrowser)[\/\s]?([\w\.]+)/i',
                '/JUC.+(ucweb)[\/\s]?([\w\.]+)/i'
                                                                                    // UCBrowser
                ], [[UAParser::NAME, 'UCBrowser'], UAParser::VERSION], [

                '/(dolfin)\/([\w\.]+)/i'                                              // Dolphin
                ], [[UAParser::NAME, 'Dolphin'], UAParser::VERSION], [

                '/((?:android.+)crmo|crios)\/([\w\.]+)/i'                             // Chrome for Android/iOS
                ], [[UAParser::NAME, 'Chrome'], UAParser::VERSION], [

                '/XiaoMi\/MiuiBrowser\/([\w\.]+)/i'                                   // MIUI Browser
                ], [UAParser::VERSION, [UAParser::NAME, 'MIUI Browser']], [

                '/android.+version\/([\w\.]+)\s+(?:mobile\s?safari|safari)/i'         // Android Browser
                ], [UAParser::VERSION, [UAParser::NAME, 'Android Browser']], [

                '/FBAV\/([\w\.]+);/i'                                                 // Facebook App for iOS
                ], [UAParser::VERSION, [UAParser::NAME, 'Facebook']], [

                '/version\/([\w\.]+).+?mobile\/\w+\s(safari)/i'                       // Mobile Safari
                ], [UAParser::VERSION, [UAParser::NAME, 'Mobile Safari']], [

                '/version\/([\w\.]+).+?(mobile\s?safari|safari)/i'                    // Safari & Safari Mobile
                ], [UAParser::VERSION, UAParser::NAME], [

                '/webkit.+?(mobile\s?safari|safari)(\/[\w\.]+)/i'                     // Safari < 3.0
                ], [UAParser::NAME, [UAParser::VERSION, [$this, 'str'], $this->maps['browser']['oldsafari']['version']]], [

                '/(konqueror)\/([\w\.]+)/i',                                          // Konqueror
                '/(webkit|khtml)\/([\w\.]+)/i'
                ], [UAParser::NAME, UAParser::VERSION], [

                // Gecko based
                '/(navigator|netscape)\/([\w\.-]+)/i'                                 // Netscape
                ], [[UAParser::NAME, 'Netscape'], UAParser::VERSION], [
                '/fxios\/([\w\.-]+)/i'                                                // Firefox for iOS
                ], [UAParser::VERSION, [UAParser::NAME, 'Firefox']], [
                '/(swiftfox)/i',                                                      // Swiftfox
                '/(icedragon|iceweasel|camino|chimera|fennec|maemo\sbrowser|minimo|conkeror)[\/\s]?([\w\.\+]+)/i',
                                                                                    // IceDragon/Iceweasel/Camino/Chimera/Fennec/Maemo/Minimo/Conkeror
                '/(firefox|seamonkey|k-meleon|icecat|iceape|firebird|phoenix)\/([\w\.-]+)/i',
                                                                                    // Firefox/SeaMonkey/K-Meleon/IceCat/IceApe/Firebird/Phoenix
                '/(mozilla)\/([\w\.]+).+rv\:.+gecko\/\d+/i',                          // Mozilla

                // Other
                '/(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf)[\/\s]?([\w\.]+)/i',
                                                                                    // Polaris/Lynx/Dillo/iCab/Doris/Amaya/w3m/NetSurf
                '/(links)\s\(([\w\.]+)/i',                                            // Links
                '/(gobrowser)\/?([\w\.]+)*/i',                                        // GoBrowser
                '/(ice\s?browser)\/v?([\w\._]+)/i',                                   // ICE Browser
                '/(mosaic)[\/\s]([\w\.]+)/i'                                          // Mosaic
                ], [UAParser::NAME, UAParser::VERSION]

                /* /////////////////////
                // Media players BEGIN
                ////////////////////////
                , [
                '/(apple(?:coremedia|))\/((\d+)[\w\._]+)/i',                          // Generic Apple CoreMedia
                '/(coremedia) v((\d+)[\w\._]+)/i'
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(aqualung|lyssna|bsplayer)\/((\d+)?[\w\.-]+)/i'                     // Aqualung/Lyssna/BSPlayer
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(ares|ossproxy)\s((\d+)[\w\.-]+)/i'                                 // Ares/OSSProxy
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(audacious|audimusicstream|amarok|bass|core|dalvik|gnomemplayer|music on console|nsplayer|psp-internetradioplayer|videos)\/((\d+)[\w\.-]+)/i',
                                                                                    // Audacious/AudiMusicStream/Amarok/BASS/OpenCORE/Dalvik/GnomeMplayer/MoC
                                                                                    // NSPlayer/PSP-InternetRadioPlayer/Videos
                '/(clementine|music player daemon)\s((\d+)[\w\.-]+)/i',               // Clementine/MPD
                '/(lg player|nexplayer)\s((\d+)[\d\.]+)/i',
                '/player\/(nexplayer|lg player)\s((\d+)[\w\.-]+)/i'                   // NexPlayer/LG Player
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(nexplayer)\s((\d+)[\w\.-]+)/i'                                     // Nexplayer
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(flrp)\/((\d+)[\w\.-]+)/i'                                          // Flip Player
                ], [[UAParser::NAME, 'Flip Player'], UAParser::VERSION], [
                '/(fstream|nativehost|queryseekspider|ia-archiver|facebookexternalhit)/i'
                                                                                    '// FStream/NativeHost/QuerySeekSpider/I'A Archiver/facebookexternalhit
                ], [UAParser::NAME], [
                '/(gstreamer) souphttpsrc (?:\([^\)]+\)){0,1} libsoup\/((\d+)[\w\.-]+)/i'
                                                                                    // Gstreamer
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(htc streaming player)\s[\w_]+\s\/\s((\d+)[\d\.]+)/i',              // HTC Streaming Player
                '/(java|python-urllib|python-requests|wget|libcurl)\/((\d+)[\w\.-_]+)/i',
                                                                                    // Java/urllib/requests/wget/cURL
                '/(lavf)((\d+)[\d\.]+)/i'                                             // Lavf (FFMPEG)
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(htc_one_s)\/((\d+)[\d\.]+)/i'                                      // HTC One S
                ], [[UAParser::NAME, /_/g, ' '], UAParser::VERSION], [
                '/(mplayer)(?:\s|\/)(?:(?:sherpya-){0,1}svn)(?:-|\s)(r\d+(?:-\d+[\w\.-]+){0,1})/i'
                                                                                    // MPlayer SVN
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(mplayer)(?:\s|\/|[unkow-]+)((\d+)[\w\.-]+)/i'                      // MPlayer
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(mplayer)/i',                                                       // MPlayer (no other info)
                '/(yourmuze)/i',                                                      // YourMuze
                '/(media player classic|nero showtime)/i'                             // Media Player Classic/Nero ShowTime
                ], [UAParser::NAME], [
                '/(nero (?:home|scout))\/((\d+)[\w\.-]+)/i'                           // Nero Home/Nero Scout
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(nokia\d+)\/((\d+)[\w\.-]+)/i'                                      // Nokia
                ], [UAParser::NAME, UAParser::VERSION], [
                '/\s(songbird)\/((\d+)[\w\.-]+)/i'                                    // Songbird/Philips-Songbird
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(winamp)3 version ((\d+)[\w\.-]+)/i',                               // Winamp
                '/(winamp)\s((\d+)[\w\.-]+)/i',
                '/(winamp)mpeg\/((\d+)[\w\.-]+)/i'
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(ocms-bot|tapinradio|tunein radio|unknown|winamp|inlight radio)/i'  // OCMS-bot/tap in radio/tunein/unknown/winamp (no other info)
                                                                                    // inlight radio
                ], [UAParser::NAME], [
                '/(quicktime|rma|radioapp|radioclientapplication|soundtap|totem|stagefright|streamium)\/((\d+)[\w\.-]+)/i'
                                                                                    // QuickTime/RealMedia/RadioApp/RadioClientApplication/
                                                                                    // SoundTap/Totem/Stagefright/Streamium
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(smp)((\d+)[\d\.]+)/i'                                              // SMP
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(vlc) media player - version ((\d+)[\w\.]+)/i',                     // VLC Videolan
                '/(vlc)\/((\d+)[\w\.-]+)/i',
                '/(xbmc|gvfs|xine|xmms|irapp)\/((\d+)[\w\.-]+)/i,                    // XBMC/gvfs/Xine/XMMS/i'rapp
                '/(foobar2000)\/((\d+)[\d\.]+)/i',                                    // Foobar2000
                '/(itunes)\/((\d+)[\d\.]+)/i'                                         // iTunes
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(wmplayer)\/((\d+)[\w\.-]+)/i',                                     // Windows Media Player
                '/(windows-media-player)\/((\d+)[\w\.-]+)/i'
                ], [[UAParser::NAME, /-/g, ' '], UAParser::VERSION], [
                '/windows\/((\d+)[\w\.-]+) upnp\/[\d\.]+ dlnadoc\/[\d\.]+ (home media server)/i'
                                                                                    // Windows Media Server
                ], [UAParser::VERSION, [UAParser::NAME, 'Windows']], [
                '/(com\.riseupradioalarm)\/((\d+)[\d\.]*)/i'                          // RiseUP Radio Alarm
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(rad.io)\s((\d+)[\d\.]+)/i',                                        // Rad.io
                '/(radio.(?:de|at|fr))\s((\d+)[\d\.]+)/i'
                ], [[UAParser::NAME, 'rad.io'], UAParser::VERSION]
                //////////////////////
                // Media players END
                ////////////////////*/

            ],

            'cpu' => [[

                '/(?:(amd|x(?:(?:86|64)[_-])?|wow|win)64)[;\)]/i'                     // AMD64
                ], [[UAParser::ARCHITECTURE, 'amd64']], [

                '/(ia32(?=;))/i'                                                      // IA32 (quicktime)
                ], [[UAParser::ARCHITECTURE, [$this, 'lowerize']]], [

                '/((?:i[346]|x)86)[;\)]/i'                                            // IA32
                ], [[UAParser::ARCHITECTURE, 'ia32']], [

                // PocketPC mistakenly identified as PowerPC
                '/windows\s(ce|mobile);\sppc;/i'
                ], [[UAParser::ARCHITECTURE, 'arm']], [

                '/((?:ppc|powerpc)(?:64)?)(?:\smac|;|\))/i'                           // PowerPC
                ], [[UAParser::ARCHITECTURE, '/ower/', '', [$this, 'lowerize']]], [

                '/(sun4\w)[;\)]/i'                                                    // SPARC
                ], [[UAParser::ARCHITECTURE, 'sparc']], [

                '/((?:avr32|ia64(?=;))|68k(?=\))|arm(?:64|(?=v\d+;))|(?=atmel\s)avr|(?:irix|mips|sparc)(?:64)?(?=;)|pa-risc)/i'
                                                                                    // IA64, 68K, ARM/64, AVR/32, IRIX/64, MIPS/64, SPARC/64, PA-RISC
                ], [[UAParser::ARCHITECTURE, [$this, 'lowerize']]]
            ],

            'device' => [[

                '/\((ipad|playbook);[\w\s\);-]+(rim|apple)/i'                         // iPad/PlayBook
                ], [UAParser::MODEL, UAParser::VENDOR, [UAParser::TYPE, UAParser::TABLET]], [

                '/applecoremedia\/[\w\.]+ \((ipad)/'                                  // iPad
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Apple'], [UAParser::TYPE, UAParser::TABLET]], [

                '/(apple\s{0,1}tv)/i'                                                 // Apple TV
                ], [[UAParser::MODEL, 'Apple TV'], [UAParser::VENDOR, 'Apple']], [

                '/(archos)\s(gamepad2?)/i',                                           // Archos
                '/(hp).+(touchpad)/i',                                                // HP TouchPad
                '/(kindle)\/([\w\.]+)/i',                                             // Kindle
                '/\s(nook)[\w\s]+build\/(\w+)/i',                                     // Nook
                '/(dell)\s(strea[kpr\s\d]*[\dko])/i'                                  // Dell Streak
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::TABLET]], [

                '/(kf[A-z]+)\sbuild\/[\w\.]+.*silk\//i'                               // Kindle Fire HD
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Amazon'], [UAParser::TYPE, UAParser::TABLET]], [
                '/(sd|kf)[0349hijorstuw]+\sbuild\/[\w\.]+.*silk\//i'                  // Fire Phone
                ], [[UAParser::MODEL, [$this, 'str'], $this->maps['device']['amazon']['model']], [UAParser::VENDOR, 'Amazon'], [UAParser::TYPE, UAParser::MOBILE]], [

                '/\((ip[honed|\s\w*]+);.+(apple)/i'                                   // iPod/iPhone
                ], [UAParser::MODEL, UAParser::VENDOR, [UAParser::TYPE, UAParser::MOBILE]], [
                '/\((ip[honed|\s\w*]+);/i'                                            // iPod/iPhone
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Apple'], [UAParser::TYPE, UAParser::MOBILE]], [

                '/(blackberry)[\s-]?(\w+)/i',                                         // BlackBerry
                '/(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|huawei|meizu|motorola|polytron)[\s_-]?([\w-]+)*/i',
                                                                                    // BenQ/Palm/Sony-Ericsson/Acer/Asus/Dell/Huawei/Meizu/Motorola/Polytron
                '/(hp)\s([\w\s]+\w)/i',                                               // HP iPAQ
                '/(asus)-?(\w+)/i'                                                    // Asus
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::MOBILE]], [
                '/\(bb10;\s(\w+)/i'                                                   // BlackBerry 10
                ], [UAParser::MODEL, [UAParser::VENDOR, 'BlackBerry'], [UAParser::TYPE, UAParser::MOBILE]], [
                                                                                    // Asus Tablets
                '/android.+(transfo[prime\s]{4,10}\s\w+|eeepc|slider\s\w+|nexus 7)/i'
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Asus'], [UAParser::TYPE, UAParser::TABLET]], [

                '/(sony)\s(tablet\s[ps])\sbuild\//i',                                  // Sony
                '/(sony)?(?:sgp.+)\sbuild\//i'
                ], [[UAParser::VENDOR, 'Sony'], [UAParser::MODEL, 'Xperia Tablet'], [UAParser::TYPE, UAParser::TABLET]], [
                '/(?:sony)?(?:(?:(?:c|d)\d{4})|(?:so[-l].+))\sbuild\//i'
                ], [[UAParser::VENDOR, 'Sony'], [UAParser::MODEL, 'Xperia Phone'], [UAParser::TYPE, UAParser::MOBILE]], [

                '/\s(ouya)\s/i',                                                      // Ouya
                '/(nintendo)\s([wids3u]+)/i'                                          // Nintendo
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::CONSOLE]], [

                '/android.+;\s(shield)\sbuild/i'                                      // Nvidia
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Nvidia'], [UAParser::TYPE, UAParser::CONSOLE]], [

                '/(playstation\s[3portablevi]+)/i'                                    // Playstation
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Sony'], [UAParser::TYPE, UAParser::CONSOLE]], [

                '/(sprint\s(\w+))/i'                                                  // Sprint Phones
                ], [[UAParser::VENDOR, [$this, 'str'], $this->maps['device']['sprint']['vendor']], [UAParser::MODEL, [$this, 'str'], $this->maps['device']['sprint']['model']], [UAParser::TYPE, UAParser::MOBILE]], [

                '/(lenovo)\s?(S(?:5000|6000)+(?:[-][\w+]))/i'                         // Lenovo tablets
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::TABLET]], [

                '/(htc)[;_\s-]+([\w\s]+(?=\))|\w+)*/i',                               // HTC
                '/(zte)-(\w+)*/i',                                                    // ZTE
                '/(alcatel|geeksphone|huawei|lenovo|nexian|panasonic|(?=;\s)sony)[_\s-]?([\w-]+)*/i'
                                                                                    // Alcatel/GeeksPhone/Huawei/Lenovo/Nexian/Panasonic/Sony
                ], [UAParser::VENDOR, [UAParser::MODEL, '/_/', ' '], [UAParser::TYPE, UAParser::MOBILE]], [

                '/(nexus\s9)/i'                                                       // HTC Nexus 9
                ], [UAParser::MODEL, [UAParser::VENDOR, 'HTC'], [UAParser::TYPE, UAParser::TABLET]], [

                '/[\s\(;](xbox(?:\sone)?)[\s\);]/i'                                   // Microsoft Xbox
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Microsoft'], [UAParser::TYPE, UAParser::CONSOLE]], [
                '/(kin\.[onetw]{3})/i'                                                // Microsoft Kin
                ], [[UAParser::MODEL, '/\./', ' '], [UAParser::VENDOR, 'Microsoft'], [UAParser::TYPE, UAParser::MOBILE]], [

                                                                                    // Motorola
                '/\s(milestone|droid(?:[2-4x]|\s(?:bionic|x2|pro|razr))?(:?\s4g)?)[\w\s]+build\//i',
                '/mot[\s-]?(\w+)*/i',
                '/(XT\d{3,4}) build\//i'
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Motorola'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/android.+\s(mz60\d|xoom[\s2]{0,2})\sbuild\//i'
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Motorola'], [UAParser::TYPE, UAParser::TABLET]], [

                '/android.+((sch-i[89]0\d|shw-m380s|gt-p\d{4}|gt-n8000|sgh-t8[56]9|nexus 10))/i',
                '/((SM-T\w+))/i'
                ], [[UAParser::VENDOR, 'Samsung'], UAParser::MODEL, [UAParser::TYPE, UAParser::TABLET]], [                  // Samsung
                '/((s[cgp]h-\w+|gt-\w+|galaxy\snexus|sm-n900))/i',
                '/(sam[sung]*)[\s-]*(\w+-?[\w-]*)*/i',
                '/sec-((sgh\w+))/i'
                ], [[UAParser::VENDOR, 'Samsung'], UAParser::MODEL, [UAParser::TYPE, UAParser::MOBILE]], [
                '/(samsung);smarttv/i'
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::SMARTTV]], [

                '/\(dtv[\);].+(aquos)/i'                                              // Sharp
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Sharp'], [UAParser::TYPE, UAParser::SMARTTV]], [
                '/sie-(\w+)*/i'                                                       // Siemens
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Siemens'], [UAParser::TYPE, UAParser::MOBILE]], [

                '/(maemo|nokia).*(n900|lumia\s\d+)/i',                                // Nokia
                '/(nokia)[\s_-]?([\w-]+)*/i'
                ], [[UAParser::VENDOR, 'Nokia'], UAParser::MODEL, [UAParser::TYPE, UAParser::MOBILE]], [

                '/android\s3\.[\s\w;-]{10}(a\d{3})/i'                                 // Acer
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Acer'], [UAParser::TYPE, UAParser::TABLET]], [

                '/android\s3\.[\s\w;-]{10}(lg?)-([06cv9]{3,4})/i'                     // LG Tablet
                ], [[UAParser::VENDOR, 'LG'], UAParser::MODEL, [UAParser::TYPE, UAParser::TABLET]], [
                '/(lg) netcast\.tv/i'                                                 // LG SmartTV
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::SMARTTV]], [
                '/(nexus\s[45])/i',                                                   // LG
                '/lg[e;\s\/-]+(\w+)*/i'
                ], [UAParser::MODEL, [UAParser::VENDOR, 'LG'], [UAParser::TYPE, UAParser::MOBILE]], [

                '/android.+(ideatab[a-z0-9\-\s]+)/i'                                  // Lenovo
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Lenovo'], [UAParser::TYPE, UAParser::TABLET]], [

                '/linux;.+((jolla));/i'                                               // Jolla
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::MOBILE]], [

                '/((pebble))app\/[\d\.]+\s/i'                                         // Pebble
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::WEARABLE]], [

                '/android.+;\s(glass)\s\d/i'                                          // Google Glass
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Google'], [UAParser::TYPE, UAParser::WEARABLE]], [

                '/android.+(\w+)\s+build\/hm\1/i',                                        // Xiaomi Hongmi 'numeric' models
                '/android.+(hm[\s\-_]*note?[\s_]*(?:\d\w)?)\s+build/i',                   // Xiaomi Hongmi
                '/android.+(mi[\s\-_]*(?:one|one[\s_]plus)?[\s_]*(?:\d\w)?)\s+build/i'    // Xiaomi Mi
                ], [[UAParser::MODEL, '/_/', ' '], [UAParser::VENDOR, 'Xiaomi'], [UAParser::TYPE, UAParser::MOBILE]], [

                '/(mobile|tablet);.+rv\:.+gecko\//i'                                  // Unidentifiable
                ], [[UAParser::TYPE, [$this, 'lowerize']], UAParser::VENDOR, UAParser::MODEL]

                /*//////////////////////////
                // TODO: move to string map
                ////////////////////////////
                '/(C6603)/i'                                                          // Sony Xperia Z C6603
                ], [[UAParser::MODEL, 'Xperia Z C6603'], [UAParser::VENDOR, 'Sony'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(C6903)/i'                                                          // Sony Xperia Z 1
                ], [[UAParser::MODEL, 'Xperia Z 1'], [UAParser::VENDOR, 'Sony'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(SM-G900[F|H])/i'                                                   // Samsung Galaxy S5
                ], [[UAParser::MODEL, 'Galaxy S5'], [UAParser::VENDOR, 'Samsung'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(SM-G7102)/i'                                                       // Samsung Galaxy Grand 2
                ], [[UAParser::MODEL, 'Galaxy Grand 2'], [UAParser::VENDOR, 'Samsung'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(SM-G530H)/i'                                                       // Samsung Galaxy Grand Prime
                ], [[UAParser::MODEL, 'Galaxy Grand Prime'], [UAParser::VENDOR, 'Samsung'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(SM-G313HZ)/i'                                                      // Samsung Galaxy V
                ], [[UAParser::MODEL, 'Galaxy V'], [UAParser::VENDOR, 'Samsung'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(SM-T805)/i'                                                        // Samsung Galaxy Tab S 10.5
                ], [[UAParser::MODEL, 'Galaxy Tab S 10.5'], [UAParser::VENDOR, 'Samsung'], [UAParser::TYPE, UAParser::TABLET]], [
                '/(SM-G800F)/i'                                                       // Samsung Galaxy S5 Mini
                ], [[UAParser::MODEL, 'Galaxy S5 Mini'], [UAParser::VENDOR, 'Samsung'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(SM-T311)/i'                                                        // Samsung Galaxy Tab 3 8.0
                ], [[UAParser::MODEL, 'Galaxy Tab 3 8.0'], [UAParser::VENDOR, 'Samsung'], [UAParser::TYPE, UAParser::TABLET]], [
                '/(R1001)/i'                                                          // Oppo R1001
                ], [UAParser::MODEL, [UAParser::VENDOR, 'OPPO'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(X9006)/i'                                                          // Oppo Find 7a
                ], [[UAParser::MODEL, 'Find 7a'], [UAParser::VENDOR, 'Oppo'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(R2001)/i'                                                          // Oppo YOYO R2001
                ], [[UAParser::MODEL, 'Yoyo R2001'], [UAParser::VENDOR, 'Oppo'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(R815)/i'                                                           // Oppo Clover R815
                ], [[UAParser::MODEL, 'Clover R815'], [UAParser::VENDOR, 'Oppo'], [UAParser::TYPE, UAParser::MOBILE]], [
                 '/(U707)/i'                                                          // Oppo Find Way S
                ], [[UAParser::MODEL, 'Find Way S'], [UAParser::VENDOR, 'Oppo'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(T3C)/i'                                                            // Advan Vandroid T3C
                ], [UAParser::MODEL, [UAParser::VENDOR, 'Advan'], [UAParser::TYPE, UAParser::TABLET]], [
                '/(ADVAN T1J\+)/i'                                                    // Advan Vandroid T1J+
                ], [[UAParser::MODEL, 'Vandroid T1J+'], [UAParser::VENDOR, 'Advan'], [UAParser::TYPE, UAParser::TABLET]], [
                '/(ADVAN S4A)/i'                                                      // Advan Vandroid S4A
                ], [[UAParser::MODEL, 'Vandroid S4A'], [UAParser::VENDOR, 'Advan'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(V972M)/i'                                                          // ZTE V972M
                ], [UAParser::MODEL, [UAParser::VENDOR, 'ZTE'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(i-mobile)\s(IQ\s[\d\.]+)/i'                                        // i-mobile IQ
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::MOBILE]], [
                '/(IQ6.3)/i'                                                          // i-mobile IQ IQ 6.3
                ], [[UAParser::MODEL, 'IQ 6.3'], [UAParser::VENDOR, 'i-mobile'], [UAParser::TYPE, UAParser::MOBILE]], [
                '/(i-mobile)\s(i-style\s[\d\.]+)/i'                                   // i-mobile i-STYLE
                ], [UAParser::VENDOR, UAParser::MODEL, [UAParser::TYPE, UAParser::MOBILE]], [
                '/(i-STYLE2.1)/i'                                                     // i-mobile i-STYLE 2.1
                ], [[UAParser::MODEL, 'i-STYLE 2.1'], [UAParser::VENDOR, 'i-mobile'], [UAParser::TYPE, UAParser::MOBILE]], [

                '/(mobiistar touch LAI 512)/i'                                        // mobiistar touch LAI 512
                ], [[UAParser::MODEL, 'Touch LAI 512'], [UAParser::VENDOR, 'mobiistar'], [UAParser::TYPE, UAParser::MOBILE]], [
                /////////////
                // END TODO
                ///////////*/

            ],

            'engine' => [[

                '/windows.+\sedge\/([\w\.]+)/i'                                       // EdgeHTML
                ], [UAParser::VERSION, [UAParser::NAME, 'EdgeHTML']], [

                '/(presto)\/([\w\.]+)/i',                                             // Presto
                '/(webkit|trident|netfront|netsurf|amaya|lynx|w3m)\/([\w\.]+)/i',     // WebKit/Trident/NetFront/NetSurf/Amaya/Lynx/w3m
                '/(khtml|tasman|links)[\/\s]\(?([\w\.]+)/i',                          // KHTML/Tasman/Links
                '/(icab)[\/\s]([23]\.[\d\.]+)/i'                                      // iCab
                ], [UAParser::NAME, UAParser::VERSION], [

                '/rv\:([\w\.]+).*(gecko)/i'                                           // Gecko
                ], [UAParser::VERSION, UAParser::NAME]
            ],

            'os' => [[

                // Windows based
                '/microsoft\s(windows)\s(vista|xp)/i'                                 // Windows (iTunes)
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(windows)\snt\s6\.2;\s(arm)/i',                                     // Windows RT
                '/(windows\sphone(?:\sos)*|windows\smobile|windows)[\s\/]?([ntce\d\.\s]+\w)/i'
                ], [UAParser::NAME, [UAParser::VERSION, [$this, 'str'], $this->maps['os']['windows']['version']]], [
                '/(win(?=3|9|n)|win\s9x\s)([nt\d\.]+)/i'
                ], [[UAParser::NAME, 'Windows'], [UAParser::VERSION, [$this, 'str'], $this->maps['os']['windows']['version']]], [

                // Mobile/Embedded OS
                '/\((bb)(10);/i'                                                      // BlackBerry 10
                ], [[UAParser::NAME, 'BlackBerry'], UAParser::VERSION], [
                '/(blackberry)\w*\/?([\w\.]+)*/i',                                    // Blackberry
                '/(tizen)[\/\s]([\w\.]+)/i',                                          // Tizen
                '/(android|webos|palm\sos|qnx|bada|rim\stablet\sos|meego|contiki)[\/\s-]?([\w\.]+)*/i',
                                                                                    // Android/WebOS/Palm/QNX/Bada/RIM/MeeGo/Contiki
                '/linux;.+(sailfish);/i'                                              // Sailfish OS
                ], [UAParser::NAME, UAParser::VERSION], [
                '/(symbian\s?os|symbos|s60(?=;))[\/\s-]?([\w\.]+)*/i'                 // Symbian
                ], [[UAParser::NAME, 'Symbian'], UAParser::VERSION], [
                '/\((series40);/i'                                                    // Series 40
                ], [UAParser::NAME], [
                '/mozilla.+\(mobile;.+gecko.+firefox/i'                               // Firefox OS
                ], [[UAParser::NAME, 'Firefox OS'], UAParser::VERSION], [

                // Console
                '/(nintendo|playstation)\s([wids3portablevu]+)/i',                    // Nintendo/Playstation

                // GNU/Linux based
                '/(mint)[\/\s\(]?(\w+)*/i',                                           // Mint
                '/(mageia|vectorlinux)[;\s]/i',                                       // Mageia/VectorLinux
                '/(joli|[kxln]?ubuntu|debian|[open]*suse|gentoo|arch|slackware|fedora|mandriva|centos|pclinuxos|redhat|zenwalk|linpus)[\/\s-]?([\w\.-]+)*/i',
                                                                                    // Joli/Ubuntu/Debian/SUSE/Gentoo/Arch/Slackware
                                                                                    // Fedora/Mandriva/CentOS/PCLinuxOS/RedHat/Zenwalk/Linpus
                '/(hurd|linux)\s?([\w\.]+)*/i',                                       // Hurd/Linux
                '/(gnu)\s?([\w\.]+)*/i'                                               // GNU
                ], [UAParser::NAME, UAParser::VERSION], [

                '/(cros)\s[\w]+\s([\w\.]+\w)/i'                                       // Chromium OS
                ], [[UAParser::NAME, 'Chromium OS'], UAParser::VERSION],[

                // Solaris
                '/(sunos)\s?([\w\.]+\d)*/i'                                           // Solaris
                ], [[UAParser::NAME, 'Solaris'], UAParser::VERSION], [

                // BSD based
                '/\s([frentopc-]{0,4}bsd|dragonfly)\s?([\w\.]+)*/i'                   // FreeBSD/NetBSD/OpenBSD/PC-BSD/DragonFly
                ], [UAParser::NAME, UAParser::VERSION],[

                '/(ip[honead]+)(?:.*os\s*([\w]+)*\slike\smac|;\sopera)/i'             // iOS
                ], [[UAParser::NAME, 'iOS'], [UAParser::VERSION, '/_/', '.']], [

                '/(mac\sos\sx)\s?([\w\s\.]+\w)*/i',
                '/(macintosh|mac(?=_powerpc)\s)/i'                                    // Mac OS
                ], [[UAParser::NAME, 'Mac OS'], [UAParser::VERSION, '/_/', '.']], [

                // Other
                '/((?:open)?solaris)[\/\s-]?([\w\.]+)*/i',                            // Solaris
                '/(haiku)\s(\w+)/i',                                                  // Haiku
                '/(aix)\s((\d)(?=\.|\)|\s)[\w\.]*)*/i',                               // AIX
                '/(plan\s9|minix|beos|os\/2|amigaos|morphos|risc\sos|openvms)/i',
                                                                                    // Plan9/Minix/BeOS/OS2/AmigaOS/MorphOS/RISCOS/OpenVMS
                '/(unix)\s?([\w\.]+)*/i'                                              // UNIX
                ], [UAParser::NAME, UAParser::VERSION]
            ]
        ];
    }


    protected function rgx($userAgent, $type)
    {
        $this->init();

        $i = 0;
        $matches = null;
        $result = null;
        $args = $this->regexes[$type];
        while ($i < count($args) && !$matches) {
            $regex = $args[$i];
            $props = $args[$i + 1];

            if (null === $result) {
                $result = [];
                foreach ($props as $p => $q) {
                    if (is_array($q)) {
                        $result[$q[0]] = null;
                    } else {
                        $result[$q] = null;
                    }
                }
            }

            $j = $k = 0;
            while ($j < count($regex) && !$matches) {
                if (!preg_match($regex[$j++], $userAgent, $matches)) {
                    continue;
                }

                for ($p = 0; $p < count($props); $p++) {
                    ++$k;
                    $match = isset($matches[$k]) ? $matches[$k] : null;
                    $q = $props[$p];
                    if (is_array($q) && count($q) > 0) {
                        if (count($q) == 2) {
                            if (is_callable($q[1])) {
                                $result[$q[0]] = call_user_func($q[1], $match);
                            } else {
                                $result[$q[0]] = $q[1];
                            }
                        } elseif (count($q) == 3) {
                            if (is_callable($q[1])) {
                                $result[$q[0]] = $match ? call_user_func($q[1], $match, $q[2]) : null;
                            } else {
                                $result[$q[0]] = $match ? preg_replace($q[1], $q[2], $match) : null;
                            }
                        } elseif (count($q) == 4) {
                            $result[$q[0]] = $match ? call_user_func($q[3], preg_replace($q[1], $q[2], $match)) : null;
                        }
                    } else {
                        $result[$q] = $match ?: null;
                    }
                }
            }

            $i += 2;
        }

        return $result;
    }

    protected function str($str, $map)
    {
        $strLower = mb_strtolower($str, 'UTF-8');
        foreach ($map as $i => $value) {
            $value = (array)$value;
            for ($j = 0; $j < count($value); $j++) {
                if (false !== strpos($strLower, mb_strtolower($value[$j], 'UTF-8'))) {
                    return ($i === UAParser::UNKNOWN) ? null : $i;
                }
            }
        }
        return $str;
    }

    protected function lowerize($str)
    {
        return mb_strtolower($str, 'UTF-8');
    }
}
