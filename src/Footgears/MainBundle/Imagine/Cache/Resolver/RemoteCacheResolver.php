<?php

namespace Footgears\MainBundle\Imagine\Cache\Resolver;

use Liip\ImagineBundle\Imagine\Cache\Resolver\WebPathResolver;

class RemoteCacheResolver extends WebPathResolver
{
    public function isStored($path, $filter)
    {
        $filePath = $this->getFilePath($path, $filter);
        $stored = $this->filesystem->exists($filePath);

        if ($stored) {
            $this->filesystem->touch($filePath);
        }

        return $stored;
    }
    
    /**
     * {@inheritDoc}
     */
    protected function getFileUrl($path, $filter)
    {
        $hash = md5($path);

        $extension = '';
        if (false !== strrpos($path, '.', strlen($path) - 5)) {
            $extension = strtolower(substr($path, strrpos($path, '.', strlen($path) - 5)));
        } else {
            if ($imageType = exif_imagetype($path)) {
                $extension = image_type_to_extension($imageType, false);
            }
            $knownReplacements = array(
                'jpeg' => 'jpg',
                'tiff' => 'tif'
            );
            $extension = '.' . str_replace(array_keys($knownReplacements), array_values($knownReplacements), $extension);
        }

        $dir = '/' . substr($hash, 0, 2) . '/' . substr($hash, 2, 2) . '/' . $hash . $extension;
//                              var_dump(parent::getFileUrl($dir, $filter)); die;
        return parent::getFileUrl($dir, $filter);
    }

}