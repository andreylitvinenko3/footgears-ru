<?php

namespace Footgears\MainBundle\Admitad;

use Admitad\Api\Api;

class AdmitadManager
{
    public function getApiClient()
    {
        return new Api();
    }
}
