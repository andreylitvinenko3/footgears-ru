<?php

namespace Footgears\MainBundle\EventListener;

use Monolog\Handler\MongoDBHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class RequestListener
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $referer = $request->headers->get('referer');
        $host = preg_replace('/^www\./', '', parse_url($referer, PHP_URL_HOST));
        if ('yandex.ru' == $host) {
            $request->getSession()->set('source', 'yandex');
        } elseif (preg_match('/^google.[a-z]*$/', $host)) {
            $request->getSession()->set('source', 'google');
        }

        // seo: редирект с тег+бренд/магазин на тег
        $uri = explode("?", $request->getRequestUri())[0];
        if (preg_match('/\/category\/([^\/]+)\/tag\/([^\/]+)\/shop\/([^\/]+)/i', $uri, $uriMatches)) {
            $router = $this->container->get('router');
            $redirectUrl = $router->generate('category_tag', [
                'alias' => $uriMatches[1],
                'tagAlias' => $uriMatches[2]
            ]);

            $event->setResponse(RedirectResponse::create($redirectUrl, 301));
            return;
        }

        $query = explode("?", $request->getPathInfo())[0];

        // seo: редирект на урлы в нижнем регистре
        if ($query !== mb_strtolower($query) && preg_match('/^\/(category|brand|shop)/i', $query, $uriMatches)) {
            $redirectUrl = sprintf('%s%s', mb_strtolower($query), $request->getQueryString() ? '?' . $request->getQueryString() : '');
            $event->setResponse(RedirectResponse::create($redirectUrl, 301));
            return;
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {

    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $request = $event->getRequest();

        $logger = $this->container->get('logger');

        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof MongoDBHandler) {
                $handler->pushProcessor(function (array $record) use ($request) {
                    $record['extra']['headers'] = $request->headers->all();

                    $record['extra']['ip'] = $request->getClientIp();
                    $record['extra']['uri'] = $request->getRequestUri();
                    return $record;
                });

                break;
            }
        }
    }
}
