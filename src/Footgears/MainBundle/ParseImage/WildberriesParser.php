<?php

namespace Footgears\MainBundle\ParseImage;

use Goutte\Client;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DomCrawler\Crawler;

class WildberriesParser extends Parser
{
    protected function getBrandImagePath($alias)
    {
        $alias = str_replace('_', '-', $alias);

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.wildberries.ru/brands/' . $alias);

        /** @var Response $response */
        $response = $client->getResponse();

        if ($response->getStatus() < 200 || $response->getStatus() >= 300) {
            throw new \Exception(sprintf('Не удалось получить страницу бренда %s', $alias));
        }

        $path = $crawler->filter('html > body > div.trunkOld > div#catalog > div.catalog-sidebar > div.brand-logo > a > img')->first()->attr('src');

        if (!$path) {
            throw new \Exception('Brand logo image path not found');
        }

        return $path;
    }
}
