<?php

namespace Footgears\MainBundle\ParseImage;

class ParserManager
{
    public function parse($alias)
    {
        $wbParser = new WildberriesParser();
        $kupivipParser = new KupivipParser();

        $wbParser->setNext($kupivipParser);

        $path = $wbParser->parse($alias);

        return $path;
    }
}
