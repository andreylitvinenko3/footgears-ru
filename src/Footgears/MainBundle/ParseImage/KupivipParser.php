<?php

namespace Footgears\MainBundle\ParseImage;

use Goutte\Client;
use Symfony\Component\BrowserKit\Response;

class KupivipParser extends Parser
{
    protected function getBrandImagePath($alias)
    {
        $alias = str_replace('-', '_', $alias);

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.kupivip.ru/brands/' . $alias);

        /** @var Response $response */
        $response = $client->getResponse();

        if ($response->getStatus() < 200 || $response->getStatus() >= 300) {
            throw new \Exception(sprintf('Не удалось получить страницу бренда %s', $alias));
        }

        $path = $crawler->filter('body > div > main > div > div > div > div > div > div > div > div.brand-subscribe_area.pull-right > div.subscribe-mobile > div.brand-img.hidden-xs > img.img-responsive')->first()->attr('src');

        if (!$path) {
            throw new \Exception('Brand logo image path not found');
        }

        return $path;
    }
}
