<?php

namespace Footgears\MainBundle\ParseImage;

abstract class Parser
{
    /**
     * @var Parser
     */
    protected $successor;

    public function setNext(Parser $parser)
    {
        $this->successor = $parser;
    }

    public function parse($alias)
    {
        try {
            $path = $this->getBrandImagePath($alias);

            return $path;
        } catch (\Exception $e) {
            if ($this->successor) {
                return $this->successor->parse($alias);
            }
        }

        throw new \Exception('None of the parsers');
    }

    abstract protected function getBrandImagePath($alias);
}
