<?php

namespace Footgears\MainBundle\Thumb;

use Intervention\Image\Constraint;
use Intervention\Image\Image;

class LogoPreviewFilter extends BaseFilter
{
    public function handle(Image $image)
    {
        $image->resize(150, null, function (Constraint $constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }
}
