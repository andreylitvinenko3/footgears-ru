<?php

namespace Footgears\MainBundle\Mail;

class MailSender
{
    private $mailer;
    private $twig;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function send($emails, $template, $params = [], $attachments = [])
    {
        if (!is_array($emails)) {
            $emails = [$emails];
        }

        foreach ($emails as $email) {
            $message = $this->prepareMessage($email, $template, $params, $attachments);
            $this->mailer->send($message);
        }
    }

    public function prepareMessage($email, $template, $params = [], $attachments = [])
    {
        /**
         * @var \Twig_Template $tpl
         */
        $tpl = $this->twig->loadTemplate('MainBundle:Mail:' . $template . '.html.twig');

        $message = new \Swift_Message();

        $message
            ->setSubject($tpl->renderBlock('subject', $params))
            ->setFrom('noreply@footgears.ru', 'Footgears')
            ->setTo($email)
            ->setBody($tpl->renderBlock('body', $params))
            ->setContentType('text/html')
        ;

        foreach ($attachments as $attach) {
            $message->attach($attach);
        }

        return $message;
    }
}
