<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Elastic\ElasticSearch;

class CategorySearchRequest extends SearchRequest implements ICategorySearch
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function buildFilters(ElasticSearch $search)
    {
        $search->addFilter(['term' => ['categoryIds' => $this->category->getId()]]);
    }

    public function getCategory()
    {
        return $this->category;
    }
}
