<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Elastic\ElasticSearch;

class TagSearchRequest extends SearchRequest implements ICategorySearch
{
    protected $category;

    private $tag;

    private $isAdminMode;

    public function __construct(Tag $tag, $isAdminMode = false)
    {
        $this->category = $tag->getCategory();

        $this->tag = $tag;
        $this->isAdminMode = (bool)$isAdminMode;
    }

    public function buildFilters(ElasticSearch $search)
    {
        if ($this->category) {
            $search->addFilter(['term' => ['categoryIds' => $this->category->getId()]]);
        }

        $this->addSearchFilters($search);
    }

    public function afterBuildFilters(ElasticSearch $search)
    {
        if ($this->getTag()->getAttributes()) {
            foreach ($this->getTag()->getAttributes() as $attribute) {
                $search->addFilter(['term' => ['attributes' => $attribute]]);
            }
        }
    }

    public function addSearchFilters(ElasticSearch $search)
    {
        if ($this->tag->getSearchPhrase()) {
            $search->addQuery([
                'multi_match' => [
                    'query' => $this->tag->getSearchPhrase(),
                    'type' => 'most_fields',
//                    'type' => 'cross_fields',
                    'fields' => [
                        'text'
                    ],
                    // todo: сделать настройку строгого/нестрогого поиска
                    'operator' => $this->tag->getSearchKeysOperator() ?: Tag::SEARCH_KEYS_OPERATOR_AND
//                    'minimum_should_match' => '95%',
                ]
            ]);
        } elseif ($this->getTag()->getAttributes()) {

            // todo: сделать настройку строгого/нестрогого поиска
            foreach ($this->getTag()->getAttributes() as $attribute) {
                $search->addFilter(['term' => ['attributes' => $attribute]]);
            }
        }
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getCategory()
    {
        return $this->category;
    }
}