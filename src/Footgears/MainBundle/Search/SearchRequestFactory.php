<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Document\Tag;

class SearchRequestFactory
{
    protected $sortField;
    protected $sortDirection;

    public function __construct($search_sort_field = null, $search_sort_direction = null)
    {
        $this->sortField = $search_sort_field;
        $this->sortDirection = $search_sort_direction;
    }

    protected function prepareRequest(SearchRequest $request)
    {
//        if ($this->sortField) {
//            $request->setSortField($this->sortField);
//        }

//        if ($this->sortDirection) {
//            $request->setSortDirection($this->sortDirection);
//        }
    }

    /**
     * @return SearchRequest
     */
    public function createSearchRequest()
    {
        $request = new SearchRequest();
        $this->prepareRequest($request);

        return $request;
    }

    /**
     * @param Category $category
     *
     * @return CategorySearchRequest
     */
    public function createCategorySearchRequest(Category $category)
    {
        $request = new CategorySearchRequest($category);
        $this->prepareRequest($request);

        return $request;
    }

    /**
     * @param Brand $brand
     * @param Category $category
     * @return BrandSearchRequest
     */
    public function createBrandSearchRequest(Brand $brand, Category $category = null)
    {
        $request = new BrandSearchRequest($brand, $category);
        $this->prepareRequest($request);

        return $request;
    }

    /**
     * @param Shop $shop
     * @param Category $category
     * @return ShopSearchRequest
     */
    public function createShopSearchRequest(Shop $shop, Category $category = null)
    {
        $request = new ShopSearchRequest($shop, $category);
        $this->prepareRequest($request);

        return $request;
    }

    /**
     * @param Tag $tag
     * @param bool $isAdminMode
     * @return TagSearchRequest
     */
    public function createTagSearchRequest(Tag $tag, $isAdminMode = false)
    {
        $request = new TagSearchRequest($tag, $isAdminMode);
        $this->prepareRequest($request);

        return $request;
    }
}