<?php

namespace Footgears\MainBundle\Search;

interface SearchResultInterface
{
    public function getTotal();
    public function getIds();
}