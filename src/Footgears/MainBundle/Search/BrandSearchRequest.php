<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Elastic\ElasticSearch;

class BrandSearchRequest extends SearchRequest implements ICategorySearch
{
    private $brand;
    private $category;

    public function __construct(Brand $brand, Category $category = null)
    {
        $this->brand = $brand;
        $this->category = $category;
        $this->brandIds[] = $this->brand->getId();
    }

    public function buildFilters(ElasticSearch $search)
    {
        $search->addFilter(['term' => ['brand.id' => $this->brand->getId()]]);

        if ($this->category) {
            $search->addFilter(['term' => ['categoryIds' => $this->category->getId()]]);
        }
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getCategory()
    {
        return $this->category;
    }
}
