<?php

namespace Footgears\MainBundle\Search\SelectedFilters;

use Footgears\MainBundle\Search\SelectedFilters\Filters\FilterInterface;

class GroupFilters
{
    /**
     * @var string $title
     */
    protected $title;

    /**
     * @var FilterInterface[] $filters
     */
    protected $filters;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
        return $this;
    }

    public function addFilter(FilterInterface $filter)
    {
        $this->filters[] = $filter;
    }
}