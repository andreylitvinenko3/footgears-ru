<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;

use Footgears\MainBundle\Document\Brand;

class BrandFilter implements FilterInterface
{
    protected static $field = 'brandId[]';

    /**
     * @var Brand $brand
     */
    protected $brand;

    /**
     * BrandFilter constructor.
     * @param Brand $brand
     */
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    public function getTitle()
    {
        return $this->brand->getName();
    }

    public function getField()
    {
        return self::$field;
    }

    public function getValue()
    {
        return $this->brand->getId();
    }
}