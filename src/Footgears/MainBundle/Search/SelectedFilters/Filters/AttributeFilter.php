<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;

use Footgears\MainBundle\Document\AttributeValue;

class AttributeFilter implements FilterInterface
{
    protected static $field = 'attr[]';

    /**
     * @var AttributeValue $attributeValue
     */
    protected $attributeValue;

    /**
     * AttributeFilter constructor.
     * @param AttributeValue $attributeValue
     */
    public function __construct(AttributeValue $attributeValue)
    {
        $this->attributeValue = $attributeValue;
    }

    public function getTitle()
    {
        return $this->attributeValue->getLabel();
    }

    public function getField()
    {
        return self::$field;
    }

    public function getValue()
    {
        return $this->attributeValue->getId();
    }
}