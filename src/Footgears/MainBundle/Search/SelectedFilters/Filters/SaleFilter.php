<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;

class SaleFilter implements FilterInterface
{
    protected static $field = 'isSale';

    public function getTitle()
    {
        return 'Распродажа';
    }

    public function getField()
    {
        return self::$field;
    }

    public function getValue()
    {
        return 1;
    }
}