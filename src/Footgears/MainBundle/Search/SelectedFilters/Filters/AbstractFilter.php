<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;

use Sirian\Helpers\Url;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractFilter implements FilterInterface
{
    public function getLink($uri)
    {
        $url = new Url($uri);

        $queryParam = $url->getQueryParam($this->getField());

        if ($queryParam) {
            if (is_array($queryParam)) {
                foreach ($queryParam as $key => $param) {
                    if ($param == $this->getValue()) {
                        unset($queryParam[$key]);
                        break;
                    }
                }
            } elseif ($queryParam == $this->getValue()) {
                $url->unsetQueryParam($this->getField());
            }
        }

        return $url->getUrl();
    }
}