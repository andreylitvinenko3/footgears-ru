<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;

interface FilterInterface
{
    public function getTitle();
    public function getField();
    public function getValue();
}