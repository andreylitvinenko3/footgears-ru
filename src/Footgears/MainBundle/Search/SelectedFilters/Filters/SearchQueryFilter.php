<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;

class SearchQueryFilter implements FilterInterface
{
    protected static $field = 'q';

    /**
     * @var string $query
     */
    protected $query;

    /**
     * SearchQueryFilter constructor.
     * @param string $query
     */
    public function __construct($query)
    {
        $this->query = $query;
    }

    public function getTitle()
    {
        return $this->query;
    }

    public function getField()
    {
        return self::$field;
    }

    public function getValue()
    {
        return $this->query;
    }
}