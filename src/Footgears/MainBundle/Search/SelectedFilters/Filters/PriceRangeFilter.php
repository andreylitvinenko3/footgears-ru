<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;

class PriceRangeFilter implements FilterInterface
{
    protected static $field = 'priceFrom,priceTo';

    protected $priceFrom;

    protected $priceTo;

    /**
     * PriceRangeFilter constructor.
     * @param $priceFrom
     * @param $priceTo
     */
    public function __construct($priceFrom, $priceTo)
    {
        $this->priceFrom = $priceFrom;
        $this->priceTo = $priceTo;
    }

    public function getTitle()
    {
        return sprintf('от %d до %d руб', $this->priceFrom, $this->priceTo);
    }

    public function getField()
    {
        return self::$field;
    }

    public function getValue()
    {
        return sprintf('%d,%d', $this->priceFrom, $this->priceTo);
    }
}