<?php

namespace Footgears\MainBundle\Search\SelectedFilters\Filters;


use Footgears\MainBundle\Document\Shop;

class ShopFilter implements FilterInterface
{
    protected static $field = 'shopId[]';

    /**
     * @var Shop $shop
     */
    protected $shop;

    /**
     * ShopFilter constructor.
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    public function getTitle()
    {
        return $this->shop->getName();
    }

    public function getField()
    {
        return self::$field;
    }

    public function getValue()
    {
        return $this->shop->getId();
    }
}