<?php

namespace Footgears\MainBundle\Search\SelectedFilters;

class SelectedFilters
{
    /**
     * @var GroupFilters[] $groupFilters
     */
    protected $groupFiltersList = [];

    public function getGroupFiltersList()
    {
        return $this->groupFiltersList;
    }

    /**
     * @param GroupFilters[] $groupFiltersList
     * @return $this
     */
    public function setGroupFiltersList($groupFiltersList)
    {
        foreach ($groupFiltersList as $groupFilters) {
            $this->groupFiltersList[$groupFilters->getTitle()] = $groupFilters;
        }
        return $this;
    }

    public function addGroupFilters(GroupFilters $groupFilters)
    {
        $this->groupFiltersList[$groupFilters->getTitle()] = $groupFilters;
        return $this;
    }

    public function hasGroupFilters($name)
    {
        return array_key_exists($name, $this->groupFiltersList);
    }

    public function getGroupFilters($name)
    {
        if (!$this->hasGroupFilters($name)) {
            throw new \LogicException(sprintf('Group Filters %s not found', $name));
        }

        return $this->groupFiltersList[$name];
    }
}