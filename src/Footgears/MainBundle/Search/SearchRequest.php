<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Elastic\ElasticSearch;
use Symfony\Component\HttpFoundation\Request;

class SearchRequest
{
    public static $minPrice = 10;

    protected $shopIds;
    protected $brandIds;
    protected $categoryIds;
    protected $priceFrom;
    protected $priceTo;
    protected $searchQuery;
    protected $page = 1;
    protected $limit = 48;
    protected $sortField;
    protected $sortDirection = 'desc';
    protected $isSale = false;
    protected $attributeIds = [];
    protected $source;

    protected $filters = [];

    protected $allowedLimits = [8, 12, 24, 48, 100];
    protected $allowedSortFields = ['price', 'discount'];

    public function setFiltersFromRequest(Request $request)
    {
        $this->setFiltersFromArray($request->query->all());
        $this->setSource(parse_url($request->headers->get('referer'), PHP_URL_HOST));
    }

    public function setFiltersFromArray(array $q)
    {
        if (!empty($q['page'])) {
            $this->page = max(1, (int)$q['page']);
        }

        if (!empty($q['limit']) && in_array($q['limit'], $this->allowedLimits)) {
            $this->limit = max(1, (int)$q['limit']);
        }

        if (!empty($q['q'])) {
            $this->searchQuery = trim($q['q']);
        }

        if (!empty($q['priceFrom'])) {
            $this->priceFrom = floor($q['priceFrom']);
        }

        if (!empty($q['priceTo'])) {
            $this->priceTo = ceil($q['priceTo']);
        }

        $this->priceFrom = max(self::$minPrice, $this->priceFrom);


        if (!empty($q['brandId'])) {
            $this->brandIds = array_filter((array)$q['brandId'], function ($val) {
                return is_numeric($val);
            });
        }

        if (!empty($q['categoryId'])) {
            $this->categoryIds = array_filter((array)$q['categoryId'], function ($val) {
                return is_numeric($val);
            });
        }

        if (!empty($q['shopId'])) {
            $this->shopIds = array_filter((array)$q['shopId'], function ($val) {
                return is_numeric($val);
            });
        }

        if (!empty($q['sortField'])) {
            switch ($q['sortField']) {
                case 'price':
                    $this->sortField = $q['sortField'];
                    $this->sortDirection = 'asc';
                    break;

                case 'discount':
                    $this->sortField = $q['sortField'];
                    $this->sortDirection = 'desc';
                    break;

                default:
                    $this->sortField = null;
                    $this->sortDirection = 'asc';
                    break;
            }
        }

        if (!empty($q['attr'])) {
            $ids = array_map('intval', $q['attr']);

            $this->attributeIds = array_filter(array_merge($this->attributeIds, $ids), function ($val) {
                return is_numeric($val);
            });
        }

        if ($this->sortField == 'discount') {
            $this->isSale = true;
        } else {
            $this->isSale = !empty($q['isSale']);
        }
    }

    public function buildFilters(ElasticSearch $search)
    {
        
    }

    public function afterBuildFilters(ElasticSearch $search)
    {
        
    }

    public function getShopIds()
    {
        return $this->shopIds;
    }

    public function setShopIds($shopIds)
    {
        $this->shopIds = $shopIds;

        return $this;
    }

    public function getBrandIds()
    {
        return $this->brandIds;
    }

    public function setBrandIds($brandIds)
    {
        $this->brandIds = $brandIds;

        return $this;
    }

    public function getCategoryIds()
    {
        return $this->categoryIds;
    }

    public function setCategoryIds($categoryIds)
    {
        $this->categoryIds = $categoryIds;

        return $this;
    }

    public function getPriceFrom()
    {
        return $this->priceFrom;
    }

    public function setPriceFrom($priceFrom)
    {
        $this->priceFrom = $priceFrom;

        return $this;
    }

    public function getPriceTo()
    {
        return $this->priceTo;
    }

    public function setPriceTo($priceTo)
    {
        $this->priceTo = $priceTo;

        return $this;
    }

    public function getSearchQuery()
    {
        return $this->searchQuery;
    }

    public function setSearchQuery($searchQuery)
    {
        $this->searchQuery = $searchQuery;

        return $this;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function getSortField()
    {
        return $this->sortField;
    }

    public function setSortField($sortField)
    {
        $this->sortField = $sortField;

        return $this;
    }

    public function getSortDirection()
    {
        return $this->sortDirection;
    }

    public function setSortDirection($sortDirection)
    {
        $this->sortDirection = $sortDirection;

        return $this;
    }

    public function isSale()
    {
        return $this->isSale;
    }

    public function setIsSale($isSale)
    {
        $this->isSale = $isSale;

        return $this;
    }

    public function getAttributeIds()
    {
        return $this->attributeIds;
    }

    public function setAttributeIds($attributeIds)
    {
        $this->attributeIds = $attributeIds;

        return $this;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    public function excludeWords($text)
    {
        $excludeWords = [
            'интернет магазин', 'интернет-магазин',
            'магазин', 'купить',
            'online shop', 'online-shop',
            'shop',
            'каталог', 'коллекция'
        ];

        return trim(str_replace($excludeWords, '', $text));
    }
}
