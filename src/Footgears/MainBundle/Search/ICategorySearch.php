<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Document\Category;

interface ICategorySearch
{
    /**
     * @return Category
     */
    public function getCategory();
}