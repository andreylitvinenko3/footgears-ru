<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Elastic\ElasticSearch;

class ShopSearchRequest extends SearchRequest implements ICategorySearch
{
    private $shop;
    private $category;

    public function __construct(Shop $shop, Category $category = null)
    {
        $this->shop = $shop;
        $this->category = $category;
        $this->shopIds[] = $this->shop->getId();
    }

    public function buildFilters(ElasticSearch $search)
    {
        $search->addFilter(['term' => ['shop.id' => $this->shop->getId()]]);

        if ($this->category) {
            $search->addFilter(['term' => ['categoryIds' => $this->category->getId()]]);
        }
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function getCategory()
    {
        return $this->category;
    }
}
