<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Document\Shop;
use Footgears\MainBundle\Elastic\ElasticSearch;
use Footgears\MainBundle\Pagination\Pagination;
use Footgears\MainBundle\Service\ContainerWrapper;

class SearchManager
{
    private $container;
    private $disabledShopIds;

    public function __construct(ContainerWrapper $container)
    {
        $this->container = $container;
    }

    /**
     * @param SearchRequest $request
     * @return ElasticSearch
     */
    protected function createSearch(SearchRequest $request)
    {
        $search = $this->container->getElasticSearch();
        $search->reset();

        $disabledShopIds = $this->getDisabledShopIds();
        if ($disabledShopIds) {
            $search->addMustNotFilter(['terms' => ['shop.id' => $disabledShopIds]]);
        }

        if ($request->getSearchQuery()) {
            $searchQuery = $request->excludeWords($request->getSearchQuery());

            if (count(explode(' ', $searchQuery)) >= 3) {
                $query = [
                    'multi_match' => [
                        'query' => $searchQuery,
                        'type' => 'most_fields',
                        'minimum_should_match' => '75%',
                        'fields' => [
                            'text',
                            'name',
                            'brand.name^2',
                            'categories^2'
                        ],
                    ]
                ];
            } else {
                $query = [
                    'multi_match' => [
                        'query' => $searchQuery,
                        'type' => 'cross_fields',
                        'fields' => [
                            'text',
                            'name',
                            'brand.name^2',
                            'categories^2'
                        ],
                        'operator' => 'and'
                    ]
                ];
            }

            $search->addQuery($query);
        }

        return $search;
    }

    public function search(SearchRequest $request)
    {
        $result = new SearchResult();
        $this->collectNonFilterResult($request, $result);
        $this->collectAvailableCategories($request, $result);
        $this->collectAvailableShops($request, $result);

        $this->doSearch($request, $result);

        $result
//            ->setTotal($this->count($request))
            ->setTotal($result->getPagination()->getTotalItemCount())
            ->setPage($request->getPage())
        ;
        return $result;
    }

    public function simpleSearch(SearchRequest $request)
    {
        $result = new SearchResult();
        $this->doSearch($request, $result);

        return $result;
    }

    public function countProducts(SearchRequest $request)
    {
        $search = $this->createSearch($request);
        $request->buildFilters($search);
        $this->setRequestFilter($request, $search);

        $search->setSize(0);
        $searchResult = $search->search();

        return $searchResult->getTotal();
    }

    public function count(SearchRequest $request)
    {
        $search = $this->createSearch($request);
        $request->buildFilters($search);
        $this->setRequestFilter($request, $search);

        $search->setSize(0);
        $searchResult = $search->search();

        return $searchResult->getTotal();
    }

    public function getAvailableBrands(SearchRequest $request, $limit = 0, $setFilter = true, $excludeIds = [], $name = null)
    {
        $search = $this->createSearch($request);
        $request->buildFilters($search);

        if ($setFilter) {
            $this->setRequestFilter($request, $search);
        }
        
        if ($excludeIds) {
            $search->addMustNotFilter(['terms' => ['brand.id' => $excludeIds]]);
        }
        if ($name) {
            $search->addFilter(['match' => ['brand.name' => $name]]);
        }

        $search->setAggregation('brands', ['terms' => [
            'field' => 'brand.id',
            'size' => $limit ?: 500
        ]]);

        $searchResult = $search->aggregate();

        $data = $searchResult->getAggregate('brands');
        $brands = [];
        foreach ($data['buckets'] as $row) {
            $brands[$row['key']] = $row['doc_count'];
        }

        return $brands;
    }

    protected function collectNonFilterResult(SearchRequest $request, SearchResult $result)
    {
        $search = $this->createSearch($request);
        $request->buildFilters($search);
        $request->afterBuildFilters($search);

        $search
            ->setSize(0)
            ->setAggregation('max_price', ['max' => ['field' => 'price']])
            ->setAggregation('min_price', ['min' => ['field' => 'price']])
            ->setAggregation('max_sale_percent', ['max' => ['field' => 'discount']])
            ->setAggregation('max_sale', ['max' => ['field' => 'discount_sum']])
            ->setAggregation('avg_sale', ['avg' => ['field' => 'discount_sum']])
        ;

        $searchResult = $search->aggregate();

        $result
            ->setCountWithoutFilters($searchResult->getTotal())
            ->setPriceMax($searchResult->getAggregate('max_price')['value'])
            ->setPriceMin($searchResult->getAggregate('min_price')['value'])
            ->setMaxSale($searchResult->getAggregate('max_sale')['value'])
            ->setMaxSalePercent($searchResult->getAggregate('max_sale_percent')['value'])
            ->setAvgSale($searchResult->getAggregate('avg_sale')['value'])
        ;
    }

    protected function collectAvailableCategories(SearchRequest $request, SearchResult $result)
    {
        $clonedRequest = clone $request;

        $search = $this->createSearch($clonedRequest);
        $clonedRequest->setCategoryIds(null);
        $clonedRequest->buildFilters($search);
        $this->setRequestFilter($clonedRequest, $search);

        $search->setAggregation('categories', ['terms' => [
            'field' => 'categoryIds',
            'size' => 300
        ]]);

        $searchResult = $search->aggregate();

        $data = $searchResult->getAggregate('categories');
        $categories = [];
        foreach ($data['buckets'] as $row) {
            $categories[$row['key']] = $row['doc_count'];
        }

        $result->setAvailableCategories($categories);
    }

    private function collectAvailableShops(SearchRequest $request, SearchResult $result)
    {
        $search = $this->createSearch($request);
        $request->buildFilters($search);
        $this->setRequestFilter($request, $search);

        $parameters = [
            'shops' => [
                'field' => 'shop.id',
                'method' => 'setAvailableShops',
                'size' => 100
            ],
            'attributes' => [
                'field' => 'attributes',
                'method' => 'setAttributes',
                'size' => 100
            ]
        ];

        foreach ($parameters as $key => $params) {
            $search->setAggregation($key, ['terms' => [
                'field' => $params['field'],
                'size' => $params['size']
            ]]);
        }
        $searchResult = $search->aggregate();

        foreach ($parameters as $key => $params) {
            $data = $searchResult->getAggregate($key);
            $rows = [];
            foreach ($data['buckets'] as $bucket) {
                $rows[$bucket['key']] = $bucket['doc_count'];
            }

            $result->{$params['method']}($rows);
        }
    }

    public function getRandomProductIds(SearchRequest $request, $limit = 10)
    {
        $search = $this->createSearch($request);
        $request->buildFilters($search);
        $request->afterBuildFilters($search);

        $search
            ->addFilter(['range' => ['weight' => ['gt' => 0]]])
            ->setSize($limit * 10)
        ;

        $ids = $search->search()->getIds();
        shuffle($ids);

        return array_slice($ids, 0, $limit);
    }

    protected function doSearch(SearchRequest $request, SearchResult $result)
    {
        $search = $this->createSearch($request);
        $request->buildFilters($search);
        $this->setRequestFilter($request, $search);

        $page = (int)max(1, $request->getPage());

        $sort = [];
        if ($request->getSortField()) {
            $sort[][$request->getSortField()] = ['order' => $request->getSortDirection()];
        }
        $sort[] = ['weight' => ['order' => 'desc']];
        $sort[] = ['_score' => ['order' => 'desc']];

        $search
            ->setFilterType(ElasticSearch::QUERY_TYPE_FILTERED)
            ->setSort($sort)
            ->setSize($request->getLimit())
            ->setFrom(($page - 1) * $request->getLimit())
        ;

        $searchResult = $search->search();

        $pagination = $this->createPaginate($searchResult, [
            'limit' => $request->getLimit(),
            'page' => $request->getPage(),
        ]);

        $result
            ->setProducts($this->container->getProductRepository()->findByIds($searchResult->getIds()))
            ->setPagination($pagination)
            ->setPage($pagination->getCurrentPageNumber())
        ;
    }

    protected function createPaginate(SearchResultInterface $searchResult, $options = [])
    {
        $request = $this->container->getRequest();

        $options = array_merge([
            'defaultPage' => 1,
            'defaultLimit' => 10,
            'comment' => '',
            'params' => array_merge($request->query->all(), $request->attributes->all())
        ], $options);

        $page = $request->query->get('page', $options['defaultPage']);
        $limit = $request->query->get('limit', $options['defaultLimit']);

        if (isset($options['page'])) {
            $page = $options['page'];
        }

        if (isset($options['limit'])) {
            $limit = $options['limit'];
        }

        $limit = (int)max(1, $limit);
        $maxPage = ceil($searchResult->getTotal() / $limit);
        $page = (int)max(1, min($page, $maxPage));

        $pagination = new Pagination($options['params']);
        $pagination->setItemNumberPerPage($limit);
        $pagination->setTotalItemCount($searchResult->getTotal());
        $pagination->setItems($searchResult->getIds());
        $pagination->setMaxPage($maxPage);
        $pagination->setCurrentPageNumber($page);

        return $pagination;
    }

    protected function setRequestFilter(SearchRequest $request, ElasticSearch $search)
    {
        if ($request->getBrandIds()) {
            $search->addFilter(['terms' => ['brand.id' => $request->getBrandIds()]]);
        }

        if ($request->getShopIds()) {
            $search->addFilter(['terms' => ['shop.id' => $request->getShopIds()]]);
        }

        if ($request->getCategoryIds()) {
            $search->addFilter(['terms' => ['categoryIds' => $request->getCategoryIds()]]);
        }

        if ($request->isSale()) {
            $search->addFilter(['term' => ['sale' => true]]);
        }

        if ($request->getPriceFrom() || $request->getPriceTo()) {
            $priceFilter = [];

            if ($request->getPriceFrom()) {
                $priceFilter['range']['price']['gte'] = $request->getPriceFrom();
            }
            if ($request->getPriceTo()) {
                $priceFilter['range']['price']['lte'] = $request->getPriceTo();
            }

            if ($priceFilter) {
                $search->addFilter($priceFilter);
            }
        }

        if ($request->getAttributeIds()) {
            foreach ($request->getAttributeIds() as $attributeId) {
                $search->addFilter(['term' => ['attributes' => $attributeId]]);
            }
        }
    }

    private function getDisabledShopIds()
    {
        if (null === $this->disabledShopIds) {
            $shops = $this
                ->container
                ->getDocumentManager()
                ->getRepository(Shop::class)
                ->findBy(['active' => false])
            ;

            foreach ($shops as $shop) {
                $this->disabledShopIds[] = $shop->getId();
            }
        }
        return $this->disabledShopIds;
    }

    public function getAvailableCategories(SearchRequest $request)
    {
        $result = new SearchResult();
        $this->collectAvailableCategories($request, $result);
        return $result->getAvailableCategories();
    }
}
