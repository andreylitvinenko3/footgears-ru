<?php

namespace Footgears\MainBundle\Search;

use Footgears\MainBundle\Pagination\Pagination;

class SearchResult
{
    /**
     * @var Pagination
     */
    protected $pagination;
    protected $products;
    protected $page;
    protected $priceMin;
    protected $priceMax;
    protected $countWithoutFilters;
    protected $availableCategories;
    protected $availableShops;

    protected $attributeIds;
    protected $fixedSearchQuery;
    protected $fixedSearchQueryProductsCount;

    protected $avgSale;

    protected $maxSale;

    protected $maxSalePercent;

    protected $total;

    public function getAvgSale()
    {
        return $this->avgSale;
    }

    public function setAvgSale($avgSale)
    {
        $this->avgSale = $avgSale;

        return $this;
    }

    public function getMaxSale()
    {
        return $this->maxSale;
    }

    public function setMaxSale($maxSale)
    {
        $this->maxSale = $maxSale;

        return $this;
    }

    public function getMaxSalePercent()
    {
        return $this->maxSalePercent;
    }

    public function setMaxSalePercent($maxSalePercent)
    {
        $this->maxSalePercent = $maxSalePercent;

        return $this;
    }

    public function getPriceMin()
    {
        return $this->priceMin;
    }

    public function setPriceMin($priceMin)
    {
        $this->priceMin = max(10, floor($priceMin));

        return $this;
    }

    public function getPriceMax()
    {
        return $this->priceMax;
    }

    public function setPriceMax($priceMax)
    {
        $this->priceMax = ceil($priceMax);

        return $this;
    }

    public function getCountWithoutFilters()
    {
        return $this->countWithoutFilters;
    }

    public function setCountWithoutFilters($countWithoutFilters)
    {
        $this->countWithoutFilters = $countWithoutFilters;

        return $this;
    }

    public function getAvailableCategories()
    {
        return $this->availableCategories;
    }

    public function setAvailableCategories($availableCategories)
    {
        $this->availableCategories = $availableCategories;
        return $this;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    public function getPagination()
    {
        return $this->pagination;
    }

    public function setPagination($pagination)
    {
        $this->pagination = $pagination;

        return $this;
    }

    public function getAttributeIds()
    {
        return $this->attributeIds;
    }

    public function setAttributeIds($attributeIds)
    {
        $this->attributeIds = $attributeIds;

        return $this;
    }

    public function setAttributes($attributes)
    {
        $this->attributeIds = array_keys($attributes);
        return $this;
    }

    public function getFixedSearchQuery()
    {
        return $this->fixedSearchQuery;
    }

    public function setFixedSearchQuery($fixedSearchQuery)
    {
        $this->fixedSearchQuery = $fixedSearchQuery;

        return $this;
    }

    public function getAvailableShops()
    {
        return $this->availableShops;
    }

    public function setAvailableShops($availableShops)
    {
        $this->availableShops = $availableShops;

        return $this;
    }

    public function getFixedSearchQueryProductsCount()
    {
        return $this->fixedSearchQueryProductsCount;
    }

    public function setFixedSearchQueryProductsCount($fixedSearchQueryProductsCount)
    {
        $this->fixedSearchQueryProductsCount = $fixedSearchQueryProductsCount;
        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }
}
