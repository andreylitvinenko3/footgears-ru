<?php

namespace Footgears\MainBundle\Filter;

class BrandFilter
{
    protected $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
