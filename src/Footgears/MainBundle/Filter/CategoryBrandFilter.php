<?php

namespace Footgears\MainBundle\Filter;

use Footgears\MainBundle\Document\Brand;
use Footgears\MainBundle\Document\Category;

class CategoryBrandFilter
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * @var Brand
     */
    protected $brand;

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }
}
