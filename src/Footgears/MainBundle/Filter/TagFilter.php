<?php

namespace Footgears\MainBundle\Filter;

use Footgears\MainBundle\Document\Category;

class TagFilter
{
    protected $text;

    /**
     * @var Category
     */
    protected $category;

    protected $type;

    protected $active;

    protected $issetProducts;

    protected $sortField;

    protected $sortOrder;

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function isActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function issetProducts()
    {
        return $this->issetProducts;
    }

    public function setIssetProducts($issetProducts)
    {
        $this->issetProducts = $issetProducts;
        return $this;
    }

    public function getSortField()
    {
        return $this->sortField;
    }

    public function setSortField($sortField)
    {
        $this->sortField = $sortField;
        return $this;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }
}
