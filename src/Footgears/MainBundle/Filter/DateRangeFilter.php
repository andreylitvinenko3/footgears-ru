<?php

namespace Footgears\MainBundle\Filter;

class DateRangeFilter
{
    /**
     * @var \DateTime
     */
    protected $dateFrom;
    /**
     * @var \DateTime
     */
    protected $dateTo;

    public function __construct()
    {
        $this->dateFrom = new \DateTime();
        $this->dateTo = new \DateTime();
    }

    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    public function getDateTo()
    {
        return $this->dateTo;
    }

    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }
}
