<?php

namespace Footgears\MainBundle\Menu;

use Footgears\MainBundle\Document\MenuItem;
use Footgears\MainBundle\Service\ContainerTrait;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuBuilder
{
    use ContainerTrait;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var FactoryInterface
     */
    protected $factory;

    public function __construct(ContainerInterface $container, FactoryInterface $factory)
    {
        $this->factory = $factory;
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function createMenu($type)
    {
        $menuData = $this->loadMenuData($type);

        $root = $this->factory->createItem($type);
        $this->buildMenu($root, $menuData);
        return $root;
    }

    public function loadMenuData($type)
    {
        $cacheItem = $this->getCache()->getItem('menu.' . $type);
        if (!$cacheItem->isHit()) {
            $repo = $this->getDocumentManager()->getRepository(MenuItem::class);

            $tree = $repo->getTree($type);
            $data = $this->prepareMenuData($tree);

            $cacheItem->set($data);
            $this->getCache()->save($cacheItem);
        } else {
            $data = $cacheItem->get();
        }

        return $data;
    }

    public function createMainMenu()
    {
        return $this->createMenu(MenuItem::TYPE_MAIN);
    }

    public function createFooterMenu()
    {
        return $this->createMenu(MenuItem::TYPE_FOOTER);
    }

    public function createFooterTagsMenu()
    {
        return $this->createMenu(MenuItem::TYPE_FOOTER_TAGS);
    }

    public function createMobileMainMenu()
    {
        return $this->createMenu(MenuItem::TYPE_MOBILE_MAIN);
    }

    protected function prepareMenuData($node)
    {
        /* @var Router $router */
        $router = $this->container->get('router');

        $options = [
            'brand_id' => null,
            'category_id' => null,
            'class' => null
        ];

        /* @var MenuItem $item */
        $item = $node['item'];

        if ($item->getBrand()) {
            $options['brand_id'] = $item->getBrand()->getId();
            $options['uri'] = $router->generate('brand', ['alias' => $item->getBrand()->getAlias()]);
        } elseif ($item->getCategory()) {
            $options['category_id'] = $item->getCategory()->getId();
            $options['uri'] = $router->generate('category', ['alias' => $item->getCategory()->getAlias()]);
        } elseif ($item->getCustomUrl()) {
            $options['uri'] = $item->getCustomUrl();
        }

        $options['class'] = $item->getClass();
        $options['target'] = $item->isBlank() ? '_blank' : '';
        $options['linkTitle'] = $item->getLinkTitle();

        if ($item->isRedirect()) {
            $options['uri'] = $router->generate('redirect_out', [
                'url' => $this->container->get('footgears.cipher')->encrypt($options['uri'])
            ]);
        }

        $res = [
            'label' => $item->getLabel(),
            'options' => $options,
            'children' => []
        ];

        foreach ($node['children'] as $child) {
            $res['children'][] = $this->prepareMenuData($child);
        }

        return $res;
    }

    protected function buildMenu(ItemInterface $item, $menuNode)
    {
        $currentUrl = $this->getRequest()->getPathInfo();

        foreach ($menuNode['children'] as $node) {
            $child = $item->addChild($node['label'], $node['options']);
            $this->buildMenu($child, $node);

            if (isset($node['options']['class'])) {
                $child->setLinkAttributes([
                    'class' => $node['options']['class']
                ]);
            }

            $child->setLinkAttribute('title', $node['options']['linkTitle'] ? $node['options']['linkTitle'] : $node['label']);

            if (!empty($node['options']['target'])) {
                $child->setLinkAttributes([
                    'target' => $node['options']['target'],
                ]);
            }

            if (isset($node['options']['uri']) && $node['options']['uri'] === $currentUrl) {
                $child->setCurrent(true);
            }
        }
    }
}