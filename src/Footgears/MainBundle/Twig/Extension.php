<?php

namespace Footgears\MainBundle\Twig;

use Footgears\MainBundle\Document\Brand;

use Footgears\MainBundle\Document\Category;
use Footgears\MainBundle\Document\Tag;
use Footgears\MainBundle\Service\ContainerTrait;
use Footgears\MainBundle\Utils\SuffixMap;
use Footgears\MainBundle\Document\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class Extension extends \Twig_Extension
{
    use ContainerTrait;

    private $uploaderHelper;
    protected $container;

    public function __construct(ContainerInterface $container, UploaderHelper $uploaderHelper)
    {
        $this->uploaderHelper = $uploaderHelper;
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('ceil', 'ceil'),
            new \Twig_SimpleFilter('currency', [$this, 'currencyFilter']),
            new \Twig_SimpleFilter('replaceSuffix', [$this, 'replaceSuffix']),
            new \Twig_SimpleFilter('ucfirst', [$this, 'ucfirst']),
            new \Twig_SimpleFilter('number', [$this, 'number']),
            new \Twig_SimpleFilter('productImage', [$this, 'productImage'])
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('generateFilterUrl', [$this, 'generateFilterUrl']),
            new \Twig_SimpleFunction('getFilterTags', [$this, 'getFilterTags']),
            new \Twig_SimpleFunction('getTags', [$this, 'getTags']),
            new \Twig_SimpleFunction('replaceSeoVariables', [$this, 'replaceSeoVariables']),
            new \Twig_SimpleFunction('attributesArr', [$this, 'attributesArr']),
            new \Twig_SimpleFunction('getPopularBrandCategoriesHTML', [$this, 'getPopularBrandCategoriesHTML']),
            new \Twig_SimpleFunction('yearPeriod', [$this, 'yearPeriod']),
        ];
    }

    public function generateFilterUrl($route, $params)
    {
        $query = $this->getRequest()->query->all();
        unset($query['categoryId'], $query['page']);

        return $this->getContainer()->get('router')->generate($route, array_merge($params, $query));
    }

    public function currencyFilter($value)
    {
        switch ($value) {
            case 'UAH':
                return 'грн';

            case 'USD':
                return '$';

            case 'EUR':
                return  '€';

            case 'RUB':
                return 'руб';
            default:
                return $value;
        }
    }

    public function getTags(Category $category, $type)
    {
        /** @var Tag[] $rows */
        $rows = $this->getDocumentManager()
            ->getRepository(Tag::class)
            ->findBy([
                'category' => $category,
                'type' => $type,
                'active' => true,
                'productsCount' => ['$gt' => 0]
            ])
        ;

        return $rows;
    }

    public function getFilterTags(Category $category, $type)
    {
        /** @var Tag[] $rows */
        $rows = $this
            ->getDocumentManager()
            ->getRepository(Tag::class)
            ->findBy([
                'category' => $category,
                'type' => $type,
                'active' => true,
                'productsCount' => ['$gt' => 0]
            ])
        ;

        $pages = [];
        foreach ($rows as $row) {
            if (count($row->getAttributes()) == 1) {
                $pages[$row->getAttributes()[0]] = $row;
            }
        }

        return $pages;
    }

    public function getName()
    {
        return 'footgears_extension';
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        $this->getUser();

        if (!$this->container->has('security.context')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->container->get('security.context')->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }

    public function replaceSeoVariables($subject, array $target, array $replace)
    {
        $string = str_replace($target, $replace, $subject);
        $string = preg_replace('/\s{2,}/iu', ' ', $string);

        return $string;
    }

    public function attributesArr($attributesArr)
    {
        $attributes = [];
        foreach ($attributesArr as $valueId) {
            $attribute = $this->getAttributesService()->getAttributeByValueId($valueId);

            if ($attribute && $attribute->getValue($valueId)) {
                $attributes[$attribute->getName()][] = $attribute->getValue($valueId)->getLabel();
            }
        }

        return $attributes;
    }

    public function getPopularBrandCategoriesHTML(Brand $brand)
    {
        $categories = $this->getCategoriesService()->getPopularsByBrand($brand);

        $linkTemplate = '<a href="%s" title="%s">%s</a>';

        $links = [];
        if ($categories) {
            foreach ($categories as $category) {
                $name = $category->getName() . ' ' . $brand->getName();

                $links[] = sprintf($linkTemplate, $this->getRouter()->generate('category_brand', ['alias' => $category->getAlias(), 'brandAlias' => $brand->getAlias()]), $name, $name);
            }
        }

        return implode(', ', $links);
    }

    public function replaceSuffix($string)
    {
        return SuffixMap::replace($string);
    }

    public function ucfirst($string, $e ='utf-8')
    {
        if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) {
            $upper = mb_strtoupper($string, $e);
            preg_match('#(.)#us', $upper, $matches);
            $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
        } else {
            $string = ucfirst($string);
        }

        return $string;
    }

    public function number($value, $decimals = 0, $sep = ' ', $decimalPoint = ',')
    {
        return number_format($value, $decimals, $decimalPoint, $sep);
    }

    public function productImage($path, $filter)
    {
        $cacheManager = $this->getContainer()->get('liip_imagine.cache.manager');
        try {
            return $cacheManager->getBrowserPath($path, $filter);
        } catch (\Exception $e) {
            return '/bundles/main/images/262x393.png';
        }
    }

    public function yearPeriod()
    {
        $from = new \DateTime();
        $to = new \DateTime();

        if ($from->format('m') > 8) {
            $to->setTimestamp(strtotime('+1 year'));
        } else {
            $from->setTimestamp(strtotime('-1 year'));
        }

        $period = sprintf('%s-%s', $from->format('Y'), $to->format('Y'));

        return $period;
    }
}
