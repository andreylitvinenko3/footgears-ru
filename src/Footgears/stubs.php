<?php

class MongoWriteBatch
{
    const COMMAND_INSERT = 1;
    const COMMAND_UPDATE = 2;
    const COMMAND_DELETE = 3;

    protected function __construct(MongoCollection $collection, $batch_type, array $write_options) {}

    public function add(array $item) {}

    final public function execute(array $write_options = []) {}
}


class MongoUpdateBatch extends MongoWriteBatch
{
    public function __construct( MongoCollection $collection, $write_options = []) {}
}

class MongoInsertBatch extends MongoWriteBatch
{
    public function __construct( MongoCollection $collection, $write_options = []) {}
}
