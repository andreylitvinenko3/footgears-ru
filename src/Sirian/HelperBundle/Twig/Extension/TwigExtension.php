<?php

namespace Sirian\HelperBundle\Twig\Extension;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TwigExtension extends \Twig_Extension
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('pluralize', [$this, 'pluralize']),
            new \Twig_SimpleFunction('mergedUrl', [$this, 'mergedUrl']),
            new \Twig_SimpleFunction('mergedPath', [$this, 'mergedPath']),
        ];
    }

    public function mergedUrl($name, $parameters = array(), $schemeRelative = false)
    {
        $parameters = array_merge(
            $this->getRequest()->attributes->get('_route_params'),
            $this->getRequest()->query->all(),
            $parameters
        );

        return $this->getRouter()->generate($name, $parameters, $schemeRelative ? UrlGeneratorInterface::NETWORK_PATH : UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public function mergedPath($name, $parameters = array(), $relative = false)
    {
        if (array_key_exists('page', $parameters) && $parameters['page'] == 1) {
            // для ссылок на первые страницы убираем параметр page
            unset($parameters['page']);
            $this->getRequest()->query->remove('page');
        }

        $parameters = array_merge(
            $this->getRequest()->attributes->get('_route_params'),
            $this->getRequest()->query->all(),
            $parameters
        );
        return $this->getRouter()->generate($name, $parameters, $relative ? UrlGeneratorInterface::RELATIVE_PATH : UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    public function pluralize($number, $variants = array())
    {
        $variant = (($number % 10 == 1) && ($number % 100 != 11)) ? 0 : ((($number % 10 >= 2) && ($number % 10 <= 4) && (($number % 100 < 10) || ($number % 100 >= 20))) ? 1 : 2);
        if (isset($variants[$variant])) {
            return str_replace('%count%', $number, $variants[$variant]);
        } else {
            return '';
        }
    }

    public function getName()
    {
        return 'sirian_helper';
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->container->get('request_stack')->getMasterRequest();
    }

    /**
     * @return Router
     */
    protected function getRouter()
    {
        return $this->container->get('router');
    }
}
