(function($, window, document) {
    var pluginName = 'ajaxTable';

    if (typeof Object.create !== 'function') {
        Object.create = function (o) {
            function F() {}
            F.prototype = o;
            return new F();
        };
    }

    var Plugin = {
        _init: function (options, elem) {
            var self = this;
            self.elem = elem;
            self.$elem = $(elem);
            self.options = $.extend(true, {}, $.fn[pluginName].options, $.isPlainObject(options) ? options : {});
            self.$form = self.$elem.find('form.ajax-form');
            self.$content = self.$elem.find('.ajax-content');

            /*this.$form.on('submit', function (e) {
                e.preventDefault();
                self.load();
            });*/

            var $loader = this.$elem.find(this.options.loader);
            if (!$loader.size()) {
                $loader = this.createLoader();
                self.$content.after($loader);
            }
            this.$loader = $loader;

            //plugin init
            self.$elem.on('click.' + pluginName, '.table-navigation a', function(e) {
                e.preventDefault();
                self.load($(this).attr('data-page'));
            });

            self.load(1);
        },
        createLoader: function () {
            return $('<div class="progress progress-striped active" style="width: 50%;margin: 0 auto"><div style="width: 100%;" class="bar"></div></div>');
        },
        load: function (page) {
            this.$content.empty();
            this.$loader.show();

            var self = this;
            page = page || 1;
            this.$form.ajaxSubmit({
                data: {
                    page: page
                },
                success: function (data) {
                    self.$loader.hide();
                    self.$content.html(data);
                }
            })
        },
        update: function(options) {
            // options update
            if ($.isPlainObject(options)) {
                $.extend(true, this.options, options);
            }
        },
        destroy: function () {
            this.$elem.off('.' + pluginName).removeData(pluginName);
        }
    };

    $.fn[pluginName] = function(options) {
        var args = arguments;
        return this.each(function() {
            var obj = $.data(this, pluginName);
            if (!obj) {
                obj = Object.create(Plugin);
                $.data(this, pluginName, obj);
                obj._init(options, this);
            } else if ($.isPlainObject(options)) {
                obj.update(options);
            }

            if (typeof options == 'string' && typeof obj[options] === 'function') {
                obj[options].apply(obj, [].slice.call(args, 1));
            }
        });
    };


    $.fn[pluginName].options = {
        loader: '/bundles/admin/img/ajax-loader.gif'
    };
})(jQuery, window, document);
