var sirian = {};

sirian.showError = function (err) {
    $.gritter.add(err);

    try {
        console.log(arguments);
    } catch (ignored) {
    }
};

$(document).ready(function () {
    $('.validate').validate({
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    $('.ajax-table').ajaxTable();
});

$(document).on('click', '.crud-list-delete', function () {
    var $this = $(this);
    if (!confirm('Удалить "' + $this.attr('data-name') + '"?')) {
        return;
    }
    var $tr = $this.closest('[data-crud-item]');
    $tr.hide();
    $.ajax({
        url: $this.attr('data-url'),
        dataType: "json",
        data: {
            form: {
                _token: $this.attr('data-token')
            }
        },
        type: 'POST',
        success: function(data) {
            if (data.result == 'ok') {
                $tr.remove();
            } else {
                sirian.showError({
                    title: 'Ошибка',
                    text: data.error,
                    sticky: true
                });
                $tr.show();
            }
        },
        error: function (xhr, text, errorThrown) {
            sirian.showError({
                title: 'Error in ajax request',
                text: text,
                sticky: true,
                previous: arguments
            });
            $tr.show();
        }
    });
});

function initSuggest($input, options) {
    if ($input.data('suggest-initialized')) {
        return;
    }
    $input.data('suggest-initialized', 1);
    var nameField = $input.attr('data-entity-property') || 'text';
    var suggestUrl = $input.attr('data-suggest');
    var width = $input.outerWidth();
    $input.select2($.extend({
        width: width + 'px',
        allowClear: !$input.prop('required'),
        placeholder: "Search",
        minimumInputLength: 1,
        multiple: !!$input.attr('data-multiple'),
        ajax: {
            url: suggestUrl,
            dataType: 'json',
            data: function (term, page) {
                return {
                    page: page,
                    limit: 10,
                    q: term
                };
            },
            results: function (data, page) {
                return {
                    results: data.items,
                    more: page < data.last
                };
            }
        },
        formatResult: function (item) {
            return item[nameField]
        },
        formatSelection: function (item) {
            return item[nameField]
        },
        initSelection: function(element, callback) {
            var o = {
                id: $input.attr('data-entity-id')
            };
            o[nameField] = $input.attr('data-entity-name');
            callback(o);
        },
        dropdownCssClass: "bigdrop"
    }, options));
}

$(document).ready(function () {
    $('input[data-suggest]').each(function () {
        initSuggest($(this));
    });

    $('select.select2').each(function () {
        var $this = $(this);
        $this.select2({
            width: $this.outerWidth() + 'px'
        })
    });
});

$.ajaxSetup({
    error: function (xhr, text, errorThrown) {

        if (xhr.status === 0 || xhr.readyState === 0) {
            return;
        }
        sirian.showError({
            title: 'Error in ajax request',
            text: xhr.statusText,
            sticky: true,
            previous: arguments
        });
    }
});
