<?php

namespace Sirian\HelperBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Util\MenuManipulator;

class Matcher extends \Knp\Menu\Matcher\Matcher
{
    /**
     * @param \Knp\Menu\ItemInterface $item
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function findCurrent(ItemInterface $item, $depth = 0)
    {
        if ($this->isCurrent($item)) {
            return $item;
        }

        if ($this->isAncestor($item)) {
            foreach ($item as $child) {
                $cur = $this->findCurrent($child, $depth + 1);
                if ($cur) {
                    return $cur;
                }
            }
        }

        if (!$depth) {
            $this->clear();
            return $this->findMaxCurrentSubUri($item);
        }

        return null;
    }

    /**
     * @param \Knp\Menu\ItemInterface $item
     * @return array
     */
    public function getCurrentBreadCrumbs(ItemInterface $item)
    {
        $current = $this->findCurrent($item);
        if (!$current) {
            return array();
        } else {
            $menuManipulator = new MenuManipulator();
            return $menuManipulator->getBreadcrumbsArray($current);
        }
    }

    protected function findMaxCurrentSubUri(ItemInterface $item, $currentMax = null)
    {
        if (strpos($_SERVER['REQUEST_URI'], $item->getUri()) === 0) {

            if (!$currentMax || strlen($currentMax->getUri()) < strlen($item->getUri())) {
                if ($currentMax) {
                    $currentMax->setCurrent(false);
                }
                $item->setCurrent(true);
                $currentMax = $item;
            }
        }

        foreach ($item as $child) {
            $currentMax = $this->findMaxCurrentSubUri($child, $currentMax);
        }

        return $currentMax;
    }
}
