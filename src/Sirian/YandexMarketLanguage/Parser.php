<?php

namespace Sirian\YandexMarketLanguage;


class Parser
{
    protected $xmlReader;

    /**
     * @var Shop $currentShop
     */
    protected $currentShop;

    protected $factory;

    /**
     * @var \Closure $onOffer
     */
    protected $onOffer;

    /**
     * @var \Closure $onCategories
     */
    protected $onCategories;


    public function __construct(Factory $factory)
    {
        $this->xmlReader = new \XMLReader();
        $this->factory = $factory;
    }

    public function onOffer(\Closure $closure)
    {
        $this->onOffer = $closure;
    }

    public function onCategories(\Closure $closure)
    {
        $this->onCategories = $closure;
    }

    public function parse($fileName)
    {
        $this->xmlReader->open($fileName);
        while ($this->findNextElement()) {
            $this->onElement();
        }
        $this->xmlReader->close();
    }

    protected function findNextElement()
    {
        $xml = $this->xmlReader;
        while ($xml->read()) {
            if ($xml->nodeType == \XMLReader::ELEMENT) {
                return true;
            }
        }
        return false;
    }

    protected function onElement()
    {
        $xml = $this->xmlReader;
        switch ($xml->depth) {
            case 1:
                $this->currentShop = $this->factory->createShop();
                break;
            case 2:
                switch ($xml->name) {
                    case 'currencies':
                        $this->readCurrencies();
                    break;
                    case 'categories':
                        $this->readCategories();
                        break;
                }
                break;
            case 3:
                switch ($xml->name) {
                    case 'offer':
                        $this->readOffer();
                        break;
                }
        }
    }

    protected function readCurrencies()
    {
        $currencies = $this->loadXmlElement();
        foreach ($currencies->currency as $elem) {
            $this->currentShop->addCurrency($this->createCurrency($elem));
        }
    }

    protected function createCurrency(\SimpleXMLElement $elem)
    {
        $currency = $this->factory->createCurrency();

        $id = strtr((string)$elem['id'], ['RUR' => 'RUB']);

        $currency
            ->setId($id)
            ->setRate((string)$elem['rate'])
        ;

        return $currency;
    }

    protected function readCategories()
    {
        $categories = $this->loadXmlElement();

        $parents = [];
        foreach ($categories->category as $elem) {
            $this->currentShop->addCategory($this->createCategory($elem));
            $parents[(int)$elem['id']] = (int)$elem['parentId'];
        }


        foreach ($parents as $id => $parentId) {
            if ($id === $parentId) {
                $parent = null;
            } else {
                $parent = $this->currentShop->getCategory($parentId);
            }

            $this
                ->currentShop
                ->getCategory($id)
                ->setParent($parent)
            ;
        }
        if ($this->onCategories) {
            call_user_func($this->onCategories, $this->currentShop->getCategories());
        }
    }

    protected function createCategory(\SimpleXMLElement $elem)
    {
        $id = (int)$elem['id'];
        $parents[$id] = (int)$elem['parentId'];
        $category = $this->factory->createCategory();
        $category
            ->setId($id)
            ->setName((string)$elem)
        ;
        return $category;
    }

    protected function readOffer()
    {
        $elem = $this->loadXmlElement();

        $offer = $this->createOffer($elem);

        if ($offer && $this->onOffer) {
            call_user_func($this->onOffer, $offer);
        }
    }

    protected function createOffer(\SimpleXMLElement $elem)
    {
        $offer = $this->factory->createOffer();
        $offer
            ->setId((string)$elem['id'])
            ->setPrice((float)$elem->price)
        ;

        foreach(['url', 'vendor', 'model', 'description', 'typePrefix'] as $field) {
            call_user_func([$offer, 'set' . ucfirst($field)], (string)$elem->{$field});
        }

        foreach ($elem->picture as $picture) {
            $offer->addPicture((string)$picture);
        }

        $currencyId = (string)$elem->currencyId;
        $currencyId = strtr($currencyId, ['RUR' => 'RUB']);

        if ($this->currentShop->getCurrency($currencyId)) {
            $offer->setCurrency($this->currentShop->getCurrency($currencyId));
        }

        $categoryId = (string)$elem->categoryId;
        if ($this->currentShop->getCategory($categoryId)) {
            $offer->setCategory($this->currentShop->getCategory($categoryId));
        }

        foreach ($elem->param as $param) {
            $offer->addParameter((string)$param['name'], (string)$param);
        }

        return $offer;
    }

    protected function loadXmlElement()
    {
        return simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?>' . $this->xmlReader->readOuterXml());
    }
}
