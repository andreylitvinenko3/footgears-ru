<?php

namespace Sirian\YandexMarketLanguage;

class Shop
{
    /**
     * @var Category[] $categories
     */
    protected $categories = [];

    /**
     * @var Currency[]
     */
    protected $currencies = [];

    public function addCurrency(Currency $currency)
    {
        $this->currencies[$currency->getId()] = $currency;
        return $this;
    }

    public function addCategory(Category $category)
    {
        $this->categories[$category->getId()] = $category;
        return $this;
    }

    public function getCategory($id)
    {
        return isset($this->categories[$id]) ? $this->categories[$id] : null;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function getCurrency($id)
    {
        return isset($this->currencies[$id]) ? $this->currencies[$id] : null;
    }
}
