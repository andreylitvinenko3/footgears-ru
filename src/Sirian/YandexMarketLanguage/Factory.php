<?php

namespace Sirian\YandexMarketLanguage;

class Factory
{
    public function createShop()
    {
        return new Shop();
    }

    public function createCategory()
    {
        return new Category();
    }

    public function createCurrency()
    {
        return new Currency();
    }

    public function createOffer()
    {
        return new Offer();
    }
}
