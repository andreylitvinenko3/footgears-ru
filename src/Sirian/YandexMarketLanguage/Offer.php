<?php

namespace Sirian\YandexMarketLanguage;

class Offer
{
    protected $id;
    protected $url;
    protected $price;
    protected $pictures = [];
    protected $typePrefix;
    protected $vendor;
    protected $description;
    protected $model;
    protected $parameters = [];

    /**
     * @var Currency $currency
     */
    protected $currency;

    /**
     * @var Category $category
     */
    protected $category;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency(Currency $currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    public function getPictures()
    {
        return $this->pictures;
    }

    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
        return $this;
    }

    public function getVendor()
    {
        return $this->vendor;
    }

    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function hasParameter($name)
    {
        $name = mb_strtolower($name);
        return isset($this->parameters[$name]);
    }

    public function getParameter($name)
    {
        $name = mb_strtolower($name);
        if (!$this->hasParameter($name)) {
            return null;
        }

        return $this->parameters[$name];
    }

    public function setParameters(array $parameters)
    {
        $this->parameters = [];
        foreach ($parameters as $name => $value) {
            $this->parameters[mb_strtolower($name)] = $value;
        }

        return $this;
    }

    public function setParameter($name, $value)
    {
        $name = mb_strtolower($name);
        $this->parameters[$name] = $value;
        return $this;
    }

    public function addParameter($name, $value)
    {
        if (isset($this->parameters[$name])) {
            $this->parameters[$name] .= ', ' . $value;
        } else {
            $this->setParameter($name, $value);
        }
    }

    public function addParameters($params)
    {
        $this->setParameters(array_merge($this->parameters, $params));
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getTypePrefix()
    {
        return $this->typePrefix;
    }

    public function setTypePrefix($typePrefix)
    {
        $this->typePrefix = $typePrefix;
        return $this;
    }

    public function addPicture($picture)
    {
        if (!in_array($picture, $this->pictures)) {
            $this->pictures[] = $picture;
        }
    }
}
