<?php

namespace Sirian\YandexMarketLanguage;

class Category
{
    protected $id;
    protected $name;

    /**
     * @var Category $parent
     */
    protected $parent;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(Category $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    public function getDepth()
    {
        if (!$this->parent) {
            return 1;
        } else {
            return 1 + $this->parent->getDepth();
        }
    }
}
