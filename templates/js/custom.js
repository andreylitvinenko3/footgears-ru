// CookieChecker.setOptions({
//    allowClose: true
// });
// CookieChecker.run();

function init() {
    // // load footer
    // $.ajax({
    //     url: Routing.generate('layout_footer'),
    //     dataType: 'html',
    //     success: function (html) {
    //         $('body').append(html);
    //     }
    // });
    //
    // // load header
    // $.ajax({
    //     url: Routing.generate('layout_header'),
    //     dataType: 'html',
    //     success: function (html) {
    //         $('body').prepend(html);
    //     }
    // })
}

$(document).ready(function() {

    init();




    $('.js-set-price').on('click', function(e) {
        e.preventDefault();

        $('#slider-range')
            .slider('option', 'values', [$(this).data('price-from'), $(this).data('price-to')])
        ;

        $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) +
        " - " + $( "#slider-range" ).slider( "values", 1 ) + ' руб.' );

        $('#price-max').val($( "#slider-range" ).slider( "values", 1 ));
        $('#price-min').val($( "#slider-range" ).slider( "values", 0 ));

        //window.location.href = $('#filter_form').attr('action') + '?' + $('#filter_form').serialize();
        $('#price-min').trigger('change');
    });

    $('.js-autocomplete').each(function () {
        var autocomplete;
        var autocompleteTimeout;
        var $container = $(this);

        $container.find('input').on('change blur, keyup', function () {
            var phrase = $.trim($(this).val());

            if (!phrase) {
                return;
            }

            if (autocomplete) {
                if (phrase == autocomplete.phrase) {
                    return;
                }
                autocomplete.abort();
            }

            clearTimeout(autocompleteTimeout);
            autocompleteTimeout = setTimeout(function () {
                autocomplete = $.ajax({
                    url: $container.attr('data-url'),
                    dataType: "json",
                    data: {
                        q: phrase
                    },
                    success: function (data) {
                        $container.find('.suggest').remove();
                        $container.append(data.html);
                    }
                });
                autocomplete.query = phrase;
            }, 100);
        });
    });

    $(document).on('click', '[data-label]', function(e) {
        var category = $(this).data('category') ? $(this).data('category') : 'клик';
        var action = $(this).data('action') ? $(this).data('action') : 'по ссылке';
        var label = $(this).data('label');

        if (typeof ga === "function") {
            ga('send', 'event', category, action, label);
        }

        if (typeof yaCounter30455407 !== 'undefined') {
            yaCounter30455407.reachGoal(action);
        }
    });

    if (isTouch()) {
        $.each($('.dropdown-toggle'), function () {
            if ($(this).hasClass('disabled')) {
                $(this).removeClass('disabled');
            }
        });
    }

    function isTouch() {
        try {
            document.createEvent("TouchEvent");
            return true;
        }
        catch (e) { return false; }
    }

    $(document).on('click', '.icon-search', function (e) {
        $(this).closest('form').trigger('submit');
    });

    $(document)
        .on('mouseenter', 'a[data-href]', function() {
            var $this = $(this);
            $this.attr('href', $this.attr('data-href'));

            if ($this.attr('data-target')) {
                $this.attr('target', $(this).attr('data-target'))
            }
        })
        .on('mouseleave', 'a[data-href]', function() {
            $(this)
                .removeAttr('href')
                .removeAttr('target')
            ;
        })
    ;
});