(function () {
    var Search = {
        init: function () {

            Search.getQueryParams(document.location.search);

            Search.$filterForm = $('#filter_form');
            Search.$foundCount = $('#found_count');
            Search.$sortLimitForm = $('#sortLimitForm');
            Search.initPriceFilter();

            var countSearch;
            Search.$filterForm.on('change', '[name]', function () {
                var $filterBlock = $(this).closest('.filter-blk');
                Search.$foundCount.appendTo($filterBlock).show().find('.count').text('...');
                if (countSearch) {
                    countSearch.abort();
                }
                countSearch = $.ajax({
                    url: Routing.generate('search_count'),
                    dataType: 'json',
                    data: Search.$filterForm.serialize(),
                    success: function (res) {
                        Search.$foundCount.attr('href', Search.$filterForm.attr('action') + '?' + Search.$filterForm.serialize()).find('.count').text(res.count);
                    }
                })
            });

            Search.$filterForm.find('.js-filter-list').each(function () {
                var $list = $(this);

                var $searchItems = $list.find('.js-search-items');
                var $selectedItems = $list.find('.js-selected-items');

                var timeout;
                var ajax;
                var lastSearch;
                var template = $list.attr('data-template');

                $list.find('.search').on('change keyup blur', function (e, force) {
                    var $input = $(this);
                    var search = $.trim($input.val());
                    if (search === lastSearch && !force) {
                        return;
                    }

                    lastSearch = search;
                    clearTimeout(timeout);
                    timeout = setTimeout(function () {
                        if (ajax) {
                            ajax.abort();
                        }
                        $searchItems.parent().addClass('loading');
                        ajax = $.ajax({
                            url: $list.attr('data-url'),
                            data: 'search='+encodeURIComponent(search)+'&' + Search.$filterForm.serialize(),
                            dataType: 'json',
                            success: function(data) {
                                var html = '';

                                for (var i = 0; i < data.items.length; i++) {
                                    var item = data.items[i];
                                    html += template.replace(/ID/g, item.id).replace(/NAME/g, item.name).replace(/ALIAS/g, item.alias);
                                }
                                $searchItems.parent().removeClass('loading');
                                $searchItems.html(html);
                            },
                            error: function () {
                                $searchItems.parent().addClass('loading');
                            }
                        });
                    }, 100);
                }).trigger('change');

                $searchItems.on('change', 'input', function () {
                    var $this = $(this);
                    if (!$selectedItems.find('[value="' + $this.val() + '"]').length) {
                        $this.closest('li').prependTo($selectedItems);
                    }
                    $selectedItems.closest('.selected').show();
                });

                $selectedItems.on('change', 'input', function () {
                    var $input = $(this);
                    setTimeout(function () {
                        $input.closest('li').remove();
                        if (!$selectedItems.children().length) {
                            $selectedItems.closest('.selected').hide();
                        }
                    }, 1);
                    $list.find('.search').trigger('change', true);
                });
            });

            Search.$filterForm.on('click', '.filter-title', function (e) {
                e.preventDefault();
                $(this).next().toggle();
            });

            Search.$sortLimitForm.on('change', function(e) {
                    e.preventDefault();
                    Search.$filterForm.find('#sortField').val($(this).find('[name=sortField]').val());
                    Search.$filterForm.trigger('submit');
                });

            Search.initSelectedFiltersLinks();

            $(document).on('click', '.pagination a[data-page]', function (e) {
                e.preventDefault();

                var params = Search.getQueryParams(document.location.search.substring(1));
                $.extend(params, {page: $(this).attr('data-page')});

                window.location.href = window.location.pathname + '?' + $.param(params);
            });

            Search.loadProducts();
        },

        getQueryParams: function (p) {
            if (!p) {
                return {};
            }

            var params = {};
            var pairs = p.split('&');
            for (var i=0; i<pairs.length; i++) {
                var pair = pairs[i].split('=');
                var indices = [];
                var name = decodeURIComponent(pair[0]),
                    value = decodeURIComponent(pair[1]);

                var name = name.replace(/\[([^\]]*)\]/g,
                    function(k, idx) { indices.push(idx); return ""; });

                indices.unshift(name);
                var o = params;

                for (var j=0; j<indices.length-1; j++) {
                    var idx = indices[j];
                    var nextIdx = indices[j+1];
                    if (!o[idx]) {
                        if ((nextIdx === "") || (/^[0-9]+$/.test(nextIdx)))
                            o[idx] = [];
                        else
                            o[idx] = {};
                    }
                    o = o[idx];
                }

                idx = indices[indices.length-1];
                if (idx === "") {
                    o.push(value);
                }
                else {
                    o[idx] = value;
                }
            }
            return params;
        },

        initSelectedFiltersLinks: function () {

            var formAction = Search.$filterForm.attr('action');
            var formParameters = Search.$filterForm.serialize();

            $('.clear-all-filters').attr('href', formAction ? formAction : Routing.generate('search'));

            $('.selected-filter[data-field]').each(function () {
                $this = $(this);

                var fields = decodeURIComponent($this.data('field')).split(',');
                var values = decodeURIComponent($this.data('value')).split(',');
                var newParameters = decodeURIComponent(formParameters);

                $.each(fields, function (index, value) {
                    newParameters = newParameters.replace(value + '=' + values[index], '');
                });

                newParameters = newParameters.replace(/&&/g, '&').replace(/^&/g, '').replace(/&$/g, '');

                $this.attr('href', formAction + '?' + newParameters);
            });
        },
        initPriceFilter: function () {
            var $sliderRange = $( "#slider-range");


            var min = $sliderRange.data('min');
            var max = $sliderRange.data('max');

            var defMin = $sliderRange.data('default-min');
            var defMax = $sliderRange.data('default-max');

            $sliderRange.slider({
                range: true,
                min: defMin,
                max: defMax,
                values: [ min, max ],
                slide: function( event, ui ) {
                    $( "#amount" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] + ' руб.' );

                    $('#price-min').val(ui.values[ 0 ]);
                    $('#price-max').val(ui.values[ 1 ]);
                },
                stop: function(event, ui) {
                    $('#price-min').trigger('change');
                }
            });
            $( "#amount" ).val( $sliderRange.slider( "values", 0 ) +
                " - " + $sliderRange.slider( "values", 1 ) + ' руб.' );

            $('#price-max').val($sliderRange.slider( "values", 1 ));
            $('#price-min').val($sliderRange.slider( "values", 0 ));
        },

        loadProducts: function () {
            var $container = $('#ajax_load_products');
            if ($container.length) {

                var formData = Search.$filterForm.serializeArray();

                if ($container.attr('data-page') && $container.attr('data-page') > 1) {
                    formData.push({name: 'page', value: $container.attr('data-page')});
                }

                $.ajax({
                    url: Routing.generate('product_load_list'),
                    data: $.param(formData),
                    dataType: 'json',
                    success: function (data) {
                        $container.html('');

                        if (data.products) {
                            var $productsContainer = $('<div class="row products"></div>').html(data.products);
                            $container.append($productsContainer);
                        }

                        if (data.pagination) {
                            $container.append(data.pagination);
                        }
                    }
                });
            }
        }
    };

    $(document).ready(function () {
        Search.init();
    });
}());
