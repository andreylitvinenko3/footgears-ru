<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Debug\Debug;

class AppKernel extends Kernel
{
    protected $webHost;

    protected static $hosts;

    public function __construct($environment, $debug)
    {
        if ($debug) {
            Debug::enable();
        }

        date_default_timezone_set('Europe/Minsk');
        mb_internal_encoding('UTF-8');
        setlocale(LC_TIME, 'ru_RU.UTF-8', 'Rus');

        parent::__construct($environment, $debug);
    }

    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new Sirian\HelperBundle\SirianHelperBundle(),
            new Sirian\SuggestBundle\SirianSuggestBundle(),
            new Sirian\StorageBundle\SirianStorageBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),

            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),

            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),

            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),

            new SunCat\MobileDetectBundle\MobileDetectBundle(),

            new Footgears\MainBundle\MainBundle(),
            new Footgears\AdminBundle\AdminBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getKernelParameters()
    {
        $params = parent::getKernelParameters();

        return array_merge($params, [
            'version' => substr(md5(microtime(true)), 0, 4)
        ]);
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->environment;
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
